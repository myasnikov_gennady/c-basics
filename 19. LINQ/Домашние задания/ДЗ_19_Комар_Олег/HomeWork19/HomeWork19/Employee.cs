﻿using System.Collections.Generic;

namespace HomeWork19
{
    public class Employee
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public Employee(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public static IEnumerable<Employee> Employees()
        {
            return new List<Employee>()
            {
                new Employee(1001, "Clinton"),
                new Employee(1002, "Bush"),
                new Employee(1004, "Obama"),
                new Employee(1008, "Trump"),
                new Employee(1009, "Biden") // не имеет зарплаты :)
            };
        }
    }
}
