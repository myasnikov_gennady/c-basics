﻿using System;
using System.Linq;

namespace HomeWork19
{
    internal static class Program
    {
        private static void Main()
        {
            var result = from e in Employee.Employees().DefaultIfEmpty()
                join s in Salary.Salaries().DefaultIfEmpty() on e.Id equals s.Id
                orderby s.Id
                select new { e.Name, s.Date, s.MonthSalary };

            var result2 = Employee.Employees()
                .Join(Salary.Salaries(),
                    e => e.Id,
                    s => s.Id,
                    (e, s) => new { e.Name, e.Id, s.MonthSalary })
                .GroupBy(x => x.Name)
                .Select(x => new
                    {
                        Name = x.Key,
                        AvgMonthSalary = Math.Round(x.Average(y => y.MonthSalary),2),
                        MinSalary = x.Min(y=>y.MonthSalary),
                        MaxSalary = x.Max(y => y.MonthSalary),
                        FullSalary = x.Sum(y=>y.MonthSalary),
                        }
                ).OrderBy(y => y.Name);

            var result3 = Employee.Employees().Join(Salary.Salaries(),
                    e => e.Id,
                    s => s.Id,
                    (e, s) => new { e.Name, e.Id, s.Date, s.MonthSalary })
                .GroupBy(x => x.Date)
                .Select(x => new
                    {
                        Date = x.Key,
                        AvgMonthSalary = Math.Round(x.Average(y => y.MonthSalary),2),
                        FullMonthSalary = x.Sum(y=>y.MonthSalary)
                    }
                ).OrderBy(y => y.Date);

            foreach (var item in result)
                Console.WriteLine($"{item.Date} - {item.Name} - ${item.MonthSalary}");
            
            Console.WriteLine();
            Console.WriteLine("Зарплата по работникам");
            foreach (var item in result2)
                Console.WriteLine($"{item.Name} - Средняя: ${item.AvgMonthSalary}, Минимальная: ${item.MinSalary}, Максимальная: ${item.MaxSalary},\nвыплаченная за всё время работы - ${item.FullSalary}");
            
            Console.WriteLine();
            Console.WriteLine("Средняя зарплата по месяцам");
            foreach (var item in result3)
                Console.WriteLine($"{item.Date} - Средняя по всем работникам: ${item.AvgMonthSalary},\nвыплаченная всем сотрудникам в этом месяце - {item.FullMonthSalary}");


            /* это просто проверка сумм, в разных объектах
            var result4 = result3.Sum(x => x.FullMonthSalary);
            var result5 = result2.Sum(x => x.FullSalary);
            Console.WriteLine($"Зарплата, выплаченная всем работникам за всё время - {result4} и {result5}");
            */
        }
    }
}
