﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HW_19_Korop_Alexander
{
    class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Employee(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
