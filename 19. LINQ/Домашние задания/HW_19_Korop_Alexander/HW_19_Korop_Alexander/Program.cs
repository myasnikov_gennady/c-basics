﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace HW_19_Korop_Alexander
{
    class Program
    {
        static void Main(string[] args)
        {
            IEnumerable<Employee> employees = new List<Employee>()
            {
                new Employee(1, "Stan"),
                new Employee(2, "Kenny"),
                new Employee(3, "Kyle"),
                new Employee(4, "Eric")
            };
            IEnumerable<Salary> salary = new List<Salary>()
            {
                new Salary(1, 7),
                new Salary(3, 10),
                new Salary(1, 5),
                new Salary(4, 15),
                new Salary(4, 12),
                new Salary(4, 27),
                new Salary(3, 9),
                new Salary(3, 16),
                new Salary(4, 30),
                new Salary(4, 15),
                new Salary(4, 100),
                new Salary(1, 100),
                new Salary(3, 31)
            };
            var query = employees.Join(salary,
                    emp => emp.Id,
                    sal => sal.Id,
                    (emp, sal) => new { emp.Id, emp.Name, sal.MonthSalary })
                .GroupBy(x => x.Name)
                .Select(x => new
                    {
                        Name = x.Key,
                        Avg = x.Average(y => y.MonthSalary)
                    }
                ).ToArray();
            foreach (var item in query)
            {
                Console.WriteLine($"{item.Name} avarage salary: {item.Avg}");
            }
        }
    }
}
