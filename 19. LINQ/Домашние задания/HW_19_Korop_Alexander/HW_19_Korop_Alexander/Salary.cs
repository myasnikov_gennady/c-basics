﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HW_19_Korop_Alexander
{
    class Salary
    {
        public int Id { get; set; }
        public double MonthSalary { get; set; }
        public Salary(int id, double monthSalary)
        {
            Id = id;
            MonthSalary = monthSalary;
        }        
    }
}
