﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW_19_ULADZIMIR_SKRYPKIN
{
    public class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Employee(int id, string name)
        {
            Id = id;
            Name = name;
        }
        public static IEnumerable<Employee> Employees()
        {
           
            return new List<Employee>()
            {
                new Employee(1,"Robot Cat"),
                new Employee(2,"Robot Dog"),
                new Employee(3,"Robot Tiger"),
                new Employee(4,"Robot Snake"),
                new Employee(5,"Robot Tank"),
            };
        }
    }
}
