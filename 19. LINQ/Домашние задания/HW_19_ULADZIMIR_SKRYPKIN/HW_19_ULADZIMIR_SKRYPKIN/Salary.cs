﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW_19_ULADZIMIR_SKRYPKIN
{
    public class Salary
    {
        public int Id { get; set; }
        public int MonthSalary { get; set; }

        public Salary(int id, int monthSalary)
        {
            Id = id;
            MonthSalary = monthSalary;
        }
        public static IEnumerable<Salary> Salaries()
        {
            Random rnd = new Random();
            return new List<Salary>()
            {
                new Salary( rnd.Next(1,5),rnd.Next(1000,2000)),
                new Salary(rnd.Next(1,5),rnd.Next(1000,2000)),
                new Salary(rnd.Next(1,5),rnd.Next(1000,2000)),
                new Salary(rnd.Next(1,5),rnd.Next(1000,2000)),
                new Salary(rnd.Next(1,5),rnd.Next(1000,2000)),
                new Salary(rnd.Next(1,5),rnd.Next(1000,2000)),
                new Salary(rnd.Next(1,5),rnd.Next(1000,2000)),
                new Salary(rnd.Next(1,5),rnd.Next(1000,2000)),
                new Salary(rnd.Next(1,5),rnd.Next(1000,2000)),
                new Salary(rnd.Next(1,5),rnd.Next(1000,2000)),
               
            };
        }
    }
}
