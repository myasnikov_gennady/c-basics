﻿using System;
using System.Linq;

namespace HW_19_ULADZIMIR_SKRYPKIN
{
    class Program
    {
        static void Main(string[] args)
        {
            var data = (
                from employee in Employee.Employees().DefaultIfEmpty()
                join salary in Salary.Salaries().DefaultIfEmpty() on employee.Id equals salary.Id
                orderby employee.Id
                select new
                {
                    employee.Name,
                    employee.Id,
                    salary.MonthSalary
                });
            var result2 = data.ToArray();
            foreach (var item in result2)
                Console.WriteLine($"Сотрудник: {item.Name}  с ID {item.Id}, имеет зарплату: {item.MonthSalary} USD");
            Console.WriteLine();

            var result = Employee.Employees()
                .Join(Salary.Salaries(),
                    employee => employee.Id,
                    salary => salary.Id,
                    (employee, salary) => new { employee.Name, employee.Id, salary.MonthSalary })
                .GroupBy(employee => employee.Name)
                .Select(res => new
                {
                    Name = res.Key,
                    average = Math.Round(res.Average(res2 => res2.MonthSalary), 2),
                }
                ).OrderBy(x => x.Name);
            var result1 = result.ToArray();
            foreach (var item in result1)
                Console.WriteLine($"Сотрудник: {item.Name}, средняя зарплата: {item.average} USD");
            Console.WriteLine();
        }
    }
}
