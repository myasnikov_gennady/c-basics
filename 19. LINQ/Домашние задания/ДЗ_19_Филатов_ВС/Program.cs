﻿using System;
using System.Linq;

namespace ДЗ_19_Филатов_ВС
{
    class Program
    {
        static void Main()
        {
            AverageSalary();
            SumSalary();
        }
        private static void AverageSalary()
        {
            Console.WriteLine("Синтаксис методов расширений:");
            var query1 = Employee.Employees()
                .Join(Salary.Salaries(),
                    employee => employee.Id,
                    salary => salary.Id,
                    (employee, salary) => new { employee.Name, employee.Id, salary.MonthSalary })
                .GroupBy(employee => employee.Name)
                .Select(res => new
                    {
                        Name = res.Key,
                        AvgSal = Math.Round(res.Average(res2 => res2.MonthSalary), 2),
                    }
                ).OrderBy(x => x.Name);
            var result1 = query1.ToArray();
            foreach (var item in result1)
                Console.WriteLine($"Сотрудник: {item.Name}, средняя зарплата: {item.AvgSal} BYN");
            Console.WriteLine();
        }

        private static void SumSalary()
        {
            Console.WriteLine("Синтаксис запросов:");
            var query2 = (
                from employee in Employee.Employees().DefaultIfEmpty()
                join salary in Salary.Salaries().DefaultIfEmpty() on employee.Id equals salary.Id
                orderby employee.Id
                select new
                {
                    employee.Name,
                    employee.Id,
                    salary.MonthSalary
                });
            var result2 = query2.ToArray();

            foreach (var item in result2)
                Console.WriteLine($"Сотрудник: {item.Name} (ID {item.Id}), зарплата за период: {item.MonthSalary} BYN");
        }
    }
}
