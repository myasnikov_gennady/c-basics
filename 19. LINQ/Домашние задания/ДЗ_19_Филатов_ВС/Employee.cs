﻿using System.Collections.Generic;

namespace ДЗ_19_Филатов_ВС
{
    internal class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Employee(int id, string name)
        {
            Id = id;
            Name = name;
        }
        public static IEnumerable<Employee> Employees()
        {
            return new List<Employee>()
            {
                new Employee(238,"Viktor"),
                new Employee(982,"Mary"),
                new Employee(28,"Michael"),
                new Employee(122,"Tatyana"),
                new Employee(229,"Steve"),
            };
        }
    }
}
