﻿using System.Collections.Generic;

namespace ДЗ_19_Филатов_ВС
{
    internal class Salary
    {
        public int Id { get; set; }
        public int MonthSalary { get; set; }

        public Salary(int id, int monthSalary)
        {
            Id = id;
            MonthSalary = monthSalary;
        }
        public static IEnumerable<Salary> Salaries()    // Реализация не соответствует условию задачи.
        {
            return new List<Salary>()
            {
                new Salary(238,1380),
                new Salary(238,1450),
                new Salary(238,2500),
                new Salary(238,1450),
                new Salary(28,5200),
                new Salary(28,5400),
                new Salary(122,1820),
                new Salary(122,1800),
                new Salary(122,1420),
                new Salary(229,1230),
                new Salary(229,1320),
                new Salary(229,1400),
            };
        }
    }
}
