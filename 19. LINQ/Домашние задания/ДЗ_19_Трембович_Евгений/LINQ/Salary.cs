﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ
{
    class Salary
    {
        public int Id { get; set; }

        public int MonthSalary { get; set; }

        public Salary(int _id, int _monthSalary)
        {
            Id = _id;
            MonthSalary = _monthSalary;
        }
    }
}
