﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace LINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            var employees = new List<Employee>();

            var random = new Random();

            var employeeCount = random.Next(1, 10);

            string[] names = { "Bob", "Rob", "James", "Pit", "Oliver", "Alex"};

            for (var i = 1; i <= employeeCount; i++)
            {
                var employee = new Employee(i, names[random.Next(names.Length)], GenerateSalaries(random));

                employees.Add(employee);
            }

            var requiredQuery = employees.Select(e => new 
            { 
                Name = e.Name, 
                AverageSalary = e.EmployeeSalaries.Average(es => es.MonthSalary) 
            });

            foreach (var item in requiredQuery)
            {
                Console.WriteLine($"{item.Name}: {item.AverageSalary}");
            }
        }

        private static List<Salary> GenerateSalaries(Random random)
        {
            var salaryCount = random.Next(1, 10);

            var salaries = new List<Salary>();

            for (var i = 1; i <= salaryCount; i++)
            {
                var salary = new Salary(i, random.Next(100, 1000));

                salaries.Add(salary);
            }

            return salaries;
        }
    }
}
