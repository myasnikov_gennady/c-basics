﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ
{
    class Employee
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public List<Salary> EmployeeSalaries; // Реализация класса не соответствует условию

        public Employee(int _id, string _name, List<Salary> salaries)
        {
            Id = _id;
            Name = _name;
            EmployeeSalaries = salaries;
        }
    }
}
