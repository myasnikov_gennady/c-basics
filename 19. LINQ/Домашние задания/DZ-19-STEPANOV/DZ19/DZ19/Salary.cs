﻿using System.Collections.Generic;

namespace DZ19
{
    public class Salary
    {
        public int Date { get; set; }

        public int Id { get; set; }

        public double MonthSalary { get; set; }
        
        public Salary(int monthOfSalary,int id, double monthSalary)
        {
            Date = monthOfSalary;
            Id = id;
            MonthSalary = monthSalary;
        }

        public static IEnumerable<Salary> Salaries()
        {
            return new List<Salary>()
            {
                new Salary(202001,1001,2410),
                new Salary(202001,1002,4340),
                new Salary(202001,1004,2705),
                new Salary(202002,1001,4960),
                new Salary(202002,1002,3565),
                new Salary(202002,1004,2065),
                new Salary(202003,1001,3760),
                new Salary(202003,1002,1430),
                new Salary(202003,1004,2035),
                new Salary(202003,1008,3000),
                new Salary(202004,1001,1210),
                new Salary(202004,1002,2040),
                new Salary(202004,1008,4000),
                new Salary(202005,1001,1810),
                new Salary(202005,1004,1590),
                new Salary(202006,1001,3870),
                new Salary(202006,1004,2810),
                new Salary(202007,1001,1700),
                new Salary(202007,1002,1765),
                new Salary(202007,1004,4060),
                new Salary(202008,1001,4875),
                new Salary(202008,1002,2535),
                new Salary(202008,1004,3125),
                new Salary(202009,1001,3705),
                new Salary(202010,1001,1390),
                new Salary(202010,1002,3410),
                new Salary(202010,1004,1460),
                new Salary(202011,1001,2885),
                new Salary(202011,1004,4245),
                new Salary(202012,1001,3290),
                new Salary(202012,1004,1280)
            };
        }
    }
}
