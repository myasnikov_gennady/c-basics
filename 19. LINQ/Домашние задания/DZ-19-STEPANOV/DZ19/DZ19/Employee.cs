﻿using System.Collections.Generic;

namespace DZ19
{
    public class Employee
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public Employee(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public static IEnumerable<Employee> Employees()
        {
            return new List<Employee>()
            {
                new Employee(1001, "STEFANSKI"),
                new Employee(1002, "TARASOV"),
                new Employee(1004, "FETISOV"),
                new Employee(1008, "TKACHEV"),
                new Employee(1009, "LUKASHOV") // не имеет зарплаты :)
            };
        }
    }
}
