﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Sockets;

namespace Lesson19
{
    class Program
    {
        static void Main()
        {
            //ExpressionExample();
            LinqExample();
        }

        private static void ExpressionExample()
        {
            Expression<Func<int, bool>> math = (int arg) => (arg + 10) / 2d == 0;
            var mathFunc = math.Compile();
            var result = mathFunc(2);
            Console.WriteLine(result);
        }

        private static void LinqExample()
        {
            var users = new List<User>()
            {
                new User("John", 41),
                new User("Michael", 25),
                new User("Tedd", 35),
                new User("Maria", 25),
                new User("Donald", 41),
            };
            var query = (
                from user in users
                //where user.Name.StartsWith("M")
                orderby user.Name descending 
                group user by user.Age into userAges
                select userAges.Key + ":" + string.Join(
                    ", ",
                    from user1 in userAges
                    let tmp = "Mr."
                    select tmp + user1.Name)
                );
            var ages1 = query.ToArray();
            //users.Add(new User("Marta", 99));
            //var ages2 = query.ToArray();
            Console.WriteLine(string.Join(", ", ages1));
            //Console.WriteLine(string.Join(", ", ages2));
            Console.WriteLine();

            Console.WriteLine("------------Example2-----------------");
            var query1_1 = users
                .Skip(1)
                .Take(2)
                .OrderByDescending(x => x.Name)
                .GroupBy(x => x.Age);
            var query1_2 = query1_1
                .Select(x => x.Key + ":" + string.Join(", ", x.Select(y => y.Name)));
            var query1_3 = query1_1.SelectMany(x => x);
            var ages2 = query1_3.Select(x =>
            {
                var temp = "hello";
                return x.Age.ToString() + temp;
            }).ToArray();
            var ages4 = query1_3.Average(x => x.Age);
            Console.WriteLine(string.Join(", ", ages2.Concat(new string[] { "avg: " + ages4 })));

            Console.WriteLine();
            Console.WriteLine(string.Join( ", ", FilterByAge(users, 40).Select(x=>x.Age)));
        }

        private static IEnumerable<User> FilterByAge(IEnumerable<User> users, int age)
        {
            return users.Where(x => x.Age >= age);
        }

        //private static void ExpressionExample()
        //{
        //    Expression<Func<int, bool>> math = (int arg) => (Inc(arg,1) + 10) / 2d == 0;
        //    var mathFunc = math.Compile();
        //    var result = mathFunc(2);
        //    Console.WriteLine(result);
        //}


        //private static int Inc(int arg1, int arg2)
        //{
        //    return arg1 + arg2;
        //}
    }

    internal class User
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public User(string name, int age)
        {
            Name = name;
            Age = age;
        }
    }
}
