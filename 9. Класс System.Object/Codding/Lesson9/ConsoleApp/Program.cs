﻿using System;
using System.Collections.Generic;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            // 1
            var obj = new object();
            Console.WriteLine(obj.ToString());
            // 2
            Console.WriteLine();
            var list = new List<Class1>();

            var obj1 = new Class1() { Field1 = 5, Field2 = 6, Field3 = B.valuea };
            var obj2 = new Class1() { Field1 = 6, Field2 = 5, Field3 = 0 };
            Console.WriteLine(obj1.ToString());
            Console.WriteLine();
            Console.WriteLine(object.ReferenceEquals(obj1, obj2));
            Console.WriteLine(object.Equals(obj1, obj2));
            Console.WriteLine(object.Equals(null, null));
            Console.WriteLine(obj1.Equals(obj2));
            Console.WriteLine(obj1.Equals(obj1));
            var obj3 = (Class1)obj1.Clone();
            Console.WriteLine(obj3);
            Console.WriteLine(object.ReferenceEquals(obj1, obj3));
            var obj4 = obj3;
            Console.WriteLine(obj4);
            Console.WriteLine(object.ReferenceEquals(obj4, obj3));

            Console.WriteLine(obj1 == obj2);

            var obj5 = new Class1() { Field1 = 5, Field2 = 6, Field3 = B.valueb };

            list.AddRange(new[] { obj1, obj2, obj3, obj4, obj5 });
            foreach (var item in list)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();
            Console.WriteLine(list.Contains(obj5));
            Console.WriteLine();

            var str = new A() { FieldA = 10, FieldB = 20 };

            Console.WriteLine(str.FieldA);
            Console.WriteLine();
            var str1 = new A() { FieldA = 10, FieldB = 20 };
            Console.WriteLine(str.Equals(str1));

            Console.WriteLine(obj5.Field3 == 0);
            Console.WriteLine();
            Console.WriteLine(Factorial(5));
            Console.WriteLine();
            var i = 5;
            Console.WriteLine(i);
            object o = i;
            Console.WriteLine(o);
            Console.WriteLine(o.Equals(i));
            object oo = i;
            Console.WriteLine(oo.Equals(o));
            //var j = (byte)o;
            //Console.WriteLine(j);






        }

        static int Factorial(int i)
        {
            
            if (i==1)
            {
                return 1;
            }
            return Factorial(i - 1) * i;
        }

    }
}
