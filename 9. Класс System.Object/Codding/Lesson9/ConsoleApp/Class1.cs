﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp
{
    public class Class1: IEquatable<Class1>
    {
        public int Field1;
        public int Field2 { get; set; }

        public B Field3;

        public object Clone()
        {
            return MemberwiseClone();
        }

        public override string ToString()
        {
            return $"{{ \"{nameof(Field1)}\" : {Field1}, \"{nameof(Field2)}\" : {Field2}, \"{nameof(Field3)}\" : {Field3}  }}";
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as Class1);
        }
        public override int GetHashCode()
        {
            return (Field1*10) ^ Field2;
        }
        //public static bool operator ==(Class1 left, object right)
        //{
        //    return left.GetHashCode() == right.GetHashCode();
        //}
        //public static bool operator !=(Class1 left, object right)
        //{
        //    return left.GetHashCode() != right.GetHashCode();
        //}
        public bool Equals(Class1 obj)
        {
            if (obj == null)
            {
                return false;
            }
            return object.ReferenceEquals(this, obj);
        }

        private int CompareTo(Class1 other)
        {
            if (this.Equals(other))
            {
                return 0;
            }
            if (this.GetHashCode()>other.GetHashCode())
            {
                return -1;
            }
            if (this.GetHashCode() < other.GetHashCode())
            {
                return 1;
            }
            return 0;
        }
    }
}
