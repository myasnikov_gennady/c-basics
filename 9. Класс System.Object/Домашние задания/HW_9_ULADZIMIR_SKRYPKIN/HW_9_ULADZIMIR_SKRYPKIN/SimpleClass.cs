﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW_9_ULADZIMIR_SKRYPKIN
{
    public class SimpleClass
    {
        public int PropertyA { get; set; }
        public int PropertyB { get; set; }
        public  SimpleEnum PropertyC { get; set; }
        public readonly string _element;
        public SimpleClass(int pA, int pB, SimpleEnum pC, string element)
        {
            PropertyA = pA;
            PropertyB = pB;
            PropertyC = pC;
            _element = element;
        }
        public override string ToString() => $"{_element}{{ \"{nameof(PropertyA)}\" : {PropertyA}, \"{nameof(PropertyB)}\" : {PropertyB }, \"{nameof(PropertyC)}\" : {PropertyC }  }}";
        public override int GetHashCode() => (PropertyA +10) ^ (PropertyB +1) ^ (int)(PropertyC+1);
        public override bool Equals(object obj)     // this.GetType().Equals(obj.GetType()) гарантирует эквивалентность типов, но не объектов одного типа
        {  
            if ((obj is SimpleClass)&&(this == obj)&&(obj != null)) // Метод Equals - допускает переопределение сравнения - поэтому было показано не полное сравнение/ Исправлено
            {
                    return true;
            }
            else
            {
                return false;
            }
        }
        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
