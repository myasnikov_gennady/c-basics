﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp
{
    public class SimpleClass
    {
        public int PropertyA { get; set; }
        public int PropertyB { get; set; }
        public SimpleEnum PropertyC { get; set; }
        
        public override string ToString()
        {
            return $"{{ \"{nameof(PropertyA)}\" : {PropertyA}, \"{nameof(PropertyB)}\" : {PropertyB}, \"{nameof(PropertyC)}\" : {PropertyC}  }}";
        }
        
        public override int GetHashCode()
        {
            return (PropertyA+1)^(PropertyB - 1);
        }

        public override bool Equals(object obj)
        {
            if (obj is SimpleClass && obj != null)
            {
                SimpleClass temp = (SimpleClass)obj;
                if (this.PropertyA == temp.PropertyA &&
                    this.PropertyB == temp.PropertyB &&
                    this.PropertyC == temp.PropertyC && this != obj)
                {
                    return true;
                }
            }
            return false;
        }

        public object Clone()
        {
            return MemberwiseClone();
        }

        
    }
}
