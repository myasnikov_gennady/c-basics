﻿namespace HomeWork9
{
    public class SimpleClass
    {
        private int PropertyA { get; set; }
        private int PropertyB { get; set; }
        private SimpleEnum PropertyC { get; set; }
        private readonly string _name;
        public SimpleClass(int propertyA, int propertyB, SimpleEnum propertyC, string name)
        {
            PropertyA = propertyA;
            PropertyB = propertyB;
            PropertyC = propertyC;
            _name = name;
        }
        public object Clone()
        {
            return MemberwiseClone();
        }
        public override string ToString()
        {
            return $"{_name}\n{{\n\t\"{nameof(PropertyA)}\": {PropertyA}, \n\t\"{nameof(PropertyB)}\": {PropertyB}, \n\t\"{nameof(PropertyC)}\": \"{PropertyC}\"\n}}";
        }
        public override int GetHashCode()
        {
            return (PropertyA << 2) ^ (PropertyB >> 2) ^ (int)PropertyC;
        }
        public override bool Equals(object obj)
        {
            if ((obj == null) || (this != obj))
            {
                return false;
            }
            return true;
        }
    }
}
