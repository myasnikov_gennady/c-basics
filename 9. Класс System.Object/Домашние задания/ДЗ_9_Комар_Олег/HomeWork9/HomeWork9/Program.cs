﻿using System;

namespace HomeWork9
{
    class Program
    {
        private static void Main()
        {
            var rectangle = new SimpleClass(3,5,SimpleEnum.Red,"красный прямоугольник:");
            Console.WriteLine("Оригинальный " + rectangle);
            var rectangle2 = rectangle.Clone();
            Console.WriteLine("Клонированный " + rectangle2);
            var rectangle3 = rectangle;
            Console.WriteLine("Присвоенный "+rectangle3);
            Console.WriteLine();
            ConsoleHelper.Info("Comparison by method ReferenceEquals(object objA, object objB)");
            ReferenceEqualsToConsole(rectangle, rectangle2);
            ReferenceEqualsToConsole(rectangle, rectangle3);
            ReferenceEqualsToConsole(rectangle2, rectangle3);
            Console.WriteLine();
            ConsoleHelper.Info("Comparison by method Equals(object objA, object objB)");
            EqualsToConsole(rectangle, rectangle2);
            EqualsToConsole(rectangle, rectangle3);
            EqualsToConsole(rectangle3, rectangle2);
            Console.WriteLine();
            ConsoleHelper.Info("Comparison by method Equals(object obj)");
            EqualsToConsole2(rectangle, rectangle2);
            EqualsToConsole2(rectangle, rectangle3);
            EqualsToConsole2(rectangle3, rectangle2);
            Console.WriteLine();
            ConsoleHelper.Info("Comparison by operator ==");
            EqualsToConsole3(rectangle, rectangle2);
            EqualsToConsole3(rectangle, rectangle3);
            EqualsToConsole3(rectangle3, rectangle2);
            ConsoleHelper.Clear();
            ConsoleHelper.Info("Рекурсивные функции");
            Console.WriteLine("Факториал 5-ти");
            Console.WriteLine(Fact(5));
            Console.WriteLine();
            Console.WriteLine("Сумма чисел от 1 до 5");
            Console.WriteLine(Sum(5));
            Console.WriteLine();
            Console.WriteLine("Числа фибоначчи по 10е в ряду");
            for (var i = 0; i < 10; i++)
            {
                Console.Write($"{Fib(i)} ");
            }
            Console.WriteLine();
        }
        private static int Fact(int x)
        {

            if (x == 1)
            {
                return 1;
            }
            return x * Fact(x- 1);
        }

        private static int Sum(int x)
        {
            if (x == 1)
                return 1;
            else
                return  x + Sum(x - 1);
        }
        private static int Fib(int x)
        {
            if (x == 0 || x == 1)
            {
                return x;
            }
            else
            {

                return Fib(x - 1) + Fib(x - 2);
            }
        }
        private static void ReferenceEqualsToConsole(object obj1, object obj2)
        {
            var refEquals = ReferenceEquals(obj1, obj2);
            var result = refEquals ? "ЯВЛЯЮТСЯ" : "НЕ ЯВЛЯЮТСЯ";
            Console.WriteLine($"Объекты {result} одним и тем же экземпляром класса");
        }
        private static void EqualsToConsole(object obj1, object obj2)
        {
            var equals = Equals(obj1, obj2);
            var result = equals ? "РАВНЫ" : "НЕ РАВНЫ";
            Console.WriteLine($"Экземпляры объекта {result} между собой");
        }
        private static void EqualsToConsole2(object obj1, object obj2)
        {
            var equals = obj1.Equals( obj2);
            var result = equals ? "РАВНЫ" : "НЕ РАВНЫ";
            Console.WriteLine($"Экземпляры объекта {result} между собой");
        }
        private static void EqualsToConsole3(object obj1, object obj2)
        {
            var equals = obj1==obj2;
            var result = equals ? "РАВНЫ" : "НЕ РАВНЫ";
            Console.WriteLine($"Экземпляры объекта {result} между собой");
        }
    }
}
