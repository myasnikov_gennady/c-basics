﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SystemObject
{
    class SimpleClass : ICloneable
    {
        public int PropertyA { get; set; } = 1;
        public int PropertyB { get; set; } = 2;
        public SimpleEnum PropertyC { get; set; }

        public override string ToString()
        {
            Console.WriteLine("ToString() method overriding:");
            if (String.IsNullOrEmpty(PropertyA.ToString()) & String.IsNullOrEmpty(PropertyB.ToString()))
            {
                return base.ToString(); 
            }
            else
            {
                return $"PropertyA = {PropertyA}, PropertyB = {PropertyB}";
            }
           
        }

        public override int GetHashCode()   // необходимо переопределить базовую реализацию метода
        {
            Console.Write("GetHashCode() method overriding:\n");
            return base.GetHashCode();
        }

        public override bool Equals(object obj) // необходимо переопределить базовую реализацию метода
        {
            return base.Equals(obj);
        }

        public object Clone()
        {
            return MemberwiseClone();   
        }
    }
}
