﻿using System;

namespace SystemObject
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = new SimpleClass();
            Console.WriteLine(a.ToString());
            Console.WriteLine(a.GetHashCode());

            var object1 = new SimpleClass();
            var object2 = object1.Clone();
            var object3 = object1;

            Console.WriteLine("Comparing objects:");
            Console.WriteLine("\tReferenceEquals method:");
            Console.WriteLine($"\t\tobject1 and object2 are the same - {ReferenceEquals(object1, object2)}");
            Console.WriteLine($"\t\tobject1 and object3 are the same - {ReferenceEquals(object1, object3)}");
            Console.WriteLine($"\t\tobject2 and object3 are the same - {ReferenceEquals(object2, object3)}");

            Console.WriteLine("\tpublic static bool Equals method:");
            Console.WriteLine($"\t\tobject1 and object2 are the equal - {Equals(object1, object2)} (overrided)");
            Console.WriteLine($"\t\tobject1 and object3 are the equal - {Equals(object1, object3)} (overrided)");
            Console.WriteLine($"\t\tobject2 and object3 are the equal - {Equals(object2, object3)} (overrided)");

            Console.WriteLine("\tpublic virtual bool Equals method:");
            Console.WriteLine($"\t\tobject1 and object2 are the equal - {object1.Equals(object2)} (overrided)");
            Console.WriteLine($"\t\tobject1 and object3 are the equal - {object1.Equals(object3)} (overrided)");
            Console.WriteLine($"\t\tobject2 and object3 are the equal - {object2.Equals(object3)} (overrided)");

            Console.WriteLine("\t==:");
            Console.WriteLine($"\t\tobject1 == object2 - {object1 == object2}");
            Console.WriteLine($"\t\tobject1 == object3 - {object1 == object3}");
            Console.WriteLine($"\t\tobject2 == object3 - {object2 == object3}");

            Console.WriteLine($"\nFactorial of a number 5 = {Factorial(5)}");
            Console.WriteLine($"\nRecursion to create a Sum Method: 5 + 4 + 3 + 2 + 1 = {Sum(5)}");
            Console.WriteLine($"\nFourth item of Fibonacci sequence is {Fibonachi(4)}");


        }

        public static int Factorial(int i)
        {
            int result;
            if (i == 1)
            {
                return 1;
            }
            result = Factorial(i - 1) * i;
            return result;
        }

        public static int Sum(int i)
        {
            if (i <= 1)
            {
                return i;
            }
            return i + Sum(i - 1);
        }

        public static int Fibonachi(int n)
        {
            if (n == 0)
            {
                return 0;
            }
            else if (n == 1)
            {
                return 1;
            }
            else
            {
                return Fibonachi(n - 1) + Fibonachi(n - 2);
            }
        }
    }
}
