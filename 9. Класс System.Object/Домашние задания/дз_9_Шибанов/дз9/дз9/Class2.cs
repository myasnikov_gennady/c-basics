using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace дз9
{
    public class SimpleClass
    {
        public int PropertyA { get; set; }
        public int PropertyB { get; set; }
        public enum PropertyC { get, set }  // объявлено перечислние, но не переменная с типом перечисления. Тут. PropertyC не член класса, а вложенное перечисление SimpleClass.PropertyC
        public object Clone()
        {
            return MemberwiseClone();
        }

        public override string ToString()
        {
            return $"{{\"{nameof(PropertyA)}\":{PropertyA}, \"{ nameof(PropertyB)}\" : {PropertyB}}}";
        }

        public bool ReferenseEquals(SimpleClass obj1, SimpleClass obj2)
        {
            if (obj1 == obj2)
            {
                return true;
            }
            return object.ReferenceEquals(this, obj2);
        }
        public bool Equals(SimpleClass obj1, SimpleClass obj2)
        {
            if (obj1 == obj2)
            {
                return false;
            }
            return object.ReferenceEquals(this, obj2);
        }

        public virtual bool Equals(SimpleClass obj1)
        {
            if (obj1 == null)
            {
                return false;
            }
            return object.ReferenceEquals(this, obj1);
        }
        public static bool operator ==(SimpleClass left, object right)
        {
            return left.GetHashCode() == right.GetHashCode();
        }
        public override int GetHashCode()
        {
            return PropertyA^PropertyB;
        }

        public static bool operator !=(SimpleClass left, object right)
        {
            return left.GetHashCode() != right.GetHashCode();
        }
    }
}
