﻿using System;
using System.Collections.Generic;
using static дз9.SimpleClass;

namespace дз9
{
    class Program
    {
        static void Main(string[] args)
        {
            var obj = new Object();
            Console.WriteLine(obj.ToString());
            Console.WriteLine();

            var list = new List<SimpleClass>();

            var obj1 = new SimpleClass() { PropertyA = 12, PropertyB = 26 };
            Console.WriteLine(obj1.ToString());

            var obj2 = (SimpleClass)obj1.Clone();
            Console.WriteLine(obj2);
            var obj3 = obj1;
            Console.WriteLine(obj3);

            list.AddRange(new[] { obj1, obj2, obj3 });
            foreach (var item in list)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine(list.Contains(obj3));
            Console.WriteLine();

            Console.WriteLine(object.Equals(obj, obj2));
            Console.WriteLine(object.ReferenceEquals(obj1, obj3));
            Console.WriteLine();

            int factorial(int x)
            {
                int result;
                if (x == 1)
                { 
                    return 1;
                }
                result = factorial(x - 1) * x; return result;
                
            }
            Console.WriteLine(factorial(5));
            Console.WriteLine();


            int Fibonachi(int q)
            {
                if (q == 1)
                {
                    return 1;
                }
                else if (q == 2)
                {
                    return 2;
                }
                else
                {
                    return Fibonachi(q - 1) + Fibonachi(q - 2);
                }
                Console.WriteLine(Fibonachi(10));

                int RecursMetod(int a, int b)       // для чего тут реализован этот метод? он нигде не используется
                {
                    if (a < b)
                    {
                        Console.Write(a + " ");
                        a++;
                        RecursMetod(a, b);
                    }
                    return b;
                }

                
            }

            
        }
    }
}    
