﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ_9.STEPANOV.A.S
{
    class Program
    {
        static void Main(string[] args)
        {
            var obj1 = new SimpleClass(6, 3, SimpleEnum.Saturday, "Дни недели");
            var obj2 = obj1.Clone();
            var obj3 = obj1;
            Console.WriteLine($"/Объявленный объект/  {obj1}\n/Клонированный объект/{obj2}\n/Присвоенный объект/  {obj3} ");
            Console.WriteLine();
            Console.WriteLine("Сравнение методом 'Public static bool ReferenceEquals' двух объектов");
            Console.WriteLine($"Значение на равенство объектов /Объявленный объект/ -obj1 и /Клонированный объект/ - obj2 равно:{RefEquals(obj1, obj2)} ");
            Console.WriteLine($"Значение на равенство объектов /Объявленный объект/ -obj1 и /Присвоенный объект/ - obj3 равно:{RefEquals(obj1, obj3)} ");
            Console.WriteLine($"Значение на равенство объектов /Клонированный объект/ -obj2 и /Присвоенный объект/ - obj3 равно:{RefEquals(obj2, obj3)} ");
            Console.WriteLine();
            Console.WriteLine("Сравнение методом 'Public static bool Equals' двух объектов");
            Console.WriteLine($"Значение на равенство объектов /Объявленный объект/ -obj1 и /Клонированный объект/ - obj2 равно:{Equal(obj1, obj2)} ");
            Console.WriteLine($"Значение на равенство объектов /Объявленный объект/ -obj1 и /Присвоенный объект/ - obj3 равно:{Equal(obj1, obj3)} ");
            Console.WriteLine($"Значение на равенство объектов /Клонированный объект/ -obj2 и /Присвоенный объект/ - obj3 равно:{Equal(obj2, obj3)} ");
            Console.WriteLine();
            Console.WriteLine("Сравнение методом  'public virtual bool Equals' двух объектов");
            Console.WriteLine($"Значение на равенство объектов /Объявленный объект/ -obj1 и /Клонированный объект/ - obj2 равно:{obj1.Equals(obj2)} ");
            Console.WriteLine($"Значение на равенство объектов /Объявленный объект/ -obj1 и /Присвоенный объект/ - obj3 равно:{obj1.Equals(obj3)} ");
            Console.WriteLine($"Значение на равенство объектов /Клонированный объект/ -obj2 и /Присвоенный объект/ - obj3 равно:{obj2.Equals(obj3)} ");
            Console.WriteLine();
            Console.WriteLine("Сравнение методом  '==' двух объектов");
            Console.WriteLine($"Значение на равенство объектов /Объявленный объект/ -obj1 и /Клонированный объект/ - obj2 равно:{REquals(obj1, obj2)} ");
            Console.WriteLine($"Значение на равенство объектов /Объявленный объект/ -obj1 и /Присвоенный объект/ - obj3 равно:{REquals(obj1, obj3)} ");
            Console.WriteLine($"Значение на равенство объектов /Клонированный объект/ -obj2 и /Присвоенный объект/ - obj3 равно:{REquals(obj2, obj3)} ");
            Console.WriteLine();
            Console.WriteLine("Рекурсивные методы по вычислению");
            Console.WriteLine("Факториал десяти:");
            Console.WriteLine(Factorial(10));
            Console.WriteLine("Cуммы от восьми:");
            Console.WriteLine(Sum(8));
            Console.WriteLine("Чисел Фибоначи от восьми:");
            Console.WriteLine(Fib(8));
        }
        public static bool RefEquals(object objA, object objB) => ReferenceEquals(objA, objB);
        public static bool Equal(object objA, object objB) => Equals(objA, objB);
        public static bool REquals(object objA, object objB) => objA == objB;
        public static int Factorial(int n) => (n == 0) ? 1 : n * Factorial(n - 1);
        public static int Sum(int n) => (n == 1) || (n == 0) ? n : n + Sum(n - 1);
        public static int Fib(int n) => (n == 1) || (n == 0) ? n : Fib(n - 1) + Fib(n - 2);
    }
}

