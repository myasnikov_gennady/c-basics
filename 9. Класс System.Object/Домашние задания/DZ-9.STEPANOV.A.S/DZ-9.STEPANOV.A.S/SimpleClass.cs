﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ_9.STEPANOV.A.S
{
    class SimpleClass
    {
        public int PropertyA { get; set; }
        public int PropertyB { get; set; }
        public SimpleEnum PropertyC { get; set; }
        public readonly string _element;
        public SimpleClass(int pA, int pB, SimpleEnum pC, string element)
        {
            PropertyA = pA;
            PropertyB = pB;
            PropertyC = pC;
            _element = element;
        }
        public override string ToString() => $"{_element}{{ \"{nameof(PropertyA)}\" : {PropertyA}, \"{nameof(PropertyB)}\" : {PropertyB }, \"{nameof(PropertyC)}\" : {PropertyC }  }}";
        public override int GetHashCode() => (PropertyA + 10) ^ (PropertyB + 1) ^ (int)(PropertyC + 1);
        public override bool Equals(object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
                return true;
        }
        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
