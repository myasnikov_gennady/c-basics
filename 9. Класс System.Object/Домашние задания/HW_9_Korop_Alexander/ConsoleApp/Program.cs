﻿using System;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var obj1 = new SimpleClass() { PropertyA = 1, PropertyB = 2, PropertyC = SimpleEnum.First };
            var obj2 = (SimpleClass)obj1.Clone();
            var obj3 = obj1;
            var obj4 = new SimpleClass() { PropertyA = 2, PropertyB = 2, PropertyC = SimpleEnum.Second };

            Console.WriteLine(obj1.ToString());
            Console.WriteLine(obj4.ToString());
            Console.WriteLine();

            Console.WriteLine("ReferenceEquals: ");
            Console.WriteLine($"{nameof(obj1)} ReferenceEquals {nameof(obj2)} : {object.ReferenceEquals(obj1, obj2)}");
            Console.WriteLine($"{nameof(obj1)} ReferenceEquals {nameof(obj3)} : {object.ReferenceEquals(obj1, obj3)}");
            Console.WriteLine($"{nameof(obj2)} ReferenceEquals {nameof(obj3)} : {object.ReferenceEquals(obj2, obj3)}");
            Console.WriteLine();

            Console.WriteLine("Equals(obj, obj): ");
            Console.WriteLine($"{nameof(obj1)} Equals {nameof(obj2)} : {object.Equals(obj1, obj2)}");
            Console.WriteLine($"{nameof(obj1)} Equals {nameof(obj3)} : {object.Equals(obj1, obj3)}");
            Console.WriteLine($"{nameof(obj2)} Equals {nameof(obj3)} : {object.Equals(obj2, obj3)}");
            Console.WriteLine($"{nameof(obj1)} Equals {nameof(obj4)} : {object.Equals(obj1, obj4)}");
            Console.WriteLine();

            Console.WriteLine("obj.Equals(obj): ");
            Console.WriteLine($"{nameof(obj1)} Equals {nameof(obj2)} : {obj1.Equals(obj2)}");
            Console.WriteLine($"{nameof(obj1)} Equals {nameof(obj3)} : {obj1.Equals(obj3)}");
            Console.WriteLine($"{nameof(obj2)} Equals {nameof(obj3)} : {obj2.Equals(obj3)}");
            Console.WriteLine($"{nameof(obj1)} Equals {nameof(obj4)} : {obj1.Equals(obj4)}");
            Console.WriteLine();

            Console.WriteLine("==");
            Console.WriteLine($"{nameof(obj1)} == {nameof(obj2)} : {obj1 == obj2} ");
            Console.WriteLine($"{nameof(obj1)} == {nameof(obj3)} : {obj1 == obj3}");
            Console.WriteLine($"{nameof(obj2)} == {nameof(obj3)} : {obj2 == obj3}");
            Console.WriteLine();

            


            // Factorial, Sum of numbers, Fibonachi numbers
            Console.Write("Factorial of 5: ");
            Console.Write(Factorial(5));
            Console.WriteLine();

            Console.Write("Sum of numbers form 1 to 10: ");
            Console.Write(SumOfNumbers(10));
            Console.WriteLine();

            Console.Write("Fibonachi numbers: ");
            Console.Write(FibonachiNumbers(0, 1) + " ets...");
            Console.WriteLine();
            
        }

        static int FibonachiNumbers(int i, int j)       // ограничения не обязательны см. https://ru.wikipedia.org/wiki/Числа_Фибоначчи
        {
            if (j > 1000)
            {
                return j;
            }
            Console.Write(j + " ");
            return FibonachiNumbers(j, i = i + j);
        }

        static int SumOfNumbers(int i)
        {
            if (i == 0)
            {
                return 0;
            }
            return SumOfNumbers(i - 1) + i;
        }

        static int Factorial(int i)
        {

            if (i == 1)
            {
                return 1;
            }
            return Factorial(i - 1) * i;
        }
    }
}
