﻿using System;
using ClassLibrary;

namespace ДЗ_9_Филатов_ВС
{
    class Program
    {
        //3. Объявить 3 экземпляра класса:
        //1. Через конструктор класса.
        //2. Через клонирование первого экземпляра(метод Clone).
        //3. Через приравнивание первого экземпляра(obj3 = obj1).
        static void Main(string[] args)
        {
            var obj1 = new SimpleClass() { PropertyA = 12, PropertyB = 98, PropertyC = SimpleClass.SimpleEnum.value0 };
            var obj2 = (SimpleClass)obj1.Clone();
            var obj3 = obj1;
            Console.WriteLine("Исходные объекты:");
            Console.WriteLine("obj1" + obj1);
            Console.WriteLine("obj2" + obj2);
            Console.WriteLine("obj3" + obj3);
            Console.WriteLine($"Функция ToString: {obj1.ToString()}");
            Console.WriteLine($"Функция GetHashCode: {obj1.GetHashCode()}");

            //4.Выполнить сравнение объектов через:
            //1. public static bool ReferenceEquals(object objA, object objB)
            //2. public static bool Equals(object objA, object objB)
            //3. public virtual bool Equals(object obj)
            //4. ==

            Console.WriteLine($"Функция ReferenceEquals для obj1 и obj2: {ReferenceEquals(obj1, obj2)}");
            Console.WriteLine($"Функция ReferenceEquals для obj1 и obj3: {ReferenceEquals(obj1, obj3)}");
            Console.WriteLine($"Функция Equals для obj1 и obj2: {Equals(obj1, obj2)}");
            Console.WriteLine($"Функция Equals для obj1 и obj3: {Equals(obj1, obj3)}");
            Console.WriteLine($"Функция obj1.Equals(obj2): {obj1.Equals(obj2)}");
            Console.WriteLine($"Функция obj1.Equals(obj3): {obj1.Equals(obj3)}");
            Console.WriteLine($"Оператор obj1==obj2: {obj1 == obj2}");
            Console.WriteLine($"Оператор obj1==obj3: {obj1 == obj3}");

            //5.В классе Program реализовать рекурсивные методы по вычислению:
            //1.Факториала
            //2.Суммы чисел
            //3.Чисел Фибоначчи

            Console.Write("Введите число для вычисления факториала, суммы, чисел Фибоначчи: ");
            int x = int.Parse(Console.ReadLine());
            Console.WriteLine($"Факториал {x}! = {Factorial(x)}");
            Console.WriteLine($"Сумма натуральных чисел до {x} = {Sum(x)}");
            Console.Write($"Последовательность чисел Фибоначчи для {x}: ");
            for (var i = 0; i < x + 1 ; i++)
            {
                Console.Write($"{Fibonacci(i)} ");
            }
        }
        static int Factorial(int i)
        {
            if (i == 0)
            {
                return 1;
            }
            return i * Factorial(i - 1);
        }
        static int Sum(int i)
        {
            if (i == 0)
            {
                return 1;
            }
            return i + Sum(i - 1);
        }
        static int Fibonacci(int i)
        {
            if (i == 0 || i == 1)
            {
                return i;
            }
            return Fibonacci(i - 1) + Fibonacci(i - 2);
        }
    }
}
