﻿using System;

namespace ClassLibrary   // классы стоит описывать в раздельных файлах (обычная приктика), если не сказано иначе -> исправлено
{
    // 1. Создать класс SimpleClass с двумя автосвойствами типа int (PropertyA, PropertyB)
    // и одним автосвойством типа enum SimpleEnum (PropertyC) и переопределить методы ToString,
    // GetHashCode, Equals. Метод ToString реализовать так, чтобы отображались значения публичных свойств класса.
    public class SimpleClass
    {
        public int PropertyA { get; set; }
        public int PropertyB { get; set; }
        public SimpleEnum PropertyC { get; set; }
        public enum SimpleEnum
        {
            value0 = 19,
            value1 = 58
        }
        public override string ToString()
        {
            return $"{{ \"{nameof(PropertyA)}\" : {PropertyA}, \"{nameof(PropertyB)}\" : {PropertyB}, \"{nameof(PropertyC)}\" : {PropertyC} }}";
        }
        public override int GetHashCode()
        {
            return PropertyA * 5 + PropertyB + (int)PropertyC * 10;
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as SimpleClass);
        }
        public bool Equals(SimpleClass obj)
        {
            if (obj == null)
            {
                return false;
            }
            return object.ReferenceEquals(this, obj);
        }
        // 2. Реализовать метод Clone с использованием метода MemberwiseClone.
        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}