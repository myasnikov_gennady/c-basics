﻿using System;

namespace HW_3_
{
    class Program
    {
        static void Main(string[] args)
        {
            
            

            Console.WriteLine("Операция побитового дополнения для значений:");

            int a = 0b_0001;
            int b = 0b_0010;
            int c = 0b_0100;
            int d = 0b_1000;
            int e = 0b_1001;
            int f = 0b_1100;

            Console.WriteLine($" {Convert.ToString(a, toBase: 2).PadLeft(4, '0')} = {Convert.ToString(~a, toBase: 2)}");
            Console.WriteLine($" {Convert.ToString(b, toBase: 2).PadLeft(4, '0')} = {Convert.ToString(~b, toBase: 2)}");
            Console.WriteLine($" {Convert.ToString(c, toBase: 2).PadLeft(4, '0')} = {Convert.ToString(~c, toBase: 2)}");
            Console.WriteLine($" {Convert.ToString(d, toBase: 2).PadLeft(4, '0')} = {Convert.ToString(~d, toBase: 2)}");
            Console.WriteLine($" {Convert.ToString(e, toBase: 2).PadLeft(4, '0')} = {Convert.ToString(~e, toBase: 2)}");
            Console.WriteLine($" {Convert.ToString(f, toBase: 2).PadLeft(4, '0')} = {Convert.ToString(~f, toBase: 2)}");
            Console.WriteLine();

          

            Console.WriteLine("Операция побитового сдвига (<<, >>) для значений:");

            int leftShift_a = a << 1;
            int rightShift_a = a >> 1;
            int leftShift_b = b << 1;
            int rightShift_b = b >> 1;
            int leftShift_c = c << 1;
            int rightShift_c = c >> 1;
            int leftShift_d = d << 1;
            int rightShift_d = d >> 1;
            int leftShift_e = e << 1;
            int rightShift_e = e >> 1;
            int leftShift_f = f << 1;
            int rightShift_f = f >> 1;

            Console.WriteLine($" {Convert.ToString(a, toBase: 2).PadLeft(4, '0')} << 1 = {Convert.ToString(leftShift_a, toBase: 2).PadLeft(4,'0')}");
            Console.WriteLine($" {Convert.ToString(a, toBase: 2).PadLeft(4, '0')} >> 1 = {Convert.ToString(rightShift_a, toBase: 2).PadLeft(4,'0')}");
            Console.WriteLine($" {Convert.ToString(b, toBase: 2).PadLeft(4, '0')} << 1 = {Convert.ToString(leftShift_b, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine($" {Convert.ToString(b, toBase: 2).PadLeft(4, '0')} >> 1 = {Convert.ToString(rightShift_b, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine($" {Convert.ToString(c, toBase: 2).PadLeft(4, '0')} << 1 = {Convert.ToString(leftShift_c, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine($" {Convert.ToString(c, toBase: 2).PadLeft(4, '0')} >> 1 = {Convert.ToString(rightShift_c, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine($" {Convert.ToString(d, toBase: 2).PadLeft(4, '0')} << 1 = {Convert.ToString(leftShift_d, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine($" {Convert.ToString(d, toBase: 2).PadLeft(4, '0')} >> 1 = {Convert.ToString(rightShift_d, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine($" {Convert.ToString(e, toBase: 2).PadLeft(4, '0')} << 1 = {Convert.ToString(leftShift_e, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine($" {Convert.ToString(e, toBase: 2).PadLeft(4, '0')} >> 1 = {Convert.ToString(rightShift_e, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine($" {Convert.ToString(f, toBase: 2).PadLeft(4, '0')} << 1 = {Convert.ToString(leftShift_f, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine($" {Convert.ToString(f, toBase: 2).PadLeft(4, '0')} >> 1 = {Convert.ToString(rightShift_f, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine();

            Console.WriteLine("Операция логического И (&) для значений:");

            int a_And_b = a & b;
            int c_And_d = c & d;
            int e_And_f = e & f;

            Console.WriteLine($" {Convert.ToString(a, toBase: 2).PadLeft(4, '0')} & {Convert.ToString(b, toBase: 2).PadLeft(4, '0')} = {Convert.ToString(a_And_b, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine($" {Convert.ToString(c, toBase: 2).PadLeft(4, '0')} & {Convert.ToString(d, toBase: 2).PadLeft(4, '0')} = {Convert.ToString(c_And_d, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine($" {Convert.ToString(e, toBase: 2).PadLeft(4, '0')} & {Convert.ToString(f, toBase: 2).PadLeft(4, '0')} = {Convert.ToString(e_And_f, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine();

            Console.WriteLine("Операция логического исключения ИЛИ (^) для значений:");

            int a_ExclusiveOR_b = a ^ b;
            int c_ExclusiveOR_d = c ^ d;
            int e_ExclusiveOR_f = e ^ f;

            Console.WriteLine($" {Convert.ToString(a, toBase: 2).PadLeft(4, '0')} ^ {Convert.ToString(b, toBase: 2).PadLeft(4, '0')} = {Convert.ToString(a_ExclusiveOR_b, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine($" {Convert.ToString(c, toBase: 2).PadLeft(4, '0')} ^ {Convert.ToString(d, toBase: 2).PadLeft(4, '0')} = {Convert.ToString(c_ExclusiveOR_d, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine($" {Convert.ToString(e, toBase: 2).PadLeft(4, '0')} ^ {Convert.ToString(f, toBase: 2).PadLeft(4, '0')} = {Convert.ToString(e_ExclusiveOR_f, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine();

            Console.WriteLine("Операция логического исключения ИЛИ (|) для значений:");
            int a_OR_b = a | b;
            int c_OR_d = c | d;
            int e_OR_f = e | f;

            Console.WriteLine($"{Convert.ToString(a, toBase: 2).PadLeft(4, '0')} | {Convert.ToString(b, toBase: 2).PadLeft(4, '0')} = {Convert.ToString(a_OR_b, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine($"{Convert.ToString(c, toBase: 2).PadLeft(4, '0')} | {Convert.ToString(d, toBase: 2).PadLeft(4, '0')} = {Convert.ToString(c_OR_d, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine($"{Convert.ToString(e, toBase: 2).PadLeft(4, '0')} | {Convert.ToString(f, toBase: 2).PadLeft(4, '0')} = {Convert.ToString(e_OR_f, toBase: 2).PadLeft(4, '0')}");



        }
    }
}
