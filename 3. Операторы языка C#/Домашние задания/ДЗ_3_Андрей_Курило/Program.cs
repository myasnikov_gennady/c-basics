﻿using Homework.Operations;
using System;

namespace Homework
{
    class Program
    {
        static void Main()
        {
            new BitwiseComplement().Run();
            new LeftRightShiftOperator().Run();
            new LogicalOperations().Run();

            Console.ReadKey();
        }
    }
}
