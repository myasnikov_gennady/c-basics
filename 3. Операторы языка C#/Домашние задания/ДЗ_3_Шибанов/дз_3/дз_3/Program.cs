﻿using System;

namespace дз_3
{
    class Program
    {
        static void Main(string[] args) // значения должны быть в битовом виде, т.е. int x = 0b_0001
        {
            Console.WriteLine("Право");
            int x = 0001;
            int z = x >> 4;
            Console.WriteLine($"{x} >> 4");
            Console.WriteLine($"After: {Convert.ToString(z, toBase: 2)}");

            int h = 0010;
            int g = h >> 4;
            Console.WriteLine($"{h} >> 4");
            Console.WriteLine($"After: {Convert.ToString(g, toBase: 2)}");

            int c = 0100;
            int v = x >> 4;
            Console.WriteLine($"{c} >> 4");
            Console.WriteLine($"After: {Convert.ToString(v, toBase: 2)}");

            int b = 1000;
            int n = b >> 4;
            Console.WriteLine($"{b} >> 4");
            Console.WriteLine($"After: {Convert.ToString(n, toBase: 2)}");

            int m = 1100;
            int l = m >> 4;
            Console.WriteLine($"{m} >> 4");
            Console.WriteLine($"After: {Convert.ToString(l, toBase: 2)}");

            int k = 1001;
            int j = k >> 4;
            Console.WriteLine($"{k} >> 4");
            Console.WriteLine($"After: {Convert.ToString(j, toBase: 2)}");

            Console.WriteLine();

            int q = 0001;
            int w = 0010;
            int e = q ^ w;
            Console.WriteLine($"{Convert.ToString(q, toBase: 2)} ^ {Convert.ToString(w, toBase: 2)}");
            Console.WriteLine(Convert.ToString(q, toBase: 2));

            int r = 0100;
            int t = 1000;
            int y = r ^ t;
            Console.WriteLine($"{Convert.ToString(r, toBase: 2)} ^ {Convert.ToString(t, toBase: 2)}");
            Console.WriteLine(Convert.ToString(y, toBase: 2));

            int u = 1001;
            int i = 1100;
            int o = u ^ i;
            Console.WriteLine($"{Convert.ToString(u, toBase: 2)} ^ {Convert.ToString(i, toBase: 2)}");
            Console.WriteLine(Convert.ToString(o, toBase: 2));


            Console.WriteLine();

            int a = 0001;
            int s = 0010;
            int d = a | s;
            Console.WriteLine($"{Convert.ToString(a, toBase: 2)} | {Convert.ToString(s, toBase: 2)}");
            Console.WriteLine(Convert.ToString(d, toBase: 2));

            int f = 0100;
            int G = 1000;
            int H = f |G;
            Console.WriteLine($"{Convert.ToString(f, toBase: 2)} | {Convert.ToString(G, toBase: 2)}");
            Console.WriteLine(Convert.ToString(H, toBase: 2));

            int Q = 1001;
            int W = 1100;
            int E = Q | W;
            Console.WriteLine($"{Convert.ToString(W, toBase: 2)} | {Convert.ToString(W, toBase: 2)}");
            Console.WriteLine(Convert.ToString(E, toBase: 2));

            Console.WriteLine();

            int R = 0001;
            int T = 0010;
            int Y = R & T;
            Console.WriteLine($"{Convert.ToString(R, toBase: 2)} & {Convert.ToString(T, toBase: 2)}");
            Console.WriteLine(Convert.ToString(Y, toBase: 2));

            int U = 0100;
            int I= 1000;
            int O = r & t;
            Console.WriteLine($"{Convert.ToString(U, toBase: 2)} & {Convert.ToString(I, toBase: 2)}");
            Console.WriteLine(Convert.ToString(O, toBase: 2));

            int A = 1001;
            int S = 1100;
            int D = A & S;
            Console.WriteLine($"{Convert.ToString(A, toBase: 2)} & {Convert.ToString(S, toBase: 2)}");
            Console.WriteLine(Convert.ToString(D, toBase: 2));




        }
    }
}
