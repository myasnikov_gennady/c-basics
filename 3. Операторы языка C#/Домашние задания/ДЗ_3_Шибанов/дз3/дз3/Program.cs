﻿using System;

namespace дз3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Операции побитового дополнения для значений");
            uint q = 0001;
            uint w = ~q;

            Console.WriteLine($"~ [q] : {Convert.ToString(q, toBase: 2)}");
            Console.WriteLine(Convert.ToString(w, toBase: 2));

            uint e = 0010;
            uint r = ~e;

            Console.WriteLine($"~ [e] : {Convert.ToString(e, toBase: 2)}");
            Console.WriteLine(Convert.ToString(r, toBase: 2));

            uint t = 0100;
            uint y = ~t;

            Console.WriteLine($"~ [t] : {Convert.ToString(t, toBase: 2)}");
            Console.WriteLine(Convert.ToString(y, toBase: 2));

            uint u = 1000;
            uint i = ~u;

            Console.WriteLine($"~ [u] : {Convert.ToString(u, toBase: 2)}");
            Console.WriteLine(Convert.ToString(i, toBase: 2));

            uint o = 1001;
            uint p = ~o;

            Console.WriteLine($"~ [o] : {Convert.ToString(o, toBase: 2)}");
            Console.WriteLine(Convert.ToString(p, toBase: 2));

            uint a = 1100;
            uint s = ~a;

            Console.WriteLine($"~ [a] : {Convert.ToString(a, toBase: 2)}");
            Console.WriteLine(Convert.ToString(s, toBase: 2));

            Console.WriteLine("Операции побитового сдвига");

            uint x = 0001;
            uint z = x << 4;
            Console.WriteLine($"{x} << 4");
            Console.WriteLine($"After: {Convert.ToString(z, toBase: 2)}");

            uint h = 0010;
            uint g = h << 4;
            Console.WriteLine($"{h} << 4");
            Console.WriteLine($"After: {Convert.ToString(g, toBase: 2)}");

            uint c = 0100;
            uint v = x << 4;
            Console.WriteLine($"{c} << 4");
            Console.WriteLine($"After: {Convert.ToString(v, toBase: 2)}");

            uint b = 1000;
            uint n = b << 4;
            Console.WriteLine($"{b} << 4");
            Console.WriteLine($"After: {Convert.ToString(n, toBase: 2)}");

            uint m = 1100;
            uint l = m << 4;
            Console.WriteLine($"{m} << 4");
            Console.WriteLine($"After: {Convert.ToString(l, toBase: 2)}");

            uint k = 1001;
            uint j = k << 4;
            Console.WriteLine($"{k} << 4");
            Console.WriteLine($"After: {Convert.ToString(j, toBase: 2)}");




        }
    }
}
