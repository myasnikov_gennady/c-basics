﻿using System;

namespace BitwiseAndShiftOperators
{
    class Program
    {
        static void Main(string[] args)
        {
            int one = 0b0001;
            int two = 0b0010;
            int four = 0b0100;
            int eight = 0b1000;
            int nine = 0b1001;
            int twelve = 0b1100;

            //1. Bitwise complement operatoration ~

            Console.WriteLine($"1. Bitwise complement operatoration ~:\n\n" +
                $"{Convert.ToString(one, toBase: 2).PadLeft(32, '0')}({one})\n" +
                $"{Convert.ToString(~one, toBase: 2).PadLeft(32, '0')}({~one})\n\n" +
                $"{Convert.ToString(two, toBase: 2).PadLeft(32, '0')}({two})\n" +
                $"{Convert.ToString(~two, toBase: 2).PadLeft(32, '0')}({~two})\n\n" +
                $"{Convert.ToString(four, toBase: 2).PadLeft(32, '0')}({four})\n" +
                $"{Convert.ToString(~four, toBase: 2).PadLeft(32, '0')}({~four})\n\n" +
                $"{Convert.ToString(eight, toBase: 2).PadLeft(32, '0')}({eight})\n" +
                $"{Convert.ToString(~eight, toBase: 2).PadLeft(32, '0')}({~eight})\n\n" +
                $"{Convert.ToString(nine, toBase: 2).PadLeft(32, '0')}({nine})\n" +
                $"{Convert.ToString(~nine, toBase: 2).PadLeft(32, '0')}({~nine})\n\n" +
                $"{Convert.ToString(twelve, toBase: 2).PadLeft(32, '0')}({twelve})\n" +
                $"{Convert.ToString(~twelve, toBase: 2).PadLeft(32, '0')}({~twelve})\n");

            //2. Bitwise shift operation

            //   Left - shift operator <<

            Console.WriteLine($"2. Bitwise shift operation:\n\n" +
                $"Left - shift operator <<:\n\n" +
                $"{Convert.ToString(one, toBase: 2).PadLeft(32, '0')}({one})\n" +
                $"{Convert.ToString(one<<1, toBase: 2).PadLeft(32, '0')}({one<<1}) - shifts a one bit to the left\n\n" +
                $"{Convert.ToString(two, toBase: 2).PadLeft(32, '0')}({two})\n" +
                $"{Convert.ToString(two<<2, toBase: 2).PadLeft(32, '0')}({two<<2}) - shifts a two bits to the left\n\n" +
                $"{Convert.ToString(four, toBase: 2).PadLeft(32, '0')}({four})\n" +
                $"{Convert.ToString(four<<4, toBase: 2).PadLeft(32, '0')}({four<<4}) - shifts a four bits to the left\n\n" +
                $"{Convert.ToString(eight, toBase: 2).PadLeft(32, '0')}({eight})\n" +
                $"{Convert.ToString(eight<<8, toBase: 2).PadLeft(32, '0')}({eight <<8}) - shifts a eight bits to the left\n\n" +
                $"{Convert.ToString(nine, toBase: 2).PadLeft(32, '0')}({nine})\n" +
                $"{Convert.ToString(nine<<9, toBase: 2).PadLeft(32, '0')}({nine<<9}) - shifts a nine bits to the left\n\n" +
                $"{Convert.ToString(twelve, toBase: 2).PadLeft(32, '0')}({twelve})\n" +
                $"{Convert.ToString(twelve<<12, toBase: 2).PadLeft(32, '0')}({twelve<<12})- shifts a twelve bits to the left\n");

            //   Right-shift operator >> 

            Console.WriteLine($"Right - shift operator >>:\n\n" +
               $"{Convert.ToString(one, toBase: 2).PadLeft(32, '0')}({one})\n" +
               $"{Convert.ToString(one >> 1, toBase: 2).PadLeft(32, '0')}({one >> 1}) - shifts a one bit to the right\n\n" +
               $"{Convert.ToString(two, toBase: 2).PadLeft(32, '0')}({two})\n" +
               $"{Convert.ToString(two >> 2, toBase: 2).PadLeft(32, '0')}({two >> 2}) - shifts a two bits to the right\n\n" +
               $"{Convert.ToString(four, toBase: 2).PadLeft(32, '0')}({four})\n" +
               $"{Convert.ToString(four >> 4, toBase: 2).PadLeft(32, '0')}({four >> 4}) - shifts a four bits to the right\n\n" +
               $"{Convert.ToString(eight, toBase: 2).PadLeft(32, '0')}({eight})\n" +
               $"{Convert.ToString(eight >> 8, toBase: 2).PadLeft(32, '0')}({eight >> 8}) - shifts a eight bits to the right\n\n" +
               $"{Convert.ToString(nine, toBase: 2).PadLeft(32, '0')}({nine})\n" +
               $"{Convert.ToString(nine >> 9, toBase: 2).PadLeft(32, '0')}({nine >> 9}) - shifts a nine bits to the right\n\n" +
               $"{Convert.ToString(twelve, toBase: 2).PadLeft(32, '0')}({twelve})\n" +
               $"{Convert.ToString(twelve >> 12, toBase: 2).PadLeft(32, '0')}({twelve >> 12})- shifts a twelve bits to the right\n");

            //3. Bitwise logical AND operation &

            Console.WriteLine($"3. Bitwise logical AND operation &:\n\n" +
                $"{Convert.ToString(one, toBase: 2).PadLeft(4, '0')}({one})\n" +
                $"{Convert.ToString(two, toBase: 2).PadLeft(4, '0')}({two})\n" +
                $"{Convert.ToString(one & two, toBase: 2).PadLeft(4, '0')}({one & two})\n\n" +
                $"{Convert.ToString(four, toBase: 2).PadLeft(4, '0')}({four})\n" +
                $"{Convert.ToString(eight, toBase: 2).PadLeft(4, '0')}({eight})\n" +
                $"{Convert.ToString(four & eight, toBase: 2).PadLeft(4, '0')}({four & eight})\n\n" +
                $"{Convert.ToString(nine, toBase: 2).PadLeft(4, '0')}({nine})\n" +
                $"{Convert.ToString(twelve, toBase: 2).PadLeft(4, '0')}({twelve})\n" +
                $"{Convert.ToString(nine & twelve, toBase: 2).PadLeft(4, '0')}({nine & twelve})\n");

            //4. Bitwise logical exclusive OR operator ^

            Console.WriteLine($"4. Bitwise logical exclusive OR operator ^:\n\n" +
                $"{Convert.ToString(one, toBase: 2).PadLeft(4, '0')}({one})\n" +
                $"{Convert.ToString(two, toBase: 2).PadLeft(4, '0')}({two})\n" +
                $"{Convert.ToString(one ^ two, toBase: 2).PadLeft(4, '0')}({one ^ two})\n\n" +
                $"{Convert.ToString(four, toBase: 2).PadLeft(4, '0')}({four})\n" +
                $"{Convert.ToString(eight, toBase: 2).PadLeft(4, '0')}({eight})\n" +
                $"{Convert.ToString(four ^ eight, toBase: 2).PadLeft(4, '0')}({four ^ eight})\n\n" +
                $"{Convert.ToString(nine, toBase: 2).PadLeft(4, '0')}({nine})\n" +
                $"{Convert.ToString(twelve, toBase: 2).PadLeft(4, '0')}({twelve})\n" +
                $"{Convert.ToString(nine ^ twelve, toBase: 2).PadLeft(4, '0')}({nine ^ twelve})\n");

            //5. Bitwise logical OR operator |

            Console.WriteLine($"5. Bitwise logical OR operator |:\n\n" +
                $"{Convert.ToString(one, toBase: 2).PadLeft(4, '0')}({one})\n" +
                $"{Convert.ToString(two, toBase: 2).PadLeft(4, '0')}({two})\n" +
                $"{Convert.ToString(one | two, toBase: 2).PadLeft(4, '0')}({one | two})\n\n" +
                $"{Convert.ToString(four, toBase: 2).PadLeft(4, '0')}({four})\n" +
                $"{Convert.ToString(eight, toBase: 2).PadLeft(4, '0')}({eight})\n" +
                $"{Convert.ToString(four | eight, toBase: 2).PadLeft(4, '0')}({four | eight})\n\n" +
                $"{Convert.ToString(nine, toBase: 2).PadLeft(4, '0')}({nine})\n" +
                $"{Convert.ToString(twelve, toBase: 2).PadLeft(4, '0')}({twelve})\n" +
                $"{Convert.ToString(nine | twelve, toBase: 2).PadLeft(4, '0')}({nine | twelve})\n");
        }
    }
}
