﻿using System;

namespace ДЗ_3_Филатов_ВС
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Побитовые операторы и операторы сдвига");
            Console.WriteLine();

            // 1. Произвести операцию побитового дополнения для значений: 0001, 0010, 0100, 1000, 1001, 1100

            Console.WriteLine("Оператор побитового дополнения ~");
            Console.WriteLine();

            int a = 0b_0001;
            int b = ~a;
            Console.WriteLine($"~{Convert.ToString(a, toBase: 2)}={Convert.ToString(b, toBase: 2)}");
            Console.WriteLine();


            a = 0b_0010;
            b = ~a;
            Console.WriteLine($"~{Convert.ToString(a, toBase: 2)}={Convert.ToString(b, toBase: 2)}");
            Console.WriteLine();

            a = 0b_0100;
            b = ~a;
            Console.WriteLine($"~{Convert.ToString(a, toBase: 2)}={Convert.ToString(b, toBase: 2)}");
            Console.WriteLine();

            a = 0b_1000;
            b = ~a;
            Console.WriteLine($"~{Convert.ToString(a, toBase: 2)}={Convert.ToString(b, toBase: 2)}");
            Console.WriteLine();

            a = 0b_1001;
            b = ~a;
            Console.WriteLine($"~{Convert.ToString(a, toBase: 2)}={Convert.ToString(b, toBase: 2)}");
            Console.WriteLine();

            a = 0b_1100;
            b = ~a;
            Console.WriteLine($"~{Convert.ToString(a, toBase: 2)}={Convert.ToString(b, toBase: 2)}");
            Console.WriteLine();

            // 2.Произвести операцию побитового сдвига(<<, >>) для значений: 0001, 0010, 0100, 1000, 1001, 1100

            Console.WriteLine("Оператор побитового сдвига влево <<");
            Console.WriteLine();

            a = 0b_0001;
            b = a << 4;
            Console.WriteLine(Convert.ToString(a, toBase: 2).PadLeft(32, '0'));
            Console.WriteLine(Convert.ToString(b, toBase: 2).PadLeft(32, '0'));
            Console.WriteLine();

            a = 0b_0010;
            b = a << 4;
            Console.WriteLine(Convert.ToString(a, toBase: 2).PadLeft(32, '0'));
            Console.WriteLine(Convert.ToString(b, toBase: 2).PadLeft(32, '0'));
            Console.WriteLine();

            a = 0b_0100;
            b = a << 4;
            Console.WriteLine(Convert.ToString(a, toBase: 2).PadLeft(32, '0'));
            Console.WriteLine(Convert.ToString(b, toBase: 2).PadLeft(32, '0'));
            Console.WriteLine();

            a = 0b_1000;
            b = a << 4;
            Console.WriteLine(Convert.ToString(a, toBase: 2).PadLeft(32, '0'));
            Console.WriteLine(Convert.ToString(b, toBase: 2).PadLeft(32, '0'));
            Console.WriteLine();

            a = 0b_1001;
            b = a << 4;
            Console.WriteLine(Convert.ToString(a, toBase: 2).PadLeft(32, '0'));
            Console.WriteLine(Convert.ToString(b, toBase: 2).PadLeft(32, '0'));
            Console.WriteLine();

            a = 0b_1100;
            b = a << 4;
            Console.WriteLine(Convert.ToString(a, toBase: 2).PadLeft(32, '0'));
            Console.WriteLine(Convert.ToString(b, toBase: 2).PadLeft(32, '0'));
            Console.WriteLine();

            Console.WriteLine("Оператор побитового сдвига вправо >>");
            Console.WriteLine();

            a = 0001;
            b = a >> 1;
            Console.WriteLine(Convert.ToString(a, toBase: 2).PadLeft(32, '0'));
            Console.WriteLine(Convert.ToString(b, toBase: 2).PadLeft(32, '0'));
            Console.WriteLine();

            a = 0b_0010;
            b = a >> 1;
            Console.WriteLine(Convert.ToString(a, toBase: 2).PadLeft(32, '0'));
            Console.WriteLine(Convert.ToString(b, toBase: 2).PadLeft(32, '0'));
            Console.WriteLine();

            a = 0b_0100;
            b = a >> 1;
            Console.WriteLine(Convert.ToString(a, toBase: 2).PadLeft(32, '0'));
            Console.WriteLine(Convert.ToString(b, toBase: 2).PadLeft(32, '0'));
            Console.WriteLine();

            a = 0b_1000;
            b = a >> 1;
            Console.WriteLine(Convert.ToString(a, toBase: 2).PadLeft(32, '0'));
            Console.WriteLine(Convert.ToString(b, toBase: 2).PadLeft(32, '0'));
            Console.WriteLine();

            a = 0b_1001;
            b = a >> 1;
            Console.WriteLine(Convert.ToString(a, toBase: 2).PadLeft(32, '0'));
            Console.WriteLine(Convert.ToString(b, toBase: 2).PadLeft(32, '0'));
            Console.WriteLine();

            a = 0b_1100;
            b = a >> 1;
            Console.WriteLine(Convert.ToString(a, toBase: 2).PadLeft(32, '0'));
            Console.WriteLine(Convert.ToString(b, toBase: 2).PadLeft(32, '0'));
            Console.WriteLine();

            // 3. Произвести операцию логического И (&): 0001 и 0010, 0100 и 1000, 1001 и 1100

            Console.WriteLine("Оператор И (&)");
            Console.WriteLine();

            int c;

            a = 0b_0001;
            b = 0b_0010;
            c = a & b;
            Console.WriteLine($"{Convert.ToString(a, toBase: 2).PadLeft(4, '0')}&{Convert.ToString(b, toBase: 2).PadLeft(4, '0')}={Convert.ToString(c, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine();

            a = 0b_0100;
            b = 0b_1000;
            c = a & b;
            Console.WriteLine($"{Convert.ToString(a, toBase: 2).PadLeft(4, '0')}&{Convert.ToString(b, toBase: 2).PadLeft(4, '0')}={Convert.ToString(c, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine();

            a = 0b_1001;
            b = 0b_1100;
            c = a & b;
            Console.WriteLine($"{Convert.ToString(a, toBase: 2).PadLeft(4, '0')}&{Convert.ToString(b, toBase: 2).PadLeft(4, '0')}={Convert.ToString(c, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine();

            // 4. Произвести операцию логического исключения ИЛИ (^): 0001 и 0010, 0100 и 1000, 1001 и 1100

            Console.WriteLine("Оператор ИЛИ (^)");
            Console.WriteLine();

            a = 0b_0001;
            b = 0b_0010;
            c = a ^ b;
            Console.WriteLine($"{Convert.ToString(a, toBase: 2).PadLeft(4, '0')}&{Convert.ToString(b, toBase: 2).PadLeft(4, '0')}={Convert.ToString(c, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine();

            a = 0b_0100;
            b = 0b_1000;
            c = a ^ b;
            Console.WriteLine($"{Convert.ToString(a, toBase: 2).PadLeft(4, '0')}&{Convert.ToString(b, toBase: 2).PadLeft(4, '0')}={Convert.ToString(c, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine();

            a = 0b_1001;
            b = 0b_1100;
            c = a ^ b;
            Console.WriteLine($"{Convert.ToString(a, toBase: 2).PadLeft(4, '0')}&{Convert.ToString(b, toBase: 2).PadLeft(4, '0')}={Convert.ToString(c, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine();

            // 5. Произвести операцию логического ИЛИ (|): 0001 и 0010, 0100 и 1000, 1001 и 1100

            Console.WriteLine("Оператор ИЛИ (|)");
            Console.WriteLine();

            a = 0b_0001;
            b = 0b_0010;
            c = a | b;
            Console.WriteLine($"{Convert.ToString(a, toBase: 2).PadLeft(4, '0')}&{Convert.ToString(b, toBase: 2).PadLeft(4, '0')}={Convert.ToString(c, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine();

            a = 0b_0100;
            b = 0b_1000;
            c = a | b;
            Console.WriteLine($"{Convert.ToString(a, toBase: 2).PadLeft(4, '0')}&{Convert.ToString(b, toBase: 2).PadLeft(4, '0')}={Convert.ToString(c, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine();

            a = 0b_1001;
            b = 0b_1100;
            c = a | b;
            Console.WriteLine($"{Convert.ToString(a, toBase: 2).PadLeft(4, '0')}&{Convert.ToString(b, toBase: 2).PadLeft(4, '0')}={Convert.ToString(c, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine();
        }
    }
}
