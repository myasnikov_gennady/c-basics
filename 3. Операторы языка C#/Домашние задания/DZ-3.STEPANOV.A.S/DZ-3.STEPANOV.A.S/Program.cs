﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ_3.STEPANOV.A.S
{
    class Program
    {
        static void Main(string[] args)
        {
            // 1. Произвести операцию побитового дополнения для значений: {0001, 0010, 0100, 1000, 1001, 1100};
            int[] array = { 0b_0001, 0b_0010, 0b_0100, 0b_1000, 0b_1001, 0b_1100 };
            Console.WriteLine("Операции побитового дополнения");
            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine($" Преобразуемое значение {Convert.ToString(array[i], toBase: 2).PadLeft(4, '0')} -> {Convert.ToString(~array[i], toBase: 2)}");
                // Вывод инвертирования в консоль, для каждого из значений
            }
            Console.WriteLine();
            // 2. Произвести операцию побитового сдвига (<<, >>) для значений: 0001, 0010, 0100, 1000, 1001, 1100;
            Console.WriteLine(" Операции побитового сдвига (<<, >>)");
            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine($" Преобразуемое значение {Convert.ToString(array[i], toBase: 2).PadLeft(4, '0')} - побитовый сдвиг << на 1  = {Convert.ToString(array[i] << 1, toBase: 2).PadLeft(32, '0')}");
                Console.WriteLine($" Преобразуемое значение {Convert.ToString(array[i], toBase: 2).PadLeft(4, '0')} - побитовый сдвиг >> на 1  = {Convert.ToString(array[i] >> 1, toBase: 2).PadLeft(32, '0')}");
            }    // Вывод сдвигов в консоль, для каждого из значений
            Console.WriteLine();
            // 3. Произвести операцию логического И (&): 0001 и 0010, 0100 и 1000, 1001 и 1100;
            Console.WriteLine(" Операции  логического И (&)для 0001 и 0010, 0100 и 1000, 1001 и 1100;");
            for (int i = 0; i < array.Length; i += 2)
            {
                int j = array[i] & array[i + 1];
                Console.WriteLine($" Преобразуемое значения {Convert.ToString(array[i], toBase: 2).PadLeft(4, '0')} & {Convert.ToString(array[i + 1], toBase: 2).PadLeft(4, '0')}   = {Convert.ToString(j, toBase: 2).PadLeft(4, '0')}");
            }    // Вывод операцию логического И (&) для 0001 и 0010, 0100 и 1000, 1001 и 1100;
            Console.WriteLine();
            // 4. Произвести операцию логического исключения ИЛИ (^): 0001 и 0010, 0100 и 1000, 1001 и 1100;
            Console.WriteLine(" Операции  логического исключения ИЛИ (^): 0001 и 0010, 0100 и 1000, 1001 и 1100;");
            for (int i = 0; i < array.Length; i += 2)
            {
                int j = array[i] ^ array[i + 1];
                Console.WriteLine($" Преобразуемое значения {Convert.ToString(array[i], toBase: 2).PadLeft(4, '0')} ^ {Convert.ToString(array[i + 1], toBase: 2).PadLeft(4, '0')}   = {Convert.ToString(j, toBase: 2).PadLeft(4, '0')}");
            }    // Вывод в консоль операции логического исключения ИЛИ (^): 0001 и 0010, 0100 и 1000, 1001 и 1100;
            Console.WriteLine();
            // 5.Произвести операцию логического ИЛИ(|): 0001 и 0010, 0100 и 1000, 1001 и 1100;
            Console.WriteLine(" Операции  логического ИЛИ(|): 0001 и 0010, 0100 и 1000, 1001 и 1100;");
            for (int i = 0; i < array.Length; i += 2)
            {
                int j = array[i] | array[i + 1];
                Console.WriteLine($" Преобразуемое значения {Convert.ToString(array[i], toBase: 2).PadLeft(4, '0')} | {Convert.ToString(array[i + 1], toBase: 2).PadLeft(4, '0')}   = {Convert.ToString(j, toBase: 2).PadLeft(4, '0')}");
            }    // Вывод в консоль операции логического  ИЛИ(|): 0001 и 0010, 0100 и 1000, 1001 и 1100;
        }
    }
}
