﻿using HW3_Komar_Aleh.BitOperators;

namespace HW3_Komar_Aleh
{
    class Program
    {
        private static void Main()
        {
            new BitOp().Run();
        }
    }
}