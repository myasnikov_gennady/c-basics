﻿using System;

namespace HW3_Komar_Aleh.BitOperators

{
    public class BitOp : IExample
    {
        public void Run()
        {
            int[] numbers = { 0b0001, 0b0010, 0b0100, 0b1000, 0b1001, 0b1100, -10, 11};
            ConsoleHelp.TextHeader("Оператор побитового дополнения ~");
            ConsoleHelp.Info("Оператор ~ создает побитовое дополнение своего операнда путем инвертирования каждого бита.");
            foreach (var number in numbers)
            {
                BitDop(number);
            }
            ConsoleHelp.ConsoleClear();
            ConsoleHelp.TextHeader("Оператор сдвига влево <<");
            ConsoleHelp.Info("Оператор << сдвигает левый операнд влево на количество битов, определенное правым операндом.");
            foreach (var number in numbers)
            {
                LeftShift(number, 2);
            }
            ConsoleHelp.ConsoleClear();
            ConsoleHelp.TextHeader("Оператор сдвига вправо >>");
            ConsoleHelp.Info("Оператор >> сдвигает левый операнд вправо на количество битов, определенное правым операндом.");
            foreach (var number in numbers)
            {
                RightShift(number, 2);
            }
            ConsoleHelp.ConsoleClear();
            ConsoleHelp.TextHeader("Оператор логического И &");
            ConsoleHelp.Info("Оператор & вычисляет побитовое логическое И своих целочисленных операндов.");
            for (var i = 0; i < numbers.Length; i += 2)
            {
                LogOpAND(numbers[i], numbers[i + 1]);
            }
            ConsoleHelp.ConsoleClear();
            ConsoleHelp.TextHeader("Оператор логического исключения ИЛИ |");
            ConsoleHelp.Info("Оператор | вычисляет побитовое логическое ИЛИ своих целочисленных операндов.");
            for (var i = 0; i < numbers.Length; i += 2)
            {
                OpLogExOR(numbers[i], numbers[i + 1]);
            }
            ConsoleHelp.ConsoleClear();
            ConsoleHelp.TextHeader("Оператор логического исключения ИЛИ ^");
            ConsoleHelp.Info("Оператор ^ вычисляет побитовое логическое исключающее ИЛИ своих целочисленных операндов");
            for (var i = 0; i < numbers.Length; i += 2)
            {
                OpLogExXOR(numbers[i], numbers[i + 1]);
            }
        }

        private static void BinaryConsole(int val, int val2)
        {
            Console.WriteLine($" {Convert.ToString(val, toBase: 2).PadLeft(val2, '0')} - двоичный вид, {val2} бита");
        }
        private static void ResultConsole(int val, int val2)
        {
            Console.WriteLine($" {Convert.ToString(val, toBase: 2).PadLeft(val2, '0')} - результат в двоичном виде, {val2} бита");
        }

        private static void BitDop(int value)
        {
            var value2 = ~value;
            Console.WriteLine("Before:");
            Console.WriteLine($" {value} - десятичный вид");
            if (value >= 0)
            {
                BinaryConsole(value, 4);
            }
            BinaryConsole(value, 32);
            Console.WriteLine("After ~:");
            Console.WriteLine($" {value2} - результат в десятичном виде");
            ResultConsole(value2, 32);
            Console.WriteLine();
        }

        private static void LeftShift(int value, int value2)
        {
            var value3 = value << value2;
            Console.WriteLine("Before:");
            Console.WriteLine($" {value} - десятичный вид");
            if (value >= 0)
            {
                BinaryConsole(value, 4);
            }
            BinaryConsole(value, 32);
            Console.WriteLine($"Сдвигаем влево на {value2} разряда");
            Console.WriteLine("After <<:");
            Console.WriteLine($" {value3} - результат в десятичном виде");
            ResultConsole(value3, 32);
            Console.WriteLine();
        }

        private static void RightShift(int value, int value2)
        {
            var value3 = value >> value2;
            Console.WriteLine("Before:");
            Console.WriteLine($" {value} - десятичный вид");
            if (value >= 0)
            {
                BinaryConsole(value, 4);
            }
            BinaryConsole(value, 32);
            Console.WriteLine($"Сдвигаем вправо на {value2} разряда");
            Console.WriteLine("After >>:");
            Console.WriteLine($" {value3} - результат в десятичном виде");
            ResultConsole(value3, 32);
            Console.WriteLine();
        }

        private void LogOpAND(int value, int value2)
        {
            var c = value & value2;
            int d = (value > 0 & value2 > 0) ? 4 : 32;
            Console.WriteLine($" {value} & {value2}");
            Console.WriteLine($" {Convert.ToString(value, toBase: 2).PadLeft(d, '0')} - первое значение");
            Console.WriteLine($" {Convert.ToString(value2, toBase: 2).PadLeft(d, '0')} - второе значение");
            if (c < 0)
            {
                ResultConsole(c, 32);
            }
            else
            {
                ResultConsole(c, 4);
            }
            Console.WriteLine($" {c} - результат в десятичном виде");
            Console.WriteLine();

        }

        private void OpLogExOR(int value, int value2)
        {
            var c = value | value2;
            int d = (value > 0 & value2 > 0) ? 4 : 32;
            Console.WriteLine($" {value} | {value2}");
            Console.WriteLine($" {Convert.ToString(value, toBase: 2).PadLeft(d, '0')} - первое значение");
            Console.WriteLine($" {Convert.ToString(value2, toBase: 2).PadLeft(d, '0')} - второе значение");
            if (c < 0)
            {
                ResultConsole(c, 32);
            }
            else
            {
                ResultConsole(c, 4);
            }
            Console.WriteLine($" {c} - результат в десятичном виде");
            Console.WriteLine();
        }

        private void OpLogExXOR(int value, int value2)
        {
            var c = value ^ value2;
            int d = (value > 0 & value2 > 0) ? 4 : 32;
            Console.WriteLine($" {value} ^ {value2}");
            Console.WriteLine($" {Convert.ToString(value, toBase: 2).PadLeft(d, '0')} - первое значение");
            Console.WriteLine($" {Convert.ToString(value2, toBase: 2).PadLeft(d, '0')} - второе значение");
            if (c < 0)
            {
                ResultConsole(c, 32);
            }
            else
            {
                ResultConsole(c, 4);
            }
            Console.WriteLine($" {c} - результат в десятичном виде");
            Console.WriteLine();
        }                
    }
    public static class ConsoleHelp
    {
        public static void TextHeader(string text)
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine(text);
            Console.ResetColor();
        }

        public static void ConsoleClear()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Нажмите любую клавишу для просмотра следующего задания");
            Console.ResetColor();
            Console.ReadKey();
            Console.Clear();
        }

        public static void Info(string text)
        {
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.WriteLine(text);
            Console.ResetColor();
        }
    }
}