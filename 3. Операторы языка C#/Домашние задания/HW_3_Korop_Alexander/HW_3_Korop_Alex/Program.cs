﻿using System;

namespace HW_3_Korop_Alex
{
    class Program
    {
        static void Main(string[] args)
        {
            int value1 = 0b_0001;
            int value2 = 0b_0010;
            int value3 = 0b_0100;
            int value4 = 0b_1000;
            int value5 = 0b_1001;
            int value6 = 0b_1100;
            int value12 = 0b_0000;
            int value34 = 0b_0000;
            int value56 = 0b_0000;

            Console.WriteLine("Дано:");

            Console.WriteLine($"\nvalue1 = {Convert.ToString(value1, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine($"value2 = {Convert.ToString(value2, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine($"value3 = {Convert.ToString(value3, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine($"value4 = {Convert.ToString(value4, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine($"value5 = {Convert.ToString(value5, toBase: 2).PadLeft(4, '0')}");
            Console.WriteLine($"value6 = {Convert.ToString(value6, toBase: 2).PadLeft(4, '0')}");



            Console.WriteLine("\n\n\nОперации побитового дополнения(~):");

            Console.WriteLine($"\n~{Convert.ToString(value1, toBase: 2).PadLeft(4, '0')} = {Convert.ToString(~value1, toBase: 2)}");
            Console.WriteLine($"~{Convert.ToString(value2, toBase: 2).PadLeft(4, '0')} = {Convert.ToString(~value2, toBase: 2)}");
            Console.WriteLine($"~{Convert.ToString(value3, toBase: 2).PadLeft(4, '0')} = {Convert.ToString(~value3, toBase: 2)}");
            Console.WriteLine($"~{Convert.ToString(value4, toBase: 2).PadLeft(4, '0')} = {Convert.ToString(~value4, toBase: 2)}");
            Console.WriteLine($"~{Convert.ToString(value5, toBase: 2).PadLeft(4, '0')} = {Convert.ToString(~value5, toBase: 2)}");
            Console.WriteLine($"~{Convert.ToString(value6, toBase: 2).PadLeft(4, '0')} = {Convert.ToString(~value6, toBase: 2)}");



            Console.WriteLine("\n\n\nОперации побитового сдвига(<<, >>):");

            Console.WriteLine("\n<<:");

            Console.WriteLine($"\n{Convert.ToString(value1, toBase: 2).PadLeft(4, '0')} << 1 = {Convert.ToString(value1 << 1, toBase: 2).PadLeft(32,'0')}");
            Console.WriteLine($"{Convert.ToString(value2, toBase: 2).PadLeft(4, '0')} << 2 = {Convert.ToString(value2 << 2, toBase: 2).PadLeft(32, '0')}");
            Console.WriteLine($"{Convert.ToString(value3, toBase: 2).PadLeft(4, '0')} << 3 = {Convert.ToString(value3 << 3, toBase: 2).PadLeft(32, '0')}");
            Console.WriteLine($"{Convert.ToString(value4, toBase: 2).PadLeft(4, '0')} << 4 = {Convert.ToString(value4 << 4, toBase: 2).PadLeft(32, '0')}");
            Console.WriteLine($"{Convert.ToString(value5, toBase: 2).PadLeft(4, '0')} << 5 = {Convert.ToString(value5 << 5, toBase: 2).PadLeft(32, '0')}");
            Console.WriteLine($"{Convert.ToString(value6, toBase: 2).PadLeft(4, '0')} << 6 = {Convert.ToString(value6 << 6, toBase: 2).PadLeft(32, '0')}");

            Console.WriteLine("\n>>:");

            Console.WriteLine($"\n{Convert.ToString(value1, toBase: 2).PadLeft(4, '0')} >> 1 = {Convert.ToString(value1 >> 1, toBase: 2).PadLeft(32, '0')}");
            Console.WriteLine($"{Convert.ToString(value2, toBase: 2).PadLeft(4, '0')} >> 1 = {Convert.ToString(value2 >> 1, toBase: 2).PadLeft(32, '0')}");
            Console.WriteLine($"{Convert.ToString(value3, toBase: 2).PadLeft(4, '0')} >> 2 = {Convert.ToString(value3 >> 2, toBase: 2).PadLeft(32, '0')}");
            Console.WriteLine($"{Convert.ToString(value4, toBase: 2).PadLeft(4, '0')} >> 2 = {Convert.ToString(value4 >> 2, toBase: 2).PadLeft(32, '0')}");
            Console.WriteLine($"{Convert.ToString(value5, toBase: 2).PadLeft(4, '0')} >> 2 = {Convert.ToString(value5 >> 2, toBase: 2).PadLeft(32, '0')}");
            Console.WriteLine($"{Convert.ToString(value6, toBase: 2).PadLeft(4, '0')} >> 2 = {Convert.ToString(value6 >> 2, toBase: 2).PadLeft(32, '0')}");



            Console.WriteLine("\n\n\nОперации логического И(&):");

            value12 = value1 & value2;
            Console.WriteLine($"\n {Convert.ToString(value1, toBase: 2).PadLeft(4, '0')} & {Convert.ToString(value2, toBase: 2).PadLeft(4, '0')}: ");       // 0001 & 0010
            Console.WriteLine($" {Convert.ToString(value1, toBase: 2).PadLeft(32, '0')} " +
                              $"\n              &" +
                              $"\n {Convert.ToString(value2, toBase: 2).PadLeft(32, '0')}" +
                              $"\n              =" +
                              $"\n {Convert.ToString(value12, toBase: 2).PadLeft(32, '0')}");

            value34 = value3 & value4;
            Console.WriteLine($"\n {Convert.ToString(value3, toBase: 2).PadLeft(4, '0')} & {Convert.ToString(value4, toBase: 2).PadLeft(4, '0')}: ");     // 0100 & 1000
            Console.WriteLine($" {Convert.ToString(value3, toBase: 2).PadLeft(32, '0')} " +
                              $"\n              &" +
                              $"\n {Convert.ToString(value4, toBase: 2).PadLeft(32, '0')}" +
                              $"\n              =" +
                              $"\n {Convert.ToString(value34, toBase: 2).PadLeft(32, '0')}");

            value56 = value5 & value6;
            Console.WriteLine($"\n {Convert.ToString(value5, toBase: 2).PadLeft(4, '0')} & {Convert.ToString(value6, toBase: 2).PadLeft(4, '0')}: ");       // 1001 & 1100
            Console.WriteLine($" {Convert.ToString(value5, toBase: 2).PadLeft(32, '0')} " +
                              $"\n              &" +
                              $"\n {Convert.ToString(value6, toBase: 2).PadLeft(32, '0')}" +
                              $"\n              =" +
                              $"\n {Convert.ToString(value56, toBase: 2).PadLeft(32, '0')}");



                    Console.WriteLine("\n\n\nОперации логического исключения ИЛИ(^):");

            value12 = value1 ^ value2;
            Console.WriteLine($"\n {Convert.ToString(value1, toBase: 2).PadLeft(4, '0')} ^ {Convert.ToString(value2, toBase: 2).PadLeft(4, '0')}: ");       // 0001 ^ 0010
            Console.WriteLine($" {Convert.ToString(value1, toBase: 2).PadLeft(32, '0')} " +
                              $"\n              ^" +
                              $"\n {Convert.ToString(value2, toBase: 2).PadLeft(32, '0')}" +
                              $"\n              =" +
                              $"\n {Convert.ToString(value12, toBase: 2).PadLeft(32, '0')}");

            value34 = value3 ^ value4;
            Console.WriteLine($"\n {Convert.ToString(value3, toBase: 2).PadLeft(4, '0')} ^ {Convert.ToString(value4, toBase: 2).PadLeft(4, '0')}: ");       // 0100 ^ 1000
            Console.WriteLine($" {Convert.ToString(value3, toBase: 2).PadLeft(32, '0')} " +
                              $"\n              ^" +
                              $"\n {Convert.ToString(value4, toBase: 2).PadLeft(32, '0')}" +
                              $"\n              =" +
                              $"\n {Convert.ToString(value34, toBase: 2).PadLeft(32, '0')}");

            value56 = value5 ^ value6;
            Console.WriteLine($"\n {Convert.ToString(value5, toBase: 2).PadLeft(4, '0')} ^ {Convert.ToString(value6, toBase: 2).PadLeft(4, '0')}: ");       // 1001 ^ 1100
            Console.WriteLine($" {Convert.ToString(value5, toBase: 2).PadLeft(32, '0')} " +
                              $"\n              ^" +
                              $"\n {Convert.ToString(value6, toBase: 2).PadLeft(32, '0')}" +
                              $"\n              =" +
                              $"\n {Convert.ToString(value56, toBase: 2).PadLeft(32, '0')}");



            Console.WriteLine("\n\n\nОперации логического ИЛИ(|):");

            value12 = value1 | value2;
            Console.WriteLine($"\n {Convert.ToString(value1, toBase: 2).PadLeft(4, '0')} | {Convert.ToString(value2, toBase: 2).PadLeft(4, '0')}: ");       // 0001 | 0010
            Console.WriteLine($" {Convert.ToString(value1, toBase: 2).PadLeft(32, '0')} " +
                              $"\n              |" +
                              $"\n {Convert.ToString(value2, toBase: 2).PadLeft(32, '0')}" +
                              $"\n              =" +
                              $"\n {Convert.ToString(value12, toBase: 2).PadLeft(32, '0')}");

            value34 = value3 | value4;
            Console.WriteLine($"\n {Convert.ToString(value3, toBase: 2).PadLeft(4, '0')} | {Convert.ToString(value4, toBase: 2).PadLeft(4, '0')}: ");       // 0100 | 1000
            Console.WriteLine($" {Convert.ToString(value3, toBase: 2).PadLeft(32, '0')} " +
                              $"\n              |" +
                              $"\n {Convert.ToString(value4, toBase: 2).PadLeft(32, '0')}" +
                              $"\n              =" +
                              $"\n {Convert.ToString(value34, toBase: 2).PadLeft(32, '0')}");

            value56 = value5 | value6;
            Console.WriteLine($"\n {Convert.ToString(value5, toBase: 2).PadLeft(4, '0')} | {Convert.ToString(value6, toBase: 2).PadLeft(4, '0')}: ");       // 1001 | 1100
            Console.WriteLine($" {Convert.ToString(value5, toBase: 2).PadLeft(32, '0')} " +
                              $"\n              |" +
                              $"\n {Convert.ToString(value6, toBase: 2).PadLeft(32, '0')}" +
                              $"\n              =" +
                              $"\n {Convert.ToString(value56, toBase: 2).PadLeft(32, '0')}");

            Console.WriteLine("\nEnd of program");
            Console.ReadLine();
        }
    }
}
