﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace HomeWork17
{
    internal static class Program   // Не засчитана. Скопирована у Комара Олега
    {

        /*Много однотипных методов просто для того, чтоб в консоли красивее вывести...*/

        private static int _operand;
        private static readonly object Locker = new object();
        private static readonly Mutex Mutex = new Mutex();

        private static async Task Main()
        {
            Console.WriteLine("Несинхронизированные потоки");
            DoThread(Sum);
            Thread.Sleep(2000);

            Console.WriteLine();
            Console.WriteLine("Синхронизированные потоки");
            DoThread(SumSync);
            Thread.Sleep(4000);

            Console.WriteLine();
            Console.WriteLine("Несинхронизированные Задачи");
            DoTask();
            Thread.Sleep(2000);

            Console.WriteLine();
            Console.WriteLine("Синхронизированные Задачи");
            await DoTaskSync();
            Thread.Sleep(2000);

            Console.WriteLine();
            Console.WriteLine("Использование Parallel");
            Parallel.Invoke(() => SumTask(0), () => SumTask(1), () => SumTask(2), () => SumTask(3), () => SumTask(4));
        }

        private static void Sum(object op)
        {
            _operand = 1;
            var x = (int) op;
            for (var i = 1; i < 6; i++)
            {
                Console.WriteLine($"{Thread.CurrentThread.Name}: Сумма равна - {x + _operand}");
                _operand++;
                Thread.Sleep(100);
            }
        }

        private static void SumSync(object op)
        {
            lock (Locker)
            {
                var x = (int)op;
                _operand = 1;
                for (var i = 1; i < 6; i++)
                {
                    Console.WriteLine($"{Thread.CurrentThread.Name}: Сумма равна - {x + _operand}");
                    _operand++;
                    Thread.Sleep(100);
                }
            }
        }
        private static void SumTask(object op)
        {
            _operand = 1;
            var x = (int)op;
            for (var i = 0; i < 5; i++)
            {
                Console.WriteLine($"Task{Task.CurrentId}: Сумма равна - {x + _operand}");
                _operand++;
                Thread.Sleep(100);
            }
        }

        private static void SumTaskSync(object op)
        {
            Mutex.WaitOne();
            var y = (int) op;
            _operand = 1;
            for (var i = 0; i < 5; i++)
            {
                Console.WriteLine($"Задача {Task.CurrentId}: Сумма равна - {y + _operand}");
                _operand++;
                Thread.Sleep(200);
            }
            Mutex.ReleaseMutex();
        }


        private static void DoTask()
        {
            for (var i = 0; i < 5; i++)
            {
                var b = i;
                var task = new Task(() => SumTask(b));
                task.Start();
                
            }
        }
        
        private static async Task DoTaskSync()
        {
            for (var b = 0; b < 5; b++)
            {
                await Task.Run(() => SumTaskSync(b));
            }
        }

        private static void DoThread(ParameterizedThreadStart nameVoid)
        {
            for (var i = 0; i < 5; i++)
            {
                var thread = new Thread(nameVoid) { Name = "Поток " + i };
                thread.Start(i);
            }
        }
    }
}
