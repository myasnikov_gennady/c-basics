﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace HW_17_Korop_Alexander
{
    class Program
    {
        static object locker = new object();
        static Task[] tasks = new Task[4];
        static Thread[] threads = new Thread[4];
        static void Main(string[] args)
        {
            Threads();
            SyncThreads();
            Tasks();
            SyncTasks();
            Parallel_Invoke();
        }
        static void Method(int a)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        Console.WriteLine($"Thread {a}: {j}");
                    }
                }
        static void SyncMethod(int a)
        {
            lock (locker)
                for (int j = 0; j < 3; j++)
                {
                    Console.WriteLine($"Thread {a}: {j}");
                }           
        }        
        static void Threads()
        {
            Console.WriteLine("\nStart Threads");
            for (int i = 0; i < threads.Length; i++)
            {
                threads[i] = new Thread(() => Method(i));
                threads[i].Start();
            }
            Thread.Sleep(1000);
            Console.WriteLine("End Threads");
        }
        static void SyncThreads()
        {
            Console.WriteLine("\nStart SyncThreads");
            for (int i = 0; i < threads.Length; i++)
            {
                threads[i] = new Thread(() => SyncMethod(i));
                threads[i].Start();
            }
            Thread.Sleep(1000);
            Console.WriteLine("End SyncThreads");
        }
        static void Tasks()
        {
            Console.WriteLine("\nStart Tasks");
            for (int i = 0; i < tasks.Length; i++)
            {
                tasks[i] = new Task(() => Method(i));
                tasks[i].Start();                
            }
            Task.WaitAll(tasks);
            Console.WriteLine("End Tasks");
        }
        static void SyncTasks()
        {
            Console.WriteLine("\nStart SyncTasks");
            for (int i = 0; i < tasks.Length; i++)
            {
                tasks[i] = new Task(() => SyncMethod(i));
                tasks[i].Start();
            }
            Task.WaitAll(tasks);
            Console.WriteLine("End SyncTasks");
        }
        static void Parallel_Invoke()
        {
            Console.WriteLine("\nStart Parallel_Invoke");
            for (int i = 0; i < 4; i++)
            {
                Parallel.Invoke(() => Method(i));
            }
            Console.WriteLine("End Parallel_Invoke");
        }

    }
}
