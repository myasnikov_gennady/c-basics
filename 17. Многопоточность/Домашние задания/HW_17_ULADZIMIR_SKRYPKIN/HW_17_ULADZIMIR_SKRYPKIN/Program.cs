﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace HW_17_ULADZIMIR_SKRYPKIN
{
    class Program
    {
        static object Locker = new object();
        public static List<Thread> myThread = new List<Thread>();
        public static Task[] tasks = new Task[10];
        static void Main(string[] args)
        {
            Console.WriteLine("Использование класса Thread в задаче");
            // Чем объемнее задача, тем больше физических потоков используется->(стремящихся) к вызываемым
            // Если задача ждет синхронизации тем меньше физических потоков будет использовано по сравнению к вызываемым
            NotFunThread();// Thread Без синхронизации 
            FunThread(); // Thread C синхронизацией
            Console.WriteLine("Использование класса Task в задаче");
            NotFunTask(); // Task Без синхронизации
            FunTask(); //Task c  синхронизацией
            Console.WriteLine("Использование класса Parallel (метод Invoke) в задаче");
            Parallel.Invoke(() => Count(0), () => Count(1), () => Count(2), () => Count(3), () => Count(4));
        }
        static void NotFunTask()
        {
            for (int i = 0; i < tasks.Length; i++)
            {
                tasks[i] = new Task(() => Count(i));
                tasks[i].Start();
            }
            Task.WaitAll(tasks); // ожидаем завершения задач 
            Console.WriteLine($"Использовано было {tasks.Length} потоков, без синхронизации. Все вызваны в разнобой.");
        }
        static void FunTask()
        {
            for (int i = 0; i < tasks.Length; i++)
            {
                tasks[i] = new Task(() => CountSync(i));
                tasks[i].Start();
            }
            Task.WaitAll(tasks); // ожидаем завершения задач 
            Console.WriteLine($"Использовано было {tasks.Length} потоков, с синхронизацией. Вызваны по очереди.");
        }
        static void NotFunThread()
        {
            for (int i = 0; i < 5; i++)
            {
                myThread.Add(new Thread(() => Count(i)));
                myThread[i].Start();
                //Thread myThread = new Thread(CountSync);
                //myThread.Name = "Поток " + i.ToString();
                // myThread.Start();
            }
            for (int i = 0; i < 5; i++) // Дожидаемся завершения всех потоков
            {
                myThread[i].Join();
            }
            Console.WriteLine($"Использовано было {myThread.Count} потоков, без синхронизации. Все вызваны в разнобой.");
            myThread.Clear();// Очищаем List<thread> - для переиспользования
        }
        static void FunThread() // Использование 5 потоков с синхронизацией
        {
            for (int i = 0; i < 5; i++)
            {
                myThread.Add(new Thread(() => CountSync(i)));    
                myThread[i].Start();
                //Thread myThread = new Thread(CountSync);
                //myThread.Name = "Поток " + i.ToString();
                // myThread.Start();
            }
            for (int i = 0; i < 5; i++) // Дожидаемся завершения всех потоков
            {
                myThread[i].Join();
            }
            Console.WriteLine($"Использовано было {myThread.Count} потоков, с синхронизацией. Вызваны по очереди.");
            myThread.Clear(); // Очищаем List<thread>
        }
        static void Count(int a)
        {
            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine("Поток {0}: {1}", a, i);
            }
        }
        static void CountSync(int a)
        {
           lock (Locker)
           for (int i = 0; i < 3; i++)
           {
             Console.WriteLine("Поток {0}: {1}", a, i);
           }

        }
    }
}
