﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace ДЗ_17_Филатов_ВС
{
    class Program
    {
        public static int x;
        public static int threadCount = 2;
        private static object locker = new object();
        private static readonly Mutex mutex = new Mutex();

        public static async Task Main(string[] args)
        {
            Console.WriteLine($"Реализация в {threadCount + 1} потоках с помощью класса Thread без синхронизации:");
            for (int i = 0; i <= threadCount; i++)
            {
                Thread thread = new Thread(Count);
                thread.Name = "Поток " + i.ToString();
                thread.Start();
            }
            Thread.Sleep(1000);
            Console.ReadKey();

            Console.WriteLine($"\nРеализация в {threadCount + 1} потоках с помощью класса Thread с синхронизацией Lock:");
            for (int i = 0; i <= threadCount; i++)
            {
                Thread thread = new Thread(CountLocked);
                thread.Name = "Поток " + i.ToString();
                thread.Start();
            }
            Thread.Sleep(1000);
            Console.ReadKey();

            Console.WriteLine($"\nРеализация в {threadCount + 1} потоках с помощью класса Thread с синхронизацией Monitor:");
            for (int i = 0; i <= threadCount; i++)
            {
                Thread thread = new Thread(CountMonitor);
                thread.Name = "Поток " + i.ToString();
                thread.Start();
            }
            Thread.Sleep(1000);
            Console.ReadKey();

            Console.WriteLine($"\nРеализация в {threadCount + 1} задачах с помощью класса Task без синхронизации:");
            for (var i = 0; i <= threadCount; i++)
            {
                var task = new Task(() => CountTask());
                task.Start();
            }
            Thread.Sleep(1000);
            Console.ReadKey();

            Console.WriteLine($"\nРеализация в {threadCount + 1} задачах с помощью класса Task с синхронизацией Mutex:");
            for (var i = 0; i <= threadCount; i++)
            {
                var counter = i;
                await Task.Run(() => CountTaskAsync());
            }
            Thread.Sleep(1000);
            Console.ReadKey();

            Console.WriteLine($"\nРеализация в 3 задачах с помощью класса Parallel:");
            Parallel.Invoke(() => CountTask(), () => CountTask(), () => CountTask());

        }
        public static void Count()
        {
            x = 1;
            for (int i = 1; i < 5; i++)
            {
                Console.WriteLine("{0}: Х={1}", Thread.CurrentThread.Name, x);
                x++;
                Thread.Sleep(200);
            }
        }

        public static void CountLocked()
        {
            lock (locker)
            {
                x = 1;
                for (int i = 1; i < 5; i++)
                {
                    Console.WriteLine("{0}: Х={1}", Thread.CurrentThread.Name, x);
                    x++;
                    Thread.Sleep(200);
                }
            }
        }

        public static void CountMonitor()
        {
            bool acquiredLock = false;
            try
            {
                Monitor.Enter(locker, ref acquiredLock);
                x = 1;
                for (var i = 0; i < 5; i++)
                {
                    Console.WriteLine("{0}: Х={1}", Thread.CurrentThread.Name, x);
                    x++;
                    Thread.Sleep(200);
                }
            }
            finally
            {
                if (acquiredLock) Monitor.Exit(locker);
            }
        }

        public static void CountTask()
        {
            x = 1;
            for (var i = 0; i < 5; i++)
            {
                Console.WriteLine("Задача {0}: Х={1}", Task.CurrentId, x);
                x++;
                Thread.Sleep(100);
            }
        }

        private static void CountTaskAsync()
        {
            mutex.WaitOne();
            x = 1;
            for (var i = 0; i < 5; i++)
            {
                Console.WriteLine("Задача {0}: Х={1}", Task.CurrentId, x);
                x++;
                Thread.Sleep(200);
            }
            mutex.ReleaseMutex();
        }
    }
}
