﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreading
{
    class Program
    {
        static int globalVariable;
        static object locker = new object();

        static void Main(string[] args)
        {
            //ThreadWithoutSynchronization();
            //ThreadWithSynchronization();

            //TaskWithoutSynchronization();
            //TaskWithSynchronization();

            //ParrallelImplementation();
        }

        static void ActionAsync()
        {
            Action();
        }

        static void ActionSync()
        {
            lock (locker)
            {
                Action();
            }
        }

        private static void Action()
        {
            globalVariable = 0;

            for (var i = 0; i < 10; i++)
            {
                globalVariable++;
                Console.WriteLine(globalVariable);   
            }
        }

        private static void ThreadWithoutSynchronization() 
        {
            Console.WriteLine("ThreadWithoutSynchronization:");

            var thread1 = new Thread(() => ActionAsync());
            var thread2 = new Thread(() => ActionAsync());

            thread1.Start();
            thread2.Start();
        }

        private static void ThreadWithSynchronization()
        {
            Console.WriteLine("ThreadWithSynchronization:");

            var thread1 = new Thread(() => ActionSync());
            var thread2 = new Thread(() => ActionSync());

            thread1.Start();
            thread2.Start();
        }

        private static void TaskWithoutSynchronization()
        {
            Console.WriteLine("TaskWithoutSynchronization:");

            var task1 = new Task(() => Action());
            var task2 = new Task(() => Action());

            task1.Start();
            task2.Start();

            task1.Wait();
            task2.Wait();
        }

        private static void TaskWithSynchronization()
        {
            Console.WriteLine("TaskWithSynchronization:");

            var task1 = new Task(() => Action());
            var task2 = new Task(() => Action());

            task1.Start();
            task1.Wait();

            task2.Start();
            task2.Wait();
        }

        private static void ParrallelImplementation()
        {
            Parallel.Invoke(() => Action(), () => Action(), () => Action());
        }
    }
}
