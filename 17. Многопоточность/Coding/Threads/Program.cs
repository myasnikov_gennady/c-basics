﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Threads
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");
            //var task = Task.Run(() => Do());
            //Console.WriteLine("Выход из программы");
            //Parallel.Invoke(() => Do(1));
            var res = Parallel.For(1, 100, Factorial);
            //var res = Parallel.ForEach(new int[] { 1, 2, 3, 8, 12, 5, 7,4,6,9 },Factorial);
            Console.WriteLine($"Выполнение цикла завершено на итерации {res.LowestBreakIteration}");
            //Console.ReadKey();
        }
        static void Do()
        {
            Console.WriteLine("Что-то");
            throw new Exception("Exeption");
        }
        static void Do(int arg, ParallelLoopState pls)
        {
            if (arg == 3)
            {
                pls.Break();
            }
            Thread.Sleep(1000);
            Console.WriteLine("Что-то " + arg);
            //var str = string.Empty;
            //for (int i = 0; i < 1000000; i++)
            //{
            //    str += i;
            //}
            Console.WriteLine("Конец " + arg);
        }
        static void Factorial(int arg, ParallelLoopState pls)
        {

            var result = 1;
            for (int i = 1; i <= arg; i++)
            {
                if (i == 2)
                {
                    pls.Break();
                }
                result *= i;

            }
            Console.WriteLine($"Факториал {arg} = {result}");
            Thread.Sleep(1000);
        } 
    }
}
