﻿using System;
using System.Threading.Tasks;

namespace HW_10
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Reader reader = new FileReader(@"D:\Vir\Projects\HW_10_Korop_Alexander\HW_10\Text.txt");            
            var txt = await reader.Read();
            Console.WriteLine($"Text in file \"{txt}\"");
            Console.WriteLine($"Path to file {reader.Source}");            
            reader = new HttpReader(new Uri("https://twitch.tv"));
            txt = await reader.Read();
            Console.WriteLine(txt);
            Console.WriteLine($"Name of internet resource: {reader.Source}");
        }
    }
}
 