﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HW_10
{
    class HttpReader : Reader
    {
        private readonly Uri _uri;
        public HttpReader(Uri uri)
        {
            _uri = uri;
        }
        public override async Task<string> Read()
        {
            var httpClient = new HttpClient();
            var response = await httpClient.GetAsync(_uri);
            if (response.IsSuccessStatusCode)
            {
                var str = await response.Content.ReadAsStringAsync();
                return str;
            }

            return string.Empty;
        }
        public override string Source
        {
            get
            {
                return _uri.ToString();
            }
        }
    }
}
