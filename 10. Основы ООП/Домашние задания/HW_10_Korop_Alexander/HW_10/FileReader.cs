﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HW_10
{
    class FileReader : Reader
    {
        private readonly string _path;
        public FileReader(string path)
        {
            _path = path;
        }
        public override async Task<string> Read()
        {
            var str = await System.IO.File.ReadAllTextAsync(_path);
            return str;
        }

        public override string Source
        {
            get
            {
                return _path;
            }
            
        }

    }
}
