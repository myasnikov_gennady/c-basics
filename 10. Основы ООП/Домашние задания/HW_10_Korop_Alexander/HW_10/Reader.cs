﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HW_10
{
    public abstract class Reader
    {
        public virtual string Source { get; }
        public abstract Task<string> Read();
    }
}
