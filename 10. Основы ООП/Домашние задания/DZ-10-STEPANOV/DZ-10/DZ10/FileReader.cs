﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ10
{
    public class FileReader : Reader
    {
        private readonly string _path;  // необходимо переиспользовать уже существующее поле Source
        public FileReader(string path)
        {
            _path = path;
        }

        public override async Task<string> Read()
        {
            var str = await System.IO.File.ReadAllTextAsync(_path);
            return str;
        }
        public string Path { get { return _path; } }

    }       
}
