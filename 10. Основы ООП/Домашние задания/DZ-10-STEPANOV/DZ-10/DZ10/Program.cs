﻿using System;

namespace DZ10
{
    class Program   // А как так получается, что к Вашей ДЗ сразу же идут мои коммментарии? :) Не засчитана. Скопирована у Шибанова Никиты
    {
        static async System.Threading.Tasks.Task Main(string[] args)
        {
            FileReader reader = new FileReader(@"C:\Users\Admin\Desktop\1.txt");  // для того, чтобы не было ошибки компиляции в строке 13, необходимо использовать Reader
            var str = await reader.Read();
            Console.WriteLine(str);
            Console.WriteLine();
            reader = new HttpReader(new Uri("https://steam.com"));
            str = await reader.Read();
            Console.WriteLine(str);
        }
    }
}
