﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HW_10_Uladzimir_Skrypkin
{
    public class HttpReader : Reader
    {
        public readonly Uri uri;
        public HttpReader(Uri _uri)
        {
            uri = _uri;
        }
        public override async Task<string> Read()
        {
            var hpClient = new HttpClient();
            var answer = await hpClient.GetAsync(uri);
            if (answer.IsSuccessStatusCode)
            {
                var txt = await answer.Content.ReadAsStringAsync();
                return txt;
            }
            return string.Empty;
        }
        public override string Source => uri.ToString();

    }
}
