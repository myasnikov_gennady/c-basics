﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace HW_10_Uladzimir_Skrypkin
{
    public class FileReader : Reader
    {
        public readonly string path;
        public FileReader(string _path)
        {
            path = _path;
        }
        public override async Task<string> Read()
        {
            var txt = await File.ReadAllTextAsync(path);
            return txt;
        }
        public override string Source => Path.GetFullPath(path);

    }
}
