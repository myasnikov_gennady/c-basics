﻿using System;
using System.Threading.Tasks;

namespace HW_10_Uladzimir_Skrypkin
{
    class Program
    {
        static async Task Main(string[] args)
        {
            string path = "Text.txt";
            Reader reader = new FileReader(path);
            string txt = await reader.Read();
            Console.WriteLine($"Считывание и вывод на консоль файла класса FileReader:\n{txt}");
            Console.WriteLine($"Полный путь к файлу:\n{reader.Source}");
            Console.WriteLine();
            Console.WriteLine($"Нажмите любую клавишу для считывания интернет ресурса класса HttpReader");
            Console.ReadKey();
            reader = new HttpReader(new Uri("https://www.brest.it-academy.by"));
            txt = await reader.Read();
            Console.WriteLine(txt);
            Console.WriteLine($"Полный путь к сайту:\n{reader.Source}");


        }
    }
}
