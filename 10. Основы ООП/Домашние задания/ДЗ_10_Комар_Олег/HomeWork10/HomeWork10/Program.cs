﻿using System;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace HomeWork10
{
    class Program
    {
        static async Task Main()
        {
            ConsoleHelper.Info(" Класс FileReader");
            Console.WriteLine();
            var files = new [] 
                {
                    "Принципы ООП.txt",
                    @"..\..\..\Паттерны SOLID.txt"
                };
            //Reader reader = new FileReader("Document.txt");
            Reader reader;
            string result;
            foreach (var path in files)
            {
                reader = new FileReader(path);
                result = await reader.Read();
                Console.WriteLine(result);
                ConsoleHelper.Param("Полный путь к файлу - ", reader.Source);
                Console.WriteLine();
            }
            ConsoleHelper.Clear();

            ConsoleHelper.Info(" Класс HttpReader");
            reader = new HttpReader(new Uri("https://github.com/"));
            result = await reader.Read();
            Console.WriteLine(result);
            ConsoleHelper.Param("Адрес в сети - ", reader.Source);
            ConsoleHelper.Clear();

            /*На онлайн-занятии вы говорили сделать CSVReader, я его уже реализовал, а в текстовом документе этого задания не было.
             Но я решил его не удалять. Здесь реализован простой вывод в консоль всего текста из CSV-файла. */
            ConsoleHelper.Info(" Класс CSVReader");
            //reader = new CsvReader("export.csv");
            reader = new CsvReader(@"..\..\..\export.csv");
            result = await reader.Read();
            Console.WriteLine(result);
            ConsoleHelper.Param("Полный путь к файлу CSV - ", reader.Source);
        }
    }
}
