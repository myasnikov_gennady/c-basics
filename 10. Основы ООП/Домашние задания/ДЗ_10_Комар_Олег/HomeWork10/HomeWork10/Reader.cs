﻿using System.Threading.Tasks;

namespace HomeWork10
{
    public abstract class Reader
    {
        public abstract Task<string> Read();
        private readonly string _source;
        public virtual string Source => _source;
    }
}
