﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace HomeWork10
{
    public class HttpReader : Reader
    {
        private readonly Uri _uri;
        public override string Source => _uri.ToString();
        public HttpReader(Uri uri)
        {
            _uri = uri;
        }
        public override async Task<string> Read()
        {
            var httpClient = new HttpClient();
            var response = await httpClient.GetAsync(_uri);
            if (response.IsSuccessStatusCode)
            {
                var result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                return result;
            }
            throw new Exception();
        }
    }
}
