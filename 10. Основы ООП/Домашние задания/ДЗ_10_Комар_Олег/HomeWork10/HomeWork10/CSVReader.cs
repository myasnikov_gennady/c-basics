﻿using System.IO;
using System.Threading.Tasks;

namespace HomeWork10
{
    public class CsvReader : Reader
    {
        private readonly string _path;
        public override string Source => Path.GetFullPath(_path);
        public CsvReader(string path)
        {
            _path = path;
        }
        public override async Task<string> Read()
        {
            using FileStream fileStream = File.Open(_path,FileMode.Open,FileAccess.Read);
            var array = new byte[fileStream.Length];
            await fileStream.ReadAsync(array, 0, array.Length);
            var textFromFile = System.Text.Encoding.Default.GetString(array);
            return textFromFile;
        }
    }
}
