﻿using System.IO;
using System.Threading.Tasks;

namespace HomeWork10
{
    public class FileReader : Reader
    {
        private readonly string _path;
        public override string Source => Path.GetFullPath(_path);
        public FileReader(string path)
        {
            _path = path;
        }
        public override async Task<string> Read()
        {
            var result = await System.IO.File.ReadAllTextAsync(_path);
            return result;
        }
    }
}
