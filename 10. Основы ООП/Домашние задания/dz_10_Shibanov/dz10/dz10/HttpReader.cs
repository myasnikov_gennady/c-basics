﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace dz10
{
    public class HttpReader : Reader
    {
        private readonly Uri _uri; // необходимо переиспользовать уже существующее поле Source
        public HttpReader(Uri uri)
        {
            _uri = uri;
        }

        public override async Task<string> Read() 
        {
            var httpClient = new HttpClient();
            var responce = await httpClient.GetAsync(_uri);
            if (responce.IsSuccessStatusCode)
            {
                var str = await responce.Content.ReadAsStringAsync();
                return str;
            }
            return string.Empty;
        }
        
    }
}