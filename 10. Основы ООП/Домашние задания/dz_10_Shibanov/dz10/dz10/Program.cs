﻿using System;

namespace dz10
{
    class Program
    {
        static async System.Threading.Tasks.Task Main(string[] args)
        {
            FileReader reader = new FileReader(@"C:\Project\dz10\dz10\TextFile1.txt");  // для того, чтобы не было ошибки компиляции в строке 13, необходимо использовать Reader
            var str = await reader.Read();
            Console.WriteLine(str);
            Console.WriteLine();
            reader = new HttpReader(new Uri("https://steam.com"));
            str = await reader.Read();
            Console.WriteLine(str);
        }
    }
}
