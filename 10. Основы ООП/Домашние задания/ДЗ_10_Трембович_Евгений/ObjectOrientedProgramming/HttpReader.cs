﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ObjectOrientedProgramming
{
    class HttpReader : Reader
    {
        private readonly Uri _uri;
        public HttpReader(Uri uri)
        {
            _uri = uri;
        }
        public override async Task<string> Read()
        {
            var httpClient = new HttpClient();
            var response = await httpClient.GetAsync(_uri);
            if (response.IsSuccessStatusCode)
            {
                var text = await response.Content.ReadAsStringAsync();
                return text;
            }

            return string.Empty;
        }

        public override string Source
        {
            get
            {
                return _uri.ToString();
            }
        }
    }
}
