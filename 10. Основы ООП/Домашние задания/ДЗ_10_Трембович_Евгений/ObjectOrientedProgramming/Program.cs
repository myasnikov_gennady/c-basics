﻿using System;
using System.Threading.Tasks;

namespace ObjectOrientedProgramming
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Reader reader = new FileReader(@"A:\C#\IT_Academy\ДЗ\HW10_ObjectOrientedProgramming\ObjectOrientedProgramming\TextFile1.txt");
            var text = await reader.Read();
            Console.WriteLine($"File contents:{text}");
            Console.WriteLine($"\nFile path:\n{reader.Source}");
            Console.WriteLine("\nPress any button to read the content of the Internet resource");
            Console.ReadKey();

            reader = new HttpReader(new Uri("https://www.it-academy.by/"));
            text = await reader.Read();
            Console.WriteLine(text);
            Console.WriteLine($"\nInternet resource address:{reader.Source}");
        }
    }
}
