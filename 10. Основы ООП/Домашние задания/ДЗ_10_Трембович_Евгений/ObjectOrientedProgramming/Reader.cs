﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ObjectOrientedProgramming
{
    public abstract class Reader
    {
        public virtual string Source { get; }
        public abstract Task<string> Read();

    }
}
