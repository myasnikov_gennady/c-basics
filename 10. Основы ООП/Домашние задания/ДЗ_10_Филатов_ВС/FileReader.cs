﻿using System.Threading.Tasks;

namespace ДЗ_10_Филатов_ВС
{
    public class FileReader : Reader
    {
        private readonly string _path;
        public FileReader(string path)
        {
            _path = path;
        }

        public override async Task<string> Read()
        {
            var str = await System.IO.File.ReadAllTextAsync(_path);
            return str;
        }
        public override string Source 
        { 
            get 
            { 
                return _path; 
            } 
        }
    }
}
