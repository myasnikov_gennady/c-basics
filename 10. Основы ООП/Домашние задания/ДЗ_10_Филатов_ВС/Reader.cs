﻿using System.Threading.Tasks;

namespace ДЗ_10_Филатов_ВС
{
    public abstract class Reader
    {
        public abstract Task<string> Read();
        public virtual string Source { get; set; }
    }
}
