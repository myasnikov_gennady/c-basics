﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace ДЗ_10_Филатов_ВС
{
    public class HttpReader : Reader
    {
        private readonly Uri _uri;
        public HttpReader(Uri uri)
        {
            _uri = uri;
        }
        public override async Task<string> Read()
        {
            var httpClient = new HttpClient();
            var response = await httpClient.GetAsync(_uri);
            if (response.IsSuccessStatusCode)
            {
                var str = await response.Content.ReadAsStringAsync();
                return str;
            }

            return string.Empty;
        }
        public override string Source
        { 
            get 
            {
                //return nameof(HttpReader) + " " + _uri;
                return _uri.ToString();
            }
        }
    }
}
