﻿using System;
using System.Threading.Tasks;

namespace ДЗ_10_Филатов_ВС
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.Write("Нажмите любую клавишу для чтения файла:");
            Console.ReadKey();
            Console.WriteLine();
            Reader reader = new FileReader(@"D:\Вадим\Рабочие материалы\Учеба\C-basics\10. Основы ООП\Домашние задания\ДЗ_10_Филатов_ВС\OOP.txt");
            var str = await reader.Read();
            Console.WriteLine($"Содержимое файла: {str}\n");
            Console.WriteLine($"Дирректория файла: {((FileReader)reader).Source}");
            Console.WriteLine();
            Console.Write("Нажмите любую клавишу для очистки консоли и чтения http-страницы:");
            Console.ReadKey();
            Console.Clear();
            Console.Write("Введите полный URL: ");
            string uri = Console.ReadLine();
            try
            {
                reader = new HttpReader(new Uri(uri));
            }
            catch (UriFormatException ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }
            str = await reader.Read();
            Console.WriteLine(str + " <- содержимое ресурса\n");
            Console.WriteLine($"Адрес ресурса: {((HttpReader)reader).Source}");
        }
    }
}
