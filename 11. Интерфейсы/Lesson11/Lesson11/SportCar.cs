﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson11
{
    public class SportCar : Car
    {
        public SportCar(string vin) : base(vin)
        {            
        }

        public SportCar(Point startPosition, string vin) : base(startPosition, vin)
        {
        }

        public override void Move(Point newPosition)
        {
            Console.WriteLine($"Vroom Move from {CurrentPosition} to {newPosition}");
            CurrentPosition = newPosition;
        }

        public override void Destruct()
        {
            Console.WriteLine("Worker is crying");
        }
    }
}
