﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson11
{
    public interface ICarUtilizer<in T>
    {
       void Utilizer(T car);
    }
    public class CarUtilizer<T> : ICarUtilizer<T> where T : Car
    {
        public void Utilizer(T car)
        {
            car.Destruct();
        }
    }
}
