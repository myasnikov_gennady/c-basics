﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson11
{
    public interface ICarFabric<out T>
    {
        T CreateCar();
    }
    public class CarFabric<T> : ICarFabric<T> where T : Car, new()
    {
        public T CreateCar()
        {
            return new T();
        }
    }
}
