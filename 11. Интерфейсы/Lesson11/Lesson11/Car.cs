﻿using Lesson11.Interfases;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson11
{
    public class Car : IMoveable, IGps, IDestructable, IEquatable<Car>, IComparable<Car>
    {
        public Point CurrentPosition { get; protected set; }
        public string Vin { get; private set; }

        Point IGps.CurrentPosition
        {
            get
            {
                return new Point
                {
                    X = CurrentPosition.X - 10,
                    Y = CurrentPosition.Y + 10
                };
            }
        }

        public Car(string vin)
        {
            Vin = vin;
            CurrentPosition = new Point { X = 0, Y = 0 };
        }
        public Car(Point startPosition, string vin)
        {
            Vin = vin;
            CurrentPosition = startPosition;
        }

        public virtual void Move(Point newPosition)
        {
            Console.WriteLine($"Move from {CurrentPosition} to {newPosition}");
            CurrentPosition = newPosition;            
        }

        public override string ToString()
        {
            return $"Vin: {Vin}, {nameof(CurrentPosition)}: {CurrentPosition}";
        }

        public virtual void Destruct()
        {
            Console.WriteLine($"Utilization worker is not crying");
        }

        public bool Equals(Car other)
        {
            return this.Vin == other?.Vin;
        }

        public int CompareTo(Car other)
        {
            return this.Vin?.CompareTo(other?.Vin) ?? 0;
        }
    }
}
