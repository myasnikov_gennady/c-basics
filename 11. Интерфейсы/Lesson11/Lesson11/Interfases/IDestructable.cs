﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson11.Interfases
{
    public interface IDestructable
    {
        void Destruct();
    }
}
