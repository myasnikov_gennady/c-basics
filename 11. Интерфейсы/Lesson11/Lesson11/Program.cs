﻿using Lesson11.Interfases;
using System;
using System.Collections.Generic;

namespace Lesson11
{
    class Program
    {
        static void Main(string[] args)
        {
            //ICarFabric<Car> carFabric = new CarFabric<SportCar>();
            //ICarContainer<Car> container = new CarContainer<Car>();
            ////var car = new Car(new Point { X = 5, Y = 0 });
            //var car = carFabric.CreateCar();
            //car.Move(new Point { X = 1, Y = 1 });
            ////var sportCar = new SportCar(new Point { X = 0, Y = 0 });
            ////IGps carGps = car;
            ////Console.WriteLine(car.ToString());
            ////car.Move(new Point { X = 10, Y = 0 });
            ////Console.WriteLine(car.CurrentPosition);
            ////Console.WriteLine(carGps.CurrentPosition);
            ////car = new SportCar(new Point { X = 5, Y = 0 });
            ////Console.WriteLine(car.ToString());
            ////car.Move(new Point { X = 10, Y = 0 });
            ////container.Put(sportCar);
            //ICarUtilizer<SportCar> carUtilizer = new CarUtilizer<Car>();
            //carUtilizer.Utilizer(new SportCar());
            var car = new Car(Guid.NewGuid().ToString());
            var list = new List<Car>()
            {
                new Car(Guid.NewGuid().ToString()),
                new Car(Guid.NewGuid().ToString()),
                new Car(Guid.NewGuid().ToString()),
                new Car(car.Vin)
            };
            Console.WriteLine(string.Join("\r\n", list));
            Console.WriteLine(list.IndexOf(car));
            list.Sort();
            Console.WriteLine(string.Join("\r\n", list));
        }
    }
}
