﻿using Lesson11.Interfases;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson11
{
    public class CarContainer<T> : ICarContainer<T> where T : Car
    {
        private readonly Stack<T> _container;
        public T ContainingCar => _container.Peek();

        public CarContainer()
        {
            _container = new Stack<T>(1);
        }
        public void Put(T car)
        {
            Console.WriteLine("Put car");
            _container.Push(car);
        }

        public T Take()
        {
            return _container.Pop();
        }
    }
}
