﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace HW_11_Uladzimir_Skrypkin
{
    public class Class2<T> : IEnumerable<T> where T : class, ICloneable, IEquatable<T>, IComparable<T>
    {
        public  T[] data;
        public Class2(params T[] _data) 
        {
            this.data = _data; 
        }
        public IEnumerator<T> GetEnumerator()
        {
            return (data as IEnumerable<T>).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        public IEnumerable<T> Clone()
        {
            for (int i = 0; i < data.Length; i++)
            {
                yield return data[i].Clone() as T;
            }
        }
        
        public void Print()
        {
            for (int i = 0; i < data.Length; i++)
            {
                Console.WriteLine(data[i]);
            }
           
        }
       
    }
}
