﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HW_11_Uladzimir_Skrypkin
{
    interface ICovariant<out T>
    {
        string Name { get; set; }
        T NewPerson();
    }
}
