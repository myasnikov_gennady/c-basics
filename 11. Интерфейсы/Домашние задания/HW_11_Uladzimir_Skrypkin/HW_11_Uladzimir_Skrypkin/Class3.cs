﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HW_11_Uladzimir_Skrypkin
{
    public class Class3 : Class1, ICovariant<Class3>, IContravariant<Class1>        // не реализован п.3
    {
        public string workplace;
        public Class3(string name, int age, string _workplace) : base(name, age)
        {
            workplace = _workplace;
        }
        public Class3 NewPerson()
        {
            return new Class3("Илон Маск", 49, "Tesla");
        }
        public void PrintPerson(Class1 field)
        {
            Console.WriteLine($" Персоне {field.Name}, {field.Age} лет, работает в {workplace}");
        }
        public override string ToString() => $" Персоне {Name}, {Age} лет, работает в {workplace}";
    }
}
