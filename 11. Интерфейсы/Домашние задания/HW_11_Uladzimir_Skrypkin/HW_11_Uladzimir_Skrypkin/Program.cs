﻿using System;

namespace HW_11_Uladzimir_Skrypkin
{
    class Program
    {
        static void Main(string[] args)
        {
            var person1 = new Class1("Илон Маск", 49);
            var person2 = new Class3("Билл Гейтс", 65, " Microsoft");
            person2.PrintPerson(person1);
            var person3 = (Class1)person2.Clone();
            person3.Name = "Джефф Безос";
            person3.Age = 57;
            person3.Print();
            var group= new Class2<Class1>(person1, person2, person3);
            Console.WriteLine();
            group.Print();
            Console.WriteLine();
            ICovariant<Class3> person4 = person2;
            person4=person4.NewPerson();
            Console.WriteLine(person4);
            IContravariant<Class3> person5 = (IContravariant<Class3>)person4;
            person5.Name = "Имя";
            person5.Age = 23;
            Console.WriteLine(person5);
            Console.WriteLine(person5.Equals(person4));
            Console.WriteLine(person5.Name.CompareTo(person4.Name));
        }
    }
}
