﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HW_11_Uladzimir_Skrypkin
{
    public class Class1:ICloneable, IEquatable<Class1>, IComparable<Class1>
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public Class1(string name, int age)
        {
            Name = name;
            Age = age;
        }
        public object Clone()
        { 
            return new Class1(Name, Age);
        }
        public bool Equals(Class1 somedata)
        {
            if ((somedata is Class1) && (this == somedata) && (somedata != null)) 
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public int CompareTo(Class1 somedata)
        {
          return this.Name.CompareTo(somedata.Name);
        }
        public void  Print()
        {
            Console.WriteLine($" Персоне {Name}, {Age} лет");
        }
       public override string ToString() => $" Персоне {Name}, {Age} лет";

    }
}
