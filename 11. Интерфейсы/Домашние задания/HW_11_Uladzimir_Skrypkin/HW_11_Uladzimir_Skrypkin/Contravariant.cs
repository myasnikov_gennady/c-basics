﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HW_11_Uladzimir_Skrypkin
{
    interface IContravariant<in T>
    {
        string Name { get; set; }
        int Age { get; set; }
        void PrintPerson(T field);
    }
}
