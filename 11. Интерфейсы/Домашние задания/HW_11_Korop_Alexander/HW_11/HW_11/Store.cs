﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

namespace HW_11
{
    class Store<T> : IEnumerable<T> // не реализованы интерфейсы из п.1.
    {
        public T[] _storage;

        public Store(params T[] storage)
        {
            _storage = storage;
        }
        public IEnumerator<T> GetEnumerator()
        {
            return ((IEnumerable<T>)_storage).GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        public void Print()
        {
            foreach (var item in _storage)
            {
                Console.WriteLine(item);
            }
        }
        public void Sort()
        {
            Array.Sort(_storage);
        }
    }
}
