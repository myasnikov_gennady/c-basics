﻿using System;

namespace HW_11
{
    class Program
    {
        static void Main(string[] args)
        {
            var phone = new Phone("Soni", 80252525);
            Console.WriteLine(phone.ToString());            
            var phone2 = (Phone)phone.Clone();
            phone2.Name = "Nokia";
            phone2.Number = 802546849;
            Console.WriteLine(phone2.ToString());
            Console.WriteLine(phone.Equals(phone2));
            var smartPhone = new SmartPhone("Xiaomi", 80255018455, true);
            smartPhone.PhoneInfo(smartPhone);
            Console.WriteLine();
            var storage = new Store<Phone>(phone, phone2, smartPhone);
            storage.Print();
            Console.WriteLine();
            storage.Sort();
            storage.Print();
            Console.WriteLine();
        }
    }
}
