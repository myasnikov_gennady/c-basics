﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HW_11.Interfaces
{
    interface IPhoneInfo<in T>
    {
        public string Name { get; set; }
        public long Number { get; set; }
        void PhoneInfo(T obj);
    }
}
