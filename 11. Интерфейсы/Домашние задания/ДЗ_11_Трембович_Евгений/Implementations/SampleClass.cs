﻿using Interfaces.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Implementations
{
    public class SampleClass<T> : IEnumerable<T>, IContravariantInterface<T>, ICovariantInterface<T> where T : Parent
    {
        T[] SampleArray;

        public void CommonMethod()
        {
            Console.WriteLine("CommonMethod");
        }

        public void ContravariantInterfaceMethod(T model)
        {
            Console.WriteLine("ContravariantInterfaceMethod");
        }

        public void CovariantInterfaceMethod()
        {
            Console.WriteLine("CovariantInterfaceMethod");
        }

        public IEnumerator<T> GetEnumerator()
        {
            foreach (var o in SampleArray)
            {
                if (o == null)
                {
                    break;
                }
                yield return o;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
