﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace Interfaces.Implementations
{
    public class Parent : ICloneable, IEquatable<Parent>, IComparable<Parent>
    {
        public int ParentId;

        public int ParentCount;

        public string Name;
        
        public object Clone()
        {
            return MemberwiseClone();
        }

        public bool Equals(Parent other)
        {
            return ParentId == other.ParentId;            
        }

        public int CompareTo(Parent other)
        {
            if (ParentCount < other.ParentCount)
            {
                return 1;
            }
            else if (ParentCount > other.ParentCount)
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }
    }
}
