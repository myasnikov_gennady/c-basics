﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Interfaces
{
    public interface ICovariantInterface<out T>     // интерфейс не показывает работу контрвариативности
    {
        void CommonMethod();

        void CovariantInterfaceMethod();
    }
}
