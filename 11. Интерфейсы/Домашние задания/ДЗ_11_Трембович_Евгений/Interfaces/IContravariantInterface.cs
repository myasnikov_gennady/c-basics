﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Interfaces
{
    public interface IContravariantInterface<in T>
    {
        void CommonMethod();

        void ContravariantInterfaceMethod(T model);
    }
}
