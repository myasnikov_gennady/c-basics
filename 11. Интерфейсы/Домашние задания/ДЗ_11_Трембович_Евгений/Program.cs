﻿using Interfaces.Implementations;
using Interfaces.Interfaces;
using System;
using System.Collections.Generic;

namespace Interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
           
            ICovariantInterface<Parent> object1 = new SampleClass<Child>();
            IContravariantInterface<Child> object2 = new SampleClass<Parent>();

            int count = 0;
            int j = 2;
            for (int i = 1; i < 100; i = i + 2)
            {
                j = j - 1;
                ++count;
                while (j < 15)
                {
                    j = j + 5;
                }
            }
            Console.WriteLine(count);


        }


    }
}
