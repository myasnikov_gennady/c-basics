﻿namespace HomeWork11.Interfaces
{
    public interface IMouseConnect<out T>
    {
        string Name { get; set; }
        T MouseAdd();
    }
}
