﻿namespace HomeWork11.Interfaces
{
    public interface IMouseInfo<in T>
    {
        string Name { get; set; }
        int NumKeys { get; set; }
        void MouseInfo(T obj);
    }
}
