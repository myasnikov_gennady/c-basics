﻿using System;

namespace HomeWork11
{
    class Mouse : ICloneable, IEquatable<Mouse>, IComparable<Mouse>
    {
        public string Name { get; set; }

        public int NumKeys { get; set; }
        
        public Mouse(string name, int qtyKeys)
        {
            Name = name;
            NumKeys = qtyKeys;
        }

        public object Clone()
        {
            return new Mouse(Name, NumKeys);
        }

        public bool Equals(Mouse other)
        {
            return Name == other?.Name;
        }

        public int CompareTo(Mouse other)
        {
            if (other != null)
            {
                return Name?.CompareTo(other?.Name) ?? 0;
            }
            throw new Exception("Невозможно сравнить два объекта");
        }

        public override string ToString()
        {
            return $"Mouse {Name}, number of buttons - {NumKeys}";
        }
    }
}
