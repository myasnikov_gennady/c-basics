﻿using System;
using HomeWork11.Interfaces;

namespace HomeWork11
{
    class MouseWireless : Mouse, IMouseConnect<MouseWireless>, IMouseInfo<Mouse>
    {
        private readonly string _connection;

        public MouseWireless(string name, int qtyKeys, string connection) : base(name, qtyKeys)
        {
            _connection = connection;
        }

        public MouseWireless MouseAdd()
        {
            return new MouseWireless("Logitech MX Ergo", 8, "Bluetooth");
        }
        
        public void MouseInfo(Mouse obj)
        {
            Console.WriteLine(@$"Название мыши:{obj.Name}. Количество клавиш: {obj.NumKeys}. Тип соединения: {_connection}");
        }
    }
}
