﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace HomeWork11
{
    public class Box<T> : IEnumerable<T>                    // не реализован п.3
        where T : class, ICloneable, IEquatable<T>, IComparable<T>
    {
        private readonly T[] _items;
        public Box(params T[] items)
        {
            _items = items;
        }
        public IEnumerator<T> GetEnumerator()
        {
            return ((IEnumerable<T>)_items).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerable<T> Clone()
        {
            foreach (var item in _items)
            {
                yield return item.Clone() as T;
            }
        }

        public void Sort()
        {
            Array.Sort(_items);
        }

        public void ToConsole()
        {
            foreach (var item in _items)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();
        }
    }
}
