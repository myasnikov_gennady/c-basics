﻿using System;

namespace HomeWork11
{
    class Program
    {
        static void Main()
        {

            Mouse m1 = new Mouse("Razer Basilisk X Hyperspeed", 6);
            var m2 = (Mouse) m1.Clone();
            m2.Name = "Logitech M590";
            m2.NumKeys = 6;
            Console.WriteLine(m1);
            Console.WriteLine(m2);
            var m3 = new MouseWireless("Logitech MX Ergo", 8, "Bluetooth");
            m3.MouseInfo(m3);
            Console.WriteLine();

            var box = new Box<Mouse>(m1, m2, m3);
            box.ToConsole();

            var newMice = box.Clone();
            foreach (var item in newMice)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();
            box.Sort();
            box.ToConsole();

            Console.WriteLine(box.Equals(newMice));
        }
    }
}
