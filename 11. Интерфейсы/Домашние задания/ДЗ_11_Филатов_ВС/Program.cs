﻿using System;

namespace ДЗ_11_Филатов_ВС
{
    class Program
    {
        static void Main(string[] args)
        {
            var ammoBox1 = new AmmoBox<Rifle>();
            Rifle weap1 = ammoBox1.CreateWeapon();
            weap1.Model = "Mauser 98";
            weap1.AmmoType = "7,92x57 mm";
            Rifle weap2 = (Rifle)weap1.Clone();
            weap2.Model = "Винтовка Мосина обр. 1891 г.";
            weap2.AmmoType = "7,62x54 мм";
            var ammoBox2 = new AmmoBox<AssaultRifle>();
            AssaultRifle weap3 = ammoBox2.CreateWeapon();
            weap3.Model = "M4A1";
            weap3.AmmoType = "5,56x45 mm";
            weap3.Capacity = 25;
            weap3.IsAutomatic = true;
            var ammoBox3 = new AmmoBox<Rifle>(weap1, weap2, weap3);
            //var ammoBox3 = new AmmoBox<AssaultRifle>(weap3);
            Console.WriteLine("Исходные объекты из AmmoBox:");
            foreach (var item in ammoBox3)
            {
                ammoBox3.GetFullInfo(item);
            }
            Console.WriteLine();
            ammoBox3.Sort();
            ammoBox3.Clone();
            var newWeapons = ammoBox3;
            Console.WriteLine("Клон AmmoBox:");
            foreach (var item in newWeapons)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();
            Console.WriteLine($"Метод CompareTo для {weap1.Model} и {weap2.Model}: {weap1.CompareTo(weap2)}");
            Console.WriteLine($"Метод Equals для {weap2.Model} и {weap3.Model}: {weap2.Equals(weap3)}");
            Console.WriteLine($"Метод Equals для {weap3.Model} и {weap3.Model}: {weap3.Equals(weap3)}");
            Console.WriteLine($"Метод Equals для ammoBox3 и newWeapons: {ammoBox3.Equals(newWeapons)}");

        }
    }
}
