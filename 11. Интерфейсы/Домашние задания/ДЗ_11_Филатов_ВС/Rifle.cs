﻿using System;

namespace ДЗ_11_Филатов_ВС
{
    public class Rifle : ICloneable, IEquatable<Rifle>, IComparable<Rifle>
    {
        public string Model { get; set; }
        public string AmmoType { get; set; }
        public bool IsAutomatic { get; set; }
        public Rifle(string model, string ammoType, bool isAutomatic = false)
        {
            Model = model;
            AmmoType = ammoType;
            IsAutomatic = isAutomatic;
        }
        public object Clone()
        {
            return new Rifle(Model, AmmoType, IsAutomatic);
        }
        public Rifle()
        {

        }
        public int CompareTo(Rifle other)
        {
            if (other != null)
            {
                return Model?.CompareTo(other?.Model) ?? 0;
            }
            else
            {
                throw new Exception(message: "Сравнение невозможно");
            }
        }
        public bool Equals(Rifle other)
        {
            return Model == other?.Model;
        }

        public override string ToString()
        {
            return $"Weapon model: {Model}, Ammo type: {AmmoType}, Automatic: {IsAutomatic}";
        }
    }
}
