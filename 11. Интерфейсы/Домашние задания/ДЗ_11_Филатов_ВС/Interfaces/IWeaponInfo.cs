﻿namespace ДЗ_11_Филатов_ВС.Interfaces
{
    interface IWeaponInfo<in T>                           // Контрвариантный интерфейс
    {
        string Model { get; set; }
        string AmmoType { get; set; }
        bool IsAutomatic { get; set; }
        void GetFullInfo(T obj);
    }
}
