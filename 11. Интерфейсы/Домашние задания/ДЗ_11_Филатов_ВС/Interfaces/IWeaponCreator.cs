﻿namespace ДЗ_11_Филатов_ВС.Interfaces
{
    interface IWeaponCreator<out T>                                 // Ковариантный интерфейс
    {
        string Model { get; set; }
        T CreateWeapon();
    }
}