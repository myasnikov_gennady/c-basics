﻿using System;

namespace ДЗ_11_Филатов_ВС
{
    public class AssaultRifle : Rifle, ICloneable, IEquatable<AssaultRifle>
    {
        public int Capacity { get; set; }

        private readonly int _capacity;
        public AssaultRifle(string model, string ammoType, int capacity, bool isAutomatic) : base(model, ammoType, isAutomatic)
        {
            _capacity = capacity;
        }
        public AssaultRifle()
        {

        }
        public new object Clone()
        {
            return new AssaultRifle(Model, AmmoType, Capacity, IsAutomatic);
        }

        public bool Equals(AssaultRifle other)
        {
            return Model == other?.Model;
        }
        public override string ToString()
        {
            return $"Weapon model: {Model}, Ammo type: {AmmoType}, Magazine capacity: {Capacity}, Automatic: {IsAutomatic}";
        }
    }
}
