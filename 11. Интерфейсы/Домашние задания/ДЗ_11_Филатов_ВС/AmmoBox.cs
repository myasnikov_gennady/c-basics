﻿using System;
using System.Collections;
using System.Collections.Generic;
using ДЗ_11_Филатов_ВС.Interfaces;

namespace ДЗ_11_Филатов_ВС
{
    public class AmmoBox<T> : IEnumerable<T>, IWeaponInfo<T>,  IWeaponCreator<T>   // не реализован п.3. -> исправлено
        where T : class, ICloneable, IEquatable<T>, IComparable<T>, new()
    {
        private readonly T[] _items;

        public string Model { get; set; }
        public string AmmoType { get; set; }
        public bool IsAutomatic { get; set; }
        public int Capacity { get; set; }

        public AmmoBox(params T[] items)
        {
            _items = items;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return ((IEnumerable<T>)_items).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        public IEnumerable<T> Clone()
        {
            foreach (var item in _items)
            {
                yield return item.Clone() as T;
            }
        }
        public void Sort()
        {
            Array.Sort(_items);
        }

        public T CreateWeapon()
        {
            return new T();
        }

        public void GetFullInfo(T obj)
        {
            Console.WriteLine(obj);
        }
    }
}
