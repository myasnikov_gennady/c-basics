﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HW_11.Interfaces
{
    interface IAddPhone<out T>
    {
        public string Name { get; set; }
        T AddPhone();
    }
}
