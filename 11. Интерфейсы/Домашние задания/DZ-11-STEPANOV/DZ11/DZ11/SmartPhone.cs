﻿using HW_11.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace DZ11
{
    public class SmartPhone : Phone, IAddPhone<SmartPhone>, IPhoneInfo<Phone>
    {
        public bool _touchScreen;
        public SmartPhone(string name, long number, bool touchScreen) : base(name, number)
        {
            touchScreen = _touchScreen;
        }
        public SmartPhone AddPhone()
        {
            return new SmartPhone("Samsung", 80255321345, true);
        }
        public void PhoneInfo(Phone obj)
        {
            Console.WriteLine($"Name: {obj.Name}, Number : {obj.Number}, Touch screan: {_touchScreen}");
        }
    }
}
