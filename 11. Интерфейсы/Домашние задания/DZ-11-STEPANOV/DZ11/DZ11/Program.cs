﻿using System;

namespace DZ11
{
    class Program   // Не засчитана. Скопирована у Коропа Александра
    {
        static void Main(string[] args)
        {
            var phone = new Phone("HONOR", 80251315);
            Console.WriteLine(phone.ToString());            
            var phone2 = (Phone)phone.Clone();
            phone2.Name = "TUEW";
            phone2.Number = 802544678;
            Console.WriteLine(phone2.ToString());
            Console.WriteLine(phone.Equals(phone2));
            var smartPhone = new SmartPhone("Xiaomi", 802550174321, true);
            smartPhone.PhoneInfo(smartPhone);
            Console.WriteLine();
            var storage = new Store<Phone>(phone, phone2, smartPhone);
            storage.Print();
            Console.WriteLine();
            storage.Sort();
            storage.Print();
            Console.WriteLine();
        }
    }
}
