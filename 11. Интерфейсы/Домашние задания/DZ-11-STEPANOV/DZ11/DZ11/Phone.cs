﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace DZ11
{
    public class Phone : ICloneable, IEquatable<Phone>, IComparable<Phone>
    {
        public string Name{ get; set; }

        public long Number { get; set; }

        public Phone(string name, long number)
        {
            Name = name;
            Number = number;
        }
        public object Clone()
        {
            return new Phone(Name, Number);
        }

        int IComparable<Phone>.CompareTo(Phone other)
        {
            if (other != null)
            {
                return Name?.CompareTo(other?.Name) ?? 0;
            }
            throw new Exception("Impossible to compare ");
        }

        bool IEquatable<Phone>.Equals(Phone other)
        {
            return Name == other?.Name;
        }

        public override string ToString()
        {
            return $"Name: {Name}, number: { Number}";
        }
    }
}
