﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DllUladzimir
{
    public class Class1
    {
        public string Name;
        public int Age { get; set; }
        public Class1(string name, int age)
        {
            Name = name;
            Age = age;
        }
        public override string ToString()
        {
            return $"Персоне {Name}, {Age} лет";
        }
    }
}
