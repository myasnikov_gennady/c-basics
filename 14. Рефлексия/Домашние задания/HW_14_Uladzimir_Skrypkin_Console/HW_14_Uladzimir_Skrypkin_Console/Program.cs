﻿using System;
using System.Reflection;

namespace HW_14_Uladzimir_Skrypkin_Console
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = "DllUladzimir.dll";
            var library = Assembly.LoadFrom(path);
            Console.WriteLine($"{library.FullName}");
            Console.WriteLine();
            var type = library.GetType("DllUladzimir.Class1");
            foreach (MemberInfo item in type.GetMembers()) // Вывод содержимого из dll
            {
                Console.WriteLine($"{item.DeclaringType}: {item.MemberType}: {item.Name}");
            }
            Console.WriteLine();
            var instance = Activator.CreateInstance(type, new object[] {"Илон Маск", 49 }); // Присваивание значения свойству и полю
            Console.WriteLine(instance); // Метод override Tostring в dll файле
        }
    }
}
