﻿using System;

namespace Relflection
{
    public class ReflectionClass
    {
        public int Member1 { get; set; }

        public int Member2;

        public int Sum()
        {
            return Member1 + Member2;
        }
    }
}
