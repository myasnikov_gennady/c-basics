﻿using System;
using System.Reflection;

namespace ReflectionConsoleApp
{
    class Program
    {
        private const string ReflectionDllPath = @"Relflection.dll";

        private const string ReflectionClassInstancePath = "Relflection.ReflectionClass";

        private const string Mmember1PropertyName = "Member1";

        private const string Mmember2FieldName = "Member2";

        private const string MethodName = "Sum";

        private const int Mmember1Value = 1;

        private const int Mmember2Value = 2;

        static void Main(string[] args)
        {
            var reflectionDll = Assembly.LoadFile(System.IO.Path.Combine(Environment.CurrentDirectory, ReflectionDllPath));     // пришлось переделать получение пути бибилиотеки

            var reflectionClassInstance = reflectionDll.CreateInstance(ReflectionClassInstancePath);

            var propertyInfo = reflectionClassInstance.GetType().GetProperty(Mmember1PropertyName);
            propertyInfo.SetValue(reflectionClassInstance, Convert.ChangeType(Mmember1Value, propertyInfo.PropertyType), null);
            Console.WriteLine($"Member 1 = {propertyInfo.GetValue(reflectionClassInstance)}");

            var fieldInfo = reflectionClassInstance.GetType().GetField(Mmember2FieldName);
            fieldInfo.SetValue(reflectionClassInstance, Convert.ChangeType(Mmember2Value, fieldInfo.FieldType));
            Console.WriteLine($"Member 2 = {fieldInfo.GetValue(reflectionClassInstance)}");

            var methodInfo = reflectionClassInstance.GetType().GetMethod(MethodName);
            var result = methodInfo.Invoke(reflectionClassInstance, null);

            Console.WriteLine($"Sum = {result}");
        }
    }
}
