﻿using System;

namespace MyLibrary
{
    public class LibraryClass
    {
        public string Name;
        public int Age { get; set; }
        public LibraryClass(string name, int age)
        {
            Name = name;
            Age = age;
        }
        public void LCMethodInfo()
        {
            Console.WriteLine($"Name: {Name}, Age: {Age}");
        }
        public int ChangeAge(int newAge)
        {
            Age = newAge;
            return Age;
        }
    }
}
