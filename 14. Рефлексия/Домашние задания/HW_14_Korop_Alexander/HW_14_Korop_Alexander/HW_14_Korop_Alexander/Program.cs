﻿using System;
using System.Linq;
using System.Reflection;

namespace HW_14_Korop_Alexander
{
    class Program
    {
        static void Main(string[] args)
        {
            var library = Assembly.LoadFrom("MyLibrary.dll");
            var metaInformation = library.GetTypes();
            Console.WriteLine($"Solution: {library.FullName}:");
            Console.WriteLine();
            foreach (var type in metaInformation)
            {
                Console.WriteLine(type.FullName);
                Console.WriteLine($"{nameof(type.IsClass)}: {type.IsClass}");
                Console.WriteLine($"{nameof(type.IsInterface)}: {type.IsInterface}");
                Console.WriteLine($"{nameof(type.IsArray)}: {type.IsArray}");
                Console.WriteLine($"{nameof(type.IsAbstract)}: {type.IsAbstract}");
                Console.WriteLine();
                Console.WriteLine($"Methods:{string.Join("; ", type.GetMethods(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static).Select(x => x.Name + " с входящими параметрами типа: " + string.Join(", ", x.GetParameters().Select(y => y.ParameterType.ToString()))))}");
                Console.WriteLine($"Properties:{string.Join("; ", type.GetProperties().Select(x => x.PropertyType + " " + x.Name))}");
                Console.WriteLine($"Fields:{string.Join("; ", type.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static).Select(x => x.FieldType + " " + x.Name))}");
            }
            Console.WriteLine();
            var metaInf = library.GetType("MyLibrary.LibraryClass");
            Console.WriteLine("\nCreate instance class");
            var inst = Activator.CreateInstance(metaInf, new object[] { "Alex" , 23});

            Console.WriteLine("\nCall method LCMethodInfo");
            var method1 = metaInf.GetMethod("LCMethodInfo");
            method1.Invoke(inst, new object[] { });

            Console.WriteLine("Call method ChangeAge");
            var method2 = metaInf.GetMethod("ChangeAge");
            method2.Invoke(inst, new object[] { 18 });

            Console.WriteLine("Call method LCMethodInfo");            
            method1.Invoke(inst, new object[] { });

            Console.WriteLine("\nField and Property");
            var field = metaInf.GetField("Name");
            Console.WriteLine($"{field.Name} before changes: {field.GetValue(inst)}");
            field.SetValue(inst, "Vlad");
            Console.WriteLine($"{field.Name} after changes: {field.GetValue(inst)}");
            var property = metaInf.GetProperty("Age");
            Console.WriteLine($"{property.Name} before changes: {property.GetValue(inst)}");
            property.SetValue(inst, 70);
            Console.WriteLine($"{property.Name} after changes: {property.GetValue(inst)}");

            
        }
    }
}
