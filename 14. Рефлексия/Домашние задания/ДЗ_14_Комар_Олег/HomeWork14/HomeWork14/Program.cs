﻿using System;
using System.Linq;
using System.Reflection;

namespace HomeWork14
{
    internal static class Program
    {
        static void Main()
        {
            var lib = Assembly.LoadFrom("MyClass.dll");
            Types(lib);
            var tp = lib.GetType("MyClass.HiddenClass");
            Console.WriteLine($"Члены класса {tp.Name}: ");
            foreach (var member in tp.GetMembers(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public))
            {
                Console.WriteLine($"{member.MemberType} {member.Name}");
            }

            Console.WriteLine();
            Console.WriteLine("Более подробно о членах класса с типами и аргументами:");
            Console.WriteLine($"Конструкторы: {string.Join("; ", tp.GetConstructors().Select(x => x.Name + " " + string.Join(", ", x.GetParameters().Select(y => y.ParameterType.ToString()))))}");
            Console.WriteLine($"Методы:\n{string.Join("\n", tp.GetMethods(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public).Select(x=>x.Name + " с входящими параметрами типа: " + string.Join(", ", x.GetParameters().Select(y=>y.ParameterType.ToString()))))}");
            Console.WriteLine($"Свойства: {string.Join("; ", tp.GetProperties().Select(x => x.PropertyType + " " + x.Name))}");
            Console.WriteLine($"Поля: {string.Join("; ", tp.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static).Select(x => x.FieldType + " " + x.Name))}");
            
            Console.WriteLine();
            Console.WriteLine("Создаем экземпляр");
            var myClassInstance = Activator.CreateInstance(tp, new object[] { "Petya", 10 });
            Console.WriteLine(myClassInstance);

            Console.WriteLine();
            Console.WriteLine("Вызываем метод Wallet()");
            var myClassMethod = tp.GetMethod("Wallet");
            myClassMethod.Invoke(myClassInstance, new object[] {  25 });
            
            Console.WriteLine();
            Console.WriteLine("Переименовываем поле и изменяем значение свойства");
            var myClassField = tp.GetField("HiddenName");
            myClassField.SetValue(myClassInstance,"Aleh");
            var myClassProperty = tp.GetProperty("HiddenChips");
            myClassProperty.SetValue(myClassInstance,100);
            Console.WriteLine(myClassInstance);

            Console.WriteLine();
            Console.WriteLine("Выводим в консоль поле и свойство:");
            Console.WriteLine($"{myClassField.Name}: {myClassField.GetValue(myClassInstance)}");
            Console.WriteLine($"{myClassProperty.Name}: {myClassProperty.GetValue(myClassInstance)}");
        }

        private static void Types(Assembly lib)
        {
            Console.WriteLine($"Сборка {lib.FullName}:");
            Console.WriteLine();
            var types = lib.GetTypes();
            foreach (var tp in types)
            {
                Console.WriteLine(tp.FullName);
                Console.WriteLine($"{nameof(tp.IsClass)}: {tp.IsClass}");
                Console.WriteLine($"{nameof(tp.IsInterface)}: {tp.IsInterface}");
                Console.WriteLine($"{nameof(tp.IsArray)}: {tp.IsArray}");
                Console.WriteLine($"{nameof(tp.IsAbstract)}: {tp.IsAbstract}");
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }
}
