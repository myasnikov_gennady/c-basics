﻿using System;

namespace MyClass
{
    public class HiddenClass
    {
        public string HiddenName;
        public int HiddenChips { get; set; }

        public HiddenClass(string player, int quantity)
        {
            HiddenName = player;
            HiddenChips = quantity;
        }

        public void Wallet(int numberOfChips)
        {
            HiddenChips += numberOfChips;
            Console.WriteLine($"У игрока {HiddenName} на руках {HiddenChips} фишек");
        }

        public override string ToString()
        {
            return $"Игрок {HiddenName} имеет {HiddenChips} фишек";
        }
    }
}
