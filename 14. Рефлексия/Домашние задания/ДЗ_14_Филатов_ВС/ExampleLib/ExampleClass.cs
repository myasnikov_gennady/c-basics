﻿using System;

namespace ExampleLib
{
    public class ExampleClass
    {
        public string Name;
        public DateTime BirthDate { get; set; }
        public void GetInfo()
        {
            Console.WriteLine($"Имя: {Name}, дата рождения: {BirthDate.ToShortDateString()}");
        }
        public ExampleClass(string name, DateTime birthDate)
        {
            Name = name;
            BirthDate = birthDate;
        }
    }
}
