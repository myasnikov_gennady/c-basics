﻿using System;
using System.Linq;
using System.Reflection;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var lib = Assembly.LoadFrom("ExampleLib.dll");
            Metainfo(lib);
            var item = lib.GetType("ExampleLib.ExampleClass");

            Console.WriteLine("Cоздаем инстанс класса:");
            var instance = Activator.CreateInstance(item, new object[] { "Виктор", new DateTime(2001, 05, 12) });

            Console.WriteLine("Вызываем метод GetInfo");
            var method = item.GetMethod("GetInfo");
            method.Invoke(instance, new object[] { });

            Console.WriteLine("Задаем значения поля и свойства");
            var field = item.GetField("Name");
            field.SetValue(instance, "Павел");
            var property = item.GetProperty("BirthDate");
            property.SetValue(instance, new DateTime(1986, 04, 26));

            Console.WriteLine();
            Console.WriteLine("Выводим эти значения в консоль:");
            Console.WriteLine($"{field.Name}: {field.GetValue(instance)}");
            Console.WriteLine($"{property.Name}: {property.GetValue(instance)}");
            Console.WriteLine();

            Console.WriteLine("Вызываем повторно метод GetInfo");
            method.Invoke(instance, new object[] { });
        }
        private static void Metainfo(Assembly lib)
        {
            Console.WriteLine("Вывод метаинфориции dll-файла:");
            Console.WriteLine($"Сборка {lib.FullName}:");
            Console.WriteLine();
            var metainfo = lib.GetTypes();
            foreach (var item in metainfo)
            {
                Console.WriteLine(item.FullName);
                Console.WriteLine($"{nameof(item.IsClass)}: {item.IsClass}");
                Console.WriteLine($"{nameof(item.IsInterface)}: {item.IsInterface}");
                Console.WriteLine($"{nameof(item.IsArray)}: {item.IsArray}");
                Console.WriteLine($"{nameof(item.IsAbstract)}: {item.IsAbstract}");
                Console.WriteLine();
                Console.WriteLine($"Конструкторы: {string.Join("; ", item.GetConstructors().Select(x => x.Name + " " + string.Join(", ", x.GetParameters().Select(y => y.ParameterType.ToString()))))}");
                Console.WriteLine($"Методы:\n{string.Join("\n", item.GetMethods(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public).Select(x => x.Name + " с входящими параметрами типа: " + string.Join(", ", x.GetParameters().Select(y => y.ParameterType.ToString()))))}");
                Console.WriteLine($"Свойства: {string.Join("; ", item.GetProperties().Select(x => x.PropertyType + " " + x.Name))}");
                Console.WriteLine($"Поля: {string.Join("; ", item.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static).Select(x => x.FieldType + " " + x.Name))}");
            }
            Console.WriteLine();
        }
    }
}