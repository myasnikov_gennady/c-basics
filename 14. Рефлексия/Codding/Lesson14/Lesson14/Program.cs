﻿using System;
using System.Linq;
using System.Reflection;

namespace Lesson14
{
    class Program
    {
        static void Main(string[] args)
        {
            var typeClass = typeof(MyClass);
            var typeInterface = typeof(IMyClass);
            PrintTypeInfo(typeInterface);
            PrintTypeInfo(typeClass);
            Console.WriteLine();
            var myClassInstance = Activator.CreateInstance(typeClass);
            Console.WriteLine(myClassInstance);
            Console.WriteLine();
            myClassInstance = Activator.CreateInstance(typeClass, new object[] { "Hello World", 55 });
            Console.WriteLine(myClassInstance);
            var methodInfo = typeClass.GetMethod("MyMethod");
            methodInfo.Invoke(myClassInstance,new object[] { "Five"});
            Console.WriteLine(myClassInstance);

            //Console.Clear();
            foreach (var propertyInfo in typeClass.GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    propertyInfo.SetValue(myClassInstance,Guid.NewGuid().ToString());
                }
                Console.WriteLine($"{propertyInfo.Name}:{propertyInfo.GetValue(myClassInstance)}");
            }

            //var propertyInfo1 = typeof(MyClass1).GetProperty("MyFieldOne");
            //propertyInfo1.SetValue(myClassInstance,"Eight");
            //Console.WriteLine(myClassInstance);
        }

        private static void PrintTypeInfo(Type type)
        {
            var bindingFlags = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public |
                               BindingFlags.Static;
            Console.WriteLine(type.Assembly.FullName);
            Console.WriteLine(type.FullName);
            Console.WriteLine($"{nameof(type.IsClass)}: {type.IsClass}");
            Console.WriteLine($"{nameof(type.IsInterface)}: {type.IsInterface}");
            Console.WriteLine($"{nameof(type.IsArray)}: {type.IsArray}");
            Console.WriteLine($"{nameof(type.IsAbstract)}: {type.IsAbstract}");
            Console.WriteLine(string.Join("; ", type.GetConstructors().Select(x =>x.Name + " " + string.Join(", ", x.GetParameters().Select(y => y.ParameterType.ToString() + " " + y.Name)))));
            Console.WriteLine(string.Join("; ", type.GetProperties().Select(x=>x.PropertyType + " " + x.Name)));
            Console.WriteLine(string.Join("; ", type.GetFields(bindingFlags).Select(x=>x.FieldType + " " + x.Name)));

        }
    }
}
