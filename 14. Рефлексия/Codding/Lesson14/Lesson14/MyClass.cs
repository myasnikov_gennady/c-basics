﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson14
{
    public class MyClass : IMyClass
    {
        private string _myFieldTwo;
        public int MyFieldThree;

        public string MyFieldOne { get; set; }

        public MyClass()
        {
        }

        public MyClass(string text)
        {
            _myFieldTwo = text;
        }

        public MyClass(string text, int number)
        {
            _myFieldTwo = text;
            MyFieldThree = number;
        }

        public string MyMethod(string field)
        {
            _myFieldTwo = field;
            return _myFieldTwo;
        }

        public override string ToString()
        {
            return $"{MyFieldOne}, {_myFieldTwo}, {MyFieldThree}" ;
        }
    }
}
