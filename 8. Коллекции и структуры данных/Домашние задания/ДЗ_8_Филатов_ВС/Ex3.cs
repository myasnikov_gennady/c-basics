﻿using System;
using System.Collections.Generic;

namespace ДЗ_8_Филатов_ВС
{
    public class Ex3
    {
        public static void Run()
        {
            // 3. Объявить переменные типа: Stack<string>, Queue<string>. Каждая коллекция должна содержать 8 элементов

            var stack = new Stack<string>();
            var strings = new[] { "a", "b", "c", "d", "e", "f", "g", "h" };
            foreach (var node in strings)
            {
                stack.Push(node);
            }
            var queue = new Queue<string>();
            foreach (var node in strings)
            {
                queue.Enqueue(node);
            }

            Console.Write("Stack: ");
            foreach (var key in queue)
            {
                Console.Write(key);
            }
            Console.WriteLine();
            Console.Write("Queue: ");
            foreach (var key in stack)
            {
                Console.Write(key);
            }
            Console.WriteLine();
            Console.WriteLine();

            // 1. Добавить новую запись
            Console.WriteLine("Добавить новую запись");
            stack.Push("i");
            queue.Enqueue("i");

            Console.Write("Stack: ");
            foreach (var key in queue)
            {
                Console.Write(key);
            }
            Console.WriteLine();
            Console.Write("Queue: ");
            foreach (var key in stack)
            {
                Console.Write(key);
            }
            Console.WriteLine();
            Console.WriteLine();

            // 2. Получить запись без удаления из коллекции
            Console.WriteLine("Получить запись без удаления из коллекции");
            Console.WriteLine($"Получаемый крайний элемент: {stack.Peek()}");
            Console.Write("Stack: ");
            foreach (var key in queue)
            {
                Console.Write(key);
            }
            Console.WriteLine();

            Console.WriteLine($"Получаемый крайний элемент: {queue.Peek()}");
            Console.Write("Queue: ");
            foreach (var key in stack)
            {
                Console.Write(key);
            }
            Console.WriteLine();
            Console.WriteLine();

            // 3. Получить запись с удалением из коллекции
            Console.WriteLine("Получить запись с удалением из коллекции");
            stack.Pop();
            Console.Write("Stack: ");
            foreach (var key in queue)
            {
                Console.Write(key);
            }
            Console.WriteLine();

            queue.Dequeue();
            Console.Write("Queue: ");
            foreach (var key in stack)
            {
                Console.Write(key);
            }
            Console.WriteLine();
            Console.WriteLine();
        }
    }
}