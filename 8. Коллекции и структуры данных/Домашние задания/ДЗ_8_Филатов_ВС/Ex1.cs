﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ДЗ_8_Филатов_ВС
{
    public class Ex1
    {
        public static void Run()
        {
            // 1. Объявить переменные типов: List<string>, Collection<string>, LinkedList<string>.
            // Каждая коллекция должна содержать 10 элементов.

            var list = new List<string>();
            list.AddRange(new string[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j" });
            var collection = new Collection<string>();
            collection.Add("a");
            collection.Add("b");
            collection.Add("c");
            collection.Add("d");
            collection.Add("e");
            collection.Add("f");
            collection.Add("g");
            collection.Add("h");
            collection.Add("i");
            collection.Add("j");
            var linkedList = new LinkedList<string>();
            linkedList.AddFirst("a");
            linkedList.AddLast("b");
            linkedList.AddLast("c");
            linkedList.AddLast("d");
            linkedList.AddLast("e");
            linkedList.AddLast("f");
            linkedList.AddLast("g");
            linkedList.AddLast("h");
            linkedList.AddLast("i");
            linkedList.AddLast("j");

            Console.WriteLine("List: ");
            foreach (var item in list)
            {
                Console.Write(item);
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Collection: ");
            foreach (var item in collection)
            {
                Console.Write(item);
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("LinkedList: ");
            foreach (var item in linkedList)
            {
                Console.Write(item);
            }
            Console.WriteLine();
            Console.WriteLine();

            // 1. Добвить в колекцию новую запись. Добавить в середину коллекции новую запись.

            Console.WriteLine("Добвить в колекцию новую запись. Добавить в середину коллекции новую запись");
            list.Add("k");
            collection.Add("k");
            linkedList.AddLast("k");
            list.Insert(6, "z");
            collection.Insert(7, "z");
            linkedList.AddAfter(linkedList.Find("e"), "z");

            Console.WriteLine("List: ");
            foreach (var item in list)
            {
                Console.Write(item);
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Collection: ");
            foreach (var item in collection)
            {
                Console.Write(item);
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("LinkedList: ");
            foreach (var item in linkedList)
            {
                Console.Write(item);
            }
            Console.WriteLine();
            Console.WriteLine();

            //2.Удалить первую запись из коллекции.

            Console.WriteLine("Удалить первую запись из коллекции");
            list.RemoveAt(0);
            collection.RemoveAt(0);
            linkedList.RemoveFirst();

            Console.WriteLine("List: ");
            foreach (var item in list)
            {
                Console.Write(item);
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Collection: ");
            foreach (var item in collection)
            {
                Console.Write(item);
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("LinkedList: ");
            foreach (var item in linkedList)
            {
                Console.Write(item);
            }
            Console.WriteLine();
            Console.WriteLine();

            // 3.Удалить последнюю запись из коллекции.

            Console.WriteLine("Удалить последнюю запись из коллекции");
            list.RemoveAt(list.Count - 1);
            collection.RemoveAt(collection.Count - 1);
            linkedList.RemoveLast();

            Console.WriteLine("List: ");
            foreach (var item in list)
            {
                Console.Write(item);
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Collection: ");
            foreach (var item in collection)
            {
                Console.Write(item);
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("LinkedList: ");
            foreach (var item in linkedList)
            {
                Console.Write(item);
            }
            Console.WriteLine();
            Console.WriteLine();

            // 4.Удалить произвольную запись из коллекции.
            Console.WriteLine("Удалить произвольную запись из коллекции");
            var random = list.GetRandom();
            list.Remove(random);
            collection.Remove(random);
            linkedList.Remove(random);

            Console.WriteLine("List: ");
            foreach (var item in list)
            {
                Console.Write(item);
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Collection: ");
            foreach (var item in collection)
            {
                Console.Write(item);
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("LinkedList: ");
            foreach (var item in linkedList)
            {
                Console.Write(item);
            }
            Console.WriteLine();
            Console.WriteLine();
        }
    }
}
