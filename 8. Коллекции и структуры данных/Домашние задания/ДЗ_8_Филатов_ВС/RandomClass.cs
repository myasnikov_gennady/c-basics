﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ДЗ_8_Филатов_ВС
{
    public static class RandomMemeber
    {
        // как можно реализовать алгоритм по-другому? решение отправить .cs файл по адресу job.myasnikov.gennady@gmail.com
        private static readonly Random random = new Random();

        public static T GetRandom<T>(this IEnumerable<T> collection)
        {
            if (collection.Count() - 1 == 0)
                return default(T);
            return collection.ElementAt(random.Next(collection.Count() - 1));
        }
    }
}