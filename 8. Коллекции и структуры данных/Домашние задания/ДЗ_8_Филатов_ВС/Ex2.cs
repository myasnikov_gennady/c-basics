﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ДЗ_8_Филатов_ВС
{
    public class Ex2
    {
        public static void Run()
        {
            // 2.Объявить переменные типа: Dictionary<string, string>, Hashtable.
            // Каждая коллекция должна содержать 10 элементов.

            var dict = new Dictionary<string, string>
            {
                { "key 1", "one"},
                { "key 2", "two"},
                { "key 3", "three"},
                { "key 4", "four"},
                { "key 5", "five"},
                { "key 6", "six"},
                { "key 7", "seven"},
                { "key 8", "eight"},
                { "key 9", "nine"},
                { "key 10", "ten"}
            };

            var hashtable = new Hashtable()
            {
                { "key 1", "one"},
                { "key 2", "two"},
                { "key 3", "three"},
                { "key 4", "four"},
                { "key 5", "five"},
                { "key 6", "six"},
                { "key 7", "seven"},
                { "key 8", "eight"},
                { "key 9", "nine"},
                { "key 10", "ten"}
            };

            Console.WriteLine("Dicionary:");
            foreach (var item in dict)
            {
                Console.WriteLine($"{item.Key}: {item.Value}");
            }
            Console.WriteLine();
            Console.WriteLine("Hashtable:");
            foreach (DictionaryEntry item in hashtable)
            {

                Console.WriteLine($"{item.Key}: {item.Value}");
            }
            Console.WriteLine();

            // 1. Добвить в колекцию новую запись.
            Console.WriteLine("Добвить в колекцию новую запись");
            dict.Add("key 11", "eleven");
            hashtable.Add("key 11", "eleven");
            Console.WriteLine("Dicionary:");
            foreach (var item in dict)
            {
                Console.WriteLine($"{item.Key}: {item.Value}");
            }
            Console.WriteLine();
            Console.WriteLine("Hashtable:");
            foreach (DictionaryEntry item in hashtable)
            {

                Console.WriteLine($"{item.Key}: {item.Value}");
            }
            Console.WriteLine();

            // 2. Удалить запись из коллекции.
            Console.WriteLine("Удалить запись из коллекции");
            dict.Remove("key 8");
            hashtable.Remove("key 2");
            Console.WriteLine("Dicionary:");
            foreach (var item in dict)
            {
                Console.WriteLine($"{item.Key}: {item.Value}");
            }
            Console.WriteLine();
            Console.WriteLine("Hashtable:");
            foreach (DictionaryEntry item in hashtable)
            {
                Console.WriteLine($"{item.Key}: {item.Value}");
            }
            Console.WriteLine();

            // 3. Получить произвольную запись из коллекции.
            Console.WriteLine("Получить произвольную запись из коллекции");
            var random = dict.GetRandom();
            Console.WriteLine($"Random Dicionary key-value pair: {random.Key}: {random.Value}");
            var randomHashKey = hashtable.Keys.Cast<string>().ToList().GetRandom();
            Console.WriteLine($"Random Hashtable key-value pair: {randomHashKey}: {hashtable[randomHashKey]}");
            Console.WriteLine();
        }
    }
}