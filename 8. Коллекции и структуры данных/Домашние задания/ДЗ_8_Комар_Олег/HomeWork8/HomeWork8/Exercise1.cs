﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace HomeWork8
{
    public class Exercise1
    {
        public static void Run()
        {
            Console.WriteLine("Task One");
            Console.WriteLine();
            WorkWithList();
            WorkWithCollection();
            WorkWithLinkedList();
        }

        private static void WorkWithList()
        {
            var list = new List<string>() { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten" };
            ConsoleHelper.Info("List<T> original:");
            ListToConsole(list);
            Console.WriteLine();
            list.Add("eleven");
            ConsoleHelper.Info("List<T> Add() record:");
            ListToConsole(list);
            Console.WriteLine();
            list.Insert(5, "twelve");
            ConsoleHelper.Info("List<T> Insert() record:");
            ListToConsole(list);
            Console.WriteLine();
            list.RemoveAt(0);
            ConsoleHelper.Info("List<T> RemoveAt(0):");
            ListToConsole(list);
            Console.WriteLine();
            list.RemoveAt(list.Count-1);
            ConsoleHelper.Info("Remove last record from List<T>:");
            ListToConsole(list);
            Console.WriteLine();
            var element = list.GetRandom();
            list.Remove(element);
            ConsoleHelper.Info("Remove random record from List<T>:");
            ConsoleHelper.Param($"Random value ",$"'{element}'");
            ListToConsole(list);
            Console.WriteLine();
            ConsoleHelper.Clear();
        }

        private static void WorkWithCollection()
        {
            var collection = new Collection<string>() { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten" };
            ConsoleHelper.Info("Collection<T> original:");
            CollectionToConsole(collection);
            Console.WriteLine();
            collection.Add("eleven");
            ConsoleHelper.Info("Collection<T> Add() record:");
            CollectionToConsole(collection);
            Console.WriteLine();
            collection.Insert(8, "twelve");
            ConsoleHelper.Info("Collection<T> Insert() record:");
            CollectionToConsole(collection);
            Console.WriteLine();
            collection.RemoveAt(0);
            ConsoleHelper.Info("Collection<T> RemoveAt(0):");
            CollectionToConsole(collection);
            Console.WriteLine();
            collection.RemoveAt(collection.Count - 1);
            ConsoleHelper.Info("Remove last record from Collection<T>:");
            CollectionToConsole(collection);
            Console.WriteLine();
            var element = collection.GetRandom();
            collection.Remove(element);
            ConsoleHelper.Info("Remove random record from Collection<T>:");
            ConsoleHelper.Param($"Random value ", $"'{element}'");
            CollectionToConsole(collection);
            Console.WriteLine();
            ConsoleHelper.Clear();
        }
        private static void WorkWithLinkedList()
        {
            var linkedList = new LinkedList<string>();
            var nodes = new[] {"one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"};
            foreach (var node in nodes)
            {
                linkedList.AddLast(node);
            }
            ConsoleHelper.Info("LinkedList<T> original:");
            LinkedListToConsole(linkedList);
            Console.WriteLine();
            linkedList.AddLast("eleven");
            ConsoleHelper.Info("LinkedList<T> AddLast():");
            LinkedListToConsole(linkedList);
            Console.WriteLine();
            linkedList.AddFirst("zero");
            ConsoleHelper.Info("LinkedList<T> AddFirst():");
            LinkedListToConsole(linkedList);
            Console.WriteLine();
            var search = linkedList.Find("two");
            linkedList.AddAfter(search ?? throw new InvalidOperationException(), "twelve");
            ConsoleHelper.Info("LinkedList<T> AddAfter():");
            LinkedListToConsole(linkedList);
            Console.WriteLine();
            linkedList.RemoveFirst();
            ConsoleHelper.Info("LinkedList<T> RemoveFirst():");
            LinkedListToConsole(linkedList);
            Console.WriteLine();
            linkedList.RemoveLast();
            ConsoleHelper.Info("Remove last node from LinkedList<T>:");
            LinkedListToConsole(linkedList);
            Console.WriteLine();
            var element = linkedList.GetRandom();
            linkedList.Remove(element);
            ConsoleHelper.Info("Remove random node from LinkedList<T>:");
            ConsoleHelper.Param($"Random value ", $"'{element}'");
            LinkedListToConsole(linkedList);
            Console.WriteLine();
            ConsoleHelper.Clear();
        }
        private static void ListToConsole(List<string> nameList)
        {
            foreach (var record in nameList)
            {
                Console.Write($"{record} ");
            }
        }
        private static void CollectionToConsole(Collection<string> nameCollection)
        {
            foreach (var record in nameCollection)
            {
                Console.Write($"{record} ");
            }
        }

        private static void LinkedListToConsole(LinkedList<string> nameLinkedList)
        {
            foreach (var record in nameLinkedList)
            {
                Console.Write($"{record} ");
            }
        }
    }
}
