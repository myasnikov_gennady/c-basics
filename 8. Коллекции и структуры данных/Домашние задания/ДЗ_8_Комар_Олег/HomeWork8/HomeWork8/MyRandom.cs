﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork8
{
    public static class MyRandom
    {
        // как можно реализовать алгоритм по-другому? решение отправить .cs файл по адресу job.myasnikov.gennady@gmail.com
        private static readonly Random _rnd = new Random();

        public static T GetRandom<T>(this IEnumerable<T> collection)
        {
            var max = collection.Count() - 1;
            var pos = _rnd.Next(0,max);
            return pos == 0 ? collection.First() : collection.Skip(pos).First();
        }
    }
}