﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace HomeWork8
{
    public static class Exercise2
    {
        public static void Run()
        {
            Console.WriteLine("Task Two");
            Console.WriteLine();
            WorkWithDictionary();
            WorkWithHashtable();
        }

        private static void WorkWithDictionary()
        {
            var dictionary = new Dictionary<string, string>()
            {
                { "key 1", "one"},
                { "key 2", "two"},
                { "key 3", "three"},
                { "key 4", "four"},
                { "key 5", "five"},
                { "key 6", "six"},
                { "key 7", "seven"},
                { "key 8", "eight"},
                { "key 9", "nine"},
                { "key 10", "ten"}
            };
            ConsoleHelper.Info("Original Dictionary");
            CollectionToConsole(dictionary);
            ConsoleHelper.Info("Add KeyValuePair");
            var addKey = "key 11";
            try
            {
                dictionary.Add(addKey, "eleven");
                CollectionToConsole(dictionary);
            }
            catch (ArgumentException)
            {
                Console.WriteLine($"An element with Key = '{addKey}' already exists.");
            }
            var removeKey = "key 8";
            ConsoleHelper.Param($"Remove KeyValuePair with key ", $"'{removeKey}'");
            if (dictionary.ContainsKey(removeKey))
            {
                dictionary.Remove(removeKey);
            }
            else
            {
                Console.WriteLine($"Key '{removeKey}' is not found.");
            }
            CollectionToConsole(dictionary);
            var element = dictionary.GetRandom();
            dictionary.Remove(element.Key);
            ConsoleHelper.Param($"Remove random KeyValuePair ", $"'{element}'");
            CollectionToConsole(dictionary);
            ConsoleHelper.Clear();
        }
        private static void WorkWithHashtable()
        {
            var hashtable = new Hashtable()
            {
                { "key 1", "one"},
                { "key 2", "two"},
                { "key 3", "three"},
                { "key 4", "four"},
                { "key 5", "five"},
                { "key 6", "six"},
                { "key 7", "seven"},
                { "key 8", "eight"},
                { "key 9", "nine"},
                { "key 10", "ten"}
            };
            ConsoleHelper.Info("Original Hashtable");
            HashtableToConsole(hashtable);
            ConsoleHelper.Info("Add KeyValuePair");
            var addKey = "key 11";
            try
            {
                hashtable.Add(addKey, "eleven");
            }
            catch (ArgumentException)
            {
                Console.WriteLine($"An element with Key = '{addKey}' already exists.");
            }
            HashtableToConsole(hashtable);
            var removeKey = "key 7";
            ConsoleHelper.Param($"Remove KeyValuePair with key ", $"'{removeKey}'");
            if (hashtable.ContainsKey(removeKey))
            {
                hashtable.Remove(removeKey);
            }
            else
            {
                Console.WriteLine($"Key '{removeKey}' is not found.");
            }
            HashtableToConsole(hashtable);
            var element = hashtable.Keys.Cast<string>().ToList().GetRandom();
            ConsoleHelper.Param($"Remove KeyValuePair with random key ", $"'{element}'");
            hashtable.Remove(element);
            HashtableToConsole(hashtable);
            ConsoleHelper.Clear();
        }
        private static void CollectionToConsole(Dictionary<string, string> nameDictionary)
        {
            foreach (var kvp in nameDictionary)
            {
                Console.WriteLine($"{kvp.Key}: {kvp.Value}");
            }
        }
        private static void HashtableToConsole(Hashtable nameHashtable)
        {
            foreach (DictionaryEntry kvp in nameHashtable)
            {

                Console.WriteLine($"{kvp.Key}: {kvp.Value}");
            }
        }

        // можно реализовать вот такой метод
        private static void Print(IDictionary dictionary)
        {
            foreach(DictionaryEntry item in dictionary)
            {
                Console.WriteLine($"{item.Key}:{item.Value}");
            }
        }
    }
}
