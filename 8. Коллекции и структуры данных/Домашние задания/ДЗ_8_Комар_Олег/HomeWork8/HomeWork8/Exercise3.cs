﻿using System;
using System.Collections.Generic;

namespace HomeWork8
{
    public class Exercise3
    {
        public static void Run()
        {
            Console.WriteLine("Task Three");
            Console.WriteLine();
            WorkWithStack();
            WorkWithQueue();
        }

        private static void WorkWithStack() 
        {
            var stack = new Stack<string>();
            var strings = new[] { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten" };
            foreach (var node in strings)
            {
                stack.Push(node);
            }
            ConsoleHelper.Info("Stack<T> original:");
            StackToConsole(stack);
            ConsoleHelper.Param($"Stack<T>.Peek() - ", $"'{stack.Peek()}'");
            Console.WriteLine("Stack<> after Peek():");
            StackToConsole(stack);
            ConsoleHelper.Param($"Stack<T>.Pop() - ", $"'{stack.Pop()}'");
            Console.WriteLine("Stack<> after Pop():");
            StackToConsole(stack);
            ConsoleHelper.Clear();
        }

        private static void WorkWithQueue()
        {
            var queue = new Queue<string>();
            var strings = new[] { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten" };
            foreach (var node in strings)
            {
                queue.Enqueue(node);
            }
            ConsoleHelper.Info("Queue<T> original:");
            QueueToConsole(queue);
            ConsoleHelper.Param($"Queue<T>.Peek() - ", $"'{queue.Peek()}'");
            Console.WriteLine("Queue<T> after Peek():");
            QueueToConsole(queue);
            ConsoleHelper.Param($"Queue<T>.Pop() - ", $"'{queue.Dequeue()}'");
            Console.WriteLine("Queue<T> after DeQueue():");
            QueueToConsole(queue);
        }

        private static void StackToConsole(Stack<string> nameSatck)
        {
            foreach (var key in nameSatck)
            {
                Console.WriteLine(key);
            }
        }
        private static void QueueToConsole(Queue<string> nameSatck)
        {
            foreach (var key in nameSatck)
            {
                Console.WriteLine(key);
            }
        }
    }
}
