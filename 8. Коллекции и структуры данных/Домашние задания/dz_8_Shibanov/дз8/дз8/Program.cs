﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace дз8
{
    class Program
    {
        static void Main(string[] args)
        {
            var list = new List<string>();
            var collection = new Collection<string>();
            list.Add("qwe");
            list.AddRange(new string[] { "fdgdg" });
            foreach (var item in list) 
            {
                Console.WriteLine(item);
            }
          
            list = new List<string>(new string[] { "sdvfsb"});
            foreach (var item in list)
            {
                Console.WriteLine(item);
            }
            

            Console.WriteLine("Collection");
            collection.Add("gfnf");
          
            foreach (var item in collection)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine(collection[0]);
            collection = new Collection<string>(new string[] { "cbxcnb" });
            foreach (var item in collection)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("LinkedList");
            var linkedlist = new LinkedList<string>(new string[] { "vbxfh"});
            foreach (var item in linkedlist) 
            {
                Console.WriteLine(item);
            }


            Console.WriteLine("2 punkt");

            var dictionary = new Dictionary<string, string>() 
            {
                { "key one", "asf"},
                { "key two", "asfaf"}
            };
            dictionary.Add("key three", "asfdg");
            foreach (var item in dictionary)
            {
                Console.WriteLine($"{item.Key}:{ item.Value}");
            }

            Console.WriteLine("3 punkt");
            Console.WriteLine("Stack");
            var stack = new Stack<string>();
            stack.Push("xbbfd");
            foreach (var item in stack) 
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("Queue");
            var queue = new Queue<string>();
            queue.Enqueue("hfghj");
            foreach (var item in queue) 
            {
                Console.WriteLine(item);
            }
        }
    }
}
