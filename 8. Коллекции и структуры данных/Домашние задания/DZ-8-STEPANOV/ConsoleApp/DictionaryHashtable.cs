﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp
{
    public class DictionaryHashtable
    {
        public static void StartDictionaryHashtable()
        {
            var dictionaey = new Dictionary<string, string>()
            {
                {"1", "DField1" },
                {"2", "DField2" },
                {"3", "DField3" },
                {"4", "DField4" },
                {"5", "DField5" },
                {"6", "DField6" },
                {"7", "DField7" },
                {"8", "DField8" },
                {"9", "DField9" },
                {"10", "DField10" }
            };

            var hashtable = new Hashtable()
            {
                {"1", "HField1" },
                {"2", "HField2" },
                {"3", "HField3" },
                {"4", "HField4" },
                {"5", "HField5" },
                {"6", "HField6" },
                {"7", "HField7" },
                {"8", "HField8" },
                {"9", "HField9" },
                {"10", "HField10" }
            };

            var key = RandomRetriver.GetRandom<string>(hashtable.Keys.Cast<string>());
            var item = hashtable[key];

            Console.WriteLine("All elemens in Dictionary, Hashtable");
            PrintDictionary(dictionaey);
            PrintHashTable(hashtable);

            Console.WriteLine();
            Console.WriteLine("Add new element in Dictionary, Hashtable");
            dictionaey.Add("11", "DField11");
            hashtable.Add("11", "HField11");

            Console.WriteLine();
            PrintDictionary(dictionaey);
            Console.WriteLine();
            PrintHashTable(hashtable);
            Console.WriteLine("\n");

            Console.WriteLine();
            Console.WriteLine("Delete element from Dictionary, Hashtable");
            dictionaey.Remove("1");
            hashtable.Remove("1");

            Console.WriteLine();
            PrintDictionary(dictionaey);
            Console.WriteLine();
            PrintHashTable(hashtable);
            Console.WriteLine("\n");

            Console.WriteLine();
            Console.WriteLine("Delete random element from Dictionary, Hashtable");
            dictionaey.Remove(key);
            hashtable.Remove(key);

            Console.WriteLine();
            PrintDictionary(dictionaey);
            Console.WriteLine();
            PrintHashTable(hashtable);
            Console.WriteLine("\n");
                                 
        }
        public static void PrintDictionary(Dictionary<string, string> _dictionary)
        {
            Console.WriteLine("Elements of dictionary:");
            foreach (var item in _dictionary)
            {
                Console.WriteLine(item + " ");
            }
        }
        public static void PrintHashTable(Hashtable _hashtable)
        {
            Console.WriteLine("Elements of hashtable:");
            foreach (DictionaryEntry item in _hashtable)
            {
                Console.WriteLine($"[{item.Key}, {item.Value}] ");
            }
        }
    }
}
