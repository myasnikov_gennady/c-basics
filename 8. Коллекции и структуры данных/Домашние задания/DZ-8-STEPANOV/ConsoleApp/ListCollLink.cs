﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace ConsoleApp
{
    public class ListCollLink
    {
        public static void StartListCollLink()
        {
            var list = new List<string>() { "LField1", "LField2", "LField3", "LField4", "LField5", "LField6", "LField7", "LField8", "LField9", "LField10" };
            var collection = new Collection<string>() { "CField1", "CField2", "CField3", "CField4", "CField5", "CField6", "CField7", "CField8", "CField9", "CField10" };
            string[] strll = new[] { "LLField1", "LLField2", "LLField3", "LLField4", "LLField5", "LLField6", "LLField7", "LLField8", "LLField9", "LLField10" };
            var linkedList = new LinkedList<string>(strll);
            Random rnd = new Random();


            Console.WriteLine("Basic data of list, collection, linkedlist:");
            PrintList(list);
            Console.WriteLine();
            PrintCollection(collection);
            Console.WriteLine();
            PrintLinkedList(linkedList);
            Console.WriteLine();
            Console.WriteLine("\n");

            Console.WriteLine("Add new elements in List, Collection, LinkedList");
            list.Add("LField11");
            collection.Add("CField11");
            linkedList.AddLast("LLField11");

            PrintList(list);
            Console.WriteLine();
            PrintCollection(collection);
            Console.WriteLine();
            PrintLinkedList(linkedList);
            Console.WriteLine("\n");

            Console.WriteLine("Add new elements in middle of List, Collection, LinkedList");
            list.Insert((list.Count/2), "LFieilMiddle");
            collection.Insert((collection.Count / 2), "CFieilMiddle");
            linkedList.AddAfter(linkedList.Find("LLField5"), "LLFieilMiddle");
            
            PrintList(list);
            Console.WriteLine();
            PrintCollection(collection);
            Console.WriteLine();
            PrintLinkedList(linkedList);
            Console.WriteLine("\n");

            Console.WriteLine();
            Console.WriteLine("Delete 1st element in List, Collection, LinkedList");
            list.RemoveAt(0);
            collection.RemoveAt(0);
            linkedList.RemoveFirst();

            PrintList(list);
            Console.WriteLine();
            PrintCollection(collection);
            Console.WriteLine();
            PrintLinkedList(linkedList);
            Console.WriteLine("\n");

            Console.WriteLine();
            Console.WriteLine("Delete last element in List, Collection, LinkedList");
            list.RemoveAt(list.Count-1);
            collection.RemoveAt(collection.Count-1);
            linkedList.RemoveLast();

            PrintList(list);
            Console.WriteLine();
            PrintCollection(collection);
            Console.WriteLine();
            PrintLinkedList(linkedList);
            Console.WriteLine("\n");

            Console.WriteLine();
            Console.WriteLine("Delete random element in List, Collection, LinkedList");
            list.RemoveAt(rnd.Next(0,list.Count-1));
            collection.RemoveAt(rnd.Next(0, collection.Count-1));
            linkedList.Remove(linkedList.Find(strll[rnd.Next(1, linkedList.Count)]));

            PrintList(list);
            Console.WriteLine();
            PrintCollection(collection);
            Console.WriteLine();
            PrintLinkedList(linkedList);
            Console.WriteLine("\n");

            Console.WriteLine();
            Console.WriteLine("Take random element in List, Collection, LinkedList");

            Console.WriteLine(list[rnd.Next(0, list.Count)]);
            Console.WriteLine(collection[rnd.Next(0, collection.Count)]);
            Console.WriteLine(strll[rnd.Next(0, linkedList.Count)]);
            

        }

        public static void PrintList(List<string> _list)
        {
            Console.WriteLine("Elements of list:");
            foreach (var item in _list)
            {
                Console.Write(item + " ");
            }
        }
        public static void PrintCollection(Collection<string> _collection)
        {
            Console.WriteLine("Elements of collection:");
            foreach (var item in _collection)
            {
                Console.Write(item + " ");
            }
        }

        public static void PrintLinkedList(LinkedList<string> _linkedList)
        {
            Console.WriteLine("Elements of linkedList:");
            foreach (var item in _linkedList)
            {
                Console.Write(item + " ");
            }
        }



    }
}
