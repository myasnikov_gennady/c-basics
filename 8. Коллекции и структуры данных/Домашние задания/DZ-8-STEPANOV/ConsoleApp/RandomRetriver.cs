﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp
{
    public static class RandomRetriver
    {
        private static Random rnd = new Random();
        public static T GetRandom<T>(IEnumerable<T> collection )
        {
            var index = 0;
            var randomIndex = rnd.Next(0, collection.Count() - 1);
            foreach (var item in collection)
            {
                if (index == randomIndex)
                {
                    return item;
                }
                index++;
            }
            return default;

        }

    }
}
