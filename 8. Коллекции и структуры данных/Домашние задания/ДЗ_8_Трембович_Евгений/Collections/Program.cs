﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Collections
{
    class Program
    {
        static void Main(string[] args)
        {
            //1. Declaring variables.
            var list = new List<string> { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
            Console.WriteLine("Elements of list:");
            PrintListToConsole(list);

            var collection = new Collection<string> { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
            Console.WriteLine("\n\nElements of collection:");
            PrintCollectionToConsole(collection);

            var linkedList = new LinkedList<string>(new string[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" });
            Console.WriteLine("\n\nElements of linked list:");
            PrintLinkedListToConsole(linkedList);


            //1.1. Adding elements.
            list.Add("11");
            Console.WriteLine("\n\nElements of list after adding item:");
            PrintListToConsole(list);

            collection.Add("11");
            Console.WriteLine("\n\nElements of collection after adding item:");
            PrintCollectionToConsole(collection);

            linkedList.AddLast("11");
            Console.WriteLine("\n\nElements of linked list after adding item:");
            PrintLinkedListToConsole(linkedList);

            list.Insert(list.Count / 2, "middle");
            Console.WriteLine("\n\nElements of list after adding element in the middle:");
            PrintListToConsole(list);

            collection.Insert(collection.Count / 2, "middle");
            Console.WriteLine("\n\nElements of collection after adding element in the middle:");
            PrintCollectionToConsole(collection);

            LinkedListNode<string> firstNodeOfLinkedList = linkedList.First;
            LinkedListNode<string> middleNode = firstNodeOfLinkedList;
            for (int i = 0; i < (linkedList.Count / 2) - 1; i++)
            {
                middleNode = middleNode.Next;
            }
            linkedList.AddAfter(middleNode, "middle");
            Console.WriteLine("\n\nElements of linked list after adding element in the middle:");
            PrintLinkedListToConsole(linkedList);


            //1.2. Removing first elements.
            list.RemoveAt(0);
            Console.WriteLine("\n\nElements of list after removing first item:");
            PrintListToConsole(list);

            collection.RemoveAt(0);
            Console.WriteLine("\n\nElements of collection after removing first item:");
            PrintCollectionToConsole(collection);

            linkedList.RemoveFirst();
            Console.WriteLine("\n\nElements of linked list after removing first item:");
            PrintLinkedListToConsole(linkedList);

            //1.3. Removing last elements.
            list.RemoveAt(list.Count - 1);
            Console.WriteLine("\n\nElements of list after removing last item:");
            PrintListToConsole(list);

            collection.RemoveAt(collection.Count - 1);
            Console.WriteLine("\n\nElements of collection after removing last item:");
            PrintCollectionToConsole(collection);

            linkedList.RemoveLast();
            Console.WriteLine("\n\nElements of linked list after removing last item:");
            PrintLinkedListToConsole(linkedList);


            //1.4. Removing random element
            var rdm = new Random();
            int rdmIndexForList = rdm.Next(list.Count);
            list.Remove(list[rdmIndexForList]);
            Console.WriteLine("\n\nElements of list after removing random item:");
            PrintListToConsole(list);

            int rdmIndexForCollection = rdm.Next(collection.Count);
            collection.Remove(collection[rdmIndexForCollection]);
            Console.WriteLine("\n\nElements of collection after removing random item:");
            PrintCollectionToConsole(collection);

            LinkedListNode<string> firstNodeOfLinkedListForRandomOperation = linkedList.First;
            LinkedListNode<string> randomNodeOfLinkedList = firstNodeOfLinkedListForRandomOperation;
            int rdmIndexForLinkedList = rdm.Next(linkedList.Count);
            if (rdmIndexForLinkedList == 0)
            {
                randomNodeOfLinkedList = linkedList.First;
            }
            else
            {
                for (int i = 0; i < rdmIndexForLinkedList; i++)
                {
                    randomNodeOfLinkedList = randomNodeOfLinkedList.Next;
                }
            }
            linkedList.Remove(randomNodeOfLinkedList);
            Console.WriteLine("\n\nElements of linked list after removing random item:");
            PrintLinkedListToConsole(linkedList);


            //1.5. Get random element

            Console.WriteLine($"\n\nRandom element of list: {list[rdm.Next(list.Count)]}");

            Console.WriteLine($"\nRandom element of collection: {collection[rdm.Next(collection.Count)]}");

            LinkedListNode<string> firstNodeOfLinkedListForRandomOperation2 = linkedList.First;
            LinkedListNode<string> randomNodeOfLinkedList2 = firstNodeOfLinkedListForRandomOperation2;
            int rdmIndexForLinkedList2 = rdm.Next(linkedList.Count);
            if (rdmIndexForLinkedList2 == 0)
            {
                randomNodeOfLinkedList2 = linkedList.First;
            }
            else
            {
                for (int i = 0; i < rdmIndexForLinkedList2; i++)
                {
                    randomNodeOfLinkedList2 = randomNodeOfLinkedList2.Next;
                }
            }
            Console.WriteLine($"\nRandom element of linked list: {randomNodeOfLinkedList2.Value}");


            //2. Declaring Dictionary<string, string>, Hashtable variables.
            var dictionary = new Dictionary<string, string>
            {
                { "key1", "value1" },
                { "key2", "value2" },
                { "key3", "value3" },
                { "key4", "value4" },
                { "key5", "value5" },
                { "key6", "value6" },
                { "key7", "value7" },
                { "key8", "value8" },
                { "key9", "value9" },
                { "key10", "value10"}

            };
            Console.WriteLine("\nElements of dictionary:");
            PrintDictionaryToConsole(dictionary);

            var hashtable = new Hashtable(dictionary);
            Console.WriteLine("\nElements of hashtable:");
            PrintHashtableToConsole(hashtable);


            //2.1 Adding elements.
            dictionary.Add("key11", "value11");
            Console.WriteLine("\nElements of dictionary after adding item:");
            PrintDictionaryToConsole(dictionary);

            hashtable.Add("key11", "value11");
            Console.WriteLine("\nElements of hashtable after adding item:");
            PrintHashtableToConsole(hashtable);


            //2.2 Removing elements.
            dictionary.Remove("key11");
            Console.WriteLine("\nElements of dictionary after removing item:");
            PrintDictionaryToConsole(dictionary);

            hashtable.Remove("key11");
            Console.WriteLine("\nElements of hashtable after removing item:");
            PrintHashtableToConsole(hashtable);


            //2.3 Get random element
            string randomKeyValue = dictionary.Keys.ElementAt(rdm.Next(dictionary.Count));
            Console.WriteLine($"\nRandom element of dictionary: {randomKeyValue} - {dictionary[randomKeyValue]}");

            var arrayKeys = new string[hashtable.Count];
            hashtable.Keys.CopyTo(arrayKeys, 0);
            string randomKey = arrayKeys[rdm.Next(hashtable.Count)];
            Console.WriteLine($"\nRandom element of hashtable: {randomKey} - {hashtable[randomKey]}");


            //3. Declaring Stack<string>, Queue<string> variables.
            Stack<string> stack = new Stack<string>();
            for(int i = 1; i <= 5; i++)
            {
                stack.Push(i.ToString());
            }
            Console.WriteLine("\nElements of stack:");
            PrintStackToConsole(stack);

            Queue<string> queue = new Queue<string>();
            for (int i = 1; i <= 5; i++)
            {
                queue.Enqueue(i.ToString());
            }
            Console.WriteLine("\n\nElements of queue:");
            PrintQueueToConsole(queue);


            //3.1 Adding element
            stack.Push("6");
            Console.WriteLine("\n\nElements of stack after adding item:");
            PrintStackToConsole(stack);

            queue.Enqueue("6");
            Console.WriteLine("\n\nElements of queue after adding item:");
            PrintQueueToConsole(queue);


            //3.2 Get element whithout removing
            Console.WriteLine($"\n\nGetting element - {stack.Peek()}");
            Console.WriteLine($"Elements of stack after getting item whithout removing");
            PrintStackToConsole(stack);

            Console.WriteLine($"\n\nGetting element - {queue.Peek()}");
            Console.WriteLine($"Elements of queue after getting item whithout removing");
            PrintQueueToConsole(queue);


            //3.3 Get element whit removing
            Console.WriteLine($"\n\nGetting element - {stack.Pop()}");
            Console.WriteLine($"Elements of stack after getting item whith removing");
            PrintStackToConsole(stack);

            Console.WriteLine($"\n\nGetting element - {queue.Dequeue()}");
            Console.WriteLine($"Elements of queue after getting item whith removing");
            PrintQueueToConsole(queue);
            Console.WriteLine();
        }
        public static void PrintListToConsole(List<string> list)
        {
            foreach (string item in list)
            {
                Console.Write($"{item}   ");
            }
        }
        public static void PrintCollectionToConsole(Collection<string> collection)
        {
            foreach (string item in collection)
            {
                Console.Write($"{item}   ");
            }
        }

        public static void PrintLinkedListToConsole(LinkedList<string> linkedList)
        {
            foreach (string item in linkedList)
            {
                Console.Write($"{item}   ");
            }
        }
        public static void PrintDictionaryToConsole(Dictionary<string, string> dictionary)
        {
            foreach (KeyValuePair<string, string> keyValue in dictionary)
            {
                Console.WriteLine(keyValue.Key + " - " + keyValue.Value);
            }
        }
        public static void PrintHashtableToConsole(Hashtable hashtable)
        {
            ICollection keys = hashtable.Keys;
            foreach (string keyValue in keys)
            {
                Console.WriteLine(keyValue + " - " + hashtable[keyValue]);
            }
        }
        public static void PrintStackToConsole(Stack<string> stack)
        {
            foreach (string item in stack)
            {
                Console.Write($"{item}   ");
            }
        }
        public static void PrintQueueToConsole(Queue<string> queue)
        {
            foreach (string item in queue)
            {
                Console.Write($"{item}   ");
            }
        }
    }
}
