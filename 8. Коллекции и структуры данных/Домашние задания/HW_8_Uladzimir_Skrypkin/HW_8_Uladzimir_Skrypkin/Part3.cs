﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW_8_Uladzimir_Skrypkin
{
    class Part3
    {
        public static void Stack_Queue()
        {
            var stack = new Stack<string>();
            var arraystack = new [] {"stack one", "stack two", "stack three", "stack four", "stack five", "stack six", "stack seven", "stack eight", "stack nine", "stack ten", };
            var queue = new Queue<string>();
            var arrayqueue = new [] { "queue one", "queue two", "queue three", "queue four", "queue five", "queue six", "queue seven", "queue eight", "queue nine", "queue ten", };
            foreach (var item in arraystack) { stack.Push(item);}
            foreach (var item in arrayqueue) { queue.Enqueue(item);}
            Console.WriteLine("Добавление новой записи: Stack, Queue");
            Print3(stack, queue); 
            Console.WriteLine();
            Console.WriteLine("Получение записи без удаления из коллекции: Stack, Queue");
            Console.WriteLine($"Запись из Stack:='{stack.Peek()}', запись из Queue:='{queue.Peek()}'");
            Print3(stack, queue);
            Console.WriteLine();
            Console.WriteLine("Получение записи с удалением из коллекции: Stack, Queue");
            Console.WriteLine($"Запись из Stack:='{stack.Pop()}', запись из Queue:='{queue.Dequeue()}'");
            Print3(stack, queue);
            Console.WriteLine();
        }
        public static void Print3(Stack<string> stack, Queue<string> queue)
        {
            foreach (var item in stack) { Console.Write($"{item} "); }
            Console.WriteLine();
            foreach (var item in queue) { Console.Write($"{item} "); }
            Console.WriteLine();
        }
    }
}
