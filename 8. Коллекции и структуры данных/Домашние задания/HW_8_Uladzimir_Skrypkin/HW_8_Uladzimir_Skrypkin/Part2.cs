﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW_8_Uladzimir_Skrypkin
{
    class Part2
    {
        public static void Dictionary_Hashtable()
        {
            var dictionary = new Dictionary<string, string> {
                { "dictionary 1","element 1" }, { "dictionary 2", "element 2" }, { "dictionary 3", "element 3" }, { "dictionary 4", "element 4" },
                { "dictionary 5", "element 5" }, { "dictionary 6", "element 6" }, { "dictionary 7", "element 7" }, { "dictionary 8", "element 8" },
                { "dictionary 9", "element 9" }, { "dictionary 10", "element 10" }
            };
            var hashtable = new Hashtable() {    { "hashtable 1","item 1" }, { "hashtable 2", "item 2" }, { "hashtable 3", "item 3" }, { "hashtable 4", "item 4" },
                { "hashtable 5", "item 5" }, { "hashtable 6", "item 6" }, { "hashtable 7", "item 7" }, { "hashtable 8", "item 8" },
                { "hashtable 9", "item 9" }, { "hashtable 10", "item 10" }
            };
            Console.WriteLine("Объявленные переменные коллекций: Dictionary,Hashtable");
            Print2(dictionary, hashtable);
            Console.WriteLine();
            Console.WriteLine("Добавление в коллекции новой записи:");
            // The Add method throws an exception if the new key is
            // already in the hash table.
            try
            {
                dictionary.Add("dictionary new", "element new");
            }
            catch
            {
                Console.WriteLine("An element with Key = \"dictionary new\" already exists.");
            }
            try
            {
                hashtable.Add("hashtable new", "item new");
            }
            catch
            {
                Console.WriteLine("An element with Key = \"hashtable new\" already exists.");
            }
            Print2(dictionary, hashtable);
            Console.WriteLine();
            Console.WriteLine("Удаление в коллекции записи:");
            if(dictionary.ContainsKey("dictionary new"))
            {
                dictionary.Remove("dictionary new");
            }
            else
            {
                Console.WriteLine("An element with Key = \"dictionary new\" doesn't exists.");
            }
            if (hashtable.ContainsKey("hashtable new"))
            {
                hashtable.Remove("hashtable new");
            }
            else
            {
                Console.WriteLine("An element with Key = \"hashtable new\" doesn't exists.");
            }
            Print2(dictionary, hashtable);
            Console.WriteLine();
            Console.WriteLine("Получение рандомной записи из коллекции:");
            Random rnd = new Random();  
            var r5 = dictionary.ElementAt(rnd.Next(1, dictionary.Count));
            Console.WriteLine($"Произвольная запись из Dictionary: {r5}");
            var myTargetArray = new String[hashtable.Count*2];            
            hashtable.Keys.CopyTo(myTargetArray, hashtable.Count- hashtable.Count);     // для чего тут вызывается этот метод? для чего тут разница двух одинаковых переменных? это дополнительные операции.
            hashtable.Values.CopyTo(myTargetArray, hashtable.Count);
            var r6 = rnd.Next(hashtable.Count- hashtable.Count, hashtable.Count-1);     // для чего тут разница двух одинаковых переменных? это дополнительные операции.
            Console.WriteLine($"Произвольная запись из hashtable: {myTargetArray[r6]}, {myTargetArray[r6 + hashtable.Count]}");     // произвольную запись можно получить по ключу (hashtable[randomKey]), а ключ получить случайным образом из массива
            Console.WriteLine();
            // В hashtable.Keys.CopyTo - вызывается чтобы скопировать в массив keys c начала массива => (0) - hashtable.Count- hashtable.Count - только чтобы показать откуда он взялся
            // hastable не копирует в массив встроенной функцией сразу keys - соответствующим value; в массиве лежит с 0-9 - keys, с 10-18 - values (соответствующей разницей в hashtable.Count- соотв key=cooтв value)
           // Console.WriteLine($"Произвольная запись из hashtable: {hashtable[myTargetArray[r6]]},{hashtable[myTargetArray[r6 + 10]]}");  // - выводит только value и ограничивает массив размером hashtable не давая считать key 

        }

        public static void Print2(Dictionary<string, string> dictn, Hashtable table)
        {
            foreach (var item in dictn) { Console.WriteLine($"{item} "); }
            Console.WriteLine();
            foreach (DictionaryEntry item in table)
            {
                Console.WriteLine($"{item.Key},{item.Value}");
            }
            Console.WriteLine();
        }
    } 
}
