﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using static HW_8_Uladzimir_Skrypkin.Part1;
using static HW_8_Uladzimir_Skrypkin.Part2;
using static HW_8_Uladzimir_Skrypkin.Part3;

namespace HW_8_Uladzimir_Skrypkin
{
    class Program
    {
        static void Main(string[] args)
        {
            Collection();
            Dictionary_Hashtable();
            Stack_Queue();
        }
    }
}
