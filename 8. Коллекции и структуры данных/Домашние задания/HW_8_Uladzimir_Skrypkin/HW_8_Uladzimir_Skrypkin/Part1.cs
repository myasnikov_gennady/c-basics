﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW_8_Uladzimir_Skrypkin
{
    public class Part1
    {
        public static void Collection()
        {
            var list = new List<string>() { "List one", "List two", "List three", "List four", "List five", "List six", "List seven", "List eight", "List nine", "List ten" };
            var collection = new Collection<string>() { "Coll one", "Coll two", "Coll three", "Coll four", "Coll five", "Coll six", "Coll seven", "Coll eight", "Coll nine", "Coll ten" };
            string[] _item = new[] { "LnList one", "LnList two", "LnList three", "LnList four", "LnList five", "LnList six", "LnList seven", "LnList eight", "LnList nine", "LnList ten" };
            LinkedList<string> linkedList = new LinkedList<string>(_item);

            Console.WriteLine("Объявленные переменные коллекций: List, Collection, LinkedList");
            Print(list, collection, linkedList);
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Добавление новой записи и добавить в середину:"); 
            list.Add("элемент 11"); 
            collection.Add("элемент 11"); 
            linkedList.AddLast("элемент 11");
            list.Insert(5, "элемент 11"); 
            collection.Insert(5, "элемент 11");
            var a = linkedList.Find("LnList six");
            linkedList.AddBefore(a, "элемент 11");
            Print(list, collection, linkedList);
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Удаление первой записи в коллекции:");
            list.RemoveAt(0);
            collection.RemoveAt(0);
            linkedList.Remove("LnList one");
            Print(list, collection, linkedList);
            Console.WriteLine();
            Console.WriteLine("Удаление последней записи в коллекции:");
            list.RemoveAt(list.Count-1);
            collection.RemoveAt(collection.Count-1);
            linkedList.RemoveLast();
            Print(list, collection, linkedList);
            Console.WriteLine();
            Console.WriteLine("Удаление произвольной записи в коллекции:");
            var rnd = new Random();
            list.RemoveAt(rnd.Next(0,list.Count-1));       // рандомное число создается
            collection.RemoveAt(rnd.Next(0, collection.Count-1)); // рандомное число создается
            var r= linkedList.Find(_item[rnd.Next(1, linkedList.Count)]); // рандомный  string создается
            linkedList.Remove(r);
            Print(list, collection, linkedList);
            Console.WriteLine();
            Console.WriteLine("Получение произвольной записи из коллекции:");
            var r1 = list[rnd.Next(0, list.Count-1)];       
            Console.WriteLine($"Произвольная запись из List: {r1}");
            var r2 = collection[rnd.Next(0, collection.Count-1)];
            Console.WriteLine($"Произвольная запись из Collection: {r2}");
            Console.WriteLine($"Произвольная запись из linkedList: {_item[rnd.Next(1, linkedList.Count)]}");
            Console.WriteLine();
        }
        public static void Print(List<string> _list, Collection<string> coll, LinkedList<string> link)
        {
            foreach (var item in _list) { Console.Write($"{item} "); }
            Console.WriteLine();
            foreach (var item in coll) { Console.Write($"{item} "); }
            Console.WriteLine();
            foreach (var item in link) { Console.Write($"{item} "); }
            Console.WriteLine();
        }
    } 
}
