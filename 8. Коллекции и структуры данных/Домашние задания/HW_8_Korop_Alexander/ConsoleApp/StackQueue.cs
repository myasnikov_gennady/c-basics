﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp
{
    public class StackQueue
    {
        public static void StartStackQueue()
        {
            var stack = new Stack<string>();
            var queue = new Queue<string>();
            Console.WriteLine();
            Console.WriteLine("Add new element in Stack, Queue:");
            stack.Push("SField1");
            stack.Push("SField2");
            stack.Push("SField3");
            queue.Enqueue("QField1");
            queue.Enqueue("QField2");
            queue.Enqueue("QField3");

            PrintStack(stack);
            Console.WriteLine();
            PrintQueue(queue);
            Console.WriteLine("\n");

            Console.WriteLine("Take element without delete it in Stack, Queue:");            
            Console.WriteLine($"Element form stack: {stack.Peek()}");
            Console.WriteLine($"Element form queue: {queue.Peek()}");
                        
            PrintStack(stack);
            Console.WriteLine();
            PrintQueue(queue);
            Console.WriteLine("\n");

            Console.WriteLine("Take element with delete it in Stack, Queue:");
            Console.WriteLine($"Element form stack: {stack.Pop()}");
            Console.WriteLine($"Element form queue: {queue.Dequeue()}");

            PrintStack(stack);
            Console.WriteLine();
            PrintQueue(queue);
            Console.WriteLine("\n");
        }

        public static void PrintStack(Stack<string> _stack)
        {
            Console.WriteLine("Elements of stack:");
            foreach (var item in _stack)
            {
                Console.Write(item + " ");
            }
        }
        public static void PrintQueue(Queue<string> _queue)
        {
            Console.WriteLine("Elements of queue:");
            foreach (var item in _queue)
            {
                Console.Write(item + " ");
            }
        }
    }
}
