﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            var list = new List<int>();
            var collection = new Collection<int>();
            list.Add(1);
            list.AddRange(new int[] {1, 2, 3 });
            foreach (var item in list)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine(list[2]);
            Console.WriteLine();
            list = new List<int>(new int[] { 1, 2, 3, 4, 5, 6 });
            foreach (var item in list)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("Работа с коллекциями");

            collection.Add(1);
           
            foreach (var item in collection)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine(collection[0]);
            Console.WriteLine();
            collection = new Collection<int>(new int[] { 1, 2, 3, 4, 5, 6 });
            foreach (var item in collection)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("Работа со словарями");
            var dictionary = new Dictionary<string, int>()
            {
                { "key one", 1},
                { "key two", 1}
            };
            dictionary.Add("key three", 3);
            foreach (var item in dictionary)
            {
                Console.WriteLine($"{item.Key}:{item.Value}");
            }
            Console.WriteLine($"{dictionary["key two"]}");
            //dictionary = new Dictionary<string, int>()
            //{
            //    { "key one", 1},
            //    { "key one", 2}
            //};
            if (dictionary.TryGetValue("key one", out var value))
            {
                Console.WriteLine(value);
            }
            if (dictionary.ContainsKey("key one"))
            {
                Console.WriteLine(dictionary["key one"]);
            }
            //dictionary.Clear();
            //Console.WriteLine("clear");
            //foreach (var item in dictionary)
            //{
            //    Console.WriteLine($"{item.Key}:{item.Value}");
            //}
            foreach (var key in dictionary.Keys)
            {
                Console.WriteLine(key);
                Console.WriteLine(dictionary[key]);
            }
            dictionary.Remove("key one");
            foreach (var key in dictionary.Keys)
            {
                Console.WriteLine(key);
                Console.WriteLine(dictionary[key]);
            }
            Console.WriteLine("Работа со стеком");
            var stack = new Stack<int>();
            Console.WriteLine("Stack push");
            stack.Push(1);
            stack.Push(2);
            stack.Push(3);
            foreach (var key in stack)
            {
                Console.WriteLine(key);
            }
            Console.WriteLine("Stack pop");
            var i = stack.Pop();
            foreach (var key in stack)
            {
                Console.WriteLine(key);
            }
            Console.WriteLine("Работа с очередями");
            var queue = new Queue<int>();
            queue.Enqueue(1);
            queue.Enqueue(2);
            queue.Enqueue(3);
            foreach (var key in queue)
            {
                Console.WriteLine(key);
            }
            Console.WriteLine(" Работа с Dequeue");
            var k = queue.Dequeue();
            foreach (var key in queue)
            {
                Console.WriteLine(key);
            }
            Console.WriteLine(" Работа с hashtable");
            var hashtable = new Hashtable();
            hashtable.Add("key one", 1);
            hashtable.Add("key two", 2);
            foreach (var key in hashtable.Keys)
            {
                Console.WriteLine(key);
            }
            Console.WriteLine(" Работа с hashsets");
            var hashset = new HashSet<int>();
            hashset.Add(1);
            hashset.Add(1);
            foreach (var key in hashset)
            {
                Console.WriteLine(key);
            }
            Console.WriteLine(" Работа с arraylist");
            var arraylist = new ArrayList();
            arraylist.Add(1);
            arraylist.Add(3);
            arraylist.Add(2);
            arraylist.Add(5);
            arraylist.Add(4);
            arraylist.Add("six");
            foreach (var key in arraylist)
            {
                Console.WriteLine(key);
            }
            for(var index =0;index<arraylist.Count;index++)
            {
                Console.WriteLine(arraylist[index]);
            }
            arraylist.Sort(new MyComparer());

            Console.WriteLine(" Работа с arraysort");
            foreach (var key in arraylist)
            {
                Console.WriteLine(key);
            }
            Console.WriteLine();
            for (var index = 0; index < arraylist.Count; index++)
            {
                Console.WriteLine(arraylist[index]);
            }
            
        }
    }
}
