﻿using System.Collections.Generic;

namespace Algorithm_by_Oleg
{
    class Bread
    {
        public List<PieceOfBread> BreadPieces { get; set; }

        public Bread()
        {
            BreadPieces = new List<PieceOfBread>()
            {
                new PieceOfBread(),
                new PieceOfBread(),
                new PieceOfBread(),
                new PieceOfBread()
            };
        }
    }
}
