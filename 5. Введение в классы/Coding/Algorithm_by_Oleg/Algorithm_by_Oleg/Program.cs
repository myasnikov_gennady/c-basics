﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Algorithm_by_Oleg
{
    class Program
    {
        static void Main()
        {
            var bread = new Bread();
            var butters = new List<Butter>()
            {
                new Butter(),
                new Butter(),
                new Butter()
            };
            var knife = new Knife();
            var piecesOfBread = new List<PieceOfBread>();
            do
            {
                var pieceOfBread = knife.MakePiece(bread);
                piecesOfBread.Add(pieceOfBread);
            } while (piecesOfBread.Count < 3);

            foreach (var pieceOfBread in piecesOfBread)
            {
                var lastPieceOfButter = butters.Last();
                butters.Remove(lastPieceOfButter);
                knife.SpreadButter(pieceOfBread, lastPieceOfButter);
            }

            Console.WriteLine("Количество хлеба осталось: " + bread.BreadPieces.Count);
            Console.WriteLine("Количество масла осталось: " + butters.Count);
            Console.WriteLine("Количество кусочков хлеба с маслом: " + piecesOfBread.Count);
        }
    }
}
