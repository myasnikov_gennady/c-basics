﻿using System;
using System.Linq;

namespace Algorithm_by_Oleg
{
    class Knife
    {
        public PieceOfBread MakePiece(Bread bread)
        {
            if (bread.BreadPieces.Count > 0)
            {
                var lastPiece = bread.BreadPieces.Last();
                bread.BreadPieces.Remove(lastPiece);
                return lastPiece;

            }

            Console.WriteLine("Нельзя порезать хлеб, его мало");
            return null;
        }

        public PieceOfBread SpreadButter(PieceOfBread pieceOfBread, Butter butter)
        {
            pieceOfBread.Butter = butter;
            return pieceOfBread;
        }
    }
}
