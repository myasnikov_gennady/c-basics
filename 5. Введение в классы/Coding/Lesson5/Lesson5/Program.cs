﻿using System;
using ClassLibrary;

namespace Lesson5
{
    class Program
    {
        static void Main()
        {
            Class1.Dsmth();
            var a = new Class1();
            Console.WriteLine(a.ToString());
            Console.WriteLine(a.ToString());
            a.Name = "Goodbye";
            Console.WriteLine(a.ToString());
            var b = a.Name;
            Console.WriteLine(a.ToString());
            a.ResetCounters();
            Console.WriteLine(a.ToString());
            GC.SuppressFinalize(a);
            GC.Collect();
            Console.WriteLine("End");
        }
    }
}
