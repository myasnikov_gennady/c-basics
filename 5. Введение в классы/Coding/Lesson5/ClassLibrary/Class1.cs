﻿using System;

namespace ClassLibrary
{
    public class Class1
    {
        private string _name;
        private int _counterGet;
        private int _counterSet;
        public string Name
        {
            get
            {
                _counterGet++;
                return _name;
            }
            set
            {
                _counterSet++;
                _name = value;
            }
        }
        public Class1()
        {
            Console.WriteLine("Не статический конструктор вызван");
            _name = "Hello People";

        }

        static Class1()
        {
            Console.WriteLine("Статический конструктор вызван");
        }

        public override string ToString()
        {
            return $"{_name}, {_counterGet}, {_counterSet}";
        }

        public void ResetCounters()
        {
            _counterGet = 0;
            _counterSet = 0;
        }

        public static void Dsmth()
        {
            Console.WriteLine("Статический метод вызван");
        }

        ~Class1()
        {
            Console.WriteLine("Деструктор вызван");
        }

    }
}
