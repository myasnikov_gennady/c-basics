﻿using System;
using System.Collections.Generic;

namespace Algorithm
{
    internal class Quantity<T> where T : new()
        {
        public List<T> Pieces { get; }
        private readonly Random _rnd = new Random();

        protected Quantity()
        {
            Pieces = new List<T>();
            var count = _rnd.Next(1, 10);   // от 1, т.к. у нас даны и хлеб и масло, т.е. хоть на 1 кусочек должно быть
            for (var i = 0; i < count; i++)
            {
                Pieces.Add(new T());
            }
        }
    }
}
