﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Algorithm
{
    internal static class Program
    {
        private static void Main()
        {
            /*            Задача.
             *    Дано: нож, хлеб и масло.
             *    Требуется: сделать 3 бутерброда с маслом.
             *    Сколько точно имеется хлеба и масла неизвестно.
            */
            var quantityOfSandwich = 3;
            var sandwich = 0;

            var knife = new Knife();
            knife.Notify += Knife_Notify;

            var bread = new Bread();
            Console.WriteLine($"Изначально имеем хлеба на {bread.Pieces.Count} кусочков");
            try
            {
                if (bread.Pieces.Count < quantityOfSandwich)
                    throw new Exception("Хлеба не хватает для нужного количества бутербродов");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Ошибка: {e.Message}");
            }

            var butter = new Butter();
            Console.WriteLine($"Изначально имеем масла на {butter.Pieces.Count} кусочков");
            try
            {
                if (butter.Pieces.Count < quantityOfSandwich)
                    throw new Exception("Масла не хватает для нужного количества бутербродов");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Ошибка: {e.Message}");
            }

            quantityOfSandwich = Math.Min(3, Math.Min(bread.Pieces.Count, butter.Pieces.Count));

            var piecesOfBread = new List<PieceOfBread>();
            do
            {
                var pieceOfBread = knife.MakePieceBread(bread);
                piecesOfBread.Add(pieceOfBread);
            } while (piecesOfBread.Count < quantityOfSandwich);

            var piecesOfButter = new List<PieceOfButter>();
            do
            {
                var pieceOfButter = knife.MakePieceButter(butter);
                piecesOfButter.Add(pieceOfButter);
            } while (piecesOfButter.Count < quantityOfSandwich);

            foreach (var pieceOfBread in piecesOfBread)
            {
                var lastPieceOfButter = piecesOfButter.Last();
                knife.SpreadButter(pieceOfBread, lastPieceOfButter);
                piecesOfButter.Remove(lastPieceOfButter);
                sandwich++;
            }

            Console.WriteLine("Количество хлеба осталось: " + bread.Pieces.Count);
            Console.WriteLine("Количество масла осталось: " + butter.Pieces.Count);
            Console.WriteLine("Количество бутербродов с маслом: " + sandwich);
            if (sandwich < 3)
            {
                Console.WriteLine("Мы не смогли сделать необходимое количество бутербродов, т.к. изначально не хватало нужных ингредиентов");
            }
            knife.Notify -= Knife_Notify;
        }

        private static void Knife_Notify(string message)
        {
            Console.WriteLine(message);
        }
    }
}
