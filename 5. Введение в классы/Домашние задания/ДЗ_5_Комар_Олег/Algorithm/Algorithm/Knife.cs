﻿using System.Linq;

namespace Algorithm
{
    internal class Knife
    {
        public delegate void ActionHandler(string message);
        public event ActionHandler Notify;
        public PieceOfBread MakePieceBread(Bread bread)
        {
            if (bread.Pieces.Count > 0)
            {
                var lastPiece = bread.Pieces.Last();
                bread.Pieces.Remove(lastPiece);
                Notify?.Invoke($"Отрезали кусок хлеба");
                return lastPiece;
            }
            Notify?.Invoke($"Хлеба не осталось");
            return null;
        }

        public PieceOfButter MakePieceButter(Butter butter)
        {
            if (butter.Pieces.Count > 0)
            {
                var lastPieceOfButter = butter.Pieces.Last();
                butter.Pieces.Remove(lastPieceOfButter);
                Notify?.Invoke($"Отрезали кусочек масла");
                return lastPieceOfButter;
            }
            Notify?.Invoke($"Масла не осталось");
            return null;
        }
        
        public PieceOfBread SpreadButter(PieceOfBread pieceOfBread, PieceOfButter pieceOfButter)
        {
                pieceOfBread.PieceOfButter = pieceOfButter;
                Notify?.Invoke($"Намазали кусок хлеба маслом");
                return pieceOfBread;
        }
    }
}
