﻿using System;
using System.Diagnostics;
using Library;

namespace StudentClasses
{
    public static class Program
    {
        private static void Main()
        {
            var aleh = new Student(); //Студент, созданный с помощью конструктора без параметров, не имеет автомобиля
            Console.WriteLine();

            var dasha = new Student("Даша", 3, true, "Renault", "Clio");
            Console.WriteLine();

            var vadzim = new Student("Вадим", 2, false, "Audi", "A6");
            Console.WriteLine();

            Console.WriteLine($"Вадим на каком курсе учится?");
            Console.WriteLine($"На {vadzim.WriteCourse()}-м");

            Console.WriteLine("Даша получает стипендию? - " + Student.DaNet(dasha.Scholarship));

            Console.WriteLine();

            Console.WriteLine("Количество студентов - " + Student.Count);
            Console.WriteLine();


            // Пример свойства с аксессорами
            var dog = new Dogs("Шарик", 10);
            dog.ToConsole();
            Console.WriteLine();

            // Индексаторы
            var andrew = new User {["Name"] = "Андрей", ["Last Name"] = "Иванов"};
            Console.WriteLine("Имя Иванова - " + andrew["Name"]);
            var oleg = new User() {["Name"] = "Олег", ["Last Name"] = "Комар"};
            Console.WriteLine("Фамилия Олега - " + oleg["Last Name"]);




        }

    }
    

}
