﻿using System;

namespace Library
{
    public class Student
    {
        private readonly string _name;
        private int _course;
        public static int Count;
        public bool Scholarship { get; }
        private readonly Cars _car = new Cars("Peugeot", "406");

        private int Course
        {
            get
            {
                return _course;
            }
            set
            {
                _course = value;
            }
        }


        public Student()
        {
            _name = "Андрей";
            Course = 1;
            Scholarship = true;
            Count++;
            Print();
        }

        public Student(string name, int course, bool scholarship, string make, string model)
        {
            _name = name;
            Course = course;
            Scholarship = scholarship;
            _car.Make = make;
            _car.Model = model;
            Count++;
            Print();
            Console.WriteLine($"Этот студент ездит на {_car.Make} {_car.Model}");
        }

        private void Print()
        {
            Console.WriteLine("Имя студента " + _name);
            Console.WriteLine("Курс - " + Course);
            Console.WriteLine("Получает стипендию - " + DaNet(Scholarship));
        }

        public static string DaNet(bool par)    // Методы и переменные желательно называть понятными именами на английском языке (хорошие манеры, на оценку не влияет)
        {
            switch (par)
            {
                case true:
                    return "Да";
                case false:
                    return "Нет";
            }
        }

        public int WriteCourse()
        {
            return Course;
        }

         private protected bool IsScholarchipPp()
        {
            return Scholarship;
        }

        protected  bool IsScholarchipP()
        {
            return Scholarship;
        }

        internal bool IsScholarchipI()
        {
            return Scholarship;
        }

        protected internal bool IsScholarchipPi(bool scholarship)
        {
            return scholarship;
        }
    }
}
