﻿namespace Library
{
    // Индексатор
    public class User
    {
        private string _name;
        private string _lastName;

        public string this[string property]
        {
            get
            {
                switch (property)
                {
                    case "Name": return _name;
                    case "Last Name": return _lastName;
                    default: return null;
                }
            }
            set
            {
                switch (property)
                {
                    case "Name":
                        _name = value;
                        break;
                    case "Last Name":
                        _lastName = value;
                        break;

                }
            }
        }
    }
}
