﻿namespace Library
{
    internal class Cars // Чаще всего имя класса объявляется в единственном числе, т.к. отображает его сущность. (На оценку не влияет, т.к. об этом я не говорил)
    {
        private string _make;
        private string _model;

        internal string Make
        {
            get => _make;
            set => _make = value;
        }

        internal string Model
        {
            get => _model;
            set => _model = value;
        }

        internal Cars(string make, string model)
        {
            Make = make;
            Model = model;
        }
    }
}