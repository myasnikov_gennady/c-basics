﻿
using System;

namespace Library
{
    public class Dogs
    {
        private int Age { get; set; }
        private string Name { get; set; }

        public Dogs(string name, int age)
        {
            Name = name;
            Age = age;
        }

        public void ToConsole()
        {
            Console.WriteLine($"Собака по кличке {Name} возрастом {Age} лет");
        }

    }
}