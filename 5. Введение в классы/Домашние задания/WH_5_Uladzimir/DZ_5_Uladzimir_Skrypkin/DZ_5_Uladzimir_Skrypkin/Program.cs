﻿using System;
using ClassLibrary1;
using System.Reflection;

namespace DZ_5_Uladzimir_Skrypkin
{
  class Program  
  {
     static Class2.Notebook GetNotebook()
     {
       Random rnd = new Random();
       var notebook = new Class2.Notebook();
       notebook.age = rnd.Next(0, 30);                               // Рандомный срок эксплуатации
       notebook.company = "HP";
       notebook.owner_name = "Илон Маск";
       notebook.price_receipt = rnd.Next(100) < 50;              // Рандомный - наличие кассового чека
       notebook.warranty_time = 24;                             // Срок гарантии 24 месяца
        return notebook;
     }
     static Class2.Notebook.Details GetDetails()
     {
       Random rnd = new Random();
       var details = new Class2.Notebook.Details();            // Установка рандома на все детали true или false
       details.hard_drive = rnd.Next(100) < 50;
       details.keyboard = rnd.Next(100) < 50;
       details.display = rnd.Next(100) < 50;
       details.memory = rnd.Next(100) < 50;
       details.powersupply = rnd.Next(100) < 50;
       details.moterboard = rnd.Next(100) < 50;
       return details;
     }    
      static void Main(string[] args)
      {
        Class2.Notebook newnotebook = GetNotebook();
        Class2.Notebook.Details new_detail = GetDetails();
        Console.WriteLine("Прибытие в мастерскую по гарантийному обслуживанию ноутбука!");   // 1.Сдача ноутбука в сервис с неисправностью - администратору. 
        Console.WriteLine();
        Class2.Print(newnotebook);                              //2.Осмотр ноутбука и выдача администратором  акт приемки клиенту.
            //4.Разобрать ноутбук.
            //5.Поиск неисправности пока не будет найдена и устранить.
            //6.Собрать ноутбук.
        if ((newnotebook.age <= newnotebook.warranty_time) && (newnotebook.price_receipt == true))
        {
            Console.WriteLine();
            Console.WriteLine("Гарантийный талон и кассового чек в наличии - прием ноутбука");
            Console.WriteLine();
            Console.WriteLine("Передача ноутбука на диагностику");
            Console.WriteLine();
            Class2.PrintDetail(new_detail);
            Console.WriteLine();
            Type fieldsType = typeof(Class2.Notebook.Details);                                                // в коде отступы строк не должны быть, если это не блоки кода или последовательный вызов методов друг за другом (цепочкой)
            FieldInfo[] fields = fieldsType.GetFields(BindingFlags.Public | BindingFlags.Instance);           
            for (int i = 0; i < fields.Length; i++)
            {
               if ((bool)fields[i].GetValue(new_detail)==false)                                        // Вслучае false - изменить на  true - починить неисправность в ноутбуке по условию задачи
               {
                 Console.WriteLine();
                  Console.WriteLine($"Выявлена неистравность: {fields[i].Name}, статус {fields[i].GetValue(new_detail)}");
                  fields[i].SetValue(new_detail,true);                        
                  Console.WriteLine($"Выявленная неисправность: {fields[i].Name},  починено и заменено на статус {fields[i].GetValue(new_detail)}");
               }
            }
                //7.Позвонить клиенту и сообщить , что ноутбук отремонтирован.
                //8.В присутствии клиента проверить работоспособность или работает.    
                //9.Выдача ноутбука клиенту.
           Console.WriteLine();
           Console.WriteLine("Сообщение собственнику о починке  ноутбука, его проверка.");
           Console.WriteLine();
           Class2.PrintDetail(new_detail);
           Console.WriteLine();
           Console.WriteLine("Илон Маск доволен, ноутбук починен!!! - программа завершена успешно.");
        }
        else
        {
            Console.WriteLine();
            Console.WriteLine("Отказ в ремонте - нет кассового чека и (или) срок гарантийного ремонта истек");
            Console.WriteLine($"{newnotebook.owner_name} не доволен!    // Для успешного старта программы запустите еще раз!");
        }
      }
   }
}
