﻿using System;

namespace ClassLibrary1
{
  public class Class2
  {
     public class Notebook
     {
       public string company;
       public int warranty_time;
       public int age;
       public bool price_receipt;
       public string owner_name;
       public class Details
       {
         public bool hard_drive;
         public bool keyboard;
         public bool display;
         public bool memory;
         public bool powersupply;
         public bool moterboard;
       }
     }
     public static void Print(Notebook notebook)
     {
       Console.WriteLine(" Информация о ноутбуке: ");
       Console.WriteLine($" Производство компании: {notebook.company}");
       Console.WriteLine($" Гарантийный срок: {notebook.warranty_time} месяца ");
       Console.WriteLine($" Срок эксплуатиции покупателем: {notebook.age} месяцев  // При равном или меньше 24 месяцев - прием в мастерскую на диагностику и ремонт");
       Console.WriteLine($" Кассовый чек: {notebook.price_receipt} //  При наличии кассового чека - true - прием в мастерскую на диагностику и ремонт");
       Console.WriteLine($" Имя Собственника: {notebook.owner_name}");
     }
     public static void PrintDetail(Notebook.Details details)      
     {
       Console.WriteLine(" Тестирование исправности оборудования: ");
       Console.WriteLine($" Исправность жесткого диска: {details.hard_drive}");
       Console.WriteLine($" Истправность дисплея: {details.display}");
       Console.WriteLine($" Исправность клавиатуры: {details.keyboard}");
       Console.WriteLine($" Исправность памяти: {details.memory}");
       Console.WriteLine($" Исправность материнской карты: {details.moterboard}");
       Console.WriteLine($" Исправность системы питания: {details.powersupply}");
     }
  }
}