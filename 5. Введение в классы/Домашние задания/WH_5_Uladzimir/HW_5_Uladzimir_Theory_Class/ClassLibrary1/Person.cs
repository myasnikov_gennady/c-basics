﻿using System;

namespace ClassLibrary1
{
    public class Person
    {
        // 3.2 2 поля (public, private), 

        private string _name;
        private string _lastName;
        private int Age { get; set; }//3.3 1 авто-свойство, 
        internal bool Brain { get; }   
        private string _ship;
        private static string _company;

        //3.1 методы (по одному на модификатор доступа) 

        //3.4 1 свойство с настройкой геттера и сеттера для доступа к private полю.
        
        internal string Ship
        {
            get => _ship;
            set => _ship = value;

        }     
        public Person(string name, bool brain, string star, int age, string lastname, string company)
        {
            Console.WriteLine("Public метод вызван");
            _name = name;
            Ship = star;
            Console.WriteLine("Internal метод вызван");
            Brain = brain;
            _lastName = PP(lastname);         // обращение к private protected методу
            Age = P(age);                   // обращение к protected методу
            _company = PI(company);        // обращение к protected internal методу           
            Print();                     // Печать заданных характеристик вызовом private метода
        }

        protected int P(int age)
        {
            Console.WriteLine("Protected метод вызван");
            return age;
        }
        private protected string PP(string lastname)
        {
            Console.WriteLine("Private protected метод вызван");
            return lastname;
        }

        protected internal static string PI(string company)
        {
            Console.WriteLine("Protected internal метод вызван");
            return company;
        } 

        private  void Print()
        {
            Console.WriteLine("Private  метод вызван");
            Console.WriteLine();
            Console.WriteLine($"Имя:{_name}");
            Console.WriteLine($"Фамилия:{_lastName}");
            Console.WriteLine($"Возраст, лет:{Age}");
            Console.WriteLine($"Статус первого гражданина Марса:{Brain}");
            Console.WriteLine($"Владелец компании:{_company}");
            Console.WriteLine($"Запуск корабля:{_ship}  вперед!!!");

        }
    }
    internal class Class2
    {

    }
    class Class3
    {     
    }
}

