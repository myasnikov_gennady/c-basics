﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HW_1_In_Code
{
    class Program
    {
        static void Main(string[] args)
        {
            var hole = new Hole();
            var measuring = new MeasuringDevice();
            var rnd = new Randomiz();
            var drill = new Drill();        
            
            var augers = new List<Auger> { new Auger(), new Auger() };            
            
            //Требуемая глубина
            int desiderDepth;
            
            Console.WriteLine("Enter desider depth: ");
            EnterNumber:
            try
            {
                desiderDepth = int.Parse(Console.ReadLine());
            }
            catch (Exception)
            {
                Console.WriteLine("Enter correct number: ");
                goto EnterNumber;
            }

            hole.Depth = rnd.Randomizer(0, 20);

            var currenAuger = TakeAuger(augers);

            while(hole.Depth < desiderDepth && currenAuger != null)
            {
                if (currenAuger.Durability > 0)
                {                
                    if (currenAuger.Tempereture == "hot")
                    {
                        Console.WriteLine($"Temerature of auger is hot. You should wait 1 min");
                        currenAuger.Tempereture = "cold";
                        Console.WriteLine($"Now temerature of auger is cold. We can continue");
                    }
                    else
                    {
                        drill.Drilling(hole, currenAuger, rnd);
                        measuring.Measuring(hole);
                    }
                }
                else
                {
                    Console.WriteLine("Auger is broken");
                    currenAuger = TakeAuger(augers);
                }
            }

            if (hole.Depth < desiderDepth)
            {
                Console.WriteLine("We don't have augers");
            }
            else
            {
                Console.WriteLine($"Hole depth is {hole.Depth}. Thas's what we need!");

            }
                      

            Console.ReadKey();
        }

        public static Auger TakeAuger(List<Auger> augers)
        {
            if (augers.Count > 0)
            {
                var auger = augers.FirstOrDefault();
                augers.Remove(auger);
                return auger;
            }
            return null;
        }
    }
}
