﻿using System;

namespace ClassLibrary
{
    public class Class1
    {
        public string GetTheName()
        {
            return "Alex";
        }

        private int GetTheAge()
        {
            return 23;
        }

        internal void GetTheNewName(string newName)
        {
            Console.WriteLine(newName);
        }

        protected void ProtectedMethod()
        {
            Console.WriteLine("ProtectedInternalMethod");
        }

        protected internal void ProtectedInternalMethod()
        {
            Console.WriteLine("ProtectedInternalMethod");
        }

        private protected void PrivateProtectedMethod()
        {
            Console.WriteLine("PrivateProtectedMethod");
        }

        public string field1;
        private string field2;

        public string Autho_Property { get; set; }

        public string Property
        {
            get
            {
                return field2;
            }

            set
            {
                field2 = value;
            }
        }
    }    
}
