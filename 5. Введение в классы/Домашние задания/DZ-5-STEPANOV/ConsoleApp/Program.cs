﻿using ClassLibrary;
using System;

namespace ConsoleApp
{
    class Program   // Не засчитана. Скопирована у Коропа Александра
    {
        static void Main(string[] args)
        {
            //Создание экземпляра 
            var name = new Class1();                                

            //Вызов метода GetTheName
            Console.WriteLine($"Name is {name.GetTheName()}");

            //Присвоение полю field1 значения "Vlad"
            name.field1 = "Nikita";

            //Вывод нового имени на консоль путем обращения к полю field1
            Console.WriteLine(name.field1);

            //Вызов авто-свойства
            Console.WriteLine(name.Autho_Property);

            //Присвоение приватному полю значения
            name.Property = "Damir";

            //Чтение значения приватного поля
            Console.WriteLine(name.Property);
            
            Console.ReadKey();
        }
    }
}
