﻿using System;

namespace ClassLibrary1
{
    /*  нет private, internal, protected, protected internal, private protected методов
     *  
     */
    public class Class1
    {
        public string Name;


        private string name;
        private int counterGet;
        private int counterSet;
        public string w
        {
            get
            {
                counterGet++;
                return name;
            }
            set
            {
                counterSet++;
                name = value;
            }
        }
        public Class1()
        {
            name = "Hello user";
        }
        public override string ToString()
        {
            return $"{name}, {counterGet}, {counterSet}";
        }
        public string q { get; set; }   // публичные члены классов и методы пишутся с большой буквы. см https://ru.wikipedia.org/wiki/CamelCase




    }
}

    
