﻿using System;
using ClassLibrary1;

namespace ClassesAccessModifiersMethodsFieldsProperties
{
    class Program
    {
        static void Main(string[] args)
        {
            var PublicClass = new PublicClass();
            PublicClass.PublicMethod("Hello World!");
            PublicClass.PublicField = "Hello World!";
            Console.WriteLine(PublicClass.AutoProperty);
            PublicClass.Property = "C#";
            Console.WriteLine(PublicClass.Property);
        }
    }
}
