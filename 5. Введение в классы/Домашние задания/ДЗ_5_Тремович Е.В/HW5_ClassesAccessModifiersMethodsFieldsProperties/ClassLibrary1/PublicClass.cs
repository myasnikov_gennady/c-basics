﻿using System;

namespace ClassLibrary1
{
    public class PublicClass
    {
        //3.1 Methods with different acces modifiers  
        public void PublicMethod(string word)
        {
            Console.WriteLine(word);
        }

        private int PrivateMethod()
        {
            return 2 + 2;
        }

        internal string InternalMethod()
        {
            return "Internal method is run";
        }

        protected char[] ProtectedMethod()
        {
            return new char[5];
        }

        protected internal void ProtectedInternalMethod()
        {
            Console.WriteLine("Protected internal method is run");
        }

        private protected bool PrivateProtectedMethod()
        {
            return true;  
        }

        //3.2 Fields
        public string PublicField;
        private string PrivateField;

        //3.3 Auto property
        public string AutoProperty { get; set; }

        //3.4 Property  
        public string Property
        {
            get
            {
                return PrivateField;
            }
            set
            {
                PrivateField = value;
            }
        }
    }
}
