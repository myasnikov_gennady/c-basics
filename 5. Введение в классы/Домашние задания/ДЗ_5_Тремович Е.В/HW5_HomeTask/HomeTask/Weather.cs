﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeTask
{
    class Weather
    {
        private bool isSnowExist;

        public Weather()
        {
            Random gen = new Random();
            int prob = gen.Next(100);
            isSnowExist = prob <= 50;
        }

        public bool IsSnowExist()
        {
            return isSnowExist;
        }
    }
}
