﻿using System;

namespace HomeTask
{
    class Program
    {
        static void Main(string[] args)
        {
            DirtyCarpet dirtyCarpet = new DirtyCarpet();
            Weather weather = new Weather();
            Person person = new Person();
            person.LookAtTheWindow();
            person.CheckIsSnowExist(weather);
            person.TakeTheCarpetAndBeaterOutside();
            person.UnrollTheCarpet();
            person.KnockOutTheCarpet(dirtyCarpet);
            person.UnrollTheCarpet();
            person.BringTheCarpetAndBeaterHome();
            person.UnrollTheCarpet();
            Console.WriteLine($"Carpet contamination level after cleaning: {dirtyCarpet.CarpetContaminationLevel}%");
        }
    }
}
