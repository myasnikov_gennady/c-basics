﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeTask
{
    class DirtyCarpet
    {
        public int CarpetContaminationLevel;

        public DirtyCarpet()
        {
            Random gen = new Random();
            CarpetContaminationLevel = gen.Next(1, 100);
            Console.WriteLine($"Dirty carpet: contamination level={CarpetContaminationLevel}%");
        }
    }
}
