﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace HomeTask
{
    class Person
    {
        public void LookAtTheWindow()
        {
            Console.WriteLine("Person looking out the window");    
        }

        public void CheckIsSnowExist(Weather weather)
        {
            if (weather.IsSnowExist())
            {
                Console.WriteLine($"Is snow exist: {weather.IsSnowExist().ToString()}");
                RollUpTheCarpet();
            }
            else
            {
                WaitForSnow();
                weather = new Weather();
                Console.WriteLine("Snow appeared after waiting");
                RollUpTheCarpet();
            }
        }

        public void RollUpTheCarpet()
        {
            Console.WriteLine("Person roll up the carpet");
        }


        public void WaitForSnow()
        {
            Console.WriteLine("Waiting for snow...");
            Thread.Sleep(4000);
        }

        public void TakeTheCarpetAndBeaterOutside()
        {
            Console.WriteLine("Person take the carpet and beater outside");
        }

        public void UnrollTheCarpet()
        {
            Console.WriteLine("Person unrolls the carpet");
        }

        public void KnockOutTheCarpet(DirtyCarpet dirtyCarpet)
        {
            Console.WriteLine("Person knocks out the carpet");
            int HitCounter = 1;
            while (dirtyCarpet.CarpetContaminationLevel > 0)
            {
                Console.WriteLine($"Hit!({HitCounter})");
                dirtyCarpet.CarpetContaminationLevel--;
                HitCounter += 1;
            }
        }

        public void BringTheCarpetAndBeaterHome()
        {
            Console.WriteLine("Person brings a carpet and a beater home");
        }
    }
}
