﻿using ClassLibrary;
using System;

namespace Algorithm
{
    public class Program
    {
        static void Main()
        {
            var WorkingDay = new WorkingDay();
            WorkingDay.GetTime(WorkingDay);
            Console.WriteLine("***Начало смены***");
            var MillingMachine = new MillingMachine();
            var Worker = new Worker();
            var Counter = new Counter(0,0,0);
            var PieceDrawing = new PieceDrawing()
            {
                Lenght = 30,
                Width = 40,
                Heigt = 15,
                GrooveDepth = 5,
                GrooveLenght = 30,
                GrooveWidth = 10,
                GrooveInitialX = 0,
                GrooveInitialY = 25,
                MachiningTime = 25
            };
            PieceDrawing.GetPieceDimentions();
            PieceDrawing.GetGrooveDimentions();
            PieceDrawing.GetMachiningTime();
            Worker.TurnMachineOn(MillingMachine);
            Worker.ChooseMill(MillingMachine, PieceDrawing);
            Worker.InstallMill(MillingMachine);
            while (Counter.SummaryQuantity == 0)
            {
                Console.Write("Введите общее количество заготовок: ");
                bool WorkpieceQuantityResult = int.TryParse(Console.ReadLine(), out int _summaryQuantity);
                if (WorkpieceQuantityResult)
                {
                     Counter.SummaryQuantity = _summaryQuantity;
                    if (Counter.SummaryQuantity > 3)
                    {
                        Console.WriteLine($"Программу придется выполнить {Counter.SummaryQuantity} раз. Продолжить? y/n");
                        string largeQuantity = Console.ReadLine();
                        switch (largeQuantity)
                        {
                            case "n":
                                Counter.SummaryQuantity = 0;
                                break;
                            case "y":
                                break;
                            default:
                                Console.WriteLine("Введен неверный символ!");
                                break;
                        }
                    }
                    if ((PieceDrawing.MachiningTime * Counter.SummaryQuantity) > WorkingDay.Time)
                    {
                        Console.WriteLine($"Для обработки {Counter.SummaryQuantity} заготовок потребуется больше одной смены! Укажите меньшее количество.");
                        Counter.SummaryQuantity = 0;
                    }
                }
                else
                {
                    Console.WriteLine("Введены недопустимые символы!");
                }
            }
            Console.WriteLine($"Общее количество заготовок: {Counter.SummaryQuantity} шт.");
            while (Counter.SummaryQuantity > 0)
            {
                var Workpiece = new Workpiece(0, 0, 0, 0);
                Workpiece.GetWorkpieceDimentions(Workpiece);
                Worker.WorkpieceDimentionCompare(PieceDrawing, Workpiece);
                if (Worker.WorkpieceIsGood == 1)
                {
                    Worker.InstallWorkpiece(MillingMachine, Counter);
                }
                else
                {
                    Console.WriteLine("Рабочий укладывает заготовку в изолятор брака");
                    Counter.DefectedQuantity++;
                    Counter.SummaryQuantity--;
                    Worker.WorkpieceIsGood = 0;
                }
                if (Worker.WorkpieceIsGood == 1)
                {
                    Worker.ReadyToMillCheck(MillingMachine);
                    Worker.TurnSpindleOn(MillingMachine);
                    Worker.TouchAndZero(MillingMachine);
                    while (MillingMachine.Xcoord != PieceDrawing.GrooveInitialX | MillingMachine.Ycoord != PieceDrawing.GrooveInitialY | MillingMachine.Zcoord != PieceDrawing.GrooveDepth * -1)
                    {
                        Worker.MoveTo(MillingMachine);
                    }
                    Worker.GrooveMilling(MillingMachine, PieceDrawing, Workpiece, Worker);
                    Worker.TurnSpindleOn(MillingMachine);
                    Worker.InstallWorkpiece(MillingMachine, Counter);
                    Counter.SummaryQuantity--;
                }
            }
            Console.WriteLine("***Конец смены***");
            Console.WriteLine($"Количество заготовок в изоляторе брака: {Counter.DefectedQuantity}");
            Console.WriteLine($"Количество деталей в таре для готовых деталей: {Counter.ReadyQuantity}"); 
        }
    }
}
