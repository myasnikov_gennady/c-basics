﻿using System;

namespace ClassLibrary
{
    public class MillingMachine
    // описание станка
    {
        public void GetIsOnStatus()
        // статус вкл-выкл станка
        {
            if (IsOn)
            {
                Console.WriteLine("Станок включен");
            }
            else
            {
                Console.WriteLine("Станок выключен");
            }
        }
        public void GetMillStatus()
        // статус установки фрезы
        {
            if (MillIsInstalled)
            {
                Console.WriteLine("Фреза установлена в шпиндель станка");
            }
            else
            {
                Console.WriteLine("Фреза не установлена в шпиндель станка");
            }
        }
        public void GetWorkpieceStatus()
        // статус установки заготовки
        {
            if (WorkpieceIsInstalled)
            {
                Console.WriteLine("Заготовка установлена в тиски станка");
            }
            else
            {
                Console.WriteLine("Заготовка не установлена в тиски станка");
            }
        }
        public void GetCoordStatus()
        // текущий статус координат
        {
            Console.WriteLine($"Текущие координаты: X = {Math.Round(Xcoord, 1)}, Y = {Math.Round(Ycoord, 1)}, Z = {Math.Round(Zcoord, 1)}");
        }
        public bool IsOn { get; set; } // вкл-выкл
        public bool MillIsInstalled { get; set; } // фреза уст-не уст
        public bool WorkpieceIsInstalled { get; set; } // заготовка уст-не уст
        public bool SpindleIsOn { get; set; } // шпиндель вкл-выкл
        public int MillDiameter { get; set; } // диаметр фрезы
        public double Xcoord { get; set; } // координата X
        public double Ycoord { get; set; } // координата Y
        public double Zcoord { get; set; } // координата Z
    }
}
