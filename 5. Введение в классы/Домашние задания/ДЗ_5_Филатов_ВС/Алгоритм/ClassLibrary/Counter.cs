﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary
{
    public class Counter
    {
        public int ReadyQuantity; // кол-во деталей в таре
        public int DefectedQuantity; // кол-во деталей в изоляторе
        public int SummaryQuantity; // суммарное кол-во заготовок
        public Counter(int _readyQuantity, int _defectedQuantity, int _summaryQuantity)
        {
            DefectedQuantity = _defectedQuantity;
            ReadyQuantity = _readyQuantity;
            SummaryQuantity = _summaryQuantity;
        }
    }
}
