﻿using System;

namespace ClassLibrary
{
    public class Worker
    // описание рабочего
    {
        public void TurnMachineOn(MillingMachine millingMachine)
        // включение-выключение станка
        {
            if (millingMachine.IsOn)
            {
                millingMachine.IsOn = false;
                Console.WriteLine("Рабочий выключает станок");
            }
            else
            {
                millingMachine.IsOn = true;
                Console.WriteLine("Рабочий включает станок");
            }
        }
        public void ChooseMill(MillingMachine millingMachine, PieceDrawing pieceDrawing)
        // подбор фрезы
        {
            while (millingMachine.MillDiameter == 0)
            {
                Console.WriteLine("Рабочий подбирает фрезу:");
                Console.WriteLine("Диаметр фрезы?");
                bool millDiameterResult = int.TryParse(Console.ReadLine(), out int millDiameter);
                if (millDiameterResult)
                {
                    if (millDiameter <= pieceDrawing.GrooveWidth - 1 & millDiameter >= pieceDrawing.GrooveWidth - 4)
                    {
                        Console.WriteLine($"Подобрана фреза диаметром {millDiameter} мм");
                        millingMachine.MillDiameter = millDiameter;
                    }
                    else
                    {
                        Console.WriteLine($"Диаметр фрезы должен быть от {pieceDrawing.GrooveWidth - 4} до {pieceDrawing.GrooveWidth - 1} мм!");
                    }
                }
                else
                {
                    Console.WriteLine("Введены недопустимые символы!");
                }
            }
        }
        public void InstallMill(MillingMachine millingMachine)
        // установка фрезы
        {
            while (millingMachine.MillIsInstalled == false)
            {
                Console.WriteLine("Фреза установлена? y/n");
                MillStat = Console.ReadLine();
                switch (MillStat)
                {
                    case "n":
                        Console.WriteLine("Рабочий устанавливает фрезу в шпиндель станка");
                        millingMachine.MillIsInstalled = true;
                        break;
                    case "y":
                        millingMachine.MillIsInstalled = true;
                        break;
                    default:
                        Console.WriteLine("Введен неверный символ!");
                        break;
                }
            }
        }
        public void WorkpieceDimentionCompare(PieceDrawing pieceDrawing, Workpiece workpiece)
        // сравнение размеров заготовки с чертежом
        {
            if (workpiece.lenght == pieceDrawing.Lenght && workpiece.width == pieceDrawing.Width && workpiece.height == pieceDrawing.Heigt)
            {
                Console.WriteLine("Размеры заготовки соответствуют чертежу");
                WorkpieceIsGood = 1;
            }
            else
            {
                Console.WriteLine("Размеры заготовки не соответствуют чертежу!");
                WorkpieceIsGood = -1;
            }
        }

        public void InstallWorkpiece(MillingMachine millingMachine, Counter counter)
        // установка детали на станок
        {
            if (millingMachine.WorkpieceIsInstalled)
            {
                millingMachine.WorkpieceIsInstalled = false;
                Console.WriteLine("Рабочий снимает деталь и укладывает ее в тару для готовых деталей");
                counter.ReadyQuantity++;
            }
            else
            {
                millingMachine.WorkpieceIsInstalled = true;
                Console.WriteLine("Рабочий устанавливает заготовку");
            }
        }
        public void ReadyToMillCheck(MillingMachine millingMachine)
        // проверка готовности к фрезерованию
        {
            millingMachine.GetIsOnStatus();
            millingMachine.GetMillStatus();
            millingMachine.GetWorkpieceStatus();
            if (millingMachine.IsOn && millingMachine.WorkpieceIsInstalled && millingMachine.MillIsInstalled)
            {
                Console.WriteLine("Все готово к началу работы!");
            }
            else
            {
                Console.WriteLine("Что-то пошло не так...");
            }
        }
        public void TurnSpindleOn(MillingMachine millingMachine)
        // включить-выключить шпиндель станка
        {
            if (millingMachine.SpindleIsOn)
            {
                millingMachine.SpindleIsOn = false;
                Console.WriteLine("Рабочий выключает вращение шпинделя");
            }
            else
            {
                millingMachine.SpindleIsOn = true;
                Console.WriteLine("Рабочий включает вращение шпинделя");
            }
        }
        public void TouchAndZero(MillingMachine millingMachine)
        // коснуться детали и обнулить координаты
        {
            Console.WriteLine("Рабочий подвел фрезу по оси X до касания и обнулил координату");
            millingMachine.Xcoord = 0;
            Console.WriteLine("Рабочий подвел фрезу по оси Y до касания и обнулил координату");
            millingMachine.Ycoord = 0;
            Console.WriteLine("Рабочий подвел фрезу по оси Z до касания и обнулил координату");
            millingMachine.Zcoord = 0;
        }
        public void MoveTo(MillingMachine millingMachine)
        // перемещение стола станка по координатам
        {
            millingMachine.GetCoordStatus();
            Console.WriteLine("Укажите координату, по которой нужно переместить стол: x/y/z");
            CoordSelect = Console.ReadLine();
            switch (CoordSelect)
            {
                case "x":
                    Console.Write("Введите координату: ");
                    bool XCoordResult = double.TryParse(Console.ReadLine(), out double Xcoord);
                    if (XCoordResult)
                    {
                        millingMachine.Xcoord = Xcoord;
                    }
                    else
                    {
                        Console.WriteLine("Введены недопустимые символы!");
                    }
                    break;
                case "y":
                    Console.Write("Введите координату: ");
                    bool YCoordResult = double.TryParse(Console.ReadLine(), out double Ycoord);
                    if (YCoordResult)
                    {
                        millingMachine.Ycoord = Ycoord;
                    }
                    else
                    {
                        Console.WriteLine("Введены недопустимые символы!");
                    }
                    break;
                case "z":
                    Console.Write("Введите координату: ");
                    bool ZCoordResult = double.TryParse(Console.ReadLine(), out double Zcoord);
                    if (ZCoordResult)
                    {
                        millingMachine.Zcoord = Zcoord;
                    }
                    else
                    {
                        Console.WriteLine("Введены недопустимые символы!");
                    }
                    break;
                default:
                    Console.WriteLine("Введен неверный символ!");
                    break;
            }
        }
        public void GrooveMilling(MillingMachine millingMachine, PieceDrawing pieceDrawing, Workpiece workpiece, Worker worker)
        // цикл фрезерования паза
        {
            millingMachine.GetCoordStatus();
            Console.Write("Нажмите любую клавишу чтобы начать фрезерование паза...");
            CoordSelect = Console.ReadLine();
            Console.WriteLine();
            Console.WriteLine("Начало фрезерования");
            int run = pieceDrawing.GrooveLenght + millingMachine.MillDiameter;
            millingMachine.Xcoord = millingMachine.Xcoord + run;
            workpiece.grooveWidth = millingMachine.MillDiameter;
            millingMachine.GetCoordStatus();
            Console.WriteLine($"Ширина паза: {workpiece.grooveWidth} мм");
            workpiece.grooveWidth = millingMachine.MillDiameter;
            double step = 0.2;
            Console.WriteLine($"Толщина съема металла за каждый следующий проход фрезы: {step} мм");
            while (workpiece.grooveWidth < pieceDrawing.GrooveWidth)
            {
                millingMachine.Ycoord = millingMachine.Ycoord - step;
                if (millingMachine.Xcoord > 0)
                {
                    millingMachine.Xcoord = millingMachine.Xcoord - run;
                    workpiece.grooveWidth = Math.Round(workpiece.grooveWidth + step, 1);
                    millingMachine.GetCoordStatus();
                    Console.WriteLine($"Ширина паза: {workpiece.grooveWidth} мм");
                }
                else
                {
                    millingMachine.Xcoord = millingMachine.Xcoord + run;
                    workpiece.grooveWidth = Math.Round(workpiece.grooveWidth + step, 1);
                    millingMachine.GetCoordStatus();
                    Console.WriteLine($"Ширина паза: {workpiece.grooveWidth} мм");
                }
            }
            Console.WriteLine("Конец фрезерования \n");
            worker.WorkpieceIsGood = 0;
        }
        public string MillStat { get; set; } // статус фрезы
        public string CoordSelect { get; set; } // выбор координаты
        public int WorkpieceIsGood { get; set; } // условие "заготовка годная"
    }
}
