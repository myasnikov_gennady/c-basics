﻿using System;

namespace ClassLibrary
{
    public class WorkingDay
    // описание рабочего дня
    {
        public int Time { get; set; }
        public int TimeMax = 12;
        public int TimeMin = 6;
        public void GetTime(WorkingDay workingDay)
        {
            while (workingDay.Time == 0)
            {
                Console.Write("Укажите продолжительность смены в часах: ");
                bool TimeResult = int.TryParse(Console.ReadLine(), out int Time);
                if (TimeResult)
                {
                    if (Time < TimeMin || Time > TimeMax)
                    {
                        Console.WriteLine($"Смена не может быть короче {TimeMin} и дольше {TimeMax} часов!");
                    }
                    else
                    {
                        Console.WriteLine($"Продолжительность смены {Time} часов");
                        workingDay.Time = Time * 60;

                    }
                }
                else
                {
                    Console.WriteLine("Введены недопустимые символы!");
                }
            }
        }
    }
}
