﻿using System;

namespace ClassLibrary
{
    public class Workpiece
    // описание заготовки
    {
        public Workpiece(int Lenght, int Width, int Height, double GrooveWidth)
        {
            lenght = Lenght;
            width = Width;
            height = Height;
            grooveWidth = GrooveWidth;
        }
        internal int lenght { get; set; } // длина
        internal int width { get; set; } // ширина
        internal int height { get; set; } // высота
        internal double grooveWidth { get; set; } // ширина паза

        public void GetWorkpieceDimentions(Workpiece workpiece)
        // получение размеров заготовки
        {
            Console.WriteLine();
            Console.WriteLine("Получена новая заготовка. Введите ее размеры (целые числа)");
            while (workpiece.lenght == 0)
            {
                Console.WriteLine("Длина?");
                bool lenghtResult = int.TryParse(Console.ReadLine(), out int _lenght);
                if (lenghtResult)
                {
                    Console.WriteLine($"Длина: {_lenght} мм");
                    workpiece.lenght = _lenght;
                }
                else
                {
                    Console.WriteLine("Введены недопустимые символы!");
                }
            }
            while (workpiece.width == 0)
            {
                Console.WriteLine("Ширина?");
                bool widthResult = int.TryParse(Console.ReadLine(), out int _width);
                if (widthResult)
                {
                    Console.WriteLine($"Ширина: {_width} мм");
                    workpiece.width = _width;
                }
                else
                {
                    Console.WriteLine("Введены недопустимые символы!");
                }
            }
            while (workpiece.height == 0)
            {
                Console.WriteLine("Высота?");
                bool heightResult = int.TryParse(Console.ReadLine(), out int _height);
                if (heightResult)
                {
                    Console.WriteLine($"Высота: {_height} мм");
                    workpiece.height = _height;
                }
                else
                {
                    Console.WriteLine("Введены недопустимые символы!");
                }
            }
            Console.WriteLine("Размеры заготовки");
            Console.WriteLine($"Длина: {lenght} мм");
            Console.WriteLine($"Ширина: {width} мм");
            Console.WriteLine($"Высота: {height} мм");
        }
    }
}
