﻿using System;

namespace ClassLibrary
{
    public class PieceDrawing
    // описание чертежа детали
    {
        public void GetPieceDimentions()
        // получение габаритных размеров по чертежу
        {
            Console.WriteLine("Получен чертеж детали");
            Console.WriteLine($"Длина: {Lenght} мм");
            Console.WriteLine($"Ширина: {Width} мм");
            Console.WriteLine($"Высота: {Heigt} мм");
        }
        public void GetGrooveDimentions()
        // получение размеров паза по чертежу
        {
            Console.WriteLine($"Глубина паза: {GrooveDepth} мм");
            Console.WriteLine($"Длина паза: {GrooveLenght} мм");
            Console.WriteLine($"Ширина паза: {GrooveWidth} мм");
            Console.WriteLine($"Координата Х начала паза: {GrooveInitialX} мм");
            Console.WriteLine($"Координата Y начала паза: {GrooveInitialY} мм");
        }
        public void GetMachiningTime()
        // получение времени обработки
        {
            Console.WriteLine($"Время на изготовление детали: {MachiningTime} мин");
        }

        public int Lenght { get; set; } // длина по чертежу
        public int Width { get; set; } // ширина по чертежу
        public int Heigt { get; set; } // высота по чертежу
        public int GrooveDepth { get; set; } // глубина паза по чертежу
        public int GrooveLenght { get; set; } // длина паза по чертежу
        public int GrooveWidth { get; set; } // ширина паза по чертежу
        public int GrooveInitialX { get; set; } // координата X начала паза по чертежу
        public int GrooveInitialY { get; set; } // координата Y начала паза по чертежу
        public int MachiningTime { get; set; } // координата Z начала паза по чертежу
    }
}
