﻿using ClassLibrary;
using System;

namespace ДЗ_5_Филатов_ВС
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Вызов метода класса");
            var person = new Class1.PersonInfo()
            {
                Age = 35,
                Gender = "male",
                Name = "Bill"
            };
            person.GetInfo();
            System.Console.ReadLine();

        }
    }
}