﻿using System;

namespace ClassLibrary
{
    public class Class1
    {
        public class PersonInfo                                                         // создание класса
        {
            public void GetInfo()                                                       // создание методов класса
            {
                Console.WriteLine($"Имя: {Name}  Возраст: {Age} Пол: {Gender}");
            }

            private void MethodTwo()
            {
                Console.WriteLine("Private метод");
            }

            public void MethodThree()
            {
                Console.WriteLine("Public метод");
            }

            protected void MethodFour()
            {
                Console.WriteLine("Protected метод");
            }

            protected internal void MethodFive()
            {
                Console.WriteLine("Protected internal метод");
            }

            private protected void MethodSix()
            {
                Console.WriteLine("Private protected метод");
            }

            private int _age;
            public string Name;                                                 // создание полей класса


            public string Gender { get; set; }                                  // создание автосвойства

            public int Age                                                      // создание свойства с set и get к private полю
            {
                get
                {
                    return _age;
                }

                set
                {
                    _age = value;
                }
            }

            public PersonInfo(int age, string name, string gender)                  // Конструктор
            {
                Age = age;
                Name = name;
                Gender = gender;
            }
            public PersonInfo()
            {

            }

            private class ClassTwo
            {

            }

            internal class ClassThree
            {

            }
        }

    }
}
