﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exceptions
{
    public class SampleException : Exception
    {
        public SampleException() : base("Massage") 
        {
            A = "Tatata";
        }

        public string A { get; private set; } 
    }
}
