﻿using System;

namespace Exceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                SomeMethod();
            }

            catch (SampleException e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.A);
            }
        }

        static void SomeMethod()
        {
            try
            {
                var valueInt = int.Parse("0");
                var i = 1 / valueInt;
            }

            catch (DivideByZeroException e) when (e.Message == null)
            {
                Console.WriteLine($"Wooow {e.Message}");
            }

            catch (ArithmeticException)
            {
                Console.WriteLine("We got some error");
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }

            finally
            {
                throw new SampleException();
            }
        }
    }
}
