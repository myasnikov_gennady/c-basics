﻿using System;

namespace HomeWork12
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                if (SomeMethod(5, 6) > 10)
                {
                    throw new MyException();
                }
            }

            catch (MyException e)
            {
                Console.WriteLine(e.MyMessage);
            }
        }

        private static int SomeMethod(int a, int b)
        {
            var sum = a+b;
            return sum;
        }
    }
}
