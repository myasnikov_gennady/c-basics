﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exceptions
{
    public class MyDivideByZeroException : Exception
    {
        public MyDivideByZeroException(string message) : base(message) { }
    }
}
