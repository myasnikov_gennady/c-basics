﻿using System;

namespace Exceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                double result = DivisionOperation(5, 0);
                Console.WriteLine(result);
            }

            catch (MyDivideByZeroException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        static double DivisionOperation(double firstMemeber, double secondfMember)
        {
            if(secondfMember == 0)
            {
                throw new MyDivideByZeroException("Attempt to divide by zero"); 
            }
            return firstMemeber / secondfMember;
        }
    }
}
