﻿using System;
using System.Runtime.Serialization;

namespace DZ12
{
    class Program   // Не засчитана. Скопирована у Скрипкина Владимира
    {
        static void Main(string[] args)
        {
            int[] numbers = new int[6];
            try
            {
                for (int i = 0; i < 7; i++)
                {
                    if (i >= numbers.Length)
                    {
                        throw new MesException();
                    }
                    numbers[i] = i;
                }
            }
            catch (MesException e)
            {
                Console.WriteLine(e.Message);
            }
            finally 
            {
                Print(numbers);
            }
        }
        public static void Print(int [] array)
        {
            Console.WriteLine("Вывод блока - finally.");
            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine($"{array[i]}");
            }
        }
    }
    internal class MesException : Exception // настоятельно рекомендуется использовать в окончании икласса исключений слово Exception, например MesException - Исправлено!
    {
        public string Message { get; set; }
        public MesException()
        {
            Message = "Вызвано исключение - IndexOutOfRangeException";
        }
    }
}
