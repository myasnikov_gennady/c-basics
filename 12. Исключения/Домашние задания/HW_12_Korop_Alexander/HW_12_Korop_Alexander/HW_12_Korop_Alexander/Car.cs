﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DZ12
{
    class Car
    {
        public string nameOfCar { get; set; }
        private int wheel;
        public int Wheel
        {
            get
            {
                return wheel;
            }
            set 
            {
                if (value < 4)
                {
                    throw new DZ12.MyExc("Too few wheels ", value);
                }
                else
                {
                    wheel = value;
                }
            }
        }
    }
}
