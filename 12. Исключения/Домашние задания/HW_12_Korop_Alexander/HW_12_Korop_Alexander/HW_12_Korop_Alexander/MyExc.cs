﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DZ12
{
    class MyExc : Exception     // настоятельно рекомендуется использовать в окончании икласса исключений слово Exception, например MyException
    {
        //public string Field { get; private set; }

        public int Value { get; set; }

        public MyExc(string message, int val) : base(message)
        {
            //Field = "MyExc message";
            Value = val;
        }
    }
}
