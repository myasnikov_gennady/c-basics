﻿using DZ12;
using System;

namespace DZ12
{
    class Program
    {
        static void Main(string[] args)
        {

            try
            {
                Car car = new Car { nameOfCar = "RENO LOGAN", Wheel = 3 };
            }
            catch (MyExc ex)
            {
                Console.WriteLine(ex.Value);
                Console.WriteLine(ex.Message);                
            }  
        }
    }
}
