﻿using System;

namespace ДЗ_12_Филатов_ВС
{
    public class ClassException : Exception     // настоятельно рекомендуется использовать в окончании икласса исключений слово Exception, например ClassException -> исправил
    {
        public ClassException() : base("Message:")
        {
            msg = "Exception class was called";
        }

        public string msg { get; private set; }
    }
}
