﻿using System;

namespace ДЗ_12_Филатов_ВС
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Example();
            }
            catch (ClassException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.msg);
                Console.WriteLine(ex.StackTrace);
            }          
        }
        static void Example()
        {
            try
            {
                int[] array = new int[3];
                array[0] = 5;
                array[1] = 8;
                array[2] = 44;
                array[3] = 22;
                foreach (var item in array)
                {
                    Console.Write(item + " ");
                }
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            finally
            {
                throw new ClassException();
            }

        }
    }
}
