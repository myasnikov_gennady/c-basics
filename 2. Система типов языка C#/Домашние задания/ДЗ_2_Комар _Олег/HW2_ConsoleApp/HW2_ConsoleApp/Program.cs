﻿using System;

namespace HW2_ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            // 2.Произвести неявные преобразования над числовыми типами.
            Console.WriteLine("Implicit numeric conversions:");
            sbyte a = -5;               // Переменной а типа sbyte присвоено значение -5.
            byte b = 3;                 // Переменной b типа byte присвоено значение 3.
            short c = a;                // Неявное преобразование переменной типа sbyte в тип ushort.
            ushort d = b;               // Неявное преобразование переменной типа byte в тип ushort.
            int e = c + 3;              // Неявное преобразование переменной типа short в тип int.
            uint f = d;                 // Неявное преобразование переменной типа ushort в тип uint.
            long g = e;                 // Неявное преобразование переменной типа int в тип long.
            ulong h = f;                // Неявное преобразование переменной типа uint в тип ulong.
            float i = g;                // Неявное преобразование переменной типа long в тип float.
            double j = i;               // Неявное преобразование переменной типа float в тип double.
            decimal k = h;              // Неявное преобразование переменной типа ulong в тип decimal.
            Console.WriteLine($@" a = {a} - {a.GetType().Name},  b = {b} - {b.GetType().Name},    c = {c} - {c.GetType().Name}, d = {d} - {d.GetType().Name},
 e = {e} - {e.GetType().Name},  f = {f} - {f.GetType().Name},  g = {g} - {g.GetType().Name}, h = {h} - {h.GetType().Name},
 i = {i} - {i.GetType().Name}, j = {j} - {j.GetType().Name}, k = {k} - {k.GetType().Name}");
            Console.WriteLine();

            // 3.Произвести явные преобразования над числовыми типами.
            Console.WriteLine("Explicit numeric conversions:");
            decimal l = 9228162514264337M;
            j = (double)l;              // Явное преобразование переменной типа decimal в тип double.
            i = (float)j;               // Явное преобразование переменной типа doubleв тип float.
            float m = 9999.555555f;
            h = (ulong)m;               // Явное преобразование переменной типа float в тип ulong.
            g = (long)(h);              // Явное преобразование переменной типа ulong в тип long.
            f = (uint)g;                // Явное преобразование переменной типа long в тип uint.
            e = (int)f;                 // Явное преобразование переменной типа uint в тип int.
            d = (ushort)e;              // Явное преобразование переменной типа int в тип ushort.
            c = (short)d;               // Явное преобразование переменной типа ushort в тип short.
            b = (byte)c;                // Явное преобразование переменной типа short в тип byte.
            a = (sbyte)b;               // Явное преобразование переменной типа byte в тип sbyte.
            Console.WriteLine($@" l = {l} - {l.GetType().Name}, j = {j} - {j.GetType().Name}, i = {i} - {i.GetType().Name}, 
 h = {h} - {h.GetType().Name}, g = {g} - {g.GetType().Name}, f = {f} - {f.GetType().Name}, e = {e} - {e.GetType().Name}, 
 d = {d} - {d.GetType().Name}, c = {c} - {c.GetType().Name}, b = {b} - {b.GetType().Name},     a = {a} - {a.GetType().Name}");
            Console.WriteLine();

            // Явные преобразования над числовыми типами с проверкой на переполнение.
            Console.WriteLine("The same actions, with \"CHECKED\"");
            decimal p = 9228162514264337593543950335M;
            j = checked((double)p);     // Явное преобразование переменной типа decimal в тип double с проверкой на переполнение.
            i = checked((float)j);      // Явное преобразование переменной типа doubleв тип float с проверкой на переполнение.
            float r = 9999.555555f;
            h = checked((ulong)r);      // Явное преобразование переменной типа float в тип ulong с проверкой на переполнение.
            g = checked((long)(h));     // Явное преобразование переменной типа ulong в тип long с проверкой на переполнение.
            f = checked((uint)g);       // Явное преобразование переменной типа long в тип uint с проверкой на переполнение.
            e = checked((int)f);        // Явное преобразование переменной типа uint в тип int с проверкой на переполнение.
            d = checked((ushort)e);     // Явное преобразование переменной типа int в тип ushort с проверкой на переполнение.
            c = checked((short)d);      // Явное преобразование переменной типа ushort в тип short с проверкой на переполнение.
            try
            {
                b = checked((byte)c);   // Явное преобразование переменной типа short в тип byte с проверкой на переполнение.
            }
            catch (OverflowException ex)
            {
                b = 25;                 // Обработка исключения.
                Console.WriteLine(ex.Message);
                Console.WriteLine("Exception: {0} > {1}. byte b = 25.", c, Byte.MaxValue);   // Запись в консоль текста при возникновении исключения.
            }
            a = checked((sbyte)b);      // Явное преобразование переменной типа byte в тип sbyte с проверкой на переполнение.
            Console.WriteLine($@" l = {l} - {l.GetType().Name}, j = {j} - {j.GetType().Name}, i = {i} - {i.GetType().Name}, 
 h = {h} - {h.GetType().Name}, g = {g} - {g.GetType().Name}, f = {f} - {f.GetType().Name}, e = {e} - {e.GetType().Name}, 
 d = {d} - {d.GetType().Name}, c = {c} - {c.GetType().Name}, b = {b} - {b.GetType().Name},     a = {a} - {a.GetType().Name}");
            Console.WriteLine();

            // 4. Для каждого числового типа вывести минимальное и максимальное значение.
            Console.WriteLine("Min&Max for types");
            Console.WriteLine($" {typeof(sbyte).Name}; Min value: {sbyte.MinValue}; Max value: {sbyte.MaxValue}");
            Console.WriteLine($" {typeof(byte).Name}; Min value: {byte.MinValue}; Max value: {byte.MaxValue}");
            Console.WriteLine($" {typeof(short).Name}; Min value: {short.MinValue}; Max value: {short.MaxValue}");
            Console.WriteLine($" {typeof(ushort).Name}; Min value: {ushort.MinValue}; Max value: {ushort.MaxValue}");
            Console.WriteLine($" {typeof(int).Name}; Min value: {int.MinValue}; Max value: {int.MaxValue}");
            Console.WriteLine($" {typeof(uint).Name}; Min value: {uint.MinValue}; Max value: {uint.MaxValue}");
            Console.WriteLine($" {typeof(long).Name}; Min value: {long.MinValue}; Max value: {long.MaxValue}");
            Console.WriteLine($" {typeof(ulong).Name}; Min value: {ulong.MinValue}; Max value: {ulong.MaxValue}");
            Console.WriteLine($" {typeof(float).Name}; Min value: {float.MinValue}; Max value: {float.MaxValue}");
            Console.WriteLine($" {typeof(double).Name}; Min value: {double.MinValue}; Max value: {double.MaxValue}");
            Console.WriteLine($" {typeof(decimal).Name}; Min value: {decimal.MinValue}; Max value: {decimal.MaxValue}");
            Console.WriteLine();

            // 5. Для целочисленных типов записать выражения с операциями сложения, вычитания, умножения. Можно в одном выражении.
            Console.WriteLine("Arithmetic operations on integral numeric types");
            int n;
            n = ((int)g / 99 - 11 * b+5*a);
            Console.WriteLine($"g / 99 - 11 * b + 5* a = {n}");
            Console.WriteLine();

            // 6.Для типов с плавающей запятой записать выражения с операциями сложения, вычитания, умножения, деления.Можно в одном выражении.
            Console.WriteLine("Arithmetic operations on floating-point numeric types");
            float s = 0.4f;
            float t = 1.1f;
            double u = 4.0;
            double v;
            v = ((float)u + 10*s) / (t-0.5*s);
            Console.WriteLine($"(u + 10s) / (t-0.5s) = {v}");
            Console.WriteLine();

            // 7. Для типа string записать выражение сложения строк.
            Console.WriteLine("Addition of strings");
            string hi = "Hello ";
            string name = "Alexander";
            string hello;
            hello = hi + name[0..4];  // Конкатенируем два строковых параметра, у второго выводим только первые четыре символа.
            Console.WriteLine(hello);
            Console.WriteLine();

            // 8. Для типа bool записать выражения с использованием |, ||, &, &&, ^. К каждому выражению добавить комментарий как работает оператор. Объяснить разницу между & и &&, | и ||.
            Console.WriteLine("Boolean logical operators");
            bool aa = true;
            bool ab = false;
            Console.WriteLine($"aa = {aa}, ab = {ab}");
            Console.WriteLine($"!aa = {!aa}");              /* Унарный префиксный оператор ! выполняет логическое отрицание операнда, возвращая true, если операнд имеет значение false, 
                                                               и false, если операнд имеет значение true. */
            Console.WriteLine($"aa | ab - {aa | ab}");      /* Оператор | вычисляет логическое ИЛИ для всех своих операндов. Результат операции принимает значение true, если хотя бы один 
                                                               из операндов имеет значение true. В противном случае результат будет false. Оператор | вычисляет оба операнда, даже если 
                                                               левый операнд имеет значение true. При этом операция должна вернуть значение true, независимо от значения правого операнда. */
            Console.WriteLine($"aa & ab - {aa & ab}");      /* Оператор & вычисляет логическое И для всех своих операндов. Результат операции принимает значение true, если оба операнда 
                                                               имеют значение true. В противном случае результат будет false. Оператор & вычисляет оба операнда, даже если левый операнд 
                                                               имеет значение false. При этом операция должна вернуть значение false, независимо от значения правого операнда. */
            Console.WriteLine($"aa || ab - {aa || ab}");    /* Условный оператор логического ИЛИ || (оператор короткого замыкания) вычисляет логическое ИЛИ для своих операндов. Результат 
                                                               операции принимает значение true, если хотя бы один из операндов имеет значение true. В противном случае результат будет false. 
                                                               Если первый операнд имеет значение true, второй операнд не вычисляется. */
            Console.WriteLine($"aa && ab - {aa && ab}");    /* Условный оператор логического И && (оператор короткого замыкания) вычисляет логическое И для своих операндов. Результат операции 
                                                               принимает значение true, если оба операнда имеют значение true. В противном случае результат будет false. Если первый операнд 
                                                               имеет значение false, второй операнд не вычисляется. */
            Console.WriteLine($"aa ^ ab - {aa ^ ab}");      /* Оператор ^ вычисляет логическое исключение ИЛИ для всех своих операндов, возвращая true, если первый операнд имеет значение true 
                                                               и второй операнд имеет значение false или первый операнд имеет значение false и второй операнд имеет значение true. В противном 
                                                               случае результат будет false. */
        }
    }
}