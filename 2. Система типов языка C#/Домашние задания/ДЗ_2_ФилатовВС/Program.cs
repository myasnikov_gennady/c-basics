﻿using System;

// 1. Создать консольное приложение.
namespace ДЗ_2_ФилатовВС
{
    class Program
    {
        static void Main(string[] args)
        {
            /* 2. Произвести неявные преобразования над числовыми типами.
            Минимум по одному для целочисленного типа и типа с плавающей запятой */

            Console.WriteLine("Implicit conversions");
            sbyte a = -10;
            short b = a;
            int c = b;
            long d = c;
            
            byte e = 18;
            ushort f = e;
            uint g = f;
            ulong h = g;

            float i = 12.5f;
            double j = i;
            decimal k = h;

            Console.WriteLine($"a = {a}, type {a.GetType().Name};");
            Console.WriteLine($"b = {b}, type {b.GetType().Name};");
            Console.WriteLine($"c = {c}, type {c.GetType().Name};");
            Console.WriteLine($"d = {d}, type {d.GetType().Name};");
            Console.WriteLine($"e = {e}, type {e.GetType().Name};");
            Console.WriteLine($"f = {f}, type {f.GetType().Name};");
            Console.WriteLine($"g = {g}, type {g.GetType().Name};");
            Console.WriteLine($"h = {h}, type {h.GetType().Name};");
            Console.WriteLine($"i = {i}, type {i.GetType().Name};");
            Console.WriteLine($"j = {j}, type {j.GetType().Name};");
            Console.WriteLine($"k = {k}, type {k.GetType().Name};");

            /* 3. Произвести явные преобразования над числовыми типами.
            Минимум по одному для целочисленного типа и типа с плавающей запятой. */

            Console.WriteLine();
            Console.WriteLine("Explicit conversions");
            k = 12358978238895m;
            j = (double)k;
            i = (float)k;

            d = 218;
            c = (int)d;
            b = (short)c;
            a = (sbyte)b;

            Console.WriteLine($"k = {k}, type {k.GetType().Name};");
            Console.WriteLine($"j = {j}, type {j.GetType().Name};");
            Console.WriteLine($"i = {i}, type {i.GetType().Name};");
            Console.WriteLine($"d = {d}, type {d.GetType().Name};");
            Console.WriteLine($"c = {c}, type {c.GetType().Name};");
            Console.WriteLine($"b = {b}, type {b.GetType().Name};");
            Console.WriteLine($"a = {a}, type {a.GetType().Name};");

            // Так же продублировать эти же привидения с ключевым словом checked
             
            Console.WriteLine();
            Console.WriteLine("Checked explicit conversions");
            Console.WriteLine();

            k = 12358978238895m;
            Console.WriteLine($"k = {k}, type {k.GetType().Name};");

            j = checked((double)k);
            Console.WriteLine($"j = {j}, type {j.GetType().Name};");

            i = checked((float)k);
            Console.WriteLine($"i = {i}, type {i.GetType().Name};");

            d = 218;
            Console.WriteLine($"d = {d}, type {d.GetType().Name};");

            c = checked((int)d);
            Console.WriteLine($"c = {c}, type {c.GetType().Name};");

            b = checked((short)c);
            Console.WriteLine($"b = {b}, type {b.GetType().Name};");

            try
            {
                a = checked((sbyte)b);
            }
            catch (OverflowException ex)
            {
                a = (sbyte)b;
                Console.WriteLine(ex.Message);
                Console.WriteLine($"b = {b} > {sbyte.MaxValue} (type sbyte max value)");
                Console.WriteLine($"a = {a}, type {a.GetType().Name};");
            }

            // 4.Для каждого числового типа вывести минимальное и максимальное значение.

            Console.WriteLine();
            Console.WriteLine("Min and max values for numeric data types");
            Console.WriteLine($"{typeof(byte).Name}; Min value: {byte.MinValue}; Max value: {byte.MaxValue}");
            Console.WriteLine($"{typeof(sbyte).Name}; Min value: {sbyte.MinValue}; Max value: {sbyte.MaxValue}");
            Console.WriteLine($"{typeof(short).Name}; Min value: {short.MinValue}; Max value: {short.MaxValue}");
            Console.WriteLine($"{typeof(ushort).Name}; Min value: {ushort.MinValue}; Max value: {ushort.MaxValue}");
            Console.WriteLine($"{typeof(int).Name}; Min value: {int.MinValue}; Max value: {int.MaxValue}");
            Console.WriteLine($"{typeof(uint).Name}; Min value: {uint.MinValue}; Max value: {uint.MaxValue}");
            Console.WriteLine($"{typeof(long).Name}; Min value: {long.MinValue}; Max value: {long.MaxValue}");
            Console.WriteLine($"{typeof(ulong).Name}; Min value: {ulong.MinValue}; Max value: {ulong.MaxValue}");
            Console.WriteLine($"{typeof(float).Name}; Min value: {float.MinValue}; Max value: {float.MaxValue}");
            Console.WriteLine($"{typeof(double).Name}; Min value: {double.MinValue}; Max value: {double.MaxValue}");
            Console.WriteLine($"{typeof(decimal).Name}; Min value: {decimal.MinValue}; Max value: {decimal.MaxValue}");

            /* 5.Для целочисленных типов записать выражения с операциями сложения, вычитания, умножения.
            Можно в одном выражении. */

            d = 142;
            int calc1 = (c + (int)d * 5) - 388;
            Console.WriteLine();
            Console.WriteLine("Integer Calculation");
            Console.WriteLine($"(218+142*5)-388={calc1}");

            /* 6.Для типов с плавающей запятой записать выражения с операциями сложения, вычитания, умножения, деления.
            Можно в одном выражении. */

            i = 12.8f;
            j = 10.9;
            k = 289.22m;
            double calc2 = ((double)i + (double)k / j) - 38.8 * 2.3;
            Console.WriteLine();
            Console.WriteLine("Double Calculation");
            Console.WriteLine($"(12.8+289.22/10.9)-38.8*2.3={calc2}");

            // 7.Для типа string записать выражение сложения строк.

            string str1 = "Сложение";
            string str2 = "строк";
            string strSum = str1 + " " + str2;
            Console.WriteLine();
            Console.WriteLine("String + string");
            Console.WriteLine(strSum);

            /* 8. Для типа bool записать выражения с использованием |, ||, &, &&, ^.
            К каждому выражению добавить комментарий как работает оператор. Объяснить разницу между & и &&, | и ||. */

            Console.WriteLine();
            Console.WriteLine("Boolean logic");
            bool f1 = (5 > 8) | (5 > 4);                            
            bool f2 = (5 > 8) | (2 > 4);                            /* Оператор | вычисляет логическое ИЛИ для всех своих операндов.
                                                                    Результат операции принимает значение true, если хотя бы один из
                                                                    операторов имеет значение true. */
            Console.WriteLine($"(5 > 8) | (5 > 4) = {f1}");
            Console.WriteLine($"(5 > 8) | (2 > 4) = {f2}");

            f1 = (5 > 4) || (5 > 8);
            f2 = (5 > 8) || (2 > 4);                                /* Условный оператор логического ИЛИ || (оператор короткого замыкания)
                                                                    вычисляет логическое ИЛИ для своих операндов. Результат операции принимает
                                                                    значение true, если хотя бы один из операторов имеет значение true.
                                                                    Если первый операнд имеет значение true, значение второго НЕ ВЫЧИСЛЯЕТСЯ.*/
            Console.WriteLine($"(5 > 8) || (5 > 4) = {f1}");
            Console.WriteLine($"(5 > 8) || (2 > 4) = {f2}");

            f1 = (5 == 5) & (8 > 5);
            f2 = (5 > 8) & (2 > 4);                                 /* Оператор & вычисляет логическое И для всех своих операндов. Результат операции принимает значение true, 
                                                                    если оба оператора имеют значение true. В противном случае результат будет false. Оператор вычисляет оба операнда,
                                                                    даже если левый операнд имеет значение false.*/
            Console.WriteLine($"(5 = 5) & (8 > 5) = {f1}");
            Console.WriteLine($"(5 > 8) & (2 > 4) = {f2}");

            f1 = (5 == 5) && (8 > 5);
            f2 = (5 > 8) && (2 > 4);                                /* Условный оператор логического И && (оператор короткого замыкания)
                                                                    вычисляет логическое И для своих операндов. Результат операции принимает значение true,
                                                                    если оба оператора имеют значение true. В противном случае результат будет false.
                                                                    Если первый операнд имеет значение false, значение второго НЕ ВЫЧИСЛЯЕТСЯ.*/
            Console.WriteLine($"(5 = 5) && (8 > 5) = {f1}");
            Console.WriteLine($"(5 > 8) && (2 > 4) = {f2}");

            bool f3 = true;
            bool f4 = false;
            bool f5 = false;

            bool f6 = f3 ^ f4;
            bool f7 = f4 ^ f5;                                      /* Оператор ^ вычисляет логическое исключение ИЛИ для всех своих операндов,
                                                                    возвращая true для x ^ y, если x имеет значение true и y имеет значение false или
                                                                    x имеет значение false и y имеет значение true. В противном случае результат будет false.*/
            Console.WriteLine($"true ^ false = {f6}");
            Console.WriteLine($"false ^ false = {f7}");

            f7 = !f3;
            Console.WriteLine($"!{f3} = {f7}");                     /* Унарный префиксный оператор! выполняет логическое отрицание операнда,
                                                                    возвращая true, если операнд имеет значение false, и false, если операнд имеет значение true.*/
        }
    }
}
