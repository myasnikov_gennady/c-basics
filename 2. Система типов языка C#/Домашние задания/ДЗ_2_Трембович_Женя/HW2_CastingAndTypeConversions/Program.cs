﻿using System;

namespace CastingAndTypeConversions
{
    class Program
    {
        static void Main(string[] args)
        {
            // 2. Implicit numeric conversions
            byte byteVar = 2;               // Declaring and initializing byte variable;
            sbyte sbyteVar = -1;            // Declaring and initializing sbyte variable;  
            ushort ushortVar = byteVar;     // Implicit conversion from byte to ushort;
            short shortVar = sbyteVar;      // Implicit conversion from sbyte to short;
            uint uintVar = ushortVar;       // Implicit conversion from ushort to uint;
            int intVar = shortVar;          // Implicit conversion from short to int;
            ulong ulongVar = uintVar;       // Implicit conversion from uint to ulong;
            long longVar = intVar;          // Implicit conversion from int to long;
            decimal decimalVar = ulongVar;  // Implicit conversion from ulong to decimal;
            float floatVar = longVar;       // Implicit conversion from long to float;
            double doubleVar = floatVar;    // Implicit conversion from float to double;
            
            // 3. Explicit numeric conversions
            floatVar = (float)doubleVar;    //Explicit conversion from double to float;
            longVar = (long)floatVar;       //Explicit conversion from float to long;
            ulongVar = (ulong)decimalVar;   //Explicit conversion from decimal to ulong;
            intVar = (int)longVar;          //Explicit conversion from long to int;
            uintVar = (uint)ulongVar;       //Explicit conversion from ulong to uint;
            shortVar = (short)intVar;       //Explicit conversion from int to short;
            ushortVar = (ushort)uintVar;    //Explicit conversion from uint to ushort;
            sbyteVar = (sbyte)shortVar;     //Explicit conversion from short to sbyte;
            byteVar = (byte)ushortVar;      //Explicit conversion from ushort to byte;
            byteVar = (byte)sbyteVar;       //Explicit conversion from sbyte to byte;
            sbyteVar = (sbyte)byteVar;      //Explicit conversion from byte to sbyte;

            floatVar = checked((float)doubleVar);   //Explicit conversion from double to float with checked;
            longVar = checked((long)floatVar);      //Explicit conversion from float to long with checked;
            ulongVar = checked((ulong)decimalVar);  //Explicit conversion from decimal to ulong with checked;
            intVar = checked((int)longVar);         //Explicit conversion from long to int with checked;
            uintVar = checked((uint)ulongVar);      //Explicit conversion from ulong to uint with checked;
            shortVar = checked((short)intVar);      //Explicit conversion from int to short with checked;
            ushortVar = checked((ushort)uintVar);   //Explicit conversion from uint to ushort with checked;
            sbyteVar = checked((sbyte)shortVar);    //Explicit conversion from short to sbyte with checked;
            byteVar = checked((byte)ushortVar);     //Explicit conversion from ushort to byte with checked;
            
            try
            {
                byteVar = checked((byte)sbyteVar);  //Explicit conversion from sbyte to byte with checked;
            }
            catch (OverflowException ex)
            {
                Console.WriteLine($"{ex.Message}\n");  
            }

            //4. Max and min value of type.
            Console.WriteLine($"Max and min value of type:\n{typeof(sbyte).Name}; Min value: {sbyte.MinValue}; Max value: {sbyte.MaxValue}\n" +
                $"{typeof(byte).Name}; Min value: {byte.MinValue}; Max value: {byte.MaxValue}\n" +
                $"{typeof(short).Name}; Min value: {short.MinValue}; Max value: {short.MaxValue}\n" +
                $"{typeof(ushort).Name}; Min value: {ushort.MinValue}; Max value: {ushort.MaxValue}\n" +
                $"{typeof(int).Name}; Min value: {int.MinValue}; Max value: {int.MaxValue}\n" +
                $"{typeof(uint).Name}; Min value: {uint.MinValue}; Max value: {uint.MaxValue}\n" +
                $"{typeof(long).Name}; Min value: {long.MinValue}; Max value: {long.MaxValue}\n" +
                $"{typeof(ulong).Name}; Min value: {ulong.MinValue}; Max value: {ulong.MaxValue}\n" +
                $"{typeof(float).Name}; Min value: {float.MinValue}; Max value: {float.MaxValue}\n" +
                $"{typeof(double).Name}; Min value: {double.MinValue}; Max value: {double.MaxValue}\n" +
                $"{typeof(decimal).Name}; Min value: {decimal.MinValue}; Max value: {decimal.MaxValue}\n");

            // 5. Arithmetic operations for integer types
            byte ByteVar = 5;
            sbyte SbyteVar = 33;
            ushort UshortVar = 96;
            int result = (ByteVar + SbyteVar - UshortVar) * 4;
            Console.WriteLine($"Arithmetic operations for integer types:\n({ByteVar}+{SbyteVar}-{UshortVar})*4={result}");

            //6. Arithmetic operations for floating-point types
            float FloatVar = 5.5F;
            double DoubleVar = 7.23;
            double Result = FloatVar * 0.5 / DoubleVar + 1.2F - FloatVar;
            Console.WriteLine($"\nArithmetic operations for floating - point types:\n{FloatVar} * 0,5 / {DoubleVar} + 1,2 - {FloatVar} = {Result}");

            // 7. Addition of strings
            string stringVar1 = "Hello";
            string stringVar2 = " World";
            string AddResult = stringVar1 + stringVar2;
            Console.WriteLine($"\nAddition of strings: {AddResult}\n");

            // 8. Boolean logical operators
            bool a = (5 > 1) | (2 < 3);         //Operators |,|| - returns true, when one of the operands (left or right) or both operands are evaluates to true.
            bool b = (6 > 7) || (4 < 3);        //Otherwise, the result is false. Operator | evaluates both operands even if the left operand evaluates to true.
                                                //But || operator does not evaluate right operand when left operand is true
            Console.WriteLine(a);   // true
            Console.WriteLine(b);   // false

            bool c = (5 > 1) & (2 < 3);         //Operators &,&& - returns true, when both operands (left and right) are evaluates to true.
            bool d = (6 > 7) && (4 < 3);        //Otherwise, the result is false. Operator & evaluates both operands even if the left operand evaluates to false.
                                                //But && operator does not evaluate right operand when left operand is false    
            Console.WriteLine(c);   // true
            Console.WriteLine(d);   // false

            bool e = (4 > 1) ^ (5 < 4);         //Operator ^ - returns true if left operand evaluates to true and right operand evaluates to false, or
            bool f = (3 > 1) ^ (4 < 5);         //if right operand evaluates to true and left operand evaluates to false.
                                                //Otherwise, the result is false.
            Console.WriteLine(e);   // true
            Console.WriteLine(f);   // false
        }
    }
}
