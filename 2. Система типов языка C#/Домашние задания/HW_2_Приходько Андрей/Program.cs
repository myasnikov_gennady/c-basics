﻿using System;

namespace HW_ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            // Произвести неявные преобразования над числовыми типами. Минимум по одному для целочисленного типа и типа с плавающей запятой.
            byte a = 1;
            byte b = 2;
            int c = a + b;

            float d = 1.5f;
            float e = 2.5f;
            double f = d + e;


            // Произвести явные преобразования над числовыми типами. Минимум по одному для целочисленного типа и типа с плавающей запятой.
            // Так же продублировать эти же привидения с ключевым словом checked.
            short g = 5;
            short h = 6;
            byte i = (byte)(g + h);

            float j = 13.6f;
            float k = 15.5f;
            decimal l = (decimal)(j + k);

            try
            {
                short _g = 500;
                short _h = 600;
                byte _i = checked((byte)(_g + _h));
            }
            catch (OverflowException OverFLow)
            {
                Console.WriteLine(OverFLow);
                Console.WriteLine();
                Console.WriteLine();
                try
                {
                    float _j = 13.6f;
                    float _k = 15.5f;
                    decimal _l = checked((decimal)(_j + _k));
                }
                catch (OverflowException)
                { 
                }

                // Для каждого числового типа вывести минимальное и максимальное значение
                
                Console.WriteLine($"{typeof(byte).Name}; Min value: {byte.MinValue}; Max value: {byte.MaxValue}");
                Console.WriteLine($"{typeof(int).Name}; Min value: {int.MinValue}; Max value: {int.MaxValue}");
                Console.WriteLine($"{typeof(float).Name}; Min value: {float.MinValue}; Max value: {float.MaxValue}");
                Console.WriteLine($"{typeof(double).Name}; Min value: {double.MinValue}; Max value: {double.MaxValue}");
                Console.WriteLine($"{typeof(short).Name}; Min value: {short.MinValue}; Max value: {short.MaxValue}");
                Console.WriteLine($"{typeof(decimal).Name}; Min value: {decimal.MinValue}; Max value: {decimal.MaxValue}");
                
            }
            // Для целочисленных типов записать выражения с операциями сложения, вычитания, умножения. Можно в одном выражении.

            int m = (a + b - g) * g / h;

            // Для типов с плавающей запятой записать выражения с операциями сложения, вычитания, умножения, деления. Можно в одном выражении.

            double n = (d + e - j) * j / k;

            // Для типа string записать выражение сложения строк.
            string Hello = "Hello ";
            string World = "World ";
            Console.WriteLine($"{ Hello}{World}!");

            // Для типа bool записать выражения с использованием |, ||, &, &&, ^.
            // К каждому выражению добавить комментарий как работает оператор. Объяснить разницу между & и &&, | и ||.
            bool b1 = (c > i) | (g > h);
            bool b2 = (c < i) || (g < h);
            bool b3 = (c > i) & (g > h);
            bool b4 = (c < i) && (g < h);
            bool b5 = (c > i) ^ (g > h);
            Console.WriteLine($"{c} > {i} | {g} > {h} = {b1}");  // Оператор | (ИЛИ) возвращает true, если хотя бы один true (Проверяет оба)
            Console.WriteLine($"{c} < {i} || {g} < {h} = {b2}"); // Оператор || (ИЛИ) возвращает true, если хотя бы один true (Если первый true, заканчивает проверку, возвращает true)
            Console.WriteLine($"{c} > {i} & {g} > {h} = {b3}");  // Оператор & (И) возвращает true, если оба true (Проверяет оба)
            Console.WriteLine($"{c} < {i} && {g} < {h} = {b4}"); // Оператор && (И) возвращает true, если оба true (Если первый false, заканчивает проверку, возвращает false)
            Console.WriteLine($"{c} < {i} ^ {g} < {h} = {b5}");  // Оператор ^ (ИЛИ) возвращает true, при условии что true только один. Если оба возвращает false








        }
    }
}
            

