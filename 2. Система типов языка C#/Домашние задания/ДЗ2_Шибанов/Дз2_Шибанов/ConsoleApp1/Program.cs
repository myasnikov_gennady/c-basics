﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            byte a = 1;
            short c = a;
            Console.WriteLine($"{c}, {c.GetType().Name}");
            Console.WriteLine($"{typeof(byte).Name}; Min value: {byte.MinValue}; Max value: {byte.MaxValue}");
            sbyte q = 1;
            short w = q;
            Console.WriteLine($"{w}, {w.GetType().Name}");
            Console.WriteLine($"{typeof(sbyte).Name}; Min value: {sbyte.MinValue}; Max value: {sbyte.MaxValue}");
            short e = 1;
            short r = e;
            Console.WriteLine($"{r}, {r.GetType().Name}");
            Console.WriteLine($"{typeof(short).Name}; Min value: {short.MinValue}; Max value: {short.MaxValue}");
            byte t = 1;
            short y = t;
            Console.WriteLine($"{y}, {y.GetType().Name}");
            Console.WriteLine($"{typeof(ushort).Name}; Min value: {ushort.MinValue}; Max value: {ushort.MaxValue}");
            byte i = 1;
            int o = i;
            Console.WriteLine($"{o}, {o.GetType().Name}");
            Console.WriteLine($"{typeof(int).Name}; Min value: {int.MinValue}; Max value: {int.MaxValue}");
            byte z = 1;
            uint x = z;
            Console.WriteLine($"{x}, {x.GetType().Name}");
            Console.WriteLine($"{typeof(uint).Name}; Min value: {uint.MinValue}; Max value: {uint.MaxValue}");
            byte v = 1;
            long b = v;
            Console.WriteLine($"{b}, {b.GetType().Name}");
            Console.WriteLine($"{typeof(long).Name}; Min value: {long.MinValue}; Max value: {long.MaxValue}");
            byte n = 1;
            long m = n;
            Console.WriteLine($"{m}, {m.GetType().Name}");
            Console.WriteLine($"{typeof(ulong).Name}; Min value: {ulong.MinValue}; Max value: {ulong.MaxValue}");
        }
    }
}
