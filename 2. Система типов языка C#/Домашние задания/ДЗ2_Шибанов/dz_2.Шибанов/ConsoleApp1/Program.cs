﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            char _a = ' ';
            string a = "Здравствуй";
            string b = "Григорий";
            string c = a + _a + b;
            Console.WriteLine(c);
            float q = 1.2f;
            float w = 23.4f;
            float r = q + w * q / w - q;
            Console.WriteLine(r);

            double l = 10.3;
            double k = 16.5;
            Console.WriteLine(l + (double)k);
            byte j = 12;
            byte h = 10;
            int g = j * h - j + h;
            Console.WriteLine(g);
            short f = 22;
            int d = checked((int)(f + 22));
            Console.WriteLine(d);
        }
    }
}
