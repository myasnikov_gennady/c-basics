﻿using System;

// 1. Создание консольного приложения
namespace HW_2_Uladzimir_Skrypkin    
{
    class Program
    {
        static void Main(string[] args)
        {
            // 2. Произвести неявные преобразования над числовыми типами. Минимум по одному для целочисленного типа и типа с плавающей запятой

            byte a8 = 32;
            sbyte b8 = -16;

            short a16 = -9;
            ushort b16 = 45;

            int c32 = -1200200;
            uint b32 = 20001000;

            long a64 = 300000000;
            ulong b64 = 2300000000;            

            float fl = 12.26f;
            double db = 12.3333333333333333333333333;
            decimal dec = 12343434.9m;

            int a32 = (b8+b16);
            ulong c64 = (b32+a8);

            Console.WriteLine(a32);
            Console.WriteLine(c64);

            Console.WriteLine(db + (double)fl);
            Console.WriteLine((decimal)db + dec);

            /*3. Произвести явные преобразования над числовыми типами. Минимум по одному для целочисленного типа и типа с плавающей запятой.
             Так же продублировать эти же привидения с ключевым словом checked.*/

            int d32 = (int)(a16+c32);        
            double fl1 = (double)dec;

            Console.WriteLine(d32);
            Console.WriteLine(fl1);

            int maxIntValue = 2147483647;
            
            
            // По умолчанию значение в  true, Чтобы проверить нужно - добавить +10 - будет false - проверка выполняется 
            try
            {
                d32 = checked(maxIntValue);                                       
            }
            catch (System.OverflowException e)
            {
               
                Console.WriteLine("CHECKED and CAUGHT:  " + e.ToString());
            }

           
                fl1 = checked((double)dec);
                Console.WriteLine($"fl1 = {fl}, type {fl.GetType().Name};");



            //4.Для каждого числового типа вывести минимальное и максимальное значение.

            Console.WriteLine();

            Console.WriteLine($"{typeof(byte).Name}; Min value: {byte.MinValue}; Max value: {byte.MaxValue}");
            Console.WriteLine($"{typeof(sbyte).Name}; Min value: {sbyte.MinValue}; Max value: {sbyte.MaxValue}");
            Console.WriteLine($"{typeof(short).Name}; Min value: {short.MinValue}; Max value: {short.MaxValue}");
            Console.WriteLine($"{typeof(ushort).Name}; Min value: {ushort.MinValue}; Max value: {ushort.MaxValue}");
            Console.WriteLine($"{typeof(int).Name}; Min value: {int.MinValue}; Max value: {int.MaxValue}");
            Console.WriteLine($"{typeof(uint).Name}; Min value: {uint.MinValue}; Max value: {uint.MaxValue}");
            Console.WriteLine($"{typeof(long).Name}; Min value: {long.MinValue}; Max value: {long.MaxValue}");
            Console.WriteLine($"{typeof(ulong).Name}; Min value: {ulong.MinValue}; Max value: {ulong.MaxValue}");
            Console.WriteLine($"{typeof(float).Name}; Min value: {float.MinValue}; Max value: {float.MaxValue}");
            Console.WriteLine($"{typeof(double).Name}; Min value: {double.MinValue}; Max value: {double.MaxValue}");
            Console.WriteLine($"{typeof(decimal).Name}; Min value: {decimal.MinValue}; Max value: {decimal.MaxValue}");

            // 5. Для целочисленных типов записать выражения с операциями сложения, вычитания, умножения. Можно в одном выражении.


            b64 = ((uint)a64 - (uint)b16 + (uint)a16) * 2;

            Console.WriteLine();
            Console.WriteLine($" (a64-b16+a16)*2={b64}  ");

            //6.Для типов с плавающей запятой записать выражения с операциями сложения, вычитания, умножения, деления.Можно в одном выражении.

            decimal collect = dec + (decimal)db*7 -1234455;

            Console.WriteLine($" dec + db*7 -1234455={collect}  ");

            // 7.Для типа string записать выражение сложения строк.


            string st = "сумма";
            string st1 = "string";
            string stsum = st + " "+st1;
           
            Console.WriteLine(stsum);

            /* 8. Для типа bool записать выражения с использованием |, ||, &, &&, ^. К каждому выражению добавить комментарий
             как работает оператор. Объяснить разницу между & и &&, | и ||.*/

            Console.WriteLine();

            bool a =  (1<2) & (5>3); // Оператор & возвращает true если оба обьекта одинаковыми условием, . оба - true, или оба - false.
             Console.WriteLine(a);    // 



            bool bb = (5 < 6) ^ (8 > 5);  // Оператор ^ вычисляет логическое исключение ИЛИ для всех своих операндов, возвращая true,
                                          // если x имеет значение true и y имеет значение false или x имеет значение false и y имеет
                                          // значение true. В противном случае результат будет false. 
            Console.WriteLine(bb);


            bool cc = (7 < 5) | (6 > 5);   // Оператор | вычисляет логическое ИЛИ для всех своих операндов. Результат
                                            // операции  |  принимает значение true, если хотя бы один из операторов
                                            //  имеет значение true. В противном случае результат будет false.

            Console.WriteLine(cc);


            bool dd = (5 < 9) && (5 > 8);       //Условный оператор логического И && (оператор короткого замыкания) вычисляет
                                                //логическое И для своих операндов. Результат операции x && y принимает значение
                                                //true, если оба оператора x и y имеют значение true. В противном случае результат
                                                //будет false. Если x имеет значение false, y не вычисляется.
                                                // В отличии от &, && - Если x -  имеет значение false, y не вычисляется.

            Console.WriteLine(dd);


            bool ff = (4<6) || (8>6);          // Условный оператор логического ИЛИ || (оператор короткого замыкания) вычисляет логическое
                                              // ИЛИ для своих операндов. Результат операции x || y принимает значение true, если хотя
                                              // бы один из операторов x или y имеет значение true.
                                               // В противном случае результат будет false.Если x имеет значение true, y не вычисляется.

            Console.WriteLine(ff);




        }
    }
}
