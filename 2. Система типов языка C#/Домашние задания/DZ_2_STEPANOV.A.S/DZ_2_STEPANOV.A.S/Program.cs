﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// Создание  консольного приложения
namespace DZ_2_STEPANOV.A.S
{
    class Program
    {
        static void Main(string[] args)
        {
            // Произвести неявные преобразования над числовыми типами. Минимум по одному для целочисленного типа и типа с плавающей запятой
            byte a = 9;
            byte b = 6;
            byte c = 7;
            int x = (a + b) - c;

            float d = 1.25f;
            float u = 2.19f;
            float e = 3.14f;
            double y = d + u + e;

            // Произвести явные преобразования над числовыми типами. Минимум по одному для целочисленного типа и типа с плавающей запятой.
            // Так же продублировать эти же привидения с ключевым словом checked.
            short i = 9;
            short q = 5;
            short s = 12;
            byte w = (byte)((s - i) + q);

            float r = 20.9f;
            float o = 28.4f;
            decimal j = (decimal)(o - r);

            try
            {
                short t = 739;
                short n = 625;
                short m = 569;
                byte _w = checked((byte)((t - n) + m));
            }
            catch (OverflowException OverFLow)
            {
                Console.WriteLine(OverFLow);
                Console.WriteLine();
                Console.WriteLine();

                try
                {
                    float _j = 26.6f;
                    float k = 18.5f;
                    decimal _l = checked((decimal)(_j - k));
                }
                catch (OverflowException)
                {
                }

                // Для каждого числового типа вывести минимальное и максимальное значение

                Console.WriteLine($"{typeof(byte).Name}; Min value: {byte.MinValue}; Max value: {byte.MaxValue}");
                Console.WriteLine($"{typeof(int).Name}; Min value: {int.MinValue}; Max value: {int.MaxValue}");
                Console.WriteLine($"{typeof(float).Name}; Min value: {float.MinValue}; Max value: {float.MaxValue}");
                Console.WriteLine($"{typeof(double).Name}; Min value: {double.MinValue}; Max value: {double.MaxValue}");
                Console.WriteLine($"{typeof(short).Name}; Min value: {short.MinValue}; Max value: {short.MaxValue}");
                Console.WriteLine($"{typeof(decimal).Name}; Min value: {decimal.MinValue}; Max value: {decimal.MaxValue}");
            }
            // Для целочисленных типов записать выражения с операциями сложения, вычитания, умножения. Можно в одном выражении.
            int _m = (a + b - i) * q / s;
            // Для типов с плавающей запятой записать выражения с операциями сложения, вычитания, умножения, деления. Можно в одном выражении.
            double _n = (d + u - e) * o / r;
            // Для типа string записать выражение сложения строк.
            string Hello = "Hello ";
            string World = "World ";
            Console.WriteLine($"{ Hello}{World}!"); // Это не является сложением строк, это является интерполяцией. Сложение строк: Hello + World

            // Для типа bool записать выражения с использованием |, ||, &, &&, ^.
            // К каждому выражению добавить комментарий как работает оператор. Объяснить разницу между & и &&, | и ||.
            bool b1 = (x > w) | (q > i);
            bool b2 = (x < w) || (q < i);
            bool b3 = (x > w) & (q > i);
            bool b4 = (x < w) && (q < i);
            bool b5 = (x > w) ^ (q > i);
            Console.WriteLine($"{x} > {w} | {q} > {i} = {b1}");  // Оператор | (ИЛИ) возвращает true, если хотя бы один true (Проверяет оба)
            Console.WriteLine($"{x} < {w} || {q} < {i} = {b2}"); // Оператор || (ИЛИ) возвращает true, если хотя бы один true (Если первый true, заканчивает проверку, возвращает true)
            Console.WriteLine($"{x} > {w} & {q} > {i} = {b3}");  // Оператор & (И) возвращает true, если оба true (Проверяет оба)
            Console.WriteLine($"{x} < {w} && {q} < {i} = {b4}"); // Оператор && (И) возвращает true, если оба true (Если первый false, заканчивает проверку, возвращает false)
            Console.WriteLine($"{x} < {w} ^ {q} < {i} = {b5}");  // Оператор ^ (ИЛИ) возвращает true, при условии что true только один. Если оба возвращает false
            Console.ReadKey();
        }

    } }

