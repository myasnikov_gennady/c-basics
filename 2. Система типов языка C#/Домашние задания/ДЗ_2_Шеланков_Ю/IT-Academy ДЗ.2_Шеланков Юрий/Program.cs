﻿using System;

namespace IT_Academy_ДЗ._2_Шеланков_Юрий
{
    class Program
    {
        static void Main(string[] args)
        {
            //Шеланков Юрий
            //Домашнее задание 2
            // Для проверки раскомментировать

            //1.Создать консольное приложение.
            //2.Произвести неявные преобразования над числовыми типами.
            //    Минимум по одному для целочисленного типа и типа с плавающей запятой.

            byte a1 = 7;
            byte b1 = 3;
            int c1 = (a1 + b1);
            Console.WriteLine(c1);


            float a2 = 7.3f;
            float b2 = 3.7f;
            double c2 = (a2 + b2);
            Console.WriteLine(c2);




            //3.Произвести явные преобразования над числовыми типами.
            //    Минимум по одному для целочисленного типа и типа с плавающей запятой.

            int a3 = 7;
            int b3 = 3;
            byte c3 = (byte)(a3 + b3);
            Console.WriteLine(c3); ;

            double a4 = 7.3;
            double b4 = 3.7;
            float c4 = (float)(a4 + b4);
            Console.WriteLine(c4);



            //4.Для каждого числового типа вывести минимальное и максимальное значение.

            Console.WriteLine($"{typeof(byte).Name}; Min value: {byte.MinValue}; Max value: {byte.MaxValue}");
            Console.WriteLine($"{typeof(sbyte).Name}; Min value: {sbyte.MinValue}; Max value: {sbyte.MaxValue}");
            Console.WriteLine($"{typeof(int).Name}; Min value: {int.MinValue}; Max value: {int.MaxValue}");
            Console.WriteLine($"{typeof(uint).Name}; Min value: {uint.MinValue}; Max value: {uint.MaxValue}");
            Console.WriteLine($"{typeof(short).Name}; Min value: {short.MinValue}; Max value: {short.MaxValue}");
            Console.WriteLine($"{typeof(ushort).Name}; Min value: {ushort.MinValue}; Max value: {ushort.MaxValue}");
            Console.WriteLine($"{typeof(long).Name}; Min value: {long.MinValue}; Max value: {long.MaxValue}");
            Console.WriteLine($"{typeof(ulong).Name}; Min value: {ulong.MinValue}; Max value: {ulong.MaxValue}");
            Console.WriteLine($"{typeof(float).Name}; Min value: {float.MinValue}; Max value: {float.MaxValue}");
            Console.WriteLine($"{typeof(double).Name}; Min value: {double.MinValue}; Max value: {double.MaxValue}");
            Console.WriteLine($"{typeof(decimal).Name}; Min value: {decimal.MinValue}; Max value: {decimal.MaxValue}");




            //5.Для целочисленных типов записать выражения с операциями сложения,

            //    вычитания, умножения.Можно в одном выражении.
            int a5 = 4;
            int b5 = 3;
            int c5 = 2;
            int d5 = 2;
            int e5 = (a5 + b5 - c5) * d5;
            Console.WriteLine(e5);



            //6.Для типов с плавающей запятой записать выражения с операциями 
            //    сложения, вычитания, умножения, деления.
            //    Можно в одном выражении.

            double a6 = 7.3;
            double b6 = 3.7;
            double c6 = 2.5;
            double d6 = 2.0;
            double e6 = 3.0;
            double res6 = (a6 + b6 - c6) * d6 / e6;
            Console.WriteLine(res6);





            //7.Для типа string записать выражение сложения строк.

            string a7 = "kkkkkkk";
            string b7 = "ooooooo";
            Console.WriteLine(a7 + b7);




            //8.   Для типа bool записать выражения с использованием |, ||, &, &&, ^.
            //    К каждому выражению добавить комментарий как работает оператор.
            //    Объяснить разницу между & и &&, | и ||.

            //bool a = true && true; // && - есть "И"...истина И истина дает истину.
            //Console.WriteLine(a);

            //bool a = true && false; // && - есть "И"...истина И ложь  дает ложь.
            //Console.WriteLine(a);

            //bool a = true || true; // && - есть "И"...истина ИЛИ истина  дает истину.
            //Console.WriteLine(a);

            //bool a = true || false; // && - есть "И"...истина ИЛИ ложь  дает истину,так как может быть то и то.
            //Console.WriteLine(a); 


            /* рассмотрим пары операций | и || ( & и &&)
            *они выполняют похожие действия, но они не совсем одинаковы.

           * В выражениях с | и & ---вычисляется оба значения true и  false по очереди и потом принимается решение .
            *В выражениях с || и && ---вычисляется по одному значенияю, в зависимости true или  false и сразу
            *принимается решение,т.е.более быстрое решение.*/



            //Рассмотрим операцию   ^- операция исключающая "ИЛИ":

            //bool a = true ^ false;/*возвращает true... .Возвращает true если первый 
            //                          или второй операнд равны true,но не вместе,
            //                             иначе возвращает false*/
            //Console.WriteLine(a);


            //bool a = true ^ true;// возвращает false
            //Console.WriteLine(a);

            //bool a = false ^ false;// возвращает false
            //Console.WriteLine(a);

            //bool a = false ^ true;// возвращает true
            //Console.WriteLine(a);

        }
    }
}

        
    

