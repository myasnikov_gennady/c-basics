﻿using System;

namespace HW_2_Korop_Alex
{
    class Program
    {
        static void Main(string[] args)
        {
            sbyte _sbyte = 100;
            byte _byte = 200;
            short _short = 300;
            ushort _ushort = 400;
            int _int = 500;
            uint _uint = 600;
            long _long = 700;
            ulong _ulong = 800;
            float _float = 20.2f;
            double _double = 200.2d;
            decimal _decimal = 2000.2m;
            string _string1 = "1234";
            string _string2 = "5678";
            bool _bool;

            Console.WriteLine("Начальные данные:");
            Console.WriteLine($"sbyte = {_sbyte}");
            Console.WriteLine($"byte = {_byte}");
            Console.WriteLine($"short = {_short}");
            Console.WriteLine($"ushort = {_ushort}");
            Console.WriteLine($"int = {_int}");
            Console.WriteLine($"uint = {_uint}");
            Console.WriteLine($"long = {_long}");
            Console.WriteLine($"ulong = {_ulong}");
            Console.WriteLine($"float = {_float}");
            Console.WriteLine($"double = {_double}");
            Console.WriteLine($"decimal = {_decimal}");
            Console.WriteLine($"string1 = {_string1}");
            Console.WriteLine($"string2 = {_string2}");

               

            Console.WriteLine("\n\n\nНеявные преобразования: ");
            Console.WriteLine("\nНе поддерживается неявное преобразование в типы byte и sbyte");
            Console.WriteLine("Не поддерживается неявное преобразование между типом decimal и типами float или double.");

            _short = _byte;
            Console.WriteLine($"\nПреобразование из byte в short = {_short} ");
            _short = 300;
            _short = _sbyte;
            Console.WriteLine($"Преобразование из sbyte в short = {_short}");
            _short = 300;
            _ushort = _byte;
            Console.WriteLine($"Преобразование из byte в ushort = {_ushort}");
            _ushort = 400;
            _int = _short;
            Console.WriteLine($"Преобразование из short в int = {_int}");
            _int = 500;
            _uint = _ushort;
            Console.WriteLine($"Преобразование из ushort в uing = {_uint}");
            _uint = 600;
            _long = _int;
            Console.WriteLine($"Преобразование из int в long = {_long}");
            _long = 700;
            _ulong = _uint;
            Console.WriteLine($"Преобразование из uing в ulong = {_ulong}");
            _ulong = 800;
            _float = _ulong;
            Console.WriteLine($"Преобразование из ulong в float = {_float}");
            _float = 20.2f;
            _float = _long;
            Console.WriteLine($"Преобразование из long в float = {_float}");
            _float = 20.2f;
            _double = _float;
            Console.WriteLine($"Преобразование из float в double = {_double}");
            _double = 200.2d;
            _decimal = _int;
            Console.WriteLine($"Преобразование из int в decimal = {_decimal}");
            _decimal = 2000.2m;



            Console.WriteLine("\n\n\nЯвные преобразования: ");



            _sbyte = (sbyte)_byte;
            Console.WriteLine($"\nПреобразование из byte в sbyte = {_sbyte} ");
            _sbyte = 100;
            _byte = (byte)_short;
            Console.WriteLine($"Преобразование из short в byte = {_byte} ");
            _byte = 200;
            _short = (short)_ushort;
            Console.WriteLine($"Преобразование из ushort в short = {_short} ");
            _short = 300;
            _ushort = (ushort)_int;
            Console.WriteLine($"Преобразование из int в ushort = {_ushort} ");
            _ushort = 400;
            _int = (int)_uint;
            Console.WriteLine($"Преобразование из uint в int = {_int} ");
            _int = 500;
            _uint = (uint)_long;
            Console.WriteLine($"Преобразование из long в uint = {_uint} ");
            _uint = 600;
            _long = (long)_ulong;
            Console.WriteLine($"Преобразование из ulong в long = {_long} ");
            _long = 700;
            _ulong = (ulong)_float;
            Console.WriteLine($"Преобразование из float в ulong = {_ulong} ");
            _ulong = 800;
            _float = (float)_double;
            Console.WriteLine($"Преобразование из double в float = {_float} ");
            _float = 20.2f;
            _double = (double)_decimal;
            Console.WriteLine($"Преобразование из decimal в double = {_double} ");
            _double = 200.2d;



            Console.WriteLine("\n\n\nЯвные преобразования со словом checked: ");



           
            
            _short = checked((short)_ushort);
            Console.WriteLine($"Преобразование из ushort в short = {_short} ");
            _short = 300;
            _ushort = checked((ushort)_int);
            Console.WriteLine($"Преобразование из int в ushort = {_ushort} ");
            _ushort = 400;
            _int = checked((int)_uint);
            Console.WriteLine($"Преобразование из uint в int = {_int} ");
            _int = 500;
            _uint = checked((uint)_long);
            Console.WriteLine($"Преобразование из long в uint = {_uint} ");
            _uint = 600;
            _long = checked((long)_ulong);
            Console.WriteLine($"Преобразование из ulong в long = {_long} ");
            _long = 700;
            _ulong = checked((ulong)_float);
            Console.WriteLine($"Преобразование из float в ulong = {_ulong} ");
            _ulong = 800;
            _float = checked((float)_double);
            Console.WriteLine($"Преобразование из double в float = {_float} ");
            _float = 20.2f;
            _double = checked((double)_decimal);
            Console.WriteLine($"Преобразование из decimal в double = {_double} ");
            _double = 200.2d;

            try
            {
                _byte = checked((byte)_short);                          
            }
            catch (OverflowException)
            {
                Console.WriteLine($"Исключение для явного преобразования из short в s byte: Значение short = {_short} > максимального значения byte = {byte.MaxValue} ");
            }

            try
            {
                _sbyte = checked((sbyte)_byte);
            }
            catch (OverflowException)
            {
                Console.WriteLine($"Исключение для явного преобразования из byte в sbyte: Значение byte = {_byte} > максимального значения sbyte = {sbyte.MaxValue} ");
               
            }


            Console.WriteLine("\n\n\nМинимальные и максимальные значения для каждого числового типа: ");



            Console.WriteLine($"\n{typeof(sbyte).Name}; Min value: {sbyte.MinValue}; Max value: {sbyte.MaxValue}");
            Console.WriteLine($"{typeof(byte).Name}; Min value: {byte.MinValue}; Max value: {byte.MaxValue}");
            Console.WriteLine($"{typeof(short).Name}; Min value: {short.MinValue}; Max value: {short.MaxValue}");
            Console.WriteLine($"{typeof(ushort).Name}; Min value: {ushort.MinValue}; Max value: {ushort.MaxValue}");
            Console.WriteLine($"{typeof(int).Name}; Min value: {int.MinValue}; Max value: {int.MaxValue}");
            Console.WriteLine($"{typeof(uint).Name}; Min value: {uint.MinValue}; Max value: {uint.MaxValue}");
            Console.WriteLine($"{typeof(long).Name}; Min value: {long.MinValue}; Max value: {long.MaxValue}");
            Console.WriteLine($"{typeof(ulong).Name}; Min value: {ulong.MinValue}; Max value: {ulong.MaxValue}");
            Console.WriteLine($"{typeof(float).Name}; Min value: {float.MinValue}; Max value: {float.MaxValue}");
            Console.WriteLine($"{typeof(double).Name}; Min value: {double.MinValue}; Max value: {double.MaxValue}");
            Console.WriteLine($"{typeof(decimal).Name}; Min value: {decimal.MinValue}; Max value: {decimal.MaxValue}");



            Console.WriteLine("\n\n\nАрифметические опперации для целочисленных типов: ");



            _sbyte = (sbyte)((_sbyte - 70 + 4) * 2);
            Console.WriteLine($"\nВыражение для sbyte: (_sbyte - 100 + 4) * 2 = {_sbyte}");
            _byte = (byte)((_byte -150 + 8) * 2);
            Console.WriteLine($"Выражение для byte: (_byte -200 + 8) * 2 = {_byte}");
            _short = (short)((_short - 278 + 14) * 4);
            Console.WriteLine($"Выражение для short: (_short - 31244 + 457) * 4 = {_short}");
            _ushort = (ushort)((_ushort - 380 + 17) * 3);
            Console.WriteLine($"Выражение для ushort: (_ushort - 48526 + 1005) * 3 = {_ushort}");
            _int = (int)((_int - 490 + 3) * 4);
            Console.WriteLine($"Выражение для int: (_int - 1947483646 + 4845654) * 4 = {_int}");
            _uint = (uint)((_uint - 500 + 26) * 5);
            Console.WriteLine($"Выражение для uint: (_uint - 3147483646 + 854254) * 5 = {_uint}");
            _long = (long)((_long - 599 + 66) * 2);
            Console.WriteLine($"Выражение для long: (_long - 7223372036854775806 + 74829482654645) * 2 = {_long}");
            _ulong = (ulong)((_ulong - 675 + 2) * 2);
            Console.WriteLine($"Выражение для ulong: (_ulong - 15446744853709954117 + 4452429642984) * 2 = {_ulong}");
            
                                             

            Console.WriteLine("\n\n\nАрифметические опперации для типов с плавающей запятой: ");



            _float = (_float * 2 - 10) / 4;
            Console.WriteLine($"\nВыражение для float: (_float * 2 - 10) / 4 = {_float}");
            _double = (_double * 2 - 10) / 4;
            Console.WriteLine($"Выражение для double: (_double * 2 - 10) / 4 =  {_double}");
            _decimal = (_decimal * 2 - 10) / 4;
            Console.WriteLine($"Выражение для decimal: (_decimal * 2 - 10) / 4 = {_decimal}");




            Console.WriteLine($"\n\n\nСложение строк string1 и string2 = {_string1 + _string2} ");



            Console.WriteLine("Результатом операций & и && является true. Для этого оба операнда должны быть true (если хотябы" +
                              "\nодин операнд false, то и всё выражение будет false).Результатом операций | и || является true." +
                              "\nДля этого хотябы один операнд должен иметь значение true. Раздница между & и && в том, что & вычисляет" +
                              "\nоба операнда(независимо является ли первый операнд false), " +
                              "\nа && так же вычисляет оба операнда, но если левый будет false, то правый он не вычисляет." +
                              "\n Для упрощения можно принять false и true за 0 и 1 соответственно. & и && принять за умножение(*)," +
                              "\n а | и || за сложение. Оператор | так же как и & вычисляет оба операнда независимо от их значений. " +
                              "\nА || не вычисляет правую часть если левая true. Оператор ^ возвращает true если только один из операндов true," +
                              "\nесли же оба оператора одинаковы, тогда возвращает false.");

            Console.WriteLine("\nОперации логического И (&):");

            _bool = true & true;                                        // Оператор & выведет true потому что хотябы один операнд true (1 * 1 = 1); Считает обе части
            Console.WriteLine(_bool);
            _bool = true & false;                                       // Оператор & выведет false потому что правый операнд false (1 * 0 = 0); Считает обе части
            Console.WriteLine(_bool);
            _bool = false & true;                                       // Оператор & выведет false потому что левый операнд false (0 * 1 = 0); Считает обе части
            Console.WriteLine(_bool);
            _bool = false & false;                                      // Оператор & выведет false потому что оба операнда false (0 * 0 = 0); Считает обе части
            Console.WriteLine(_bool);

            Console.WriteLine("\nОперации логического И (&&):");

            _bool = true && true;                                       // Оператор && выведет true потому что левый операнд true; Считает обе части
            Console.WriteLine(_bool);
            _bool = true && false;                                      // Оператор && выведет false потому что правый операнд false; Считает обе части
            Console.WriteLine(_bool);
            _bool = false && true;                                      // Оператор && выведет false потому что первый операнд false и дальше производить опериции не требуется; Считает только левую часть
            Console.WriteLine(_bool);
            _bool = false && false;                                     // Оператор && выведет false потому что первый операнд false и дальше производить опериции не требуется; Считает только левую часть
            Console.WriteLine(_bool);
           
            Console.WriteLine("\nОперации логического ИЛИ (|):");

            _bool = true | true;                                        // Оператор | выведет true потому что хотябы один операнд true (1 + 1 >= 1); Считает обе части
            Console.WriteLine(_bool);
            _bool = true | false;                                       // Оператор | выведет true потому что левый операнд true (1 + 0 >= 1); Считает обе части
            Console.WriteLine(_bool);
            _bool = false | true;                                       // Оператор | выведет true потому что правый операнд true (0 + 1 >= 1); Считает обе части
            Console.WriteLine(_bool);
            _bool = false | false;                                      // Оператор | выведет false потому что оба операнда false (0 + 0 = 0); Считает обе части
            Console.WriteLine(_bool);

            Console.WriteLine("\nОперации логического ИЛИ (||):");

            _bool = true || true;                                       // Оператор || выведет true потому что левый операнд true; Считает только левую часть
            Console.WriteLine(_bool);
            _bool = true || false;                                      // Оператор || выведет true потому что левый операнд true; Считает только левую часть
            Console.WriteLine(_bool);
            _bool = false || true;                                      // Оператор || выведет true потому что правый операнд true; Считает обе части
            Console.WriteLine(_bool);
            _bool = false || false;                                     // Оператор || выведет false потому что оба операнд false; Считает обе части
            Console.WriteLine(_bool);

            Console.WriteLine("\nОперации логического исключания ИЛИ (^):");

            _bool = true ^ true;                                       // Оператор || выведет false потому что оба операнда одинаковы; 
            Console.WriteLine(_bool);
            _bool = true ^ false;                                      // Оператор || выведет false потому что только один операнд true а второй false(разные опперанды);
            Console.WriteLine(_bool);
            _bool = false ^ true;                                      // Оператор || выведет false потому что только один операнд true а второй false (разные опперанды);
            Console.WriteLine(_bool);
            _bool = false ^ false;                                     // Оператор || выведет false потому что оба операнда одинаковы;
            Console.WriteLine(_bool);
                       


            Console.ReadKey();
        }
    }
}
