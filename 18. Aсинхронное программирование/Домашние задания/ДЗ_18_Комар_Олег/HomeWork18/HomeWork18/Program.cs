﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HomeWork18
{
    internal static class Program
    {
        private static void Main()
        {
            var tokenSource = new CancellationTokenSource();
            var token = tokenSource.Token;

            var cancelTask = Task.Run(() =>
            {
                while (Console.ReadKey().Key != ConsoleKey.Escape)
                {
                }
                Console.WriteLine();
                Console.WriteLine("Чтение файла отменено пользователем");
                tokenSource.Cancel();
            }, token);

            var taskRead = ReadAsync(token);

            Task.WaitAny(cancelTask, taskRead);
            
            Console.WriteLine("Нажмите любую клавишу для выхода из программы...");
            Console.ReadKey();
        }

        private static async Task ReadAsync(CancellationToken token)
        {
            string data;    //Для того, чтобы удостовериться прочитан ли файл
            Console.WriteLine("Введите путь к файлу, который необходимо прочитать:");
            var path = Convert.ToString(Console.ReadLine());
            Console.WriteLine("Читаем файл - " + path);
            Console.WriteLine("Нажмите Esc, если вы хотите отменить чтение файла...");
            await using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                var array = new byte[fs.Length];
                var readFs = fs.ReadAsync(array, 0, array.Length, token);
                await await Task.WhenAny(
                    readFs,
                    Task.Delay(-1, token));
                data = Encoding.Default.GetString(array);
            }
            if (!token.IsCancellationRequested)
            {
                Console.WriteLine($"Данные из файла {path}:\n {data[^500..]}");
                Console.WriteLine("Файл прочитан");
            }
        }
    }
}