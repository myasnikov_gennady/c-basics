﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace ДЗ_18_Филатов_ВС
{
    class Program
    {
        public static void Main()
        {
            var tokenSource = new CancellationTokenSource();
            var token = tokenSource.Token;
            var taskCancellation = Task.Run(() =>
            {
                while (Console.ReadKey().Key != ConsoleKey.Y)
                {
                }
                Console.WriteLine("\nОжидайте...");
                tokenSource.Cancel();
            }, token);
            var taskRead = ReadFileAsync(token);
            Task.WaitAny(taskRead, taskCancellation);
            Console.WriteLine("Нажмите любую клавишу для выхода");
            Console.ReadKey();
            Console.Write("Ожидайте...");
        }

        private static async Task ReadFileAsync(CancellationToken token)
        {
            string file;
            Console.WriteLine("Введите полный путь файла:");
            file = Console.ReadLine();
            //file = "D://1.avi";    //использовал для отладки, чтобы не прописывать каждый раз путь
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Начало чтения файла {file}");
            Console.ResetColor();
            Console.WriteLine("Прервать чтение файла? y:");
            try
            {
                await using (var _fs = new FileStream(file, FileMode.Open, FileAccess.Read))
                {
                    var _bytes = new byte[_fs.Length];
                    var readAsync = _fs.ReadAsync(_bytes, 0, _bytes.Length, token);
                    await await Task.WhenAny(readAsync, Task.Delay(-1, token));
                }
                if (!token.IsCancellationRequested)
                {
                    Console.WriteLine($"Чтение файла {file} успешно завершено!");
                }
            }
            catch (TaskCanceledException)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Чтение файла {file} отменено!");
                Console.ResetColor();
            }
        }
    }
}
