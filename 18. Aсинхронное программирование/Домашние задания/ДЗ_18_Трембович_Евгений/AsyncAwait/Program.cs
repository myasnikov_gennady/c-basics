﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncAwait
{
    class Program
    {
        static void Main(string[] args)
        {
            var tokenSource = new CancellationTokenSource();
            
            Console.WriteLine("Enter file path:");

            var path = Console.ReadLine();

            var task = ReadFileAsync(path, tokenSource.Token);

            Console.WriteLine("Cancel ReadFileAsync? y/n:");

            var str = Console.ReadLine();
            if (str == "y")
            {
                tokenSource.Cancel();
            }

            task.Wait();
        }

        private static async Task ReadFileAsync(string path, CancellationToken token)
        {
            Console.WriteLine("Start ReadFile");

            try
            {
                await Task.Delay(1, token);

                await File.ReadAllTextAsync(path, token);

                Console.WriteLine("End ReadFile");
            }
            catch (TaskCanceledException)
            {
                Console.WriteLine("ReadFile cancelled.");
            }
        }
    }
}
