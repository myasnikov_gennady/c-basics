﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HW_18_Korop_Alexander
{
    class Program
    {
        private static async Task Main()
        {
            string cancelString = default;
            var tokenS = new CancellationTokenSource();
            Console.WriteLine("Start Main");
            var task = Read(tokenS.Token);
            var cancel = Task.Run(() =>
            {
                Console.Write("Enter y for cancel Read task:");
                while (cancelString != "y")
                {
                }
                Console.WriteLine();
                Console.WriteLine("Чтение файла отменено пользователем");
                tokenS.Cancel();
            }, tokenS.Token);
            cancelString = Console.ReadLine();
            await Task.WhenAny(task, cancel);
            Console.WriteLine("End");
            //Console.ReadKey();
        }
        private static async Task Read(CancellationToken token)
        {            
            string path;
            path = @"E:\GAIN.7z";
            Console.WriteLine("Enter path");
            path = Console.ReadLine();                  
            await using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                var bytes = new byte[fs.Length];
                var read = fs.ReadAsync(bytes, 0, bytes.Length, token);
                await await Task.WhenAny(read, Task.Delay(-1, token));                    
            }
            if (!token.IsCancellationRequested)
            {   
                Console.WriteLine($"File {path} has been read");                    
            }                       
        }
    }
}
