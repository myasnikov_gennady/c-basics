﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace DZ18
{
    class Program   // Не засчитана. Скопирована у Владимира Скрипкина
    {
        static void Main(string[] args) // отмена чтения файла происходит не во время операции чтения, а перед. см. текст консоли.
                                        // Правильная последовательность следующая:
                                        // 1. Получение пути файла и начало его считывания ("Начало чтения!")
                                        // 2. Во время чтения спришиваем "Отменить чтение файла? Да/Нет"
                                        // 3. Если Да - "завершаем" чтение и выводим "Чтение файла отменено!"
                                        // 4. Иначе - "Конец чтения файла"
        {
            var token = new CancellationTokenSource();
            string path, key = null;
            Console.WriteLine("Введите путь к файлу:");
            path = Console.ReadLine();           
            Console.WriteLine("Отменить чтение файла? Да/Нет:");
            key = Console.ReadLine();
            var task = ReadFile(path, token.Token);
            if (key == "Да")
            {
                token.Cancel();
            }
            else
            {
                task.Wait();
            }
        }
        private static async Task ReadFile(string path, CancellationToken token)
        {
            Console.WriteLine("Начало чтения!");

            try
            {
                await Task.Delay(1, token);

                await File.ReadAllTextAsync(path, token);

                Console.WriteLine("Конец чтения файла");
            }
            catch (TaskCanceledException)
            {
                Console.WriteLine("Чтение файла отменено!");
            }
        }
    }
}
