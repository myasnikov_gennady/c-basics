﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Lesson18
{
    internal static class Program
    {
        private static async Task Main()
        {
            var tokenSource = new CancellationTokenSource();
            Console.WriteLine("Start Main");
            var task1 = Do1(tokenSource.Token);
            var task2 = Do2();
            Console.WriteLine("Cancel Do1? y/n:");
            var str = Console.ReadLine();
            if (str == "y")
            {
                tokenSource.Cancel();
            }

            await Task.WhenAny(task1, task2).ContinueWith(task =>
            {
                if (task.Result == task1)
                {
                    Console.WriteLine("Do1 is done first");
                }

                if (task.Result == task2)
                {
                    Console.WriteLine("Do2 is done first");
                }
            });

            await Task.WhenAll(task1, task2);
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
            Console.WriteLine("End Main");
        }
        private static async Task Do1(CancellationToken token)
        {
            try
            {
                Console.WriteLine("Start Do1");
                await Task.Delay(10000, token);
                Console.WriteLine("End Do1");
            }
            catch(TaskCanceledException)
            {
                Console.WriteLine("Cancell Do1");
            }

            /*Console.WriteLine("Start Do1");
            await Task.Run(() =>
            {
                var currentTime = DateTime.Now;
                while (true)
                {
                    if (token.IsCancellationRequested)
                    {
                        break;
                    }
                    if (DateTime.Now - curentTime >= TimeSpan.FromSeconds(10))
                    {
                        break;
                    }
                }
                Console.WriteLine("End Do1");
            }, token);*/
        }

        private static async Task Do2()
        {
            Console.WriteLine("Start Do2");
            await Task.Run(() => Thread.Sleep(5000));
            Console.WriteLine("End Do2");
        }
    }
}
