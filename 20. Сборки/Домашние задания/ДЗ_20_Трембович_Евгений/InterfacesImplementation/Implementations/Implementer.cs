﻿using InterfacesImplementation.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InterfacesImplementation.Implementations
{
    public class Implementer<T> : ICrud<T>, ISaveReadRange<T>
    {
        private List<T> list = new List<T>();

        public void Create(T item)
        {
            list.Add(item);
        }

        public T Read()
        {
            if (!list.Any())
            {
                return default(T);
            }

            var random = new Random();

            var index = random.Next(0, list.Count - 1);

            return list[index];
        }

        public void Update(int index, T item)
        {
            CheckIndex(index);

            list[index] = item;
        }

        public void Delete(int index)
        {
            CheckIndex(index);

            list.RemoveAt(index);
        }

        public void SaveRange(List<T> items)
        {
            list.AddRange(items);
        }

        public List<T> ReadAll()
        {
            return list;
        }

        private void CheckIndex(int index)
        {
            if (index > list.Count - 1)
            {
                throw new IndexOutOfRangeException("Index is out of range.");
            }
        }
    }
}
