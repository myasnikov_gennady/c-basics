﻿using System.Collections.Generic;

namespace InterfacesImplementation.Interfaces
{
    public interface ISaveReadRange<T>
    {
        void SaveRange(List<T> items);

        List<T> ReadAll();
    }
}
