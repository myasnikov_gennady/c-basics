﻿namespace InterfacesImplementation
{
    public interface ICrud <T>
    {
        void Create(T item);

        T Read();   // почему в Update и Delete есть ключ, а в Read нету?

        void Update(int index, T item); // Откуда мы знаем, что у типа есть ключ?

        void Delete(int index);
    }
}