﻿using InterfacesImplementation.Implementations;
using System.Collections.Generic;

namespace InterfacesImplementation
{
    class Program
    {
        static void Main(string[] args)
        {
            {
                var implementer = new Implementer<int>();

                var initialList = new List<int> { 1, 2, 3 };

                implementer.SaveRange(initialList);
                implementer.Create(4);
                implementer.Delete(0);
                implementer.Update(1, 5);

                var item = implementer.Read();
                var allItems = implementer.ReadAll();
            }
        }
    }
}
