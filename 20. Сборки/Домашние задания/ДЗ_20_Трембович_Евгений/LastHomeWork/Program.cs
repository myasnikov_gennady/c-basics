﻿using System;

namespace LastHomeWork
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                try
                {
                    Console.WriteLine("Enter the number");
                    int number = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Enter degree");
                    byte power = Convert.ToByte(Console.ReadLine());
                    Console.WriteLine($"Result: {number.InPow(power)}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }

    public static class IntExtension
    {
        public static long InPow(this int i, byte power)
        {
            if (power >= 0 && power <= 5)
            {
                return (long)Math.Pow(i, power);
            }
            else
            {
                throw new Exception("Available degree value from 0 to 5");
            }
        }
    }
}
