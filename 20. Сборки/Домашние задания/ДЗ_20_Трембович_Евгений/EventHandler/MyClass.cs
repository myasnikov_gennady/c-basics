﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventHandler
{
    public class MyClass
    {
        public delegate void EventHandler(object sender, int count);

        public static event EventHandler Notify;

        private static int counter = 0;

        public void Method()
        {
            counter++;
            Console.WriteLine("Method execution");
            Notify?.Invoke(this, counter);
        }
    }
}
