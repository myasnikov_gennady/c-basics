﻿using System;

namespace EventHandler
{
    class Program
    {
        static void Main(string[] args)
        {
            MyClass.Notify += DisplayMessage;

            var instance1 = new MyClass();
            var insatnce2 = new Test();
            var instance3 = new MyClass();

            instance1.Method();
            insatnce2.Method();
            instance3.Method();
        }

        private static void DisplayMessage(object sender, int count)
        {
            Console.WriteLine($"Object info who call method: {sender}" +
                $"\nNumber of method calls: {count}\n");
        }
    }
}
