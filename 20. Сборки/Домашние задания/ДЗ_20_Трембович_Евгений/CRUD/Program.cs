﻿using System;

namespace CRUD
{
    class Program
    {
        static void Main(string[] args)
        {
            var myCrud = new MyCrud<object>();
            myCrud.Create();
            var d = myCrud.Read();
            myCrud.Update(new MyClass);
        }
    }
}
