﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRUD
{
    public interface IMyCrud<T>
    {
            void Create();
            T Read();
            void Update(T obj);
            void Delete();
    }
}
