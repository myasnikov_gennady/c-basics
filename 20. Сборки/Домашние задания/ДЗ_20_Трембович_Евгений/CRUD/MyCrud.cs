﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRUD
{
    public class MyCrud<T> : IMyCrud<T> where T : class, new()  
    {
        private T obj;

        public void Create()
        {
            obj = new T();
        }
        
        public T Read()
        {
            return obj;
        }

        public void Update(T newObj)
        {
            obj = newObj;
        }

        public void Delete()
        {
            obj = null;
        }
    }
}
