﻿using HomeWork20.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HomeWork20
{
    public class NewList : ICrudable<User>, ISaveAllable<User>
    {
        private static readonly Random Random = new Random();

        private readonly List<User> _items;

        public NewList(List<User> items)
        {
            _items = items;
        }

        public void Create(User obj)
        {
            _items.Add(obj);
        }

        public User Read()
        {
            return _items.ElementAt(Random.Next(_items.Count));
        }

        public User Update(int index, string name)
        {
            if (_items.Count - 1 < index)
            {
                throw new IndexOutOfRangeException("Индекс не существует");
            }
            _items[index].Name = name;
            return _items[index];
        }

        public void Delete(int index)
        {
            if (_items.Count - 1 < index)
            {
                throw new IndexOutOfRangeException("Индекс не существует");
            }
            _items.RemoveAt(index);
        }

        public IEnumerable<User> ReadAll()
        {
            return _items;

        }

        public void SaveAll(List<User> obj)
        {
            for (var index = 0; index < obj.Count(); index++)
            {
                _items.Add(obj[index]); ;
            }
        }

    }
}
