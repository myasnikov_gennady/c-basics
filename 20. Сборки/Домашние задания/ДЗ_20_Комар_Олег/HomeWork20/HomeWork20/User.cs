﻿namespace HomeWork20
{
    public class User
    {
        public delegate void UserHandler(object sender, string message);
        public event UserHandler Notify;    // событие должно быть статическим для того, чтобы срабатывать на вызовы методов всех экземпляров, а не только для одного. В вашем случае придется подписываться на события каждого экземпляра и нумерация не будет сквозной.
        private int Count { get; set; }
        public string Name { get; set; }

        public User(string name)
        {
            Name = name;
            Count = 0;
        }

        public void GetAccess()
        {
            Count++;
            Notify?.Invoke(this, $"Пользователь {Name} запросил доступ. Количество запросов - {Count}");
        }


        public override string ToString()
        {
            return $"{Name}";
        }
    }
}
