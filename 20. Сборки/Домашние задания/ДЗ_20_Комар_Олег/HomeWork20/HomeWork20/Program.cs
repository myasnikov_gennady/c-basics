﻿using System;
using System.Collections.Generic;

namespace HomeWork20
{
    internal static class Program
    {
        private static void Main()
        {
            Console.WriteLine("5 в степени 4 равно " + 5.InPow(4));
            Console.WriteLine("15 в степени 5 равно " + 15.InPow(5));
            Console.WriteLine();

            var user = new User("Ben");
            user.Notify += On_Notify;
            user.GetAccess();
            user.GetAccess();
            user.GetAccess();
            user.Notify -= On_Notify;
            Console.WriteLine();


            var user2 = new User("Tom");
            user2.Notify += On_Notify;
            user2.GetAccess();
            user2.GetAccess();
            user2.GetAccess();
            user2.GetAccess();
            user2.Notify -= On_Notify;
            Console.WriteLine();

            var user3 = new User("John");
            var user4 = new User("Mick");
            var user5 = new User("Pol");

            var list = new List<User>() {user, user2};
            var array = new NewList(list);
            Console.WriteLine(string.Join(", ", array.ReadAll()));

            array.SaveAll(new List<User>() { user3, user4, user5 });
            Console.WriteLine(string.Join(", ", array.ReadAll()));

            Console.WriteLine(array.Read());

            array.Update(2, "Garry");
            Console.WriteLine(string.Join(", ", array.ReadAll()));

            array.Create(new User("Bill"));
            Console.WriteLine(string.Join(", ", array.ReadAll()));

            array.Delete(4);
            Console.WriteLine(string.Join(", ", array.ReadAll()));
        }

        private static void On_Notify(object sender, string message)
        {
            Console.WriteLine(message);
        }
    }
}
