﻿using System;

namespace HomeWork20
{
    public static class IntExtension
    {
        public static long InPow(this int x, byte power)
        {
            if (power<6)
            {
                return (long)Math.Pow(x, power);
            }

            throw new ArgumentOutOfRangeException(nameof(power), "Степень должна быть от 0 до 5");
        }
    }
}
