﻿using System.Collections.Generic;

namespace HomeWork20.Interfaces
{
    public interface ISaveAllable<T>
    {
        void SaveAll(List<T> obj);
        IEnumerable<T> ReadAll();
    }
}
