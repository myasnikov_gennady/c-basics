﻿namespace HomeWork20
{
    public interface ICrudable<T>
    {
        void Create(T obj);
        T Read();   // почему в Update и Delete есть ключ, а в Read нету?
        T Update(int key, string text); // Откуда мы знаем, что у типа есть поле text и ключ?
        void Delete(int key);
    }
}
