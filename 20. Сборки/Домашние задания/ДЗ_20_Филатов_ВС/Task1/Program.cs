﻿using System;

namespace Task1
{
    class Program
    {
        public static int x;
        public static byte y;
        static void Main(string[] args)
        {
            Console.WriteLine("Возведени числа Х в степень Y:\nВведите число X:");
            bool xResult = int.TryParse(Console.ReadLine(), out int _x);
            if (xResult)
            {
                x = _x;
            }
            else
            {
                Console.WriteLine("Введены недопустимые символы!");
            }
            Console.WriteLine("Введите показатель степени Y:");
            bool yResult = byte.TryParse(Console.ReadLine(), out byte _y);
            if (yResult)
            {
                y = _y;
            }
            else
            {
                Console.WriteLine("Введены недопустимые символы!");
            }
            try
            {
                var result = x.InPow(y);
                Console.WriteLine($"{x} ^ {y} = {result}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
    public static class IntExtension
    {
        public static long InPow(this int x, byte power)
        {
            if (power <= 5)
            {
                return (long)Math.Pow(x, power);
            }
            else
            {
                throw new Exception ("Показатель степени должен быть от 0 до 5!");
            }
        }
    }
}
