﻿using System;

namespace Task2
{
    class Program
    {
        static void Main()
        {
            var ehc = new EventHandlerClass();
            ehc.ValueChanged += EventHandlerClass.On_Do;
            do
            {
                ehc.Do();
            }
            while (Console.ReadKey().Key != ConsoleKey.Escape);
        }
    }
    public class EventHandlerClass
    {
        public delegate void ValueChangedHandler(object sender, int totalCount);

        public event ValueChangedHandler ValueChanged;  // событие должно быть статическим для того, чтобы срабатывать на вызовы методов всех экземпляров, а не только для одного. В вашем случае придется подписываться на события каждого экземпляра и нумерация не будет сквозной.

        //private static int totalCount;

        private int _totalCount;
        public int TotalCount
        {
            get
            {
                return _totalCount;
            }
            set
            {
                _totalCount = value;
                ValueChanged?.Invoke(this, _totalCount);
            }
        }
        public EventHandlerClass()
        {
            TotalCount = _totalCount;
        }
        internal static void On_Do(object sender, int totalCount)
        {
            Console.WriteLine($"Отправитель события: {sender}\nОбщее количество вызовов метода: {totalCount}");
        }
        public void Do()
        {
            Console.WriteLine("Вызов метода");
            TotalCount++;
            Console.WriteLine("Нажмите Esc для выхода,\nнажмите любую другую клавишу для повторного вызова метода\n");
        }
    }
}
