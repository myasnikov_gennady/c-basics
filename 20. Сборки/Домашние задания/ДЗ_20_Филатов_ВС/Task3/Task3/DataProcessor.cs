﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Task3
{
    public class DataProcessor : IElement<int>, ICollection<int>, IEnumerable<int>
    {
        private readonly List<int> _items;

        public DataProcessor(List<int> List)
        {
            _items = List;
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        public IEnumerator<int> GetEnumerator()
        {
            return _items.GetEnumerator();
        }
        public void AddElement(int item)
        {
            _items.Add(item);
            Console.WriteLine($"Элемент {item} добавлен в коллекцию");
        }

        public int ReadRandomElement()
        {
            var rnd = _items.GetRandom();
            Console.WriteLine($"Случайный элемент {rnd} прочитан из коллекции");
            return rnd;
        }

        public void RemoveRandomElement()
        {
            var rnd = _items.GetRandom();
            _items.Remove(rnd);
            Console.WriteLine($"Случайный элемент {rnd} удален из коллекции");
        }

        public List<int> ReadCollection()
        {
            Console.WriteLine("Вывод элементов коллекции:");
            var listOut = new List<int>();
            foreach (int item in _items)
            {
                listOut.Add(item);
            }
            Console.WriteLine(string.Join(" ", listOut));
            return listOut;
        }

        public void SaveCollection()
        {
            Console.WriteLine("Введите целые элементы коллекции через пробел:");
            var input = Console.ReadLine()?.Split() ?? new string[0];
            var list = new List<int>(input.Select(int.Parse));
            _items.AddRange(list);
            Console.WriteLine("Ввод и сохранение элементов коллекции");
        }
    }
}
