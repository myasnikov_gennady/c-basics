﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task3
{
    internal static class RandomMemeber
    {
        private static readonly Random random = new Random();

        internal static T GetRandom<T>(this IEnumerable<T> collection)
        {
            if (collection.Count() - 1 == 0)
                return default(T);
            return collection.ElementAt(random.Next(collection.Count() - 1));
        }
    }
}