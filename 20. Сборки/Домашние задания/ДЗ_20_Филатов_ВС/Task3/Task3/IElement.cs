﻿namespace Task3
{
    interface IElement<T>
    {
        void AddElement(T item);
        void RemoveRandomElement();
        T ReadRandomElement();
    }
}
