﻿using System.Collections.Generic;

namespace Task3
{
    class Program
    {
        static void Main()
        {
            var list = new List<int>();
            var dataProcessor = new DataProcessor(list);
            dataProcessor.SaveCollection();
            dataProcessor.AddElement(7);
            dataProcessor.AddElement(82);
            dataProcessor.ReadRandomElement();
            dataProcessor.ReadRandomElement();
            dataProcessor.ReadRandomElement();
            dataProcessor.RemoveRandomElement();
            dataProcessor.RemoveRandomElement();
            dataProcessor.RemoveRandomElement();
            dataProcessor.ReadCollection();
        }
    }
}
