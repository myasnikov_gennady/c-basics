﻿using System.Collections.Generic;

namespace Task3
{
    interface ICollection<T>
    {
        List<T> ReadCollection();
        void SaveCollection();  // не соблюдены принципы SOLID. Было бы неплохо передавать в метод коллекцию, чтобы сохранить ее. А создавать коллекцию где-то в другом месте (а не внутри метода читать из консоли).
    }
}
