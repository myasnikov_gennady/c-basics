﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Task3.Interfaces;

namespace DZ20_3
{
    class Interaction : IAddElsReturnAll<Products>, ICRUD<Products>
    {
        public List<Products> list;
        public void Create(Products item)
                {
                    list.Add(item);
                }
        public Products Read()
                {
                    Random rnd = new Random();
                    return list.ElementAt(rnd.Next(0, list.Count));
                }
        public void Delete(int key)
                {
                    list.RemoveAt(key-1);
                }
        public Products Update(int key, string name)
                {
                    list[key-1].Name = name;
                    return list[key-1];
                }
        public void AddElements(List<Products> newList)
        {
            for (int i = 0; i < newList.Count; i++)
            {
                list.Add(newList[i]);
            }
        }
        public void ReturnAll()
        {
            foreach (var item in list)
            {
                Console.WriteLine(item.Name);
            }
        }
        
    }
}
