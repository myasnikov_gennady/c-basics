﻿using System;
using System.Collections.Generic;

namespace DZ20_3
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Products> listM = new List<Products>()
            {
                new Products("POTATO"),
                new Products("TOMAT"),
                new Products("ONION"),
                new Products("PARSLEY")
            };
            List<Products> newlist = new List<Products>()
            {
                new Products("newBAKJAIAN"),
                new Products("newBURAK"),                
            };
            Interaction inter = new Interaction();
            inter.list = listM;
            inter.ReturnAll();
            //Create
            Console.WriteLine("\nCreate");
            inter.Create(new Products("ANANAS"));
            Console.WriteLine();
            inter.ReturnAll();
            //Read random elemen
            Console.WriteLine("\nRead random elemen");
            Console.WriteLine($"Return element is: {inter.Read().Name}");
            //Delete second element
            Console.WriteLine("\nDelete second element");
            inter.Delete(2);
            Console.WriteLine();
            inter.ReturnAll();
            //Updatesecont element
            Console.WriteLine("\nUpdatesecont element");
            inter.Update(2, "cabage");
            Console.WriteLine();
            inter.ReturnAll();
            //AddElements
            Console.WriteLine("\nAddElements");
            inter.AddElements(newlist);
            //ReturnAll elements
            Console.WriteLine("\nReturnAll elements");
            inter.ReturnAll();
        }        
    }
}
