﻿using System;

namespace DZ20_2
{
    class Program
    {
        static void Main(string[] args)
        {
            var de = new EventHandlerClass();
            de.Event += EventHandlerClass.EventMethod;
            for (int i = 0; i < 5; i++)
            {
                de.Method();
            }
        }
        public class EventHandlerClass
        {
            public delegate void DelegateDeclaration(object sender, int totalCount);
            public event DelegateDeclaration Event;     // событие должно быть статическим для того, чтобы срабатывать на вызовы методов всех экземпляров, а не только для одного. В вашем случае придется подписываться на события каждого экземпляра и нумерация не будет сквозной.
            private int _totalCount;
            public int TotalCount
            {
                get
                {
                    return _totalCount;
                }
                set
                {
                    _totalCount = value;
                    Event?.Invoke(this, _totalCount);
                }
            }
            internal static void EventMethod(object sender, int totalCount)
            {
                Console.WriteLine($"Information: {sender}, total count = {totalCount}");
            }
            public void Method()
            {
                TotalCount++;
            }
        }          
    }
}
