﻿using System;

namespace DZ20_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = default;
            byte power = default;

            Console.Write("Enter the number:");
            bool result1 = int.TryParse(Console.ReadLine(), out int numberTryInt);
            if (result1 == true)
                number = numberTryInt;
            else
                Console.WriteLine("Wrong number");
            Console.Write("Enter the power:");
            bool result2 = byte.TryParse(Console.ReadLine(), out byte powerTryByte);
            if (result2 == true)
                power = powerTryByte;
            else
                Console.WriteLine("Wrong power");
            try
            {
                Console.WriteLine($"Number {number} in power {power} = {number.InPow(power)}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
    public static class IntExtension
    {
        public static long InPow(this int number, byte power)
        {
            if (power <= 5)
            {
                return (long)Math.Pow(number, power);
            }
            else
            {
                throw new Exception("The power of the number must be between 0 and 5");
            }
        }
    }
}
