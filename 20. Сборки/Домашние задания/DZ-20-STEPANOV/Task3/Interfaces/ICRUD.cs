﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task3.Interfaces
{
    interface ICRUD<T>
    {
        void Create(T item);
        T Read();   // почему в Update и Delete есть ключ, а в Read нету?
        T Update(int key, string name); // Откуда мы знаем, что у типа есть поле name и ключ?
        void Delete(int key);
    }
}
