﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task3.Interfaces
{
    interface IAddElsReturnAll<T>
    {
        void ReturnAll();   // не выполнен пункт 3.2
        void AddElements(List<T> newList);
    }
}
