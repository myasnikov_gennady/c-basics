﻿using System;

namespace HomeWork4
{
    class Program
    {
        static void Main()
        {
            Arrays();
            ConsoleHelp.ConsoleClear();
            ForForeach();
        }
        public static void Arrays()
        {
            Console.WriteLine("Создадим массив целочисленных значений");

            var numA = 0;
            bool success;

            do
            {
                try
                {
                    Console.WriteLine("Введите размер массива");
                    numA = Convert.ToInt32(Console.ReadLine());
                    success = true;
                    if (numA < 2)
                    {
                        ConsoleHelp.AlertText("\nОшибка!\nВы ввели слишком маленькое значение, недостаточное для создания массива");
                        success = false;
                    }
                    else if (numA > 25)
                    {
                        Console.WriteLine("\nВнимание!\nВведите значение поменьше, иначе мы будем очень долго инициализировать массив");
                        success = false;
                    }

                }
                catch (FormatException)
                {
                    ConsoleHelp.AlertText("\nОшибка!\nВы ввели не числовое значение, введите пожалуйста целочисленное значение");
                    success = false;
                }
                catch (OverflowException)
                {
                    ConsoleHelp.AlertText("\nОшибка!\nВведенное значение больше максимально допустимого для типа System.Int32");
                    success = false;
                }
                catch (Exception)
                {
                    success = false;
                }
            } while (success != true);
            Console.WriteLine($"Длина массива равна {numA}");
            var array = new int[numA];
            for (int i = 0; i < numA; i++)
            {
                do
                {
                    try
                    {
                        Console.WriteLine($"Введите {i}-й элемент массива");
                        array[i] = Convert.ToInt32(Console.ReadLine());
                        success = true;
                    }
                    catch (FormatException)
                    {
                        ConsoleHelp.AlertText("\nОшибка!\nВы ввели не числовое значение, введите пожалуйста целочисленное значение");
                        success = false;
                    }
                    catch (OverflowException)
                    {
                        ConsoleHelp.AlertText("\nОшибка!\nВведенное значение больше максимально допустимого для типа System.Int32");
                        success = false;
                    }
                    catch (Exception)
                    {
                        success = false;
                    }
                } while (success != true);
            }

            Console.WriteLine();
            Console.WriteLine("Массив с первого до последнего элемента:");
            for (var i = 0; i < numA; i++)
            {

                Console.Write(array[i] + " ");

            }

            Console.WriteLine();
            Console.WriteLine("Массив с последнего до первого элемента:");
            for (var i = numA - 1; i >= 0; i--)
            {
                Console.Write(array[i] + " ");
            }
            Console.WriteLine();
            var a = -1;
            var b = -1;
            Console.WriteLine();
            Console.WriteLine("Вычислим сумму двух элемента массива:");
            Console.WriteLine("Обратите внимание, что элементы массива начинаются с нулевого\n");
            do
            {
                try
                {
                    Console.WriteLine("Введите номер первого складываемого элемента:");
                    a = Convert.ToInt32(Console.ReadLine());
                    a = array[a];
                    success = true;
                }
                catch (FormatException)
                {
                    ConsoleHelp.AlertText("\nОшибка!\nВы ввели не числовое значение, введите пожалуйста целочисленное значение");
                    success = false;
                }
                catch (OverflowException)
                {
                    ConsoleHelp.AlertText("\nОшибка!\nВведенное значение больше максимально допустимого для типа System.Int32");
                    success = false;
                }
                catch (IndexOutOfRangeException)
                {
                    ConsoleHelp.AlertText("\nОшибка!\nЗначение вне диапазона длины массива");
                    success = false;
                }
                catch (Exception)
                {
                    success = false;
                }
            } while (success != true);
            do
            {
                try
                {
                    Console.WriteLine("Введите номер второго складываемого элемента:");
                    b = Convert.ToInt32(Console.ReadLine());
                    b = array[b];
                    success = true;
                }
                catch (FormatException)
                {
                    ConsoleHelp.AlertText("\nОшибка!\nВы ввели не числовое значение, введите пожалуйста целочисленное значение");
                    success = false;
                }
                catch (OverflowException)
                {
                    ConsoleHelp.AlertText("\nОшибка!\nВведенное значение больше максимально допустимого для типа System.Int32");
                    success = false;
                }
                catch (IndexOutOfRangeException)
                {
                    ConsoleHelp.AlertText("\nОшибка!\nЗначение вне диапазона длины массива");
                    success = false;
                }
                catch (Exception)
                {
                    success = false;
                }
            } while (success != true);
            var c = a + b;
            Console.WriteLine("Сумма элементов: " + c);
            Console.WriteLine("\n");
        }

        public static void ForForeach()
        {
            Console.WriteLine("Выведем 2 рандомно созданных массива значений");
            var Length = 10;
            var array1 = new int[Length];
            var array2 = new int[Length - 7];
            Random rnd = new Random();
            Console.WriteLine("Первый массив:");
            for (var i = 0; i < array1.Length; i++)
            {
                array1[i] = rnd.Next(0, 9);
            }
            Console.WriteLine();

            foreach (var num in array1)
            {
                Console.WriteLine($"{num}");
            }
            Console.WriteLine();

            Console.WriteLine("Второй массив:");
            for (var i = 0; i < array2.Length; i++)
            {
                array2[i] = rnd.Next(0, 9);
            }
            Console.WriteLine();

            foreach (var num in array2)
            {
                Console.Write(num + " ");
            }
            Console.WriteLine();

            Console.WriteLine("К каждому элементу первого массива добавим значение элемента второго массива, и выведем вертикально");
            foreach (var num in array1)
            {
                foreach (var num2 in array2)
                {
                    Console.Write((num + num2) + " ");
                }
                Console.WriteLine();
            }
            Console.WriteLine();

            Console.WriteLine("Выведем только каждый второй элемент первого массива");
            for (var i = 0; i < array1.Length; i++)
            {
                if ((i % 2) == 0) continue;
                Console.WriteLine(array1[i]);
            }
            Console.WriteLine();

            Console.WriteLine("Выведем является ли значение каждого элемента первого массива четным либо нечетных");
            for (var j = 0; j < array1.Length; j++)
            {
                switch (array1[j] % 2)
                {
                    case 0 when array1[j] == 0:
                        Console.WriteLine(array1[j] + " значение равно нулю");
                        break;
                    case 0:
                        Console.WriteLine(array1[j] + " - четное");
                        break;
                    case 1:
                        Console.WriteLine(array1[j] + " - нечетное");
                        break;
                    default:
                        Console.WriteLine(array1[j] + "..." + j);
                        break;
                }
            }
            Console.WriteLine();
        }
    }
    public static class ConsoleHelp
    {
        public static void ConsoleClear()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Нажмите любую клавишу для просмотра следующего задания");
            Console.ResetColor();
            Console.ReadKey();
            Console.Clear();
        }

        public static void AlertText(string text)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(text);
            Console.ResetColor();
        }
    }
}
