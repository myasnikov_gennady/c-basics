﻿using System;

namespace DZ_4_STEPANOV.A.S
{
    class Program   // полностью скопирована работа у Скрипкина. 0 - не засчитана
    {
        static void Main(string[] args)
        {
            // 1. В работе использовать массивы целочисленных значений вида: var a = new int[] { 1, 2, 3 };
            // 2. Организовать циклы for(...) в двух направлениях.
            //  4.В циклах можно делать проверку условий if..else, switch, =?:.
            // 5.В циклах можно использовать операторы continue, breack, return.

            var a = new int[] { 1, 2, 3, 4, 5, 6, 7 };
            var b = new int[] { 8, 9, 10, 11, 12, 13, 14 };
            Console.WriteLine("Циклы for(...) в двух направлениях");
            for (int i = 0; i < a.Length; i++)
            {
                if (i == 3)
                { continue; }
                if (i < (a.Length - 1))
                {
                    Console.Write($"{a[i]}\t");
                }
                else
                {
                    Console.Write($"{a[i]}\t");
                    Console.WriteLine($"Конец вывода цикла, i = {i}\t /Был пропуск 4го элемента массива - оператором continue.");
                }
            }
            for (int i = a.Length - 1; i >= 0; i--)
            {
                switch (i)
                {

                    case 1:
                        Console.Write($"{a[i]}\t");
                        break;
                    case 2:
                        Console.Write($"{a[i]}\t");
                        break;
                    case 3:
                        Console.Write($"{a[i]}\t");
                        break;
                    case 4:
                        Console.Write($"{a[i]}\t");
                        break;
                    case 5:
                        Console.Write($"{a[i]}\t");
                        break;
                    case 6:
                        Console.Write($"{a[i]}\t");
                        break;
                    case 7:
                        Console.Write($"{a[i]}\t");
                        break;
                    default:
                        Console.Write($"{a[i]}\t i = {i} исключение, конец вывода цикла");
                        Console.WriteLine();
                        break;
                }
            }
            // 3.Организовать цикл - в - цикле foreach для двух массивов.
            Console.WriteLine("Цицикл-в-цикле foreach для двух массивов.");
            int c = 0, d = 0;
            foreach (int element1 in a)
            {
                c += element1;
                Console.Write($"   {element1}\t");
                foreach (int element2 in b)
                {
                    if (element2 > 13)
                    {
                        d += element2;             // условию удовлетворяет только - 14
                    Console.Write($"   {element2}\t");
                }
                if (element1 == a.Length)
                {
                    Console.WriteLine();
                    Console.WriteLine($" {c} - Cумма всех элементов первого массива");
                    Console.WriteLine($" {d} - Cумма всех элементов второго массива удовлетворяющих условию которые больше 10");  // 7 циклов умножить на 14 = 98
                }
                Console.WriteLine();
                Console.ReadKey();

            }
        }
    }
}
