﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW_4_Prykhodzka
{
    class Program
    {
        static void Main(string[] args)
        {

            {
                try
                {
                    Console.WriteLine("Введите первое число первого массива");
                    var a = int.Parse(Console.ReadLine());
                    Console.WriteLine("Введите второе число первого массива");
                    var b = int.Parse(Console.ReadLine());
                    Console.WriteLine("Введите третье число первого массива");
                    var c = int.Parse(Console.ReadLine());
                    Console.WriteLine("Введите четвертое число первого массива");
                    var d = int.Parse(Console.ReadLine());




                    Console.WriteLine();
                    Console.WriteLine("Введите первое число второго массива");
                    var e = int.Parse(Console.ReadLine());
                    Console.WriteLine("Введите второе число второго массива");
                    var f = int.Parse(Console.ReadLine());
                    Console.WriteLine("Введите третье число второго массива");
                    var g = int.Parse(Console.ReadLine());
                    Console.WriteLine("Введите четвертое число второго массива");
                    var h = int.Parse(Console.ReadLine());
                    Console.WriteLine();


                    var array1 = new int[] { a, b, c, d };
                    var array2 = new int[] { e, f, g, h };

                    Array.Reverse(array2);  // для чего был вызван реверс массива?
                    Console.WriteLine("Первый массив");
                    for (int i = 0; i < array1.Length; i++)
                    {

                        Console.WriteLine(array1[i]);


                    }
                    Console.WriteLine();
                    Console.WriteLine("Второй массив");
                    for (int k = 3; k >= 0; k--)
                    {

                        Console.WriteLine(array2[k]);

                    }
                    Console.WriteLine();
                    Console.WriteLine("+1 ко всем значениям первого массива");
                    foreach (int m in array1)
                    {
                        Console.WriteLine(m + 1);


                    }
                    if (a > e)
                    {
                        Console.WriteLine($"Первое число массива 1 = {a} и оно больше первого числа массива 2 = {e}");
                    }
                    else if (a == e)
                    {
                        Console.WriteLine($"Первое число массива 1 = {a} равно первому числу массива 2 = {e}");
                    }
                    else
                    {
                        Console.WriteLine($"Первое число массива 1 = {a} и оно меньше первого числа массива 2 = {e}");

                    }
                }
                catch (FormatException)
                
                {
                    Console.WriteLine("Введите число !!");
                }


            }

        }
    }
}
