﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {

            int array1Length = 4;
            int array2Length = 4;
            int h = 0;
            int sumOfEvenNumbers = 0;
            int sumOfOddNumbers = 0;
            var array1 = new int[array1Length];
            var array2 = new int[array2Length];
            var array3 = new int[array1Length, array2Length];

            Console.WriteLine("Введтите данные в первый массив");
            for (int i = 0; i < array1Length; i++)
            {               
                    array1[i] = Convert.ToInt32(Console.ReadLine());                                     
            }



            Console.WriteLine("\nВведтите данные в первый массив");
            for (int i = 0; i < array2Length; i++)
            {                 
                    array2[i] = Convert.ToInt32(Console.ReadLine());                
            }

            Console.WriteLine("\n\n\n");



            Console.WriteLine("Создание двумерного массива путём перемноженя элемента первого массива со всеми элементами второго в обратном порядке:");
            //Создание и вывод двумерного массива 
            for (int i = 0; i < array1Length; i++)
            {
                for (int j = array2Length - 1; j >= 0; j--)
                {
                    
                    array3[i, h] = array1[i] * array2[j];
                    Console.Write($"{array3[i, h]} ");
                    h++;
                }
                Console.Write("\n");
                h = 0;
            }                    

            Console.WriteLine("\n\n");



            //Подсчет четных и нечетных элементов двумерного массива
            for (int i = 0; i < array1Length; i++)                                      
            {
                for (int j = 0; j < array2Length; j++)
                {
                    switch (Math.Abs(array3[i,j]) % 2)
                    {                        
                        case 1:
                            sumOfOddNumbers++;
                            break;
                        default:
                            sumOfEvenNumbers++;
                            break;
                    }
                }                
            }

            Console.WriteLine($"Сумма четных чисел = {sumOfEvenNumbers}" +
                              $"\nСумма нечетных чисел = {sumOfOddNumbers}");

            Console.WriteLine("\n\n\n");



            Console.WriteLine($"Положительные элементы заменены на 'True', отрицательные на 'False', нули на 'Zero'\n");

            for (int i = 0; i < array1Length; i++)
            {
                for (int j = 0; j < array2Length; j++)
                {
                    var temp = array3[i, j] == 0 ? "Zero" : array3[i, j] > 0 ? "True" : "False";
                    Console.Write($"{temp} ");
                }
                Console.Write("\n");
            }

            Console.WriteLine("\n\n");



            foreach (var element1 in array1)
            {
                Console.Write($"Элемент первого массива: {element1} делится на следующие элементы второго массива без остатка: ");
                foreach (var element2 in array2)
                {
                    if (element2 == 0)
                    {
                        continue;
                    }
                    if ((element1 % element2) == 0)
                    {
                        Console.Write($"{element2} ");
                    }
                    else
                    {                        
                        continue;
                    }
                }
                Console.WriteLine("\n");
            }
                                 

            
            Console.ReadLine();
        }
    }
}
