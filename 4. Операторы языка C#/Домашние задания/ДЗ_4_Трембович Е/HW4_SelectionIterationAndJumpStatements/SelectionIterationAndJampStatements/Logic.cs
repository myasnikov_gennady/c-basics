﻿using System;

namespace SelectionIterationAndJampStatements
{
    public static class Logic
    {
        public static void Execute()
        {
            var array = new int[] { 1, 2, 3, 4, 5 };

            ForLoopInAscendingDirection(array);
            ForLoopInDescendingDirection(array);
            ForeachLoopTask();
        }

        private static void ForLoopInAscendingDirection(int[] array)
        {
            Console.WriteLine(Constants.AscendingSequenceOfNumbersMessage);

            for (var i = 0; i < array.Length; i++)
            {
                Console.WriteLine(array[i]);
            }

            JumpToNextLine(1);
        }

        private static void ForLoopInDescendingDirection(int[] array)
        {
            Console.WriteLine(Constants.DescendingSequenceOfNumbersMessage);

            for (var i = array.Length - 1; i >= 0; i--)
            {
                Console.WriteLine(array[i]);
            }

            JumpToNextLine(1);
        }

        private static void ForeachLoopTask()
        {
            var array1 = CreateAndPrintRandomArray(Constants.ElementsOfArray1Message);
            var array2 = CreateAndPrintRandomArray(Constants.ElementsOfArray2Message);

            Console.WriteLine(Constants.EnterNumberMessage);

            var enteredNumber = Convert.ToInt32(Console.ReadLine());

            var isEnteredNumberExistsInArrays = false;

            foreach (var memberOfArray1 in array1)
            {
                if (memberOfArray1 == enteredNumber)
                {
                    foreach (var memberOfArray2 in array2)
                    {
                        if (memberOfArray2 == enteredNumber)
                        {
                            isEnteredNumberExistsInArrays = true;
                            break;
                        }
                    }
                    break;
                }
            }

            var message = isEnteredNumberExistsInArrays
                ? Constants.EnteredNumberExistsInBothArraysMessage
                : Constants.EnteredNumberDoesNotExistsInBothArraysMessage;

            JumpToNextLine(1);

            Console.WriteLine(message);
        }

        private static void JumpToNextLine(int numberOfLines)
        {
            for (var i = 0; i < numberOfLines; i++)
            {
                Console.WriteLine();
            }
        }

        private static int[] CreateAndPrintRandomArray(string message)
        {
            var array = new int[Constants.ArrayLength];
            var randomNumber = new Random();
            Console.WriteLine(message);

            for (var i = 0; i < array.Length; i++)
            {
                array[i] = randomNumber.Next(Constants.MinRandomValue, Constants.MaxRandomValue);
                Console.Write($"{array[i]}  ");
            }

            JumpToNextLine(2);

            return array;
        }
    }
}
