﻿namespace SelectionIterationAndJampStatements
{
    public static class Constants
    {
        public const string AscendingSequenceOfNumbersMessage = "Ascending sequence of numbers:";

        public const string DescendingSequenceOfNumbersMessage = "Descending sequence of numbers:";

        public const string ElementsOfArray1Message = "Elements of array 1:";

        public const string ElementsOfArray2Message = "Elements of array 2:";

        public const string EnterNumberMessage = "Please enter number from 1 to 30:";

        public const string EnteredNumberExistsInBothArraysMessage = "Entered number exists in both arrays";

        public const string EnteredNumberDoesNotExistsInBothArraysMessage = "Entered number does not exist in both arrays at the same time";

        public const int ArrayLength = 10;

        public const int MinRandomValue = 1;

        public const int MaxRandomValue = 20;
    }
}
