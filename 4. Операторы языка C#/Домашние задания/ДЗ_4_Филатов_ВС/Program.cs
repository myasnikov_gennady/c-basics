﻿using System;

namespace ДЗ_4_Филатов_ВС
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Операторы циклов, переходов, проверки условий");
            Console.WriteLine();

            // 1. В работе использовать массивы целочисленных значений вида: var a = new int[] { 1, 2, 3 }

            var a = new int[] { 1, 8, 7, 15, 69 };
            var b = new int[] { 45, 3, 89, 112, 28, 33 };

            // 2. Организовать циклы for(...) в двух направлениях

            Console.WriteLine("Вывод массивов в прямом порядке:");

            Console.Write("Массив а = ");

            for (int i = 0; i < a.Length; i++)
            {
                Console.Write(a[i] + " ");
            }

            Console.WriteLine();
            Console.Write("Массив b = ");

            for (int i = 0; i < b.Length; i++)
            {
                Console.Write(b[i] + " ");
            }

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Вывод массивов в обратном порядке:");

            Console.Write("Массив а = ");

            for (int i = a.Length - 1; i > -1; i--)
            {
                Console.Write(a[i] + " ");
            }

            Console.WriteLine();
            Console.Write("Массив b = ");

            for (int i = b.Length - 1; i > -1; i--)
            {
                Console.Write(b[i] + " ");
            }
            Console.WriteLine();

            /* 3. Организовать цикл-в-цикле foreach для двух массивов.
            4. В циклах можно делать проверку условий if..else, switch, =?:
            5. В циклах можно использовать операторы continue, break, return.*/

           Console.WriteLine();
            Console.WriteLine("Цикл-в-цикле foreach для двух массивов: ");

            foreach (var member_b in b)
            {
                Console.Write(member_b + " ");
                foreach (var member_a in a)
                {
                    Console.Write(member_a + " ");
                }
            }

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Цикл do...while + блок if...else:");
            var j = 0;
            do
            {
                if (j < 5)
                {
                    Console.WriteLine(j + "<5");
                }
                else if (j > 5)
                {
                    Console.WriteLine(j + ">5");
                }
                else
                {
                    Console.WriteLine(j + "=5");
                }
                j++;
            }
            while (j < 10);

            Console.WriteLine();
            Console.WriteLine("Цикл for + оператор continue:");

            var x = 1;

            for (int y = 0; y < 10; y++)
            {
                if (y < 5)
                {
                    continue;
                }
                Console.WriteLine($"x={x}, y={y}");
                x++;
            }
            
            Console.WriteLine();
            Console.WriteLine("Тернарная операция:");

            var c = 0;

            while (c <10)
            {
                var d = c < 5 ? "c<5" : "c>=5";
                Console.WriteLine(d);
                c++;
            }

            Console.WriteLine();
            Console.WriteLine("Цикл while + блок switch...case + оператор return:");

            j = 0;

            while (j < 10)
            {
                switch (j)
                {
                    case 1:
                        Console.WriteLine(j + "=1");
                        break;
                    case 5:
                        Console.WriteLine(j + "=5");
                        break;
                    case 6:
                        Console.WriteLine(j + "=6 - end");
                        return;
                    default:
                        Console.WriteLine(j + "!=1;" + j + "!=5");
                        break;
                }
                j++;
            }
        }
    }
}
