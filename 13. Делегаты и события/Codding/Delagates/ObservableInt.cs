﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Delagates
{
    public class Observable<T>
    {
        public delegate void ValueChangedHandler(object sender, T number);

        public event ValueChangedHandler ValueChanged;

        private T _value;
        public T Value
        { 
            get 
            {
                return _value;  
            }
            set
            {
                _value = value;
                ValueChanged?.Invoke(this, _value);
            }
        }
    }
}
