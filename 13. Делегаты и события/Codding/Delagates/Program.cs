﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Delagates
{
    class Program
    {
        delegate int Operation(int a, int b);

        static int Sum(int a, int b) => a + b;

        static int Mult(int a, int b) => a * b;

        static Operation CreateOperation() 
        {
            int result = 0;
            Operation del = delegate (int j, int k)
            {
                for (int i = 0; i < j; i++)
                {
                    result += i;
                }
                return result;
            };
            return del;
        }
        static void Main(string[] args)
        {
            var op1 = new Operation(Sum);
            var res = op1(2, 3);
            Console.WriteLine(res);
            op1 = new Operation(Mult);
            res = op1(2, 3);
            Console.WriteLine(res);
            var ob2 = CreateOperation();
            res = ob2(2, 0);
            Console.WriteLine(res);
            res = ob2(3, 0);
            Console.WriteLine(res);
            var ob3 = CreateOperation();
            res = ob2(3, 0);
            Console.WriteLine(res);
            op1 = (int a, int b) => a + b;
            var list = new int[] { 1, 2, 3, 4, 5 };
            Console.WriteLine(string.Join(", ", list.Select(x => new { Value = x}).Select(x => x.Value)));
            var qwerty = new
            {
                Field1 = 1,
                Field2 = nameof(Program)
            };
            Console.WriteLine(qwerty.GetType().FullName);
            var qwerty1 = new
            {
                Field1 = 1,
                Field2 = nameof(Program)
            };
            Console.WriteLine(qwerty == qwerty1);
            var collection = new ObservableCollection<int>();
            collection.CollectionChanged += Collection_CollectionChanged;
            collection.Add(1);
            collection.Add(999);

            var obj = new Observable<int>();
            obj.ValueChanged += Obj_ValueChanged;
            obj.Value = 5;
            obj.ValueChanged -= Obj_ValueChanged;
            obj.Value = 1234;

        }

        private static void Obj_ValueChanged(object sender, int number)
        {
            Console.WriteLine($"Text: {number}");
        }

        private static void Collection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            Console.WriteLine(string.Join(", ", e.NewItems.Cast<int>()));
        }
    }
}
