﻿using System;
using static HW_13_Korop_Alexander.EventGeneratorClass;

namespace HW_13_Korop_Alexander
{
    class Program
    {        
        static void Main(string[] args)
        {
            EventGeneratorClass var = new EventGeneratorClass();
            DelegateDeclaration obj;
            var.Event += Var_Event;
            var.Text = "Var_Event method was called";
            obj = Var_Event;
            obj("hello");
        }
        private static void Var_Event(string textMessage)       // рекомендуется использовать префикс On_
        {
            Console.WriteLine(textMessage);
        }
    }
}
