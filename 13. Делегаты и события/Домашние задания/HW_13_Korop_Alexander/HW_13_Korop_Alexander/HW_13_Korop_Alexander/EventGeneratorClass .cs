﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HW_13_Korop_Alexander
{
    class EventGeneratorClass
    {
        public delegate void DelegateDeclaration(string textMessage);
        public event DelegateDeclaration Event;
        private string text;
        public string Text
        {
            set
            {
                text = value;
                Event?.Invoke(text);
            }
        }
    }
}
