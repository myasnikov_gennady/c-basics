﻿using System;

namespace ДЗ_13_Филатов_ВС
{
    class Program
    {
        public static int ammoPouch { get; private set; } = 10;

        static void Main(string[] args)
        {
            var ammo = new EventHandlerClass(100);
            ammo.ValueChanged += On_Ammo_ValueChanged;
            ammo.Value += ammoPouch;
        }
        private static void On_Ammo_ValueChanged(object sender, int number)    // рекомендуется использовать префикс On_ -> исправлено
        {
            Console.WriteLine($"Ammo: {number} (+{ammoPouch})");
        }
    }
}
