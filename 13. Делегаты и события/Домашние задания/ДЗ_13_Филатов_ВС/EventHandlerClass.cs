﻿namespace ДЗ_13_Филатов_ВС
{
    public class EventHandlerClass
    {
        public delegate void ValueChangedHandler(object sender, int number);

        public event ValueChangedHandler ValueChanged;

        private int _value;
        public int Value
        { 
            get 
            {
                return _value;  
            }
            set
            {
                _value = value;
                ValueChanged?.Invoke(this, _value);
            }
        }
        public EventHandlerClass(int ammo)
        {
            Value = ammo;
        }
    }
}
