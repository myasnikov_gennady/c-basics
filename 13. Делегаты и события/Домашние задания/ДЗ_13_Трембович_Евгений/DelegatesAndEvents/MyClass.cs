﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DelegatesAndEvents
{
    class MyClass
    {
        public delegate void MyClassHeandler(string message);
        public event MyClassHeandler Notify;

        private string field; 
        public string Property 
        { 
            set
            {
                field = value;
                Notify?.Invoke($"Field value is: {value}");
            } 
        }
    }
}
