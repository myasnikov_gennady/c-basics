﻿using System;

namespace DelegatesAndEvents
{
    class Program
    {
        static void Main(string[] args)
        {
            var obj = new MyClass();
            obj.Notify += PrintMessage;
            obj.Property= "Event";
        }
        
        static void PrintMessage(string message)    // рекомендуется использовать префикс On_, например On_PropertyChanged
        {
            Console.WriteLine(message);
        }
    }
}
