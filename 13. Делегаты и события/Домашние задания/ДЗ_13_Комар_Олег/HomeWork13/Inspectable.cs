﻿namespace HomeWork13
{
    internal class Inspectable
    {
        public delegate void ModifyHandler(object sender, string text);

        public event ModifyHandler Modified;

        private string _text;

        public string Text
        {
            get => _text;
            set
            {
                _text = value;
                Modified?.Invoke(this, _text);
            }
        }

        public Inspectable(string str)
        {
            Text = str;
        }
    }
}
