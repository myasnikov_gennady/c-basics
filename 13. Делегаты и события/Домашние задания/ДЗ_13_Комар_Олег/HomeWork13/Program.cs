﻿using System;

namespace HomeWork13
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            const int cycles = 10;
            var sb = new Inspectable(new string("People "));
            sb.Modified += Sb_Modified;
            for (var i = 0; i <= cycles; i++)
            {
                sb.Text += "Hi";
                
            }

            sb.Text = sb.Text.Replace("People", "Hello").Replace("Hi", "People");
            sb.Text = sb.Text.Remove(12);
            //Console.WriteLine($"Текстовое поле - '{sb.Text}'");
            sb.Modified -= Sb_Modified;
        }

        private static void Sb_Modified(object sender, string text)     //рекомендуется использовать префикс On_
        {
            Console.WriteLine($"Новое значение параметра: {text}");
        }
    }
}
