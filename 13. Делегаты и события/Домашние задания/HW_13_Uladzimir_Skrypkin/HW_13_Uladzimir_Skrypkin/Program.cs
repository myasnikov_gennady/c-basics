﻿using System;

namespace HW_13_Uladzimir_Skrypkin
{
    class Program
    {
        public static void Main(string[] args)
        {
            ProcessLogic p = new ProcessLogic();
            p.ProcessCompleted += On_ProcessCompleted; // В случае наступления события, выполнить действия
            p.StartProcess(5);
        }
        private static void On_ProcessCompleted(object sender, EventArgs e)      // рекомендуется использовать префикс On_
        {
            Console.WriteLine("Конец процесса!");
        }
    }
    public class ProcessLogic
    {
        public event EventHandler ProcessCompleted;  // Декларируем событие EventHandler
       
        public void StartProcess(int _number)
        {
            Console.WriteLine("Начало процесса!");
           
            OnProcessCompleted(EventArgs.Empty); //нет данных о событиях        //не самый удачный пример, т.к. тут мы по сути вручную запускаем событие
        }

        protected virtual void OnProcessCompleted(EventArgs e)
        {
            ProcessCompleted?.Invoke(this, e);
        }
    }
}
