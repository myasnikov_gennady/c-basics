﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.Json;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace HW_15_Korop_Alexander
{
    class Program
    {        
        static void Main(string[] args)
        {
            Console.WriteLine("Binary serializer");
            FieldsClass obj = new FieldsClass() { Str = "String", Data = DateTime.Today, Doub = 14.1, Integ = 30, Day = FieldsClass.Days.Monday };
            BinaryFormatter formatter1 = new BinaryFormatter();            
            using (var fs = new FileStream("TEST.dat", FileMode.OpenOrCreate))
            {
                formatter1.Serialize(fs, obj);
                Console.WriteLine("Object was seialised");
            }
            using (FileStream fs = new FileStream("TEST.dat", FileMode.OpenOrCreate))
            {
                FieldsClass newObj1 = (FieldsClass)formatter1.Deserialize(fs);

                Console.WriteLine("Object was deseialised");
                Console.WriteLine($"Str: {newObj1.Str} Data: {newObj1.Data} doubt: {newObj1.Doub} integer: {newObj1.Integ}, day: {newObj1.Day}");
            }

            Console.WriteLine();
            Console.WriteLine("XML serializer");

            XmlSerializer formatter2 = new XmlSerializer(typeof(FieldsClass));
            using (FileStream fs = new FileStream("TESTxml.xml", FileMode.OpenOrCreate))
            {
                formatter2.Serialize(fs, obj);
                Console.WriteLine("Object was seialised");
            }
            using (FileStream fs = new FileStream("TESTxml.xml", FileMode.OpenOrCreate))
            {
                FieldsClass newObj2 = (FieldsClass)formatter2.Deserialize(fs);

                Console.WriteLine("Object was deseialised");
                Console.WriteLine($"Str: {newObj2.Str} Data: {newObj2.Data} doubt: {newObj2.Doub} integer: {newObj2.Integ}, day: {newObj2.Day}");
            }

            Console.WriteLine();
            Console.WriteLine("Json serializer");

            Task.Run(async () =>
            {
                using (FileStream fs = new FileStream("TESTjson.json", FileMode.OpenOrCreate))
                {
                    await System.Text.Json.JsonSerializer.SerializeAsync(fs, obj);
                    Console.WriteLine("Object was seialised");
                }
            }).GetAwaiter().GetResult();

            Task.Run(async () =>
            {
                using (FileStream fs = new FileStream("TESTjson.json", FileMode.OpenOrCreate))
                {
                    FieldsClass newObj3 = await System.Text.Json.JsonSerializer.DeserializeAsync<FieldsClass>(fs);
                    Console.WriteLine("Object was deseialised");
                    Console.WriteLine($"Str: {newObj3.Str} Data: {newObj3.Data} doubt: {newObj3.Doub} integer: {newObj3.Integ}, day: {newObj3.Day}");
                }
            }).GetAwaiter().GetResult();

            Console.WriteLine();
            Console.WriteLine("Json serializer with Newtonsoft.Json");
           
            string buffer = JsonConvert.SerializeObject(obj);
            File.WriteAllText("TESTjsonNew.json", buffer);
            Console.WriteLine("Object was seialised");

            buffer = File.ReadAllText("TESTjsonNew.json");
            FieldsClass newObj4 = JsonConvert.DeserializeObject<FieldsClass>(buffer);
            Console.WriteLine("Object was deseialised");
            Console.WriteLine($"Str: {newObj4.Str} Data: {newObj4.Data} doubt: {newObj4.Doub} integer: {newObj4.Integ}, day: {newObj4.Day}");
        }
    }
}
