﻿using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;
using System.Xml.Serialization;

namespace HW_15_Korop_Alexander
{
    [Serializable]
    public class FieldsClass
    {
        [XmlElement("str")]
        [JsonPropertyName("str")]
        public string Str { get; set; }
        [XmlElement("data")]
        [JsonPropertyName("data")]
        public DateTime Data { get; set; }
        [XmlElement("integ")]
        [JsonPropertyName("integ")]
        public int Integ { get; set; }
        [XmlElement("doub")]
        [JsonPropertyName("doub")]
        public double Doub { get; set; }
        public Days Day { get; set; }
        [System.Text.Json.Serialization.JsonConverter(typeof(JsonStringEnumConverter))]
        [Newtonsoft.Json.JsonConverter(typeof(StringEnumConverter))]       
        public enum Days
        {
            Monday = 1,
            Tuesday,
            Wednesday,
            Thursday,
            Friday,
            Saturday,
            Sunday
        }
    }
}
