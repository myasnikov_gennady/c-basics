﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.Json;
using System.Xml.Serialization;

namespace DZ15
{
    class Program   // нет сериализации через Newtonsoft.Json   // Не засчитана. Скопирована у Владимира Скрипкина
    {
        static async System.Threading.Tasks.Task Main(string[] args)
        {
            var item = new AutoProperty("Name", 7, 29, 19.25, 9030, 3, 20);
            Console.WriteLine($"Создание объекта\nСтринг {item.Name},\n целое число {item.Age},\n перечисление {item.Day},\n десятичная дробь {item.Money},\n время {item.Time}.");
            Console.WriteLine();
            // создаем объект BinaryFormatter
            BinaryFormatter formatter = new BinaryFormatter();
            // получаем поток, куда будем записывать сериализованный объект
            using (FileStream fs = new FileStream("hometask2.dat", FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, item);

                Console.WriteLine("Объект сериализован binary");
            }
            // десериализация из файла hometask2.dat
            using (FileStream fs = new FileStream("hometask2.dat", FileMode.OpenOrCreate))
            {
                AutoProperty newitem = (AutoProperty)formatter.Deserialize(fs);

                Console.WriteLine("Объект десериализован");
                Console.WriteLine($"Стринг {newitem.Name},\n целое число {newitem.Age},\n перечисление {newitem.Day},\n десятичная дробь {newitem.Money},\n время {newitem.Time}.");
            }
            Console.WriteLine();
            // передаем в конструктор тип класса
            XmlSerializer xmlformatter = new XmlSerializer(typeof(AutoProperty));

            // получаем поток, куда будем записывать сериализованный объект
            using (FileStream fs = new FileStream("hometask3.xml", FileMode.OpenOrCreate))
            {
                xmlformatter.Serialize(fs, item);

                Console.WriteLine("Объект сериализован в xml");
            }
            // десериализация из hometask3.xml
            using (FileStream fs = new FileStream("hometask3.xml", FileMode.OpenOrCreate))
            {
                AutoProperty newitem2 = (AutoProperty)xmlformatter.Deserialize(fs);

                Console.WriteLine("Объект десериализован");
                Console.WriteLine($"Стринг {newitem2.Name},\n целое число {newitem2.Age},\n перечисление {newitem2.Day},\n десятичная дробь {newitem2.Money},\n время {newitem2.Time}.");
            }
            Console.WriteLine();
            // сохранение данных  hometask4.json
            using (FileStream fs = new FileStream("hometask4.json", FileMode.OpenOrCreate))
            {
                AutoProperty item2 = new AutoProperty() { Name = "DAMIR", Day = (AutoProperty.Days)2, Age = 18, Money = 67.99, Time= new DateTime(2050, 6, 16) };
                await JsonSerializer.SerializeAsync<AutoProperty>(fs, item2);
                Console.WriteLine("Объект сериализован в json");
            }

            // чтение данных из hometask4.json
            using (FileStream fs = new FileStream("hometask4.json", FileMode.OpenOrCreate))
            {
                Console.WriteLine("Объект десериализован");
                AutoProperty newitem3 = await JsonSerializer.DeserializeAsync<AutoProperty>(fs);
                Console.WriteLine($"Стринг {newitem3.Name},\n целое число {newitem3.Age},\n перечисление {newitem3.Day},\n десятичная дробь {newitem3.Money},\n время {newitem3.Time}.");
            }
            Console.WriteLine("Файлы находятся в folder'е - bin/Debug/net5.0");
        }
    }
    [Serializable]
    public class AutoProperty
    {   
        public string Name { get; set; }
        public Days Day { get; set; }
        public int Age { get; set; }
        public DateTime Time { get; set; }
        public double Money { get; set; }
        public AutoProperty()
        { }
        public AutoProperty(string _name, int _day, int _age, double _money, int year, int month, int day)
        {
            Name = _name;
            Day = (Days)_day;
            Age = _age;
            Money = _money;
            Time= new DateTime(year, month, day);
        }
        public enum Days : int
        {
            Monday = 1,
            Tuesday,
            Wednesday,
            Thursday,
            Friday,
            Saturday,
            Sunday
        }
    }
}
