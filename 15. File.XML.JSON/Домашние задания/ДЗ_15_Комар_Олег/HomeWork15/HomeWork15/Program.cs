﻿using System;

namespace HomeWork15
{
    internal static class Program   // нарушены концепции SOLID (на оценку не влияет). Модели (наследуемые от Serial) слишком много умеют.
    {
        public static void Main()
        {

            var car = new Car
            {
                Make = "Jeep",
                Model = "Wrangler",
                EngineCapacity = 2.4f,
                SerialNo = 10069825,
                Color = Car.ColorEnum.White,
                DateOfManufacture = new DateTime(2020, 7, 20, 14, 15, 30)
            };

            var employee = new Worker()
            {
                Name = "Ivan",
                LastName = "Petrov",
                Gender = Worker.GenderEnum.Male,
                PersonnelNumber = 20512,
                DateOfBirth = new DateTime(1985,10,17),
                Salary = 1005.5f
            };
            
            car.ToDat("car.dat");
            car.DatToConsole("car.dat");
            employee.ToDat("employee.dat");
            employee.DatToConsole("employee.dat");

            car.ToXml("car.xml");
            car.XmlToConsole("car.xml");
            employee.ToXml("employee.xml");
            employee.XmlToConsole("employee.xml");

            car.ToJson("car.json");
            car.JsonToConsole("car.json");
            employee.ToJson("employee.json");
            employee.JsonToConsole("employee.json");

            car.ToJsonNs("carNs.json");
            car.JsonNsToConsole("carNs.json");
            employee.ToJsonNs("employeeNs.json");
            employee.JsonNsToConsole("employeeNs.json");
            
            car.ToJsonNs2("carNs2.json");
            car.JsonNsToConsole2("carNs2.json");
            employee.ToJsonNs2("employeeNs2.json");
            employee.JsonNsToConsole2("employeeNs2.json");
        }
    }
}
