﻿using System;

namespace HomeWork15
{
    public static class ConsoleHelper
    {
        public static void Info(string text)
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine(text);
            Console.ResetColor();
        }
    }

}
