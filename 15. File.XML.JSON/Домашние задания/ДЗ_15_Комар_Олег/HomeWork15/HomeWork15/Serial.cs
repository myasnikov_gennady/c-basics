﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using Newtonsoft.Json;
using JsonSerializerNS = Newtonsoft.Json.JsonSerializer;

namespace HomeWork15
{
    [Serializable]
    public abstract class Serial
    {
        public abstract void DatToConsole(string path);

        public abstract void ToXml(string path);

        public abstract void XmlToConsole(string path);

        public abstract void ToJson(string path);

        public abstract void JsonToConsole(string path);

        public abstract void JsonNsToConsole(string path);
        
        public abstract void JsonNsToConsole2(string path);

        public void ToDat(string path)
        {
            var formatter = new BinaryFormatter();
            using (var fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, this);
                fs.Close();
            }
            ConsoleHelper.Info("Сериализация объекта в DAT-файл завершена");
        }

        public void ToJsonNs(string path)
        {
            Task.Run(async () =>
            {
                var options = new JsonSerializerSettings
                {
                    Formatting = Formatting.Indented
                };

                await File.WriteAllTextAsync(path, JsonConvert.SerializeObject(this, options));
            }).Wait();
            ConsoleHelper.Info("Сериализация в JSON-файл с использованием Newtonsoft.JSON завершена");
        }

        public void ToJsonNs2(string path)
        {
            var options = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
            using (var file = File.CreateText(path))
            {
                var serializer = JsonSerializerNS.Create(options);
                serializer.Serialize(file, this);
                file.Close();
            }
            ConsoleHelper.Info("Сериализация в JSON-файл с использованием Newtonsoft.JSON, вариант 2, завершена");
        }
    }
}
