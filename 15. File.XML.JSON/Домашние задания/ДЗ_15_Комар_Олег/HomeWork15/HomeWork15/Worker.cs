﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using JsonSerializer = System.Text.Json.JsonSerializer;
using JsonSerializerNS = Newtonsoft.Json.JsonSerializer;

namespace HomeWork15
{
    [Serializable]
    public class Worker : Serial
    {
        [System.Text.Json.Serialization.JsonConverter(typeof(JsonStringEnumConverter))]
        [Newtonsoft.Json.JsonConverter(typeof(StringEnumConverter))]
        public enum GenderEnum
        {
            Male =1,
            Female
        }

        [XmlElement("name")]
        [JsonPropertyName("name")]
        [JsonProperty("name")]
        public string Name { get; set; }

        [XmlElement("last_name")]
        [JsonPropertyName("last_name")]
        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [XmlElement("gender")]
        [JsonPropertyName("gender")]
        [JsonProperty("gender")]
        public GenderEnum Gender { get; set; }

        [XmlElement("personnel_number")]
        [JsonPropertyName("personnel_number")]
        [JsonProperty("personnel_number")]
        public int PersonnelNumber { get; set; }

        [XmlElement("date_of_birth")]
        [JsonPropertyName("date_of_birth")]
        [JsonProperty("date_of_birth")]
        public DateTime DateOfBirth { get; set; }

        [XmlElement("salary")]
        [JsonPropertyName("salary")]
        [JsonProperty("salary")]
        public float Salary { get; set; }

        public Worker()
        {
        }

        public override void DatToConsole(string path)
        {
            var formatter = new BinaryFormatter();
            using (var fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                var obj = (Worker)formatter.Deserialize(fs);
                obj.ToConsole();
                fs.Close();
            }
            ConsoleHelper.Info("Десериализация из DAT-файла завершена");
            Console.WriteLine();
        }

        public override void ToXml(string path)
        {
            var formatter = new XmlSerializer(typeof(Worker));

            using (var fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, this);
                fs.Close();
            }
            ConsoleHelper.Info("Сериализация объекта в XML-файл завершена");
        }

        public override void XmlToConsole(string path)
        {
            using (var fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                var formatter = new XmlSerializer(typeof(Worker));
                var obj = (Worker)formatter.Deserialize(fs);
                if (obj != null)
                {
                    obj.ToConsole();
                }
                else Console.WriteLine("XML-файл пустой");
                fs.Close();
            }
            ConsoleHelper.Info("Десериализация из XML-файла завершена");
            Console.WriteLine();
        }

        public override void ToJson(string path)
        {
            Task.Run(async () =>
            {
                var options = new JsonSerializerOptions
                {
                    WriteIndented = true
                };
                using (var fs = new FileStream(path, FileMode.OpenOrCreate))
                {
                    await JsonSerializer.SerializeAsync(fs, this, options);
                    fs.Close();
                }
            }).Wait();
            ConsoleHelper.Info("Сериализация объекта в JSON-файл завершена");
        }

        public override void JsonToConsole(string path)
        {
            Task.Run(async () =>
            {
                using (var fs = new FileStream(path, FileMode.OpenOrCreate))
                {
                    var obj = await JsonSerializer.DeserializeAsync<Worker>(fs);
                    if (obj != null)
                    {
                        obj.ToConsole();
                    }
                    else Console.WriteLine("JSON-файл пустой");
                    fs.Close();
                }
            }).Wait();
            ConsoleHelper.Info("Десериализация из JSON-файл завершена");
            Console.WriteLine();
        }

        public override void JsonNsToConsole(string path)
        {
            Task.Run(async () =>
            {
                var obj = JsonConvert.DeserializeObject<Worker>(await File.ReadAllTextAsync(path));
                if (obj != null)
                {
                    obj.ToConsole();
                }
                else Console.WriteLine("JSON-файл пустой");
            }).Wait();
            ConsoleHelper.Info("Десериализация из JSON-файл с использованием Newtonsoft.JSON завершена");
            Console.WriteLine();
        }

        public override void JsonNsToConsole2(string path)
        {
            using (var file = File.OpenText(path))
            {
                var serializer = new JsonSerializerNS();
                var obj = (Worker)serializer.Deserialize(file, typeof(Worker));
                if (obj != null)
                {
                    obj.ToConsole(); 
                }
                else Console.WriteLine("JSON-файл пустой");
                file.Close();
            }
            ConsoleHelper.Info("Десериализация из JSON-файл с использованием Newtonsoft.JSON, вариант 2, завершена");
            Console.WriteLine();
        }

        public void ToConsole()
        {
            foreach (var prop in typeof(Worker).GetProperties())
            {
                Console.WriteLine($"{prop.Name}: {prop.GetValue(this)}");
            }
        }

        /*public override void ToDat(string path)
        {
            var formatter = new BinaryFormatter();
            using (var fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, this);
                fs.Close();
            }
        }*/

        /*public override async void ToJsonNs(string path)
        {
            var optionsNs = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };

            await File.WriteAllTextAsync(path, JsonConvert.SerializeObject(this, optionsNs));
        }*/

        /*public override void ToJsonNs2(string path)
        {
            var optionsNs = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
            using (var file = File.CreateText(path))
            {
                var serializer = JsonSerializerNS.Create(optionsNs);
                serializer.Serialize(file, this);
                file.Close();
            }
        }*/
    }
}
