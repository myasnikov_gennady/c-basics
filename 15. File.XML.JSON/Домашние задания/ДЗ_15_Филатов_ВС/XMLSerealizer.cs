﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace ДЗ_15_Филатов_ВС
{
    class XMLSerealizer : ISeralizer
    {
        XmlSerializer Formatter = new XmlSerializer(typeof(Item));

        public void Serealize(Item item)
        {
            using (FileStream fs = new FileStream("Homework.xml", FileMode.Create))
            {
                Formatter.Serialize(fs, item);
                fs.Close();
            }
            Console.WriteLine("Объект сериализован");
        }
        public Item Deseralize()
        {
            using (var fs = new FileStream("Homework.xml", FileMode.Open))
            {
                Item newItem = (Item)Formatter.Deserialize(fs);
                fs.Close();
                Console.WriteLine("Объект десериализован");
                return newItem;
            }
        }
    }
}
