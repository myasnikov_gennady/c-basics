﻿using System;

namespace ДЗ_15_Филатов_ВС
{
    class Program
    {
        static void Main(string[] args)
        {
            Item item = new Item("Class", DateTime.Now , 15, 2.8, Item.KeyEnum.Pause);
            Console.WriteLine("Базовый объект:");
            Console.WriteLine(item);

            Console.WriteLine("\nБинарная сериализация:");
            var binSerealizer = new BinarySerealizer();
            binSerealizer.Serealize(item);
            Item newItem = binSerealizer.Deseralize();
            Console.WriteLine(newItem);

            Console.WriteLine("\nXML сериализация:");
            var xmlSerialixer = new XMLSerealizer();
            xmlSerialixer.Serealize(item);
            newItem = xmlSerialixer.Deseralize();
            Console.WriteLine(newItem);

            Console.WriteLine("\nJSON сериализация через System.Text.Json:");
            var jsonSerializer = new JSONSerealizer();
            jsonSerializer.Serealize(item);
            newItem = jsonSerializer.Deseralize();
            Console.WriteLine(newItem);

            Console.WriteLine("\nJSON сериализация через Newtonsoft.Json:");
            var NSjsonSerializer = new NewtonsoftJSON();
            NSjsonSerializer.Serealize(item);
            newItem = NSjsonSerializer.Deseralize();
            Console.WriteLine(newItem);
        }
    }
}
