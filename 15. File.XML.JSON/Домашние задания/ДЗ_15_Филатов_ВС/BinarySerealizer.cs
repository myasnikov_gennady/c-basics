﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace ДЗ_15_Филатов_ВС
{
    class BinarySerealizer : ISeralizer
    {
        BinaryFormatter Formatter = new BinaryFormatter();

        public void Serealize(Item item)
        {
            using var fs = new FileStream("Homework.dat", FileMode.Create);
            Formatter.Serialize(fs, item);
            fs.Close();
            Console.WriteLine("Объект сериализован");
        }
        public Item Deseralize()
        {
            using (FileStream fs = new FileStream("Homework.dat", FileMode.Open))
            {
                Item newItem = (Item)Formatter.Deserialize(fs);
                fs.Close();
                Console.WriteLine("Объект десериализован");
                return newItem;
            }
        }
    }
}
