﻿using Newtonsoft.Json;
using System;
using System.IO;

namespace ДЗ_15_Филатов_ВС
{
    class NewtonsoftJSON : ISeralizer
    {

        public void Serealize(Item item)
        {
            var options = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
            using (StreamWriter sw = new StreamWriter("Homework_NS.json"))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                JsonSerializer Formatter = JsonSerializer.Create(options);
                Formatter.Serialize(writer, item);
            }
            Console.WriteLine("Объект сериализован");
        }
        public Item Deseralize()
        {
            Item newItem = JsonConvert.DeserializeObject<Item>(File.ReadAllText("Homework_NS.json"));
            using (StreamReader file = File.OpenText("Homework_NS.json"))
            {
                JsonSerializer Formatter = new JsonSerializer();
                newItem = (Item)Formatter.Deserialize(file, typeof(Item));
                file.Close();
                Console.WriteLine("Объект десериализован");
                return newItem;
            }
        }
    }
}
