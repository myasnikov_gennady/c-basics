﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Text.Json.Serialization;
using System.Xml.Serialization;

namespace ДЗ_15_Филатов_ВС
{
    [Serializable]
    [XmlRoot(ElementName = "item")]
    public class Item
    {
        [System.Text.Json.Serialization.JsonConverter(typeof(JsonStringEnumConverter))]
        [Newtonsoft.Json.JsonConverter(typeof(StringEnumConverter))]
        public enum KeyEnum : byte
        {
            Backspace,
            Tab,
            Pause
        }

        [XmlElement("name")]
        [JsonPropertyName("name")]
        [JsonProperty("name")]
        public string Name { get; set; }

        [XmlElement("date")]
        [JsonPropertyName("date")]
        [JsonProperty("date")]
        public DateTime Date { get; set; }

        [XmlElement("integerNumber")]
        [JsonPropertyName("integerNumber")]
        [JsonProperty("integerNumber")]
        public int IntegerNumber { get; set; }

        [XmlElement("doubleNumber")]
        [JsonPropertyName("doubleNumber")]
        [JsonProperty("doubleNumber")]
        public double DoubleNumber { get; set; }

        [XmlElement("key")]
        [JsonPropertyName("key")]
        [JsonProperty("key")]
        public KeyEnum Key { get; set; }


        public Item(string name, DateTime date, int integerNumber, double doubleNumber, KeyEnum key)
        {
            Name = name;
            Date = date;
            IntegerNumber = integerNumber;
            DoubleNumber = doubleNumber;
            Key = key;
        }

        public Item()
        {

        }

        public override string ToString()
        {
            return $"{nameof(Name)}: {Name}\n{nameof(Date)}: {Date}\n{nameof(IntegerNumber)}: {IntegerNumber}\n{nameof(DoubleNumber)}: {DoubleNumber}\n{nameof(Key)}: {Key} = {(int)Key}";
        }
    }
}
