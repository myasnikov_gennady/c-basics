﻿using System;
using System.IO;
using System.Text.Json;

namespace ДЗ_15_Филатов_ВС
{
    class JSONSerealizer : ISeralizer
    {

        public void Serealize(Item item)
        {
            var options = new JsonSerializerOptions
            {
                WriteIndented = true
            };
            string jsonString = JsonSerializer.Serialize(item, options);
            File.WriteAllText("Homework.json", jsonString);
            Console.WriteLine("Объект сериализован");
        }
        public Item Deseralize()
        {
            string jsonString = File.ReadAllText("Homework.json");
            Item newItem = JsonSerializer.Deserialize<Item>(jsonString);
            Console.WriteLine("Объект десериализован");
            return newItem;
        }
    }
}
