﻿using SerializeDeserialize.Enums;
using SerializeDeserialize.Implementations;
using SerializeDeserialize.Interfaces;
using SerializeDeserialize.Models;
using System;

namespace SerializeDeserialize
{
    class Program   // очень красивая и аккуратная структура проекта. из пожеланий хотелось бы, чтобы сериализаторы в конструкторе класса принимали путь.полное имя файла для сохранения, а не были захардкоджены.
    {
        private const string stringValue = "Hello";

        private const int integerValue = 5;

        private const decimal decimalValue = 0.5M;

        private static ISerializer<MyClass> serializer;

        static void Main(string[] args)
        {
            var myClassObject = new MyClass(
                stringValue,
                DateTime.UtcNow,
                Enumeration.Five,
                integerValue,
                decimalValue);

            BinarySerialization(myClassObject);
            Console.WriteLine();

            XmlSerialization(myClassObject);
            Console.WriteLine();

            NativeJsonSerialization(myClassObject);
            Console.WriteLine();

            NewtonsoftJsonSerialization(myClassObject);
        }

        private static void BinarySerialization(MyClass model)
        {
            Console.WriteLine("BinarySerialization:");

            serializer = new BinarySerializer<MyClass>();

            Serialization(model, serializer);
        }

        private static void XmlSerialization(MyClass model)
        {
            Console.WriteLine("XmlSerialization:");

            serializer = new XmlSerializer<MyClass>();

            Serialization(model, serializer);
        }

        private static void NativeJsonSerialization(MyClass model)
        {
            Console.WriteLine("NativeJsonSerialization:");

            serializer = new NativeJsonSerializer<MyClass>();

            Serialization(model, serializer);
        }

        private static void NewtonsoftJsonSerialization(MyClass model)
        {
            Console.WriteLine("NewtonsoftJsonSerialization:");

            serializer = new NewtonsoftJsonSerializer<MyClass>();

            Serialization(model, serializer);
        }

        private static void Serialization(MyClass model, ISerializer<MyClass> serializer)
        {
            serializer.Serialize(model);

            var deserializedModel = serializer.Deserialize();

            OutputModel(deserializedModel);
        }

        private static void OutputModel(MyClass model)
        {
            Console.WriteLine($"String value = {model.String}");
            Console.WriteLine($"Date value = {model.Date}");
            Console.WriteLine($"Enum value = {model.Enum}");
            Console.WriteLine($"Integer value = {model.Integer}");
            Console.WriteLine($"Decimal value = {model.Decimal}");
        }
    }
}
