﻿using SerializeDeserialize.Interfaces;
using System.IO;
using System.Text.Json;

namespace SerializeDeserialize.Implementations
{
    public class NativeJsonSerializer<T> : ISerializer<T>
    {
        private string FileName = "NativeJson.txt";

        public void Serialize(T model)
        {
            string json = JsonSerializer.Serialize<T>(model);

            File.WriteAllText(FileName, json);
        }

        public T Deserialize()
        {
            string json = File.ReadAllText(FileName);

            return JsonSerializer.Deserialize<T>(json);
        }
    }
}
