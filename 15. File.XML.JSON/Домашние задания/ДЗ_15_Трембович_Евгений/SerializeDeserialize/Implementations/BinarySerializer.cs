﻿using SerializeDeserialize.Interfaces;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace SerializeDeserialize.Implementations
{
    public class BinarySerializer<T> : ISerializer<T>
    {
        private string FileName = "Binary.txt";

        BinaryFormatter formatter = new BinaryFormatter();

        public void Serialize(T model)
        {
            using (var fs = new FileStream(FileName, FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, model);
            }
        }

        public T Deserialize()
        {
            using (FileStream fs = new FileStream(FileName, FileMode.OpenOrCreate))
            {
                return (T)formatter.Deserialize(fs);
            }
        }
    }
}
