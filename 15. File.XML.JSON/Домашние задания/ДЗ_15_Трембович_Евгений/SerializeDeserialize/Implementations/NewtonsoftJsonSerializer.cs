﻿using Newtonsoft.Json;
using SerializeDeserialize.Interfaces;
using System.IO;

namespace SerializeDeserialize.Implementations
{
    public class NewtonsoftJsonSerializer<T> : ISerializer<T>
    {
        private string FileName = "NewtonsoftJson.txt";

        public void Serialize(T model)
        {
            string json = JsonConvert.SerializeObject(model);

            File.WriteAllText(FileName, json);
        }

        public T Deserialize()
        {
            string json = File.ReadAllText(FileName);

            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}
