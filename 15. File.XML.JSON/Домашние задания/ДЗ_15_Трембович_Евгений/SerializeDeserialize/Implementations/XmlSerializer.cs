﻿using SerializeDeserialize.Interfaces;
using System.IO;
using System.Xml.Serialization;

namespace SerializeDeserialize.Implementations
{
    public class XmlSerializer<T> : ISerializer<T>
    {
        private const string FileName = "XmlFile.xml";

        XmlSerializer formatter = new XmlSerializer(typeof(T));

        public void Serialize(T model)
        {
            using (var fs = new FileStream(FileName, FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, model);
            }
        }

        public T Deserialize()
        {
            using (var fs = new FileStream(FileName, FileMode.OpenOrCreate))
            {
                return (T)formatter.Deserialize(fs);
            }
        }
    }
}
