﻿namespace SerializeDeserialize.Interfaces
{
    public interface ISerializer<T>
    {
        void Serialize(T model);

        T Deserialize();
    }
}
