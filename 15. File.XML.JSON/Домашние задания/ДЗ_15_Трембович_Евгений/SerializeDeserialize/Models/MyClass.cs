﻿using SerializeDeserialize.Enums;
using System;

namespace SerializeDeserialize.Models
{
    [Serializable]
    public class MyClass
    {
        public MyClass()
        {
        }

        public MyClass(
            string stringValue, 
            DateTime dateValue, 
            Enumeration enumValue, 
            int integerValue, 
            decimal decimalValue)
        {
            String = stringValue;
            Date = dateValue;
            Enum = enumValue;
            Integer = integerValue;
            Decimal = decimalValue;
        }

        public string String { get; set; }

        public DateTime Date { get; set; }

        public Enumeration Enum { get; set; }

        public int Integer { get; set; }

        public decimal Decimal { get; set; }
    }
}
