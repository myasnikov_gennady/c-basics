﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.Json;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Lesson15
{
    class Program
    {
        static async Task Main(string[] args)
        {

            var bytes = File.ReadAllBytes("ids.xml");
            var text = File.ReadAllText("ids.xml");
            Console.WriteLine(text);
            var formatter = new XmlSerializer(typeof(Resources));
            Resources resources;
            using (var fs = new FileStream("ids.xml", FileMode.Open))
            {
                 resources = (Resources) formatter.Deserialize(fs);
            }

            using (var fs = new FileStream("ids.json", FileMode.OpenOrCreate))
            {
                await JsonSerializer.SerializeAsync(fs, resources);
            }

            var binFormatter = new BinaryFormatter();
            using (var fs = new FileStream("ids.dat", FileMode.OpenOrCreate))
            {
                binFormatter.Serialize(fs, resources);
            }
        }
    }
}
