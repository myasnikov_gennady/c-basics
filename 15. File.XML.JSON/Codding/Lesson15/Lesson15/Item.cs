﻿using System;
using System.Text.Json.Serialization;
using System.Xml.Serialization;

namespace Lesson15
{
    [Serializable]
    public class Item
    {
        [XmlAttribute("type")]
        [JsonPropertyName("type")]
        public string Type { get; set; }

        [XmlAttribute("name")]
        [JsonPropertyName("name")]
        public string Name { get; set; }
        }
}
