﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using System.Xml.Serialization;

namespace Lesson15
{
    [Serializable]
    [XmlRoot(ElementName = "resources")]
    public class Resources 
    {
        [XmlElement("item")]
        [JsonPropertyName("resources")]
        public List<Item> Items { get; set; }
    }
}
