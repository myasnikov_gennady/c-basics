﻿using System;

namespace Lesson16_GC
{

    class Program
    {

        static void Main()
        {
            var counter = new Counter();
            Pen[] box = new Pen[10];
            for (int i = 0; i < box.GetLength(0); i++)
            {
                box[i] = new Pen(counter.Next());
            }

            for (int i = 0; i < box.GetLength(0); i++)
            {
                box[i] = new Pen(counter.Next());
            }

            //for (int i = 0; i < box.GetLength(0); i++)
            //{
            //    box[i] = new Pen(counter.Next());
            //}

//            box[0] = new Pen();
            foreach (var varPen in box)
            {
                Console.WriteLine(varPen);
            }

            //var pen = new Pen(counter.Next());
            //Console.WriteLine(pen);
            //pen = null;

            System.GC.Collect();

            Console.WriteLine($"Total Pens created is {counter.Current()}");
            Console.ReadKey();
        }
    }
    class Pen
    {
        private readonly int _qty;
        public Pen(int qt)
        {
            _qty = qt;
        }

        ~Pen()
        {
            Console.WriteLine($"Pens left: {_qty}");
        }

        public override string ToString()
        {
            return $"Pen: {_qty}";
        }
    }

    class Counter
    {
        private int qty = 0;

        public int Next()
        {
            return qty++;
        }

        public int Current()
        {
            return qty;
        }

    }
}
