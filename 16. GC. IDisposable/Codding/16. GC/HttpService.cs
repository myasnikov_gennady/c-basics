﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace _16._GC
{
    public class HttpService : IDisposable
    {
        public readonly string testString;
        public readonly int[] _array;
        private readonly Uri _baseUri;
        private readonly HttpClient _client;
        public HttpService(string baseUri)
        {
            _baseUri = new Uri(baseUri);
            _client = new HttpClient()
            {
                BaseAddress = _baseUri
            };
            _array = new int[3000];
            var rnd = new Random();
            for (int i = 0; i < _array.Length; i++)
            {
                _array[i] = rnd.Next();
                testString += _array[i].ToString();
            }
        }

        public async Task<string> GetData()
        {
            var response = await _client.GetAsync(string.Empty);
            return await response.Content.ReadAsStringAsync();
        }
        public void Dispose()
        {
            Dispose(true);
            //GC.SuppressFinalize(this);
        }

        protected void Dispose(bool diposing)
        {
            if (diposing)
            {
                Console.WriteLine("Disposing HTTP client");
                _client.Dispose();
            }
        }

        ~HttpService()
        {
            Console.WriteLine("Finalizing HTTP Service");
            Dispose(true);
        }
    }
}
