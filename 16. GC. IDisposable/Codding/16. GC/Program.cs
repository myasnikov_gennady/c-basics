﻿using System;
using System.Runtime;
using System.Threading.Tasks;

namespace _16._GC
{
    class Program
    {
        static async Task Main(string[] args)
        {
            //string data;
            //using (var service = new HttpService("https://google.com"))
            //var service = new HttpService("https://google.com");
            //{
            //    data = await service.GetData();
            //}
            //await Task.Delay(10000);
            //for (int i = 0; i < 50; i++)
            //{
            //    Console.WriteLine(service._array[i]);
            //}
            //Console.WriteLine(service.testString.Substring(0,100));
            //Console.WriteLine(data.Substring(0,100));
            using (var fs = new FileService(@"D:\Вадим\Видео\Фильмы\Green.Book.2018.DVDScr.MVO.HDrezka.Studio.avi"))
            {
                await fs.ReadBytes();
            }
            using (var fs = new FileService(@"D:\Вадим\Видео\Фильмы\Mi.iz.bydyshego.2.BDRip-AVC.mkv"))
            {
                await fs.ReadBytes();
            }

            Console.WriteLine("Collect");
            GCSettings.LargeObjectHeapCompactionMode = GCLargeObjectHeapCompactionMode.CompactOnce;
            GCSettings.LatencyMode = GCLatencyMode.Batch;
            GC.Collect();
            Console.WriteLine("Start WaitForPendingFinalizers");
            GC.WaitForPendingFinalizers();
            Console.WriteLine("End WaitForPendingFinalizers");
            Console.ReadKey();

        }
    }
}
