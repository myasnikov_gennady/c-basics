﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace _16._GC
{
    public class FileService : IDisposable
    {
        private string _path;
        private Stream s;
        public byte[] Bytes { get; private set; }
        public FileService(string path)
        {
            Bytes = new byte[0];
            s = new FileStream(path, FileMode.Open, FileAccess.Read);
            _path = path;
        }
        public async Task ReadBytes()
        {
            Bytes = new byte[s.Length];
            await s.ReadAsync(Bytes);
        }

        public void Dispose()
        {
            s.Close();
            s.Dispose();
            Bytes = null;
            Console.WriteLine($"FS Disposed {_path}");
        }

        ~FileService()
        {
            Console.WriteLine($"Finalizing File Service {_path}");
            Dispose();
        }
    }
}
