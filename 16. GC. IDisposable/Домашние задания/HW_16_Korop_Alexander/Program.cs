﻿using System;
using System.Threading.Tasks;

namespace HW_16_Korop_Alexander
{
    class Program
    {
        static async Task Main(string[] args)
        {
            //Файлы находяться в папке с кодом
            string path1 = @"d:\Pelevin Lampa Mufusaila.fb2";
            string path2 = @"d:\poe.rar";
            try
            {
                Console.WriteLine("Read from full aaccess");
                using (var fs = new ReadFile(path1))
                {
                    await fs.Read();
                }
                using (var fs2 = new ReadFile(path1))
                {
                    await fs2.Read();
                }

                Console.WriteLine("Read from close aaccess");
                using var fs3 = new ReadFile(path2);
                await fs3.Read();
                using var fs4 = new ReadFile(path2);
                await fs4.Read();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error {ex.Message}");
            }
            finally
            {
                GC.Collect();
                Console.WriteLine("Finish");
            }

        }
    }
}
