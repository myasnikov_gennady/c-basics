﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace HW_16_Korop_Alexander
{
    class ReadFile : IDisposable
    {
        private readonly string _path;

        private FileStream _fs;

        public byte[] bytes = new byte[0];

        private string _inf;

        private bool disposed = false;
        public ReadFile(string path)
        {
            _path = path;
        }
        public async Task Read()
        {
            Console.WriteLine("Start reading");
            _fs = new FileStream(_path, FileMode.Open);
            bytes = new byte[_fs.Length];
            await _fs.ReadAsync(bytes, 0, bytes.Length);
            _inf = Encoding.Default.GetString(bytes);
            Console.WriteLine($"Data from file: {_inf.Substring(0, 50)}");
            Console.WriteLine("End reading");
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private void Dispose(bool disposing)
        {
            if (disposing)
            {                
                if (_fs != null)
                {
                    _fs.Dispose();
                    _fs = null;
                }
            }
        }
        ~ReadFile()
        {
            Dispose(true);
        }
    }
}
