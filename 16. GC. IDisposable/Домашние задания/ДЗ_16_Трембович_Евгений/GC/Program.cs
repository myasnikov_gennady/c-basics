﻿using GC.Implementations;
using System;

namespace GC
{
    class Program
    {
        private const string fileName = "Test.txt"; 

        static void Main(string[] args)
        {
            ScriptWithoutExeptions();
            ScriptWithExeption();
        }

        private static void ScriptWithoutExeptions()
        {
            var fileReader1 = new CustomFileReader();
            var fileReader2 = new CustomFileReader();

            using (fileReader1)
            {
                fileReader1.OpenFile(fileName);
            }

            using (fileReader2)
            {
                fileReader2.OpenFile(fileName);
            }
        }

        private static void ScriptWithExeption()
        {
            var fileReader1 = new CustomFileReader();
            var fileReader2 = new CustomFileReader();

            fileReader1.OpenFile(fileName);
            fileReader2.OpenFile(fileName);
        }
    }
}
