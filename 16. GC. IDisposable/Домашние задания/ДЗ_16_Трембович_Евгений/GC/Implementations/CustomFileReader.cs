﻿using System;
using System.IO;

namespace GC.Implementations
{
    public class CustomFileReader : IDisposable
    {
        private FileStream fileStream;

        public void OpenFile(string fileNeame)
        {
            fileStream = File.Open(fileNeame, FileMode.Open);
        }
        
        public void Dispose()
        {
            fileStream.Close();
        }
    }
}
