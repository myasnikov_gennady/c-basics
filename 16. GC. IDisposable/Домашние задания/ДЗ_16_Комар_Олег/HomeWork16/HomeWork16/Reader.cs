﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork16
{
    public class Reader : IDisposable
    {
        private readonly string _path;

        private Stream _fs;

        private byte[] _array = new byte[0];

        private string _data;

        private bool disposed = false;

        public Reader(string path)
        {
            _path = path;
        }

        public async Task ReadFileWithAccess()
        {
            _fs = new FileStream(_path, FileMode.Open, FileAccess.Read);
            _array = new byte[_fs.Length];
            await _fs.ReadAsync(_array, 0, _array.Length);
            _data = Encoding.Default.GetString(_array); 
            Console.WriteLine($"Данные из файла {_path}:\n {_data[..500]}");
            Console.WriteLine("Файл прочитан");
        }

        public async Task ReadFileWithoutAccess()
        {
            _fs = new FileStream(_path, FileMode.Open);
            _array = new byte[_fs.Length];
            await _fs.ReadAsync(_array, 0, _array.Length);
            _data = Encoding.Default.GetString(_array);
            Console.WriteLine($"Данные из файла 2 {_path}:\n {_data[..500]}");
            Console.WriteLine("Файл прочитан");
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!disposed && disposing)
            {
                _array = null;
                _data = null;
                _fs = null;
                disposed = true;
            }
        }

        ~Reader()
        {
            Dispose(true);
        }
    }
}
