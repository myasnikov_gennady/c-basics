﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;

namespace HomeWork16
{
    internal static class Program
    {
        [SuppressMessage("ReSharper.DPA", "DPA0003: Excessive memory allocations in LOH", MessageId = "type: System.Byte[]")]
        private static async Task Main()
        {
            const string file = @"d:\David Guetta - Lovers On The Sun (Official Video) ft Sam Martin.mp4";
            const string file2 = @"d:\JavaUninstallTool.rar";
            try
            {
                Console.WriteLine("Чтение с незаблокированным доступом:");
                Console.WriteLine();
                using (var reader = new Reader(file))
                {
                    await reader.ReadFileWithAccess();
                }

                using (var reader2 = new Reader(file))
                {
                    await reader2.ReadFileWithAccess();
                }

                Console.WriteLine();
                Console.WriteLine("Чтение с заблокированным доступом:");
                Console.WriteLine();

                using var reader3 = new Reader(file2);
                await reader3.ReadFileWithoutAccess();
                using var reader4 = new Reader(file2);
                await reader4.ReadFileWithoutAccess();

            }
            catch (/*System.IO.IO*/Exception e)
            {
                Alarm(e.Message);
            }
            finally
            {
                GC.Collect();
                Console.WriteLine("Программа отработалa. Нажмите любую клавишу");
                Console.ReadKey();
            }
        }

        private static void Alarm(string text)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(text);
            Console.ResetColor();
        }
    }
}
