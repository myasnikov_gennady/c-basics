﻿using System;
using System.Threading.Tasks;

namespace ДЗ_16_Филатов_ВС
{
    class Program
    {
        private static async Task Main()
        {
            try
            {
                Console.WriteLine("Чтение незаблокированного ресурса:");
                using (var fs = new FileService("TextFile.txt"))
                {
                    await fs.ReadFile();
                }
                
                using (var fs = new FileService("TextFile.txt"))
                {
                    await fs.ReadFile();
                }

                Console.WriteLine("Чтение заблокированного ресурса:");
                var fs2 = new FileService("TextFile.txt");
                await fs2.ReadFile();
                fs2 = new FileService("TextFile.txt");
                await fs2.ReadFile();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Beep();
                Console.WriteLine("Error: " + ex.Message);
                Console.ResetColor();
            }
            finally
            {
                Console.WriteLine("\nCollectig garbage by GC");
                GC.Collect();
                Console.ReadKey();
            }
        }
    }
}
