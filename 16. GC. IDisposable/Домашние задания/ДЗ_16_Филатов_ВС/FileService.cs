﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace ДЗ_16_Филатов_ВС
{
    public class FileService : IDisposable
    {
        private readonly string _file;
        private FileStream _fs;
        public byte[] Bytes { get; private set; }
        public FileService(string file)
        {
            Bytes = new byte[0];
            _file = file;
        }

        public async Task ReadFile()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Начало чтения файла {_file}");
            Console.ResetColor();
            _fs = new FileStream(_file, FileMode.Open);
            Bytes = new byte[_fs.Length];
            await _fs.ReadAsync(Bytes, 0, Bytes.Length);
            var _data = Encoding.Default.GetString(Bytes);
            Console.WriteLine($"Прочитаны данные из файла {_file}:\n {_data}");
        }

        public void Dispose()
        {
            _fs.Close();
            _fs.Dispose();
            GC.SuppressFinalize(this);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"FileService disposed {_file}\n");
            Console.ResetColor();
        }
    }
}
