﻿using System;
using System.IO;
using System.Security;
using System.Security.Permissions;
using System.Threading.Tasks;
namespace HW_16_ULADZIMIR_SKRYPKIN
{
    class Program   // Пункт 3.2. не реализован. Не смоделирована ситуация доступа к заблокированному ресурсу.
    {
        static async Task Main(string[] args)
        {
           const string file1 = "File1Free.txt";
            const string file2 = "File2Closed.txt";            
            try
            {
                
                Console.WriteLine("Использование метода -  доступа к файлу независимо от свойств");
                using (var fs1 = new FileReader(file1))
                {
                    await fs1.ReadBytesAnyAccess();
                }
                using (var fs2 = new FileReader(file1))
                {
                    await fs2.ReadBytesAnyAccess();
                }
                
                Console.WriteLine("Использование метода - проверки доступа к файлу ");
                using (var fs3 = new FileReader(file2))
                {
                   await fs3.ReadBytesTestAccess();
                }
                using (var fs4 = new FileReader(file2))
                {
                    await fs4.ReadBytesTestAccess();
                }
            }
            catch (ArgumentOutOfRangeException outOfRange)
            {
                Console.WriteLine("Error: {0}", outOfRange.Message);
            }
            finally
            {
                Console.WriteLine("Завершение программы");
            }


        }
    }
}
