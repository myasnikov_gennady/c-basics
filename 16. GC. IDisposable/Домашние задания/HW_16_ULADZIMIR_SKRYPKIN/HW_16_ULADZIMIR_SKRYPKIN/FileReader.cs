﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW_16_ULADZIMIR_SKRYPKIN
{
    class FileReader : IDisposable
    {
        private string _path;
        private Stream fs;
        public byte[] Bytes { get; private set; }
        public FileReader(string path)
        {
            Bytes = new byte[0];
            _path = path;
        }
        public async Task ReadBytesAnyAccess()  // Считывает файлы с любыми уровнем доступа
        {
           
            fs = new FileStream(_path, FileMode.Open, FileAccess.Read);            
            Bytes = new byte[fs.Length];
            await fs.ReadAsync(Bytes, 0, Bytes.Length);
            string txt = Encoding.Default.GetString(Bytes);
            Console.WriteLine($"Вывод на консоль данных файла:\n {_path}: {txt}");
        }
        public async Task ReadBytesTestAccess()  // Считывает файлы с проверкой доступа
        {
            fs = new FileStream(_path, FileMode.Open, (FileAccess)FileShare.None);
             //Bytes = new byte[fs.Length];
             //await fs.ReadAsync(Bytes, 0, Bytes.Length);
             //string txt = Encoding.Default.GetString(Bytes);
             //Console.WriteLine($"Вывод на консоль данных файла:\n {_path}: {txt}");
        }
        public void Dispose()
        {
            if (fs != null) { fs.Close(); fs.Dispose(); Bytes = null; } 
            Console.WriteLine($"File Disposed {_path}");
            GC.SuppressFinalize(this);
        }
    }
}
