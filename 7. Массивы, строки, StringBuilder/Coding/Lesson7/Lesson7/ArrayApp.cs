﻿using System;
using System.Linq;

namespace Lesson7
{
    class ArrayApp
    {
        public class A
        {
            public int Field { get; set; }

            public A(int field)
            {
                Field = field;
            }

            public object Clone()
            {
                return this.MemberwiseClone();
            }
        }
        public void Run()
        {
            var num = new A[]
            {
                    new A(1),
                    new A(2),
                    new A(3)
            };
            foreach (var m in num)
            {
                Console.Write(" " + m.Field);
            }

            Console.WriteLine();
            var numbers = new A[num.Length];
            // numbers = (A[]) num.Clone();
            numbers = num.Select(a => (A)a.Clone()).ToArray();
            // Array.Copy(num,numbers,num.Length);
            foreach (var i in numbers)
            {
                Console.Write(" " + i.Field);
            }

            Console.WriteLine();
            num[1].Field = 5;
            foreach (var m in num)
            {
                Console.Write(" " + m.Field);
            }

            Console.WriteLine();
            foreach (var i in numbers)
            {
                Console.Write(" " + i.Field);
            }

            Console.WriteLine("\n");
            var array2d = new int[4, 2]
            {
                {1, 2},
                {3, 4},
                {5, 6},
                {7, 8}
            };
            Console.WriteLine(array2d.Length);
            Console.WriteLine(array2d.GetLength(0));
            Console.WriteLine(array2d.GetLength(1));
            Console.WriteLine(array2d.GetUpperBound(0));
            Console.WriteLine(array2d.GetUpperBound(1));

            var array5d = new int[3][];
            array5d[0] = new[] {1, 2, 3};
            array5d[1] = new[] {1, 2, 3, 4, 5, 6};
            array5d[2] = new[] {1, 2, 3, 4, 5};
            foreach (var row in array5d)
            {
                Console.WriteLine();
                foreach (var col in row)
                {
                    Console.Write(col + " ");
                }
            }

            Console.WriteLine("\n");
            for (int i = 0; i < array5d.Length; i++)
            {
                for (int j = 0; j < array5d[i].Length; j++)
                {
                    Console.Write(array5d[i][j] + " ");
                }

                Console.WriteLine();
            }
        }
    }
}