﻿using System;
using System.Globalization;
using System.Text;
using System.Threading;

namespace Lesson7
{
    public static class StringApp
    {
        public static void Run()
        {
            string message1 = null;
            string message2 = string.Empty;
            string message3 = "Hello";

            var message4 = message3;
            message3 = "                          World \u0130";
            Console.WriteLine(message3);
            Console.WriteLine(message4);

            message4 = message1 + message2 + message3;
            Console.WriteLine(message4);

            var strings = new [] { "a", "b", "c", "d"};
            message4 = string.Join("\r\n", strings);
            Console.WriteLine(message4);
            Console.WriteLine();

            message4 = String.Format("Hello {0}",message3.Trim());
            Console.WriteLine(message4);

            Console.WriteLine(message4.ToUpper());
            Console.WriteLine(message4.ToUpperInvariant());
            Console.WriteLine();

            message4 = string.IsNullOrEmpty(null) ? "true" : "false";
            Console.WriteLine(message4);

            Console.WriteLine();
            message4 = string.IsNullOrEmpty("") ? "true" : "false";
            Console.WriteLine(message4);

            Console.WriteLine();
            message4 = string.IsNullOrEmpty(string.Empty) ? "true" : "false";
            Console.WriteLine(message4);

            Console.WriteLine();
            message4 = string.IsNullOrEmpty(" ") ? "true" : "false";
            Console.WriteLine(message4);

            Console.WriteLine();
            message4 = string.IsNullOrEmpty("\r\n") ? "true" : "false";
            Console.WriteLine(message4);
            Console.WriteLine("----------------------------------------");

            message4 = string.IsNullOrWhiteSpace(null) ? "true" : "false";
            Console.WriteLine(message4);

            Console.WriteLine();
            message4 = string.IsNullOrWhiteSpace("") ? "true" : "false";
            Console.WriteLine(message4);

            Console.WriteLine();
            message4 = string.IsNullOrWhiteSpace(string.Empty) ? "true" : "false";
            Console.WriteLine(message4);

            Console.WriteLine();
            message4 = string.IsNullOrWhiteSpace(" ") ? "true" : "false";
            Console.WriteLine(message4);

            Console.WriteLine();
            message4 = string.IsNullOrWhiteSpace("\r\n") ? "true" : "false";
            Console.WriteLine(message4);

            Console.WriteLine();
            message4 = string.IsNullOrWhiteSpace("\t") ? "true" : "false";
            Console.WriteLine(message4);
        }

        public static void TextCulture()
        {
            Console.OutputEncoding = Encoding.UTF8;
            //Thread.CurrentThread.CurrentCulture = new CultureInfo("tr-TR");
            string invariant = "iii".ToUpperInvariant();
            CultureInfo turkey = new CultureInfo("tr-TR");
            Thread.CurrentThread.CurrentCulture = turkey;
            string cultured = "iii".ToUpper();
            Console.WriteLine(cultured);
            Console.WriteLine(invariant);
        }

        public static void TextCulture2()
        {
            CultureInfo turkey = new CultureInfo("tr-TR");
            //Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            //Thread.CurrentThread.CurrentCulture = turkey;
            string a = "III";
            string b = "iii";
            Console.WriteLine(a);
            Console.WriteLine(b);
            Console.WriteLine(a==b);
            Console.WriteLine(String.Equals(a, b));
            Console.WriteLine(String.Equals(a, b, StringComparison.InvariantCulture));
            Console.WriteLine(String.Equals(a, b, StringComparison.InvariantCultureIgnoreCase));
            Console.WriteLine(String.Equals(a, b, StringComparison.CurrentCulture));
            Console.WriteLine(String.Equals(a, b, StringComparison.CurrentCultureIgnoreCase));
            Console.WriteLine(String.Equals(a, b, StringComparison.Ordinal));
            Console.WriteLine(String.Equals(a, b, StringComparison.OrdinalIgnoreCase));
            
        }
    }

}
