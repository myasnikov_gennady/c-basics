﻿using System;
using System.Text;

namespace Lesson7
{
    class StringBuilderApp
    {
        public static void Run()
        {
            Console.WriteLine(DateTime.Now);
            var _string = "Hello";
            for (var i = 0; i < 100_000; i++)
            {
                _string += "Hello";

            }
            Console.WriteLine(DateTime.Now);
            StringBuilder sb = new StringBuilder(capacity:100_000);
            for (var i = 0; i < 100_000; i++)
            {
                sb.Append("Hello");
            }

            var string2 = sb.ToString();
            Console.WriteLine(string2);
            Console.WriteLine(DateTime.Now);
        }   
    }
}
