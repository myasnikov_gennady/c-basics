﻿using System;
using System.Globalization;
using System.Text;
using System.Threading;

namespace ДЗ_7_Филатов_ВС
{
    class Program
    {
        static void Main(string[] args)
        {
            // 1.Создать двумерный массив. Пройти по строкам и столбцам.Вывести на консоль.

            Console.WriteLine("Двумерный массив");

            var array1 = new int[3, 2]
            {
                {3, 9},
                {1, 58},
                {92, 67},
            };
            int rows = array1.GetUpperBound(0) + 1;
            int columns = array1.GetUpperBound(1) + 1;

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    Console.Write($"{array1[i, j]} \t");
                }
                Console.WriteLine();
            }
            Console.WriteLine();

            // 2.Создать зубчатый массив. Пройти по строкам и столбцам.Вывести на консоль.

            Console.WriteLine("Зубчатый массив");

            int[][] array2 = new int[3][];
            array2[0] = new int[2] { 3, 98 };
            array2[1] = new int[4] { 22, 70, 20, 44 };
            array2[2] = new int[3] { 89, 2, 3 };

            foreach (int[] row in array2)
            {
                foreach (int number in row)
                {
                    Console.Write($"{number} \t");
                }
                Console.WriteLine();
            }
            Console.WriteLine();

            // 3.Вывести на колноль выполнение следующих методов работы со сроками

            Console.WriteLine("Работа со строками");

            string message1 = "Hello";
            string message2 = message1;
            string message3 = "World";

            Console.WriteLine($"Message 1: {message1}");
            Console.WriteLine($"Message 2: {message2}");
            Console.WriteLine($"Message 3: {message3}");
            Console.WriteLine();

            Console.WriteLine("Equals определяет имеют ли указанные объекты одинаковое значение");
            Console.WriteLine($"Equals {message1}, {message2}: {String.Equals(message1, message2)}");
            Console.WriteLine($"Equals {message2}, {message3}: {String.Equals(message2, message3)}");
            Console.WriteLine();

            Console.WriteLine("Compare сравнивает указанные string объекты и возвращает int, соответсвующий " +
                "их относительному положению в порядке сортировки");
            Console.WriteLine($"Compare {message1}, {message2}: {String.Compare(message1, message2)}");
            Console.WriteLine($"Compare {message1}, {message3}: {String.Compare(message1, message3)}");
            Console.WriteLine($"Compare {message3}, {message1}: {String.Compare(message3, message1)}");
            Console.WriteLine();

            Console.WriteLine("CompareOrdinal сравнивает указанные string объекты, вычисляя их числовые значения " +
                "из соответствующих объектов char в каждом string. Возвращает int, соответсвующий лексической связи" +
                "между сравниваемыми объектами");
            Console.WriteLine($"CompareOrdinal {message1}, {message2}: {String.CompareOrdinal(message1, message2)}");
            Console.WriteLine($"CompareOrdinal {message1}, {message3}: {String.CompareOrdinal(message1, message3)}");
            Console.WriteLine($"CompareOrdinal {message3}, {message1}: {String.CompareOrdinal(message3, message1)}");
            Console.WriteLine();

            Console.WriteLine("CompareTo предназначен для сравнения текущего объекта с объектом, который передается " +
                "в качестве параметра. Возвращает int, соответствующий их относительному положению в порядке сортировки");
            Console.WriteLine($"CompareTo {message1}, {message2}: {message1.CompareTo(message2)}");
            Console.WriteLine($"CompareTo {message1}, {message3}: {message1.CompareTo(message3)}");
            Console.WriteLine($"CompareTo {message3}, {message1}: {message3.CompareTo(message1)}");
            Console.WriteLine();

            Console.WriteLine("Для обрезки начальных или концевых символов используется функция Trim");
            Console.WriteLine($"Trim(H) HOOKAH: {String.Format("HOOKAH".Trim(new char[] { 'H' }))}");
            Console.WriteLine();

            Console.WriteLine("Функция TrimStart обрезает начальные символы, а функция TrimEnd обрезает конечные символы");
            Console.WriteLine($"TrimStart(H) HOOKAH: {String.Format("HOOKAH".TrimStart(new char[] { 'H' }))}");
            Console.WriteLine($"TrimEnd(H) HOOKAH: {String.Format("HOOKAH".TrimEnd(new char[] { 'H' }))}");
            Console.WriteLine();

            Console.WriteLine("Функция StartsWith возвращает логическое значение true, если вызывающая строка начинается" +
                "с подстроки value. В противном случае возвращается логическое значение false");
            Console.WriteLine($"StartsWith(H) {message1}: {message1.StartsWith('H')}");
            Console.WriteLine($"StartsWith(H) {message3}: {message3.StartsWith('H')}");
            Console.WriteLine();

            Console.WriteLine("Функция EndsWith возвращает логическое значение true, если вызывающая строка оканчивается" +
                "на подстроку value. В противном случае возвращается логическое значение false");
            Console.WriteLine($"EndsWith(o) {message1}: {message1.EndsWith('o')}");
            Console.WriteLine($"EndsWith(o) {message3}: {message3.EndsWith('o')}");
            Console.WriteLine();

            Console.WriteLine("Функция Contains возвращает логическое значение true, если вызывающая строка содержит" +
                "подстроку value. В противном случае возвращается логическое значение false");
            Console.WriteLine($"Contains(el) {message1}: {message1.Contains("el")}");
            Console.WriteLine($"Contains(w) {message1}: {message1.Contains('w')}");
            Console.WriteLine();

            Console.WriteLine("Функции ToLower и ToUpper переводят передаваемую строку в нижний и верхний регистр соответственно");
            Console.WriteLine($"ToLower {message1}: {message1.ToLower()}");
            Console.WriteLine($"ToUpper {message1}: {message1.ToUpper()}");
            Console.WriteLine();

            Console.WriteLine("Функции ToLowerInvariant и ToUpperInvariant переводят передаваемую строку в нижний и верхний регистр " +
                "соответственно в инвариантной культуре");
            string message4 = "iii";
            Console.OutputEncoding = Encoding.UTF8;
            CultureInfo turkey = new CultureInfo("tr-TR");
            Thread.CurrentThread.CurrentCulture = turkey;
            Console.WriteLine($"ToUpper {message4}: {message4.ToUpper()}");
            Console.WriteLine($"ToUpperInvariant {message4}: {message4.ToUpperInvariant()}");
            // Я не нашел прописную букву, которая будет отличаться в инвариантной и другой культурах((
            // Понимаю что ToLowerInvariant работает аналогично.
            Console.WriteLine();

            Console.WriteLine("Функции IndexOf и LastIndexOf возвращают номер первой или последней соответственно позиции" +
                "указанного объекта char или string в передаваемой строке");
            Console.WriteLine($"IndexOf(l) {message1}: {message1.IndexOf('l')}");
            Console.WriteLine($"LastIndexOf(l) {message1}: {message1.LastIndexOf('l')}");
            Console.WriteLine();

            Console.WriteLine("Функции PadLeft и PadRight возвращают новую строку общей указанной int длиной, в которой" +
                "отсутствующие символы заполняются слева или страва указанным char");
            Console.WriteLine($"PadLeft(10,!) {message1}: {message1.PadLeft(10,'!')}");
            Console.WriteLine($"PadRight(10,!) {message1}: {message1.PadRight(10, '!')}");
            Console.WriteLine();

            Console.WriteLine("Функция Replace заменяет все указанные символы char в передаваемой строке на другие указанные");
            Console.WriteLine($"Replace(l,r) {message1}: {message1.Replace('l','r')}");
            Console.WriteLine();

            Console.WriteLine("Функция Remove удаляет символы начиная с указанной позиции (все или указанное количество)");
            Console.WriteLine($"Remove(2) {message1}: {message1.Remove(2)}");
            Console.WriteLine($"Remove(2,1) {message1}: {message1.Remove(2,1)}");
            Console.WriteLine();

            Console.WriteLine("Функция Insert вствляет указанную строку в указанном индексе передаваемой строки");
            Console.WriteLine($"Insert(2,INSERT_HERE) {message1}: {message1.Insert(2, "INSERT_HERE")}");
            Console.WriteLine();

            Console.WriteLine("Функция Split в качестве параметра принимает массив символов или строк, которые и будут служить " +
                "разделителями. Позволяет разделить строку на массив подстрок");
            string message5 = "Я делаю домашнее задание по C#";
            string[] message5Splited = message5.Split(new char[] { ' ' });
            Console.WriteLine($"Split( ) {message5}:");
            foreach (string word in message5Splited)
            {
                Console.WriteLine(word);
            }
            Console.WriteLine();

            // 4. С помощью класса StringBuilder выполнить операции:

            Console.WriteLine("Работа со StringBuilder");
            StringBuilder sb = new StringBuilder(capacity: 1_000_000);
            Console.WriteLine();
            message1 = message1 + " ";

            Console.WriteLine("Метод Append добавляет к строке подстроку");
            for (var i = 0; i < 5; i++)
            {
                sb.Append(message1);
            }
            var sbMessage = sb.ToString();
            Console.WriteLine($"Append {message1} x5: {sbMessage}");
            Console.WriteLine();

            Console.WriteLine("Метод AppendFormat добавляет подстроку в конец объекта StringBuilder");
            for (var i = 0; i < 5; i++)
            {
                sb.AppendFormat(message1);
            }
            sbMessage = sb.ToString();
            Console.WriteLine($"AppendFormat {message1} x5: {sbMessage}");
            Console.WriteLine();

            Console.WriteLine("Метод Insert вставляет подстроку в объект StringBuilder, начиная с определенного индекса");
            sb.Insert(0, "Tom ");
            sbMessage = sb.ToString();
            Console.WriteLine($"Insert StringBuilderMessage(0, Tom ): {sbMessage}");
            Console.WriteLine();

            Console.WriteLine("Метод Remove удаляет определенное количество символов, начиная с определенного индекса");
            sb.Remove(9, 54);
            sbMessage = sb.ToString();
            Console.WriteLine($"Remove StringBuilderMessage(9, 54): {sbMessage}");
            Console.WriteLine();

            Console.WriteLine("Метод Replace заменяет все вхождения определенного символа или подстроки на другой символ или подстроку");
            sb.Replace("Tom","Peter");
            sbMessage = sb.ToString();
            Console.WriteLine($"Replace StringBuilderMessage(Tom, Peter): {sbMessage}");
            Console.WriteLine();

            // 5. Произвести замер времени выполнения в цикле от 0 до 1000000 присоединения строки и
            // с использованием класса StringBuilder

            Console.WriteLine("Сравнение присоединения строки и StringBuilder");
            Console.WriteLine($"Начало работы конкатенации: {DateTime.Now}");
            message1 = "Hello";
            for (var i = 0; i < 1_000_000; i++)
            {
                message1 += "Hello";

            }
            Console.WriteLine($"Конец работы конкатенации: {DateTime.Now}");
            Console.WriteLine($"Начало работы StringBuilder: {DateTime.Now}");
            for (var i = 0; i < 1_000_000; i++)
            {
                sb.Append("Hello");
            }
            sbMessage = sb.ToString();
            Console.WriteLine($"Конец работы StringBuilder: {DateTime.Now}");
        }
    }
}
