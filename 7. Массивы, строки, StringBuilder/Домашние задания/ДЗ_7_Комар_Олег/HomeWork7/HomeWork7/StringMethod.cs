﻿using System;
using System.Collections;
using System.Globalization;
using System.Text;
using System.Threading;

namespace HomeWork7
{
    public static class StringMethod
    {
        public static void WorkingWithStrings()
        {
            StringEquals();
            StringCompare();
            StringTrim();
            StringReplaceRemoveInsert();
            StringStartEndContains();
            StringPadLeftRight();
            StringSplit();
        }

        private static void StringEquals()
        {
            ConsoleHelper.Info("3. Строки. Equals.");

            var originalEncoding = Console.OutputEncoding;
            var originalCulture = CultureInfo.CurrentCulture.Name;
            Console.OutputEncoding = Encoding.UTF8;
            var strings1 = "cœur";
            var strings2 = "coeur";
            StringComparison[] comparisons = (StringComparison[])Enum.GetValues(typeof(StringComparison));
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("fr-FR");
            Console.WriteLine($"Current Culture: {CultureInfo.CurrentCulture.Name}");

            foreach (var comparison in comparisons)
            {
                Console.WriteLine($" {strings1} = {strings2} ({comparison}): {string.Equals(strings1, strings2, comparison)}");
            }
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(originalCulture);
            Console.OutputEncoding = originalEncoding;
            ConsoleHelper.Clear();
        }

        private static void StringCompare()
        {
            ConsoleHelper.Info("3. Строки. Compare. CompareOrdinal. CompareTo.");

            var string1 = "ABC";
            var string2 = "abc";
            var resultat = "равна";

            Console.WriteLine($"Сравниваем две строки '{string1}' и '{string2}':");
            ArrayList arrayMethodsCompare = new ArrayList()
            {
                String.Compare(string1, string2),
                "Без параметров в методе Compare() - строка ",
                String.Compare(string1.ToUpper(), string2.ToUpper()),
                "Обе строки в верхнем регистре ToUpper() - строка ",
                String.Compare(string1.ToUpperInvariant(), string2.ToUpperInvariant()),
                "Обе строки в верхнем регистре ToUpperInVariant() - строка ",
                String.Compare(string1.ToLower(), string2.ToLower()),
                "Обе строки в нижнем регистре ToLower() - строка ",
                String.Compare(string1.ToLowerInvariant(), string2.ToLowerInvariant()),
                "Обе строки в нижнем регистре ToLowerInVariant() - строка ",
                String.CompareOrdinal(string1, string2),
                "Сравнение используя CompareOrdinal() - строка ",
                string1.CompareTo(string2),
                "Сравнение используя CompareTo() - строка "
            };
            for (var i = 0; i < arrayMethodsCompare.Count; i += 2)
            {
                if ((int)arrayMethodsCompare[i] < 0) resultat = "меньше чем";
                if ((int)arrayMethodsCompare[i] > 0) resultat = "больше чем";
                Console.WriteLine($"{arrayMethodsCompare[i + 1]} '{string1}' {resultat} '{string2}'");
            }
            ConsoleHelper.Clear();
        }

        private static void StringTrim()
        {
            ConsoleHelper.Info("3. Строки. Trim. TrimStart. TrimEnd.");

            var text = "/*The original string*/";
            Console.WriteLine($"Оригинальная строка - '{text}'");
            char[] charsToTrim = {'*', '/'};
            string textTrimmed = text.Trim(charsToTrim);
            Console.WriteLine($"Строка после операции Trim('*', '/') - '{textTrimmed}'");
            textTrimmed = text.TrimStart(charsToTrim);
            Console.WriteLine($"Строка после операции TrimStart('*', '/') - '{textTrimmed}'");
            textTrimmed = text.TrimEnd(charsToTrim);
            Console.WriteLine($"Строка после операции TrimEnd('*', '/') - '{textTrimmed}'");
            ConsoleHelper.Clear();
        }

        private static void StringReplaceRemoveInsert()
        {
            ConsoleHelper.Info("3. Строки. Replace. Remove. Insert.");
            
            var text = "Всьм пйивьт. Ну лгл вгм дюмгшньь згдгниь пю стйюлгм?";
            Console.WriteLine($"Оригинальная строка '{text}'");
            text = text.Replace("й", "р").Replace("ю", "о").Replace("л", "к").Replace("г", "а").Replace("ь", "е");
            Console.WriteLine($"Строка, обработанная методом Replace() пятикратно - '{text}'");
            text = text.Remove(40,11);
            Console.WriteLine($"Строка, обработанная методом Remove() - '{text}'");
            text = text.Insert(23, ", интересное");
            Console.WriteLine($"Строка, обработанная методом Insert() - '{text}'");
            ConsoleHelper.Clear();
        }

        private static void StringStartEndContains()
        {
            ConsoleHelper.Info("3. Строки. StartsWith. EndsWith. Contains. IndexOf. LastIndexOf.");

            var text = "https://www.it-academy.by/";
            Console.WriteLine($"Оригинальная строка '{text}'");

            if (text.Contains("www.", StringComparison.OrdinalIgnoreCase))
            {
                var index = text.IndexOf("www.", StringComparison.OrdinalIgnoreCase);
                text = text.Remove(index, 4);
            }


            if (text.Trim().EndsWith("/", StringComparison.OrdinalIgnoreCase)) 
            { 
                var endIndex = text.LastIndexOf("/", StringComparison.Ordinal);
                text = text.Remove(endIndex, 1);
            }
            
            if (text.Trim().StartsWith("Https://", StringComparison.OrdinalIgnoreCase))
            {
                var startIndex = text.IndexOf("https://", StringComparison.OrdinalIgnoreCase);
                if (startIndex >= 0)
                {
                    text = text.Substring(startIndex + 8);
                }
            }
            Console.WriteLine($"'{text}'");
            ConsoleHelper.Clear();
        }

        private static void StringPadLeftRight()
        {
            ConsoleHelper.Info("3. Строки. PadLeft. PadRight.");
            var text = "Hello World!";
            Console.WriteLine($"Оригинальная строка - '{text}'");
            Console.WriteLine($"Модифицированная строка с помощью PadLeft(), по умолчанию заполняется пробелами - '{text.PadLeft(20)}'");
            Console.WriteLine($"Модифицированная строка с помощью PadLeft(), со звездочками - '{text.PadLeft(20, '*')}'");
            Console.WriteLine($"Модифицированная строка с помощью PadRight()- '{text.PadRight(20)}'");
            Console.WriteLine($"Модифицированная строка с помощью PadRight() с точками- '{text.PadRight(20,'.')}'");
            ConsoleHelper.Clear();
        }

        public static void StringSplit()
        {
            ConsoleHelper.Info("3. Строки. Split.");

            var text = "C# разрабатывался как язык программирования прикладного уровня для CLR и, как таковой, зависит, прежде всего, от возможностей самой CLR";
            Console.WriteLine($"Оригинальная строка - '{text}'");
            char[] separators =  { ' ', ',' };
            var subStrings = text.Split(separators,StringSplitOptions.RemoveEmptyEntries);
            Console.WriteLine("Массив строк:");
            foreach (var subString in subStrings)
            {
                Console.WriteLine($"'{subString}'");
            }
            ConsoleHelper.Clear();
        }
        public static void Timing()
        {
            ConsoleHelper.Info("5.1. Строки. Equals. Расчет времени работы цикла.");

            var start = new DateTime();
            var finish = new DateTime();
            const int cycles = 1_000_000;
            var text = "Hello";

            for (var i = 0; i <= cycles; i++)
            {
                if (i == 0)
                {
                    start = DateTime.Now;
                    Console.WriteLine($"Начало работы цикла - {start}");
                }
                text += "Hi";
                if (i == cycles)
                {
                    finish = DateTime.Now;
                    Console.WriteLine($"Конец работы цикла - {finish}");
                }
            }

            var elapsedTicks = finish.Ticks - start.Ticks;
            var elapsedSpan = new TimeSpan(elapsedTicks);
            Console.WriteLine($@"Длительность работы цикла - {elapsedSpan.TotalSeconds} секунд всего, или");
            Console.WriteLine($"{elapsedSpan.Days} дней, {elapsedSpan.Hours} часов, {elapsedSpan.Minutes} минут, {elapsedSpan.Seconds} секунд, {elapsedSpan.Milliseconds} миллисекунд");
            Console.WriteLine();
            /*     На PC с процессором Intel Core i7-8700 и 16 Gb DDR4-2666 цикл длился 5 минут, 50 секунд, 304 миллисекунд             */
        }
    }
}
