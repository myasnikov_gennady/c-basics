﻿using System;
using System.Globalization;
using System.Text;

namespace HomeWork7
{
    public class StringBuilderMethod
    {
        public static void WorkingWithStringBuilder()
        {
            Operation();
            StringBuilderAppendFormat();
        }

        private static void Operation()
        {
            ConsoleHelper.Info("4. Класс StringBuilder. Append. Insert. Replace. Remove.");

            var sb = new StringBuilder("Курс");
            var sbText = sb.ToString();
            Console.WriteLine($"Оригинальная строка - '{sbText}'");
            sb.Append(" на CSharp");
            Console.Write(" Метод Append():");
            sbText = sb.ToString();
            Console.WriteLine($"  '{sbText}'");
            sb.Insert(4, " программирования");
            Console.Write(" Метод Insert():");
            sbText = sb.ToString();
            Console.WriteLine($"  '{sbText}'");
            sb.Replace("CSharp", "C# (CSharp)");
            Console.Write(" Метод Replace():");
            sbText = sb.ToString();
            Console.WriteLine($" '{sbText}'");
            sb.Remove(27, 9);
            Console.Write(" Метод Remove():");
            sbText = sb.ToString();
            Console.WriteLine($"  '{sbText}'");
            ConsoleHelper.Clear();
        }

        private static void StringBuilderAppendFormat()
        {
            ConsoleHelper.Info("4. Класс StringBuilder. AppendFormat().");

            var sb = new StringBuilder();
            string[] cultureNames = {"ru-RU", "en-US", "en-GB"};
            var dateToday = DateTime.Now;
            foreach (var cultureName in cultureNames)
            {
                var culture = CultureInfo.CreateSpecificCulture(cultureName);
                Console.WriteLine($"Culture: {culture}");
                sb.AppendFormat(culture, "Date and Time: {0:d} at {0:t}", dateToday);
                Console.WriteLine(sb.ToString());
                sb.Clear();
            }

            ConsoleHelper.Clear();
        }

        public static void Timing()
        {
            ConsoleHelper.Info("5.2. Класс StringBuilder. Расчет времени работы цикла.");

            var start = new DateTime();
            var finish = new DateTime();
            const int cycles = 1_000_000;
            var sb = new StringBuilder();
            var text = "Hello";

            for (var i = 0; i <= cycles; i++)
            {
                if (i == 0)
                {
                    start = DateTime.Now;
                    Console.WriteLine($"Начало работы цикла - {start}");
                }
                sb.Append("Hello");
                if (i == cycles)
                {
                    finish = DateTime.Now;
                    Console.WriteLine($"Конец работы цикла - {finish}");
                }
            }
            var elapsedTicks = finish.Ticks - start.Ticks;
            var elapsedSpan = new TimeSpan(elapsedTicks);
            Console.WriteLine($@"Длительность работы цикла - {elapsedSpan.TotalSeconds} секунд всего, или");
            Console.WriteLine(
                $"{elapsedSpan.Days} дней, {elapsedSpan.Hours} часов, {elapsedSpan.Minutes} минут, {elapsedSpan.Seconds} секунд, {elapsedSpan.Milliseconds} миллисекунд");
            Console.WriteLine();
            /*     На PC с процессором Intel Core i7-8700 и 16 Gb DDR4-2666 цикл длился 0 минут, 0 секунд, 20 миллисекунд             */
        }
    }
}
