﻿namespace HomeWork7
{
    // нет замера времени работы в цикле класса StringBuilder
    // не дочитал что и там и там надо было
    class Program
    {
        private static void Main()
        {
            Array.WorkingWithArray();
            StringMethod.WorkingWithStrings();
            StringBuilderMethod.WorkingWithStringBuilder();
            StringMethod.Timing();
            StringBuilderMethod.Timing();
        }
    }
}
