﻿using System;

namespace HomeWork7
{
    public class ConsoleHelper
    {
        public static void Clear()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine();
            Console.WriteLine("Нажмите любую клавишу для просмотра следующего задания");
            Console.ResetColor();
            Console.ReadKey();
            Console.Clear();
        }
        public static void Info(string text)
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine(text);
            Console.ResetColor();
        }
    }
}
