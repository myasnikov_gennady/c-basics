﻿using System;

namespace HomeWork7
{
    public class Array
    {
        public static void WorkingWithArray()
        {
            Multidimensional_Array();
            Jagged_Array();
        }
        private static void Multidimensional_Array()
        {
            ConsoleHelper.Info("1. Массивы. Двумерный массив.");

            var matrix = new int[5, 5];
            var row = matrix.GetLength(0);
            var column = matrix.GetLength(1);

            for (var i = 0; i < row; i++)
            {
                for (var j = 0; j < column; j++)
                {
                    matrix[i, j] = i + j;
                    Console.Write($"{matrix[i, j]} ");
                }

                Console.WriteLine();
            }
            ConsoleHelper.Clear();
        }

        private static void Jagged_Array() 
        {
            ConsoleHelper.Info("2. Массивы. Ступенчатый массив.");

            var jaggedArray = new int[5][];
            jaggedArray[0] = new int[5];
            jaggedArray[1] = new int[3];
            jaggedArray[2] = new int[1];
            jaggedArray[3] = new int[2];
            jaggedArray[4] = new int[4];
            
            for (var i = 0; i < jaggedArray.Length; i++)
            {
                for (var j = 0; j < jaggedArray[i].Length; j++)
                {
                    jaggedArray[i][j] = jaggedArray[i].Length - j;
                    Console.Write(jaggedArray[i][j] + " ");
                }
                Console.WriteLine();
            }
            ConsoleHelper.Clear();
        }
    }
}
