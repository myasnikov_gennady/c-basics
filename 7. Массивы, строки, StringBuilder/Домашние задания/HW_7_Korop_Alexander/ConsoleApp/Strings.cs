﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp
{
    public static class Strings
    {
        public static void StartStrings()
        {
            string str1 = "Ola";
            string str2 = "What";
            string str3 = "up";
            string str4 = "up";
            string str5 = "****up**";
            string str6 = "    up  ";
            string str7 = "WhaT";
            char c = 'a';



            StringBuilder strb1 = new StringBuilder("Ola Amigos");
            StringBuilder strb2 = new StringBuilder("What up ");
            StringBuilder strb3 = new StringBuilder("H e l l o");
            


            Console.WriteLine();

            Console.WriteLine("Equals");
            Console.WriteLine($"Strings {str1}, {str2}: {String.Equals(str1, str2)}");
            Console.WriteLine($"Strings {str1}, {str3}: {String.Equals(str1, str3)}");
            Console.WriteLine($"Strings {str2}, {str3}: {String.Equals(str2, str3)}");
            Console.WriteLine($"Strings {str3}, {str4}: {String.Equals(str3, str4)}");
            Console.WriteLine();

            Console.WriteLine("Compare");
            Console.WriteLine($"Strings {str1}, {str3}: {String.Compare(str1, str3)}");
            Console.WriteLine($"Strings {str1}, {str2}: {String.Compare(str1, str2)}");
            Console.WriteLine($"Strings {str2}, {str3}: {String.Compare(str2, str3)}");
            Console.WriteLine($"Strings {str3}, {str4}: {String.Compare(str3, str4)}");
            Console.WriteLine();

            Console.WriteLine("CompareOrdinal");
            Console.WriteLine($"Strings {str1}, {str2}: {String.CompareOrdinal(str1, str2)}");
            Console.WriteLine($"Strings {str1}, {str3}: {String.CompareOrdinal(str1, str3)}");
            Console.WriteLine($"Strings {str2}, {str3}: {String.CompareOrdinal(str2, str3)}");
            Console.WriteLine($"Strings {str3}, {str4}: {String.CompareOrdinal(str3, str4)}");
            Console.WriteLine();

            Console.WriteLine("CompareTo");
            Console.WriteLine($"Strings {str1}, {str2}: {str1.CompareTo(str2)}");
            Console.WriteLine($"Strings {str1}, {str3}: {str1.CompareTo(str3)}");
            Console.WriteLine($"Strings {str2}, {str3}: {str2.CompareTo(str3)}");
            Console.WriteLine($"Strings {str3}, {str4}: {str3.CompareTo(str4)}");
            Console.WriteLine();

            Console.WriteLine("Trim");
            Console.WriteLine($"Strings '{str1}' Trim 'O', 'a' : {str1.Trim(new char[] { 'O','a' })} ");
            Console.WriteLine($"Strings '{str2}' Trim 'W', 't': {str2.Trim(new char[] { 'W', 't' })} ");
            Console.WriteLine($"Strings '{str3}' Trim 'u': {str3.Trim(new char[] { 'u' })} ");
            Console.WriteLine($"Strings '{str4}' Trim 'p': {str4.Trim(new char[] { 'p' })} ");
            Console.WriteLine();

            Console.WriteLine("TrimStart");
            Console.WriteLine($"Strings '{str1}' TrimStart 'O', 'a' : {str1.TrimStart(new char[] { 'O', 'a' })} ");
            Console.WriteLine($"Strings '{str2}' TrimStart 'W', 't': {str2.TrimStart(new char[] { 'W', 't' })} ");
            Console.WriteLine($"Strings '{str3}' TrimStart 'u': {str3.TrimStart(new char[] { 'u' })} ");
            Console.WriteLine($"Strings '{str4}' TrimStart 'p': {str4.TrimStart(new char[] { 'p' })} ");
            Console.WriteLine($"Strings '{str5}' TrimStart '*': {str5.TrimStart(new char[] { '*' })} ");
            Console.WriteLine($"Strings '{str6}' TrimStart ' ': {str6.TrimStart(new char[] { ' ' })} ");
            Console.WriteLine();

            Console.WriteLine("TrimEnd");
            Console.WriteLine($"Strings '{str1}' TrimEnd 'O', 'a' : {str1.TrimEnd(new char[] { 'O', 'a' })} ");
            Console.WriteLine($"Strings '{str2}' TrimEnd 'W', 't': {str2.TrimEnd(new char[] { 'W', 't' })} ");
            Console.WriteLine($"Strings '{str3}' TrimEnd 'u': {str3.TrimEnd(new char[] { 'u' })} ");
            Console.WriteLine($"Strings '{str4}' TrimEnd 'p': {str4.TrimEnd(new char[] { 'p' })} ");
            Console.WriteLine($"Strings '{str5}' TrimEnd '*': {str5.TrimEnd(new char[] { '*' })} ");
            Console.WriteLine($"Strings '{str6}' TrimEnd ' ': {str6.TrimEnd(new char[] { ' ' })} ");
            Console.WriteLine();

            Console.WriteLine("StartsWith");
            Console.WriteLine($"Strings '{str1}' StartsWith 'O': {str1.StartsWith('O')} ");
            Console.WriteLine($"Strings '{str2}' StartsWith 'w': {str2.StartsWith('w')} ");
            Console.WriteLine($"Strings '{str3}' StartsWith 'u': {str3.StartsWith('u')} ");
            Console.WriteLine($"Strings '{str4}' StartsWith 'p': {str4.StartsWith('p')} ");
            Console.WriteLine($"Strings '{str5}' StartsWith '*': {str5.StartsWith('*')} ");
            Console.WriteLine($"Strings '{str6}' StartsWith ' ': {str6.StartsWith(' ')} ");
            Console.WriteLine();

            Console.WriteLine("EndsWith");
            Console.WriteLine($"Strings '{str1}' EndsWith 'a': {str1.EndsWith('a')} ");
            Console.WriteLine($"Strings '{str2}' EndsWith 't': {str2.EndsWith('t')} ");
            Console.WriteLine($"Strings '{str3}' EndsWith 'u': {str3.EndsWith('u')} ");
            Console.WriteLine($"Strings '{str4}' EndsWith 'p': {str4.EndsWith('p')} ");
            Console.WriteLine($"Strings '{str5}' EndsWith '*': {str5.EndsWith('*')} ");
            Console.WriteLine($"Strings '{str6}' EndsWith ' ': {str6.EndsWith(' ')} ");
            Console.WriteLine();

            Console.WriteLine("Contains");
            Console.WriteLine($"Strings '{str1}' Contains 'Ol': {str1.Contains("Ol")} ");
            Console.WriteLine($"Strings '{str2}' Contains 'Wa': {str2.Contains("Wa")} ");
            Console.WriteLine($"Strings '{str3}' Contains 'up': {str3.Contains("up")} ");
            Console.WriteLine($"Strings '{str4}' Contains 'str3': {str4.Contains(str3)} ");
            Console.WriteLine($"Strings '{str5}' Contains '**up': {str5.Contains("**up")} ");
            Console.WriteLine($"Strings '{str6}' Contains '     ': {str6.Contains("     ")} ");
            Console.WriteLine();

            Console.WriteLine("ToLower");
            Console.WriteLine($"Strings '{str1}' ToLower 'Ol': {str1.Contains("Ol")} ");
            Console.WriteLine($"Strings '{str2}' ToLower 'Wa': {str2.Contains("Wa")} ");
            Console.WriteLine($"Strings '{str3}' ToLower 'up': {str3.Contains("up")} ");
            Console.WriteLine($"Strings '{str4}' ToLower 'str3': {str4.Contains(str3)} ");
            Console.WriteLine($"Strings '{str5}' ToLower '**up': {str5.Contains("**up")} ");
            Console.WriteLine($"Strings '{str6}' Contains '     ': {str6.Contains("     ")} ");
            Console.WriteLine();

            Console.WriteLine("ToLower");
            Console.WriteLine($"Strings '{str7}' ToLower : {str7.ToLower()} ");
            Console.WriteLine();

            Console.WriteLine("ToLowerInvariant");
            Console.WriteLine($"Strings '{str7}' ToLowerInvariant : {str7.ToLowerInvariant()} ");
            Console.WriteLine();

            Console.WriteLine("ToUpper");
            Console.WriteLine($"Strings '{str7}' ToUpper : {str7.ToUpper()} ");
            Console.WriteLine();

            Console.WriteLine("ToUpperInvariant");
            Console.WriteLine($"Strings '{str7}' ToUpperInvariant : {str7.ToUpperInvariant()} ");
            Console.WriteLine();

            Console.WriteLine("IndexOf");
            Console.WriteLine($"Strings '{str1}' IndexOf 'l': {str1.IndexOf("l")} ");
            Console.WriteLine($"Strings '{str2}' IndexOf 'a': {str2.IndexOf("a")} ");
            Console.WriteLine($"Strings '{str3}' IndexOf 'p': {str3.IndexOf("p")} ");
            Console.WriteLine($"Strings '{str4}' IndexOf 'str3': {str4.IndexOf(str3)} ");
            Console.WriteLine($"Strings '{str5}' IndexOf '*': {str5.IndexOf("*")} ");
            Console.WriteLine($"Strings '{str6}' IndexOf ' ': {str6.IndexOf(" ")} ");
            Console.WriteLine();

            Console.WriteLine("LastIndexOf");
            Console.WriteLine($"Strings '{str1}' LastIndexOf 'l': {str1.LastIndexOf("l")} ");
            Console.WriteLine($"Strings '{str2}' LastIndexOf 'a': {str2.LastIndexOf("a")} ");
            Console.WriteLine($"Strings '{str3}' LastIndexOf 'p': {str3.LastIndexOf("p")} ");
            Console.WriteLine($"Strings '{str4}' LastIndexOf 'str3': {str4.LastIndexOf(str3)} ");
            Console.WriteLine($"Strings '{str5}' LastIndexOf '*': {str5.LastIndexOf("*")} ");
            Console.WriteLine($"Strings '{str6}' LastIndexOf ' ': {str6.LastIndexOf(" ")} ");
            Console.WriteLine();

            Console.WriteLine("PadLeft");
            Console.WriteLine($"Strings '{str7}' PadLeft : {str7.PadLeft(6, c)} ");
            Console.WriteLine();

            Console.WriteLine("PadRight");
            Console.WriteLine($"Strings '{str7}' PadRight : {str7.PadRight(6, c)} ");
            Console.WriteLine();

            Console.WriteLine("Replace");
            Console.WriteLine($"Strings '{str1}' Replace 'O' to 'U': {str1.Replace('O', 'U')} ");
            Console.WriteLine($"Strings '{str2}' Replace 'a' to 'u': {str2.Replace('a', 'u')} ");
            Console.WriteLine($"Strings '{str3}' Replace 'u' to 'W': {str3.Replace('u', 'W')} ");
            Console.WriteLine($"Strings '{str4}' Replace 'a' to 'd': {str4.Replace('a', 'd')} ");
            Console.WriteLine($"Strings '{str5}' Replace '*' to '/': {str5.Replace('*', '/')} ");
            Console.WriteLine($"Strings '{str6}' Replace ' ' to ',': {str6.Replace(' ', ',')} ");
            Console.WriteLine();

            Console.WriteLine("Remove");
            Console.WriteLine($"Strings '{str1}' Remove index=2: {str1.Remove(2)} ");
            Console.WriteLine($"Strings '{str2}' Remove index=1 count=2: {str2.Remove(1, 2)} ");
            Console.WriteLine($"Strings '{str3}' Remove index=0: {str3.Remove(0)} ");
            Console.WriteLine($"Strings '{str4}' Remove index=1: {str4.Remove(1)} ");
            Console.WriteLine($"Strings '{str5}' Remove index=0 count=4: {str5.Remove(0, 4)} ");
            Console.WriteLine($"Strings '{str6}' Remove index=0 count=4: {str6.Remove(0, 4)} ");
            Console.WriteLine();

            Console.WriteLine("Insert");
            Console.WriteLine($"Strings '{str1}' Insert index=3 to Amigos: {str1.Insert(3, " Amigos")} ");
            Console.WriteLine($"Strings '{str2}' Insert index=4 to str3(up): {str2.Insert(4, $" {str3}")} ");
            Console.WriteLine($"Strings '{str3}' Insert index=0 to Seven: {str3.Insert(0, "Seven ")} ");
            Console.WriteLine($"Strings '{str4}' Insert index=2 to  and down: {str4.Insert(2, " and down")} ");
            Console.WriteLine($"Strings '{str5}' Insert index=0 to code1 =: {str5.Insert(0, "code1 =")} ");
            Console.WriteLine($"Strings '{str6}' Insert index=0 to code2 =: {str6.Insert(0, "code2 =")} ");
            Console.WriteLine();

            Console.WriteLine("\n\n\n");
            Console.WriteLine("StringBuilder");
            Console.WriteLine();
            Console.WriteLine("Append");
            Console.Write($"Strings '{strb1}' ");
            Console.WriteLine($"Append ' RRR': {strb1.Append(" RRR")} ");
            Console.Write($"Strings '{strb2}' ");
            Console.WriteLine($"Append 'guys': {strb2.Append("guys")} ");
            Console.Write($"Strings '{strb3}' ");
            Console.WriteLine($"Append ' w o r l d': {strb3.Append(" w o r l d")} ");

            Console.WriteLine("AppendFormat");
            Console.Write($"Strings '{strb1}' ");
            Console.WriteLine($"AppendFormat 'str1 , str2': {strb1.AppendFormat(" {0}, {1}", str1, str2)} ");
            Console.Write($"Strings '{strb2}' ");
            Console.WriteLine($"AppendFormat 'str3': {strb2.AppendFormat(" {0}", str3)} ");
            Console.Write($"Strings '{strb3}' ");
            Console.WriteLine($"AppendFormat 'str5': {strb3.AppendFormat(" {0}", str5)} ");

            Console.WriteLine("Insert");
            Console.Write($"Strings '{strb1}' ");
            Console.WriteLine($"Insert index=0, 'Gra': {strb1.Insert(0, "Gra ")} ");
            Console.Write($"Strings '{strb2}' ");
            Console.WriteLine($"Insert 'ndex=4, ' seven': {strb2.Insert(4, " seven")} ");
            Console.Write($"Strings '{strb3}' ");
            Console.WriteLine($"Insert index=9, ' 111': {strb3.Insert(9, " 111")} ");

            Console.WriteLine("Remove");
            Console.Write($"Strings '{strb1}' ");
            Console.WriteLine($"Remove index=0, count=4: {strb1.Remove(0, 4)} ");
            Console.Write($"Strings '{strb2}' ");
            Console.WriteLine($"Remove index=4, count=5: {strb2.Remove(4, 5)} ");
            Console.Write($"Strings '{strb3}' ");
            Console.WriteLine($"Remove index=0, count=4': {strb3.Remove(9, 4)} ");

            Console.WriteLine("Replace");
            Console.Write($"Strings '{strb1}' ");
            Console.WriteLine($"Replace 'a'='o': {strb1.Replace('a' , 'o')} ");
            Console.Write($"Strings '{strb2}' ");
            Console.WriteLine($"Replace 'a'='u': {strb2.Replace('a', 'u')} ");
            Console.Write($"Strings '{strb3}' ");
            Console.WriteLine($"Replace 'l'='e': {strb3.Replace('l', 'e')} ");




        }
    }
}
