﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp
{
    public static class Arrays
    {
        public static void StartArrays()
        {
            var twoDimensionalArray = new int[2, 4] { { 1, 2, 3, 4 }, { 5, 6, 7, 8 } };

            var jaggedArray = new int[2][];
            jaggedArray[0] = new[] { 1, 3, 5 };
            jaggedArray[1] = new[] { 7, 9 };


            foreach (var i in twoDimensionalArray)
            {
                Console.Write($"{i} ");
            }


            Console.WriteLine();


            foreach (var buffer1 in jaggedArray)
            {
                Console.WriteLine();
                foreach (var buffer2 in buffer1)
                {
                    Console.Write(buffer2 + " ");
                }
            }
        }
    }
}
