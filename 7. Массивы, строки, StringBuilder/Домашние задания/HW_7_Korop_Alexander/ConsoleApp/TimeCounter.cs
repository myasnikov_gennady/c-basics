﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp
{
    class TimeCounter
    {
        public static void StartTimeCounter()
        {
            string str = "start";
            StringBuilder strb = new StringBuilder("start");
            Console.WriteLine("String appened without StringBuilder");
            Console.WriteLine(DateTime.Now);
            int length = 100000;
            for (int i = 0; i < length; i++)
            {
                str += "InProcess";
            }
            Console.WriteLine(DateTime.Now);
            Console.WriteLine("String appened with StringBuilder");
            for (int i = 0; i < length; i++)
            {
                strb.Append("InProcess");
            }
            Console.WriteLine(DateTime.Now);

        }
    }
}
