﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArraysAndStrings
{
    class TrimTrimStartTrimEndMethods
    {
        string s1;
        string s2;
        public TrimTrimStartTrimEndMethods()
        {
            s1 = "   Goodbye   ";
            s2 = "....Hello***";
        }

        public void PrintTrimMethod()
        {
            Console.WriteLine("\n3.4. Trim method:");
            Console.WriteLine($"Before Trim method:{s1}" + ".");
            Console.WriteLine($"After Trim method:{s1.Trim()}" + "." + "\n(Method removes all leading and trailing white-space characters from the current String object.)");
        }

        public void PrintTrimStartMethod()
        {
            Console.WriteLine("\nTrimStart method:");
            char[] charsToTrim = { '*', '.'};
            Console.WriteLine($"Before TrimStart method:{s2}");
            Console.WriteLine($"After TrimStart method:{s2.TrimStart(charsToTrim)}" + "\n(Methods removes whitespaces and other characters from the start of a string.)");
        }

        public void PrintTrimEndMethod()
        {
            Console.WriteLine("\nTrimEnd method:");
            char[] charsToTrim = { '*', '.' };
            Console.WriteLine($"Before TrimEnd method:{s2}");
            Console.WriteLine($"After TrimEnd method:{s2.TrimEnd(charsToTrim)}" + "\n(Methods removes whitespaces and other characters from the end of a string.)");
        }

    }
}
