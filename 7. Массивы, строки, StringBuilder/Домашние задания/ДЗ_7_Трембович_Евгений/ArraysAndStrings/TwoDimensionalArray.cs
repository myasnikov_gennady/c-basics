﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArraysAndStrings
{
    public class TwoDimensionalArray
    {
        int[,] twoDimensionalArray;
        public TwoDimensionalArray() 
        {
            twoDimensionalArray = new int[3, 6] 
            {
                { 0, 1, 2, 3, 4, 5 },
                { 6, 7, 8, 9, 10, 11 },
                { 12, 13, 14, 15, 16, 17 } 
            }; 
        }
               
        public void PrintTableToConsole()
        {
            int numbersOfRowsInTheArray = twoDimensionalArray.GetUpperBound(0) + 1;
            int numbersOfColumnsInTheArray = twoDimensionalArray.Length / numbersOfRowsInTheArray;
            Console.WriteLine("1. Two dimensional array:");
            for (int i = 0; i < numbersOfRowsInTheArray; i++)
            {
                for (int j = 0; j < numbersOfColumnsInTheArray; j++)
                {
                    Console.Write($"{twoDimensionalArray[i, j]}\t");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }
}
