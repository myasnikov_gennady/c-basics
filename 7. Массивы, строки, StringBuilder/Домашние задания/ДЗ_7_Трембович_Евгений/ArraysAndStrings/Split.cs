﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArraysAndStrings
{
    class Split
    {
        string s1;

        public Split()
        {
            s1 = "Each word in separate line";
        }

        public void PrintSplitMethod()
        {
            Console.WriteLine("\n3.10. Split method:");
            Console.WriteLine($"{s1}");
            string[] wordsArray = s1.Split(' ');
            Console.WriteLine($"Using Split method:");
            foreach (string item in wordsArray)
            {
                Console.WriteLine(item);
            }
        }
    }
}
