﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArraysAndStrings
{
    class CompareMethod
    {
        string hello;
        string world;
        string hello2;

        public CompareMethod()
        {
            hello = "hello";
            world = "world";
            hello2 = "hello";
        }

        public void PrintCompareMethod()
        {
            Console.WriteLine($"\n3.2. Compare method:");
            Console.WriteLine($"<{hello}> <{world}> - Compare method returns: {String.Compare(hello, world)} - it means that first substring\nprecedes the second substring in the sort order.");
            Console.WriteLine($"<{hello}> <{hello2}> - Compare method returns: {String.Compare(hello, hello2)} - it means that substrings occur in\nthe same position in the sort order.");
            Console.WriteLine($"<{world}> <{hello}> - Compare method returns: {String.Compare(world, hello)} - it means that first substring\nfollows the second substring in the sort order.");
        }
    }

    
}
