﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArraysAndStrings
{
    class CompareToMethod
    {
        string s1;
        string s2;
        string s3;

        public CompareToMethod()
        {
            s1 = "red";
            s2 = "brown";
            s3 = "red";
        }
        public void PrintCompareToMethod()
        {
            Console.WriteLine($"\n3.3. CompareTo method:");
            Console.WriteLine($"<{s1}> <{s2}> - CompareTo method returns: {s1.CompareTo(s2)} - it means that first string follows the second in the sort order.");
            Console.WriteLine($"<{s1}> <{s3}> - CompareTo method returns: {s1.CompareTo(s3)} - it means that strings are the same.");
            Console.WriteLine($"<{s2}> <{s1}> - CompareTo method returns: {s2.CompareTo(s1)} - it means that first string \nprecedes the second in the sort order.");
        }
    }
}
