﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArraysAndStrings
{
    public class EqualsMethod
    {
        string String1;
        string String2;
        string uperrCaseString;
        string lowerCaseString;

        public  EqualsMethod()
        {
            String1 = "Name";
            String2 = "Name";
            uperrCaseString = "HELLO";
            lowerCaseString = "hello";
        }

        public void PrintExecuteEqualsMethod()
        {
            Console.WriteLine("3.1. Equals method:");
            Console.WriteLine($"{String1} = {String2} : {Equals(String1, String2)}");
            Console.WriteLine($"{uperrCaseString} = {lowerCaseString} : {uperrCaseString.Equals(lowerCaseString)}");
            Console.WriteLine($"Equals method ignoring the case:\n" +
                $"{uperrCaseString} = {lowerCaseString} : {uperrCaseString.Equals( lowerCaseString, StringComparison.CurrentCultureIgnoreCase)}");
            
        }
    }
}
