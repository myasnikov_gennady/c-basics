﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Text;

namespace ArraysAndStrings
{
    class StringBuilderOperation
    {
        StringBuilder s1;
        StringBuilder s2;
        StringBuilder s3;
        StringBuilder s4;
        StringBuilder s5;
        public StringBuilderOperation() 
        {
            s1 = new StringBuilder("Hello World");
            s2 = new StringBuilder("Your total amount is ");
            s3 = new StringBuilder("mistak");
            s4 = new StringBuilder("Prog  ramm");
            s5 = new StringBuilder("1.2.3.4.5");
        }

        public void PrintStringBuilderAppendMethod()
        {
            Console.WriteLine("\n4.1. StringBuilder.Append method:");
            Console.WriteLine($"Before Append method: {s1}");
            Console.WriteLine($"After Append method: {s1.Append("!")}");
        }

        public void PrintStringBuilderAppendFormatMethod()
        {
            decimal value = 25.46m;
            CultureInfo enUS = CultureInfo.CreateSpecificCulture("en-US");
            Console.WriteLine("\n4.2. StringBuilder.AppendFormat method:");
            Console.WriteLine($"Before AppendFormat method: {s2}");
            Console.WriteLine($"After AppendFormat method: {s2.AppendFormat(enUS, "{0:C} ", value)}");
        }

        public void PrintStringBuilderInsertMethod()
        {
            Console.WriteLine("\n4.3. StringBuilder.Insert method:");
            Console.WriteLine($"Before Insert method: {s3}");
            Console.WriteLine($"After Insert method: {s3.Insert(s3.Length, "e")}");
        }

        public void PrintStringBuilderRemoveMethod()
        {
            Console.WriteLine("\n4.4. StringBuilder.Remove method:");
            Console.WriteLine($"Before Remove method: {s4}");
            Console.WriteLine($"After Remove method: {s4.Remove(4, 2)}");
        }

        public void PrintStringBuilderReplaceMethod()
        {
            char oldChar = '.';
            char newChar = ',';
            Console.WriteLine("\n4.5. StringBuilder.Replace method:");
            Console.WriteLine($"Before Replace method: {s5}");
            Console.WriteLine($"After Replace method: {s5.Replace(oldChar, newChar)}");
        }

        public void PrintTimeAddingString()
        {
            Console.WriteLine("\n5. Time adding string:");
            var _string = "Hello";
            var time1 = new Stopwatch();
            time1.Start();
            //Console.WriteLine(DateTime.Now);
            for (int i = 0; i < 100000; i++)
            {
                _string += "Hello";
            }
            //Console.WriteLine(DateTime.Now);
            time1.Stop();
            Console.WriteLine($"Time adding whithout StringBuilder: {time1.Elapsed}");

            StringBuilder sb = new StringBuilder("Hello");
            var time2 = new Stopwatch();
            time2.Start();
            for (int i = 0; i < 100000; i++)
            {
                sb.Append("Hello");
            }
            time2.Stop();
            Console.WriteLine($"Time adding whiht StringBuilder: {time2.Elapsed}");
        }
    }
}
