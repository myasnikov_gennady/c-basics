﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArraysAndStrings
{
    public class JaggedArray
    {
        int[][] jaggedArray;
        public JaggedArray()
        {
            jaggedArray = new int[4][];
            jaggedArray[0] = new int[] { 0, 1, 2, 3, 4, 5 };
            jaggedArray[1] = new int[] { 6, 7, 8 };
            jaggedArray[2] = new int[] { 9, 10, 11, 12, 13 };
            jaggedArray[3] = new int[] { 14, 15, 16, 17 };
        }

        public void PrintJaggedArrayToConsole()
        {
            Console.WriteLine("2. Jagged array:");
            for (int i = 0; i < jaggedArray.Length; i++)
            {
                for (int j = 0; j < jaggedArray[i].Length; j++)
                {
                    Console.Write($"{jaggedArray[i][j]}\t");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }
}
