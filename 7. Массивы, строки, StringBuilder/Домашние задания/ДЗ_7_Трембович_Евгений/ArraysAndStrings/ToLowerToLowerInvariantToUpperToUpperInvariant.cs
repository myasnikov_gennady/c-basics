﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArraysAndStrings
{
    class ToLowerToLowerInvariantToUpperToUpperInvariant
    {
        string s1;
        string s2;
        string s3;
        string s4;
        public ToLowerToLowerInvariantToUpperToUpperInvariant()
        {
            s1 = "GOOD MORNING";
            s2 = "MoNdAy";
            s3 = "stop";
            s4 = "WaRnInG";
        }

        public void PrintToLowerMethod()
        {
            Console.WriteLine("\n3.6. ToLower method:");
            Console.WriteLine($"Before ToLower method: {s1}");
            Console.WriteLine($"After ToLower method: {s1.ToLower()}");
        }

        public void PrintToLoweInvariantrMethod()
        {
            Console.WriteLine("\nToLowerInvariant method:");
            Console.WriteLine($"Before ToLowerInvariant method: {s2}");
            Console.WriteLine($"After ToLowerInvariant method: {s2.ToLowerInvariant()}");
        }

        public void PrintToUpperMethod()
        {
            Console.WriteLine("\nToUpper method:");
            Console.WriteLine($"Before ToUpper method: {s3}");
            Console.WriteLine($"After ToUpper method: {s3.ToUpper()}");
        }

        public void PrintToUpperInvariantMethod()
        {
            Console.WriteLine("\nToUpperInvariant method:");
            Console.WriteLine($"Before ToUpperInvariant method: {s4}");
            Console.WriteLine($"After ToUpperInvariant method: {s4.ToUpperInvariant()}");
        }

    }
}
