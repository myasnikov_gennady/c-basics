﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArraysAndStrings
{
    class CompareOrdinalMethod
    {
        string s1;
        string s2;
        string s3;

        public CompareOrdinalMethod()
        {
            s1 = "Name";
            s2 = "Age";
            s3 = "Name";
        }

        public void PrintCompareOrdinalMethod()
        {
            Console.WriteLine($"\nCompareOrdinal method:");
            Console.WriteLine($"<{s1}> <{s2}> - CompareOrdinal method returns: {String.CompareOrdinal(s1, s2)} - it means that first substring is greater than the second substring.");
            Console.WriteLine($"<{s1}> <{s1}> - CompareOrdinal method returns: {String.CompareOrdinal(s1, s1)} - it means that substrings are equal.");
            Console.WriteLine($"<{s2}> <{s1}> - CompareOrdinal method returns: {String.CompareOrdinal(s2, s1)} - it means that first\nsubstring is less than the second substring.");
        }
    }
}
