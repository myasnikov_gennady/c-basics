﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArraysAndStrings
{
    class PadLeftPadRight
    {
        string s1;
        char character;

        public PadLeftPadRight()
        {
            s1 = "Goodbye";
            character = '.';
        }

        public void PrintPadLeftMethod()
        {
            Console.WriteLine("\n3.8. PadLeft method:");
            Console.WriteLine($"Before PadLeft method: {s1}");
            Console.WriteLine($"After PadLeft method: {s1.PadLeft(10, character)}");
        }

        public void PrintPadRightMethod()
        {
            Console.WriteLine("\nPadRight method:");
            Console.WriteLine($"Before PadRight method: {s1}");
            Console.WriteLine($"After PadRight method: {s1.PadRight(10, character)}");
        }
    }
}
