﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArraysAndStrings
{
    class StartsWithEndsWithContainsMethods
    {
        string s1;
        string s2;
        char characterC;
        char characterA;
        char characterR;

        public StartsWithEndsWithContainsMethods()
        {
            s1 = "car";
            s2 = "I play hockey";
            characterC = 'c';
            characterA = 'a';
            characterR = 'r';
        }

        public void PrintStartWithMethod()
        {
            Console.WriteLine("\n3.5. StartWith method:");
            Console.WriteLine($"string \"{s1}\" starts whith caharacter \"{characterA}\" - {s1.StartsWith(characterA)}");
            Console.WriteLine($"string \"{s1}\" starts whith caharacter \"{characterC}\" - {s1.StartsWith(characterC)}");
        }

        public void PrintEndsWithMethod()
        {
            Console.WriteLine("\nEndsWith method:");
            Console.WriteLine($"string \"{s1}\" ends in caharacter \"{characterA}\" - {s1.EndsWith(characterA)}");
            Console.WriteLine($"string \"{s1}\" ends in caharacter \"{characterR}\" - {s1.EndsWith(characterR)}");
        }

        public void PrintContainsMethod()
        {
            Console.WriteLine("\nContains method:");
            Console.WriteLine($"\"play\" is in the string \"{s2}\" - {s2.Contains("play")}");
            Console.WriteLine($"\"{s1}\" is in the string \"{s2}\" - {s2.Contains(s1)}");
        }


    }

    
}
