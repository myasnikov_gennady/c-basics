﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArraysAndStrings
{
    class IndexOfLastIndexOf
    {
        string s1;
        char characterR;
        public IndexOfLastIndexOf()
        {
            s1 = "Programm";
            characterR = 'r';
        }

        public void PrintIndexOfMethod()
        {
            Console.WriteLine("\n3.7. IndexOf method:");
            Console.WriteLine($"The index value of the first occurrence character '{characterR}' in string '{s1}' is {s1.IndexOf(characterR)}");
        }

        public void PrintLastIndexOfMethod()
        {
            Console.WriteLine("\nLastIndexOf method:");
            Console.WriteLine($"The index value of the last occurrence character '{characterR}' in string '{s1}' is {s1.LastIndexOf(characterR)} ");
        }
    }

    
}
