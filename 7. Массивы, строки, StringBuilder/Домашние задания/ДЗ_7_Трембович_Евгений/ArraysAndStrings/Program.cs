﻿using System;

namespace ArraysAndStrings
{
    class Program
    {
        static void Main(string[] args)
        {
            var table = new TwoDimensionalArray();
            table.PrintTableToConsole();

            var jaggedArray = new JaggedArray();
            jaggedArray.PrintJaggedArrayToConsole();

            var equalsMethod = new EqualsMethod();
            equalsMethod.PrintExecuteEqualsMethod();

            var compareMethod = new CompareMethod();
            compareMethod.PrintCompareMethod();

            var compareOrdinalMethod = new CompareOrdinalMethod();
            compareOrdinalMethod.PrintCompareOrdinalMethod();

            var compareToMethod = new CompareToMethod();
            compareToMethod.PrintCompareToMethod();

            var trimTrimStartTrimEndMethods = new TrimTrimStartTrimEndMethods();
            trimTrimStartTrimEndMethods.PrintTrimMethod();
            trimTrimStartTrimEndMethods.PrintTrimStartMethod();
            trimTrimStartTrimEndMethods.PrintTrimEndMethod();

            var startsWithEndsWithContainsMethods = new StartsWithEndsWithContainsMethods();
            startsWithEndsWithContainsMethods.PrintStartWithMethod();
            startsWithEndsWithContainsMethods.PrintEndsWithMethod();
            startsWithEndsWithContainsMethods.PrintContainsMethod();

            var toLowerToLowerInvariantToUpperToUpperInvariant = new ToLowerToLowerInvariantToUpperToUpperInvariant();
            toLowerToLowerInvariantToUpperToUpperInvariant.PrintToLowerMethod();
            toLowerToLowerInvariantToUpperToUpperInvariant.PrintToLoweInvariantrMethod();
            toLowerToLowerInvariantToUpperToUpperInvariant.PrintToUpperMethod();
            toLowerToLowerInvariantToUpperToUpperInvariant.PrintToUpperInvariantMethod();

            var indexOfLastIndexOf = new IndexOfLastIndexOf();
            indexOfLastIndexOf.PrintIndexOfMethod();
            indexOfLastIndexOf.PrintLastIndexOfMethod();

            var padLeftPadRight = new PadLeftPadRight();
            padLeftPadRight.PrintPadLeftMethod();
            padLeftPadRight.PrintPadRightMethod();
            
            var replaceRemoveInsert = new ReplaceRemoveInsert();
            replaceRemoveInsert.PrintReplaceMethod();
            replaceRemoveInsert.PrintRemoveMethod();
            replaceRemoveInsert.PrintInsertMethod();

            var split = new Split();
            split.PrintSplitMethod();

            var stringBuilderOperation = new StringBuilderOperation();
            stringBuilderOperation.PrintStringBuilderAppendMethod();
            stringBuilderOperation.PrintStringBuilderAppendFormatMethod();
            stringBuilderOperation.PrintStringBuilderInsertMethod();
            stringBuilderOperation.PrintStringBuilderRemoveMethod();
            stringBuilderOperation.PrintStringBuilderReplaceMethod();
            stringBuilderOperation.PrintTimeAddingString();




        }
    }
}
