﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArraysAndStrings
{
    class ReplaceRemoveInsert
    {
        string s1;
        string s2;
        string s3;
        char oldChar;
        char newChar;

        public ReplaceRemoveInsert()
        {
            s1 = "1 2 3 4 5";
            s2 = "Tuesday*";
            s3 = "BlackWhite";
            oldChar = ' ';
            newChar = ',';
        }

        public void PrintReplaceMethod()
        {
            Console.WriteLine("\n3.9. Replace method:");
            Console.WriteLine($"Before Replace method: {s1}");
            Console.WriteLine($"After Replace method: {s1.Replace(oldChar, newChar)}");
        }

        public void PrintRemoveMethod()
        {
            Console.WriteLine("\nRemove method:");
            Console.WriteLine($"Before Remove method: {s2}");
            Console.WriteLine($"After Remove method: {s2.Remove(s2.Length - 1)}");
        }

        public void PrintInsertMethod()
        {
            Console.WriteLine("\nInsert method:");
            Console.WriteLine($"Before Insert method: {s3}");
            Console.WriteLine($"After Insert method: {s3.Insert(5, " ")}");
        }

    }
}
