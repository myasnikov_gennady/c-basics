﻿using System;

namespace DZ_7.STEPANOV.A.S
{
    class Program   // полностью скопирована работа у Скрипкина. 0 - не засчитана
    {
            static void Main(string[] args)
            {
                //1. Создать двумерный массив. Пройти по строкам и столбцам. Вывести на консоль.
                Random rnd = new Random();
                int[,] array1 = new int[8, 5]
               {  { rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10)},
               { rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10)},
               { rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10)},
               { rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10)},
               { rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10)},
                 { rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10)},
                 { rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10)},
                 { rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10)}
                };
                Console.WriteLine("Двумерный массив");
                for (int i = 0; i < 8; i++)
                {
                    for (int j = 0; j < 5; j++)
                    {
                        Console.Write(" " + array1[i, j]);
                    }
                    Console.WriteLine();
                }
                Console.WriteLine();
                //2. Создать зубчатый массив. Пройти по строкам и столбцам. Вывести на консоль.
                var array3d = new int[3][];
                array3d[0] = new[] { rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10) };
                array3d[1] = new[] { rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10) };
                array3d[2] = new[] { rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10), rnd.Next(1, 10) };
                Console.WriteLine("Зубчатый массив");
                for (int i = 0; i < array3d.Length; i++)
                {
                    for (int j = 0; j < array3d[i].Length; j++)
                    {
                        Console.Write(" " + array3d[i][j]);
                    }
                    Console.WriteLine();
                }
                Console.WriteLine();
                Console.WriteLine("Сравнение String:");
                Console.WriteLine("Дано :");
                string mess1 = "\x0043";
                string mess2 = "\x0063";
                Console.WriteLine($"{mess1} // char = 67");
                Console.WriteLine($"{mess2}  // char = 99 ");
                Console.WriteLine($"Метод Equals результат = {string.Equals(mess1, mess2)}  ");  //Determines whether two specified String objects have the same value
                Console.WriteLine();
                Console.WriteLine($"Метод Compare результат = {string.Compare(mess1, mess2)}  ");  //Compares two specified String objects and returns an integer that indicates their relative position in the sort order.
                Console.WriteLine();
                Console.WriteLine($"Метод Compare.Ordinal результат = {string.CompareOrdinal(mess1, mess2)}  //67-99 = -32"); //Compares two specified String objects by evaluating the numeric values of the corresponding Char objects in each string.
                Console.WriteLine();
                Console.WriteLine($"Метод Compare результат = {string.Compare(mess1, mess2)}  ");
                Console.WriteLine();
                Console.WriteLine("String Comparison методы:");
                Console.WriteLine($" message 1 = {mess1}");
                Console.WriteLine($" message 2  = {mess2}");
                Console.WriteLine($" Метод InvariantCulture           := {String.Equals(mess1, mess2, StringComparison.InvariantCulture)}"); //Compare strings using culture-sensitive sort rules and the invariant culture.
                Console.WriteLine($" Метод InvariantCultureIgnoreCase := {String.Equals(mess1, mess2, StringComparison.InvariantCultureIgnoreCase)}"); // Compare strings using culture-sensitive sort rules, the invariant
                                                                                                                                                       // culture, and ignoring the case of the strings being compared.
                Console.WriteLine($" Метод CurrentCulture             := {String.Equals(mess1, mess2, StringComparison.CurrentCulture)}");   // Compare strings using culture-sensitive sort rules and the current culture.
                Console.WriteLine($" Метод CurrentCultureIgnoreCase   := {String.Equals(mess1, mess2, StringComparison.CurrentCultureIgnoreCase)}"); //Compare strings using culture-sensitive sort rules, the current culture,
                                                                                                                                                     //and ignoring the case of the strings being compared.
                Console.WriteLine($" Метод Ordinal                    := {String.Equals(mess1, mess2, StringComparison.Ordinal)}"); // Compare strings using ordinal (binary) sort rules.
                Console.WriteLine($" Метод OrdinalIgnoreCase          := {String.Equals(mess1, mess2, StringComparison.OrdinalIgnoreCase)}"); // Compare strings using ordinal (binary) sort rules and ignoring the
                                                                                                                                              // case of the strings being compared.
                mess1 = "You can do it";
                mess2 = "   1 You can do it";
                Console.WriteLine();
                Console.WriteLine("Comparison of CompareTo  '{0}'   and    '{1}'   : {2}", mess1, mess2, mess1.CompareTo(mess2));   // Compares this instance with a specified String object and indicates whether this instance precedes,                                                                                
                                                                                                                                    //follows, or appears in the same position in the sort order as the specified string.
                Console.WriteLine();
                char[] charsToTrim = { '*', ' ', '\'' };
                string banner = "***           YOU can do it       ***";
                string result = banner.Trim(charsToTrim);
                Console.WriteLine("Метод Trim\t   '{0}' to  := '{1}'", banner, result); //Removes all leading and trailing white-space characters from the current string.
                Console.WriteLine($"Метод TrimStart\t   {banner} to :={banner.TrimStart('*')}"); //Removes all the leading white-space characters from the current string.
                Console.WriteLine($"Метод TrimStart\t   {banner} to :={banner.TrimEnd('*')}"); //Removes all the trailing occurrences of a set of characters specified in an array from the current string.
                Console.WriteLine($"Метод EndWith\t   {banner} to :={banner.EndsWith('*')}"); //Determines whether the end of this string instance matches the specified string.
                Console.WriteLine($"Метод StartsWith\t   {banner} to :={banner.StartsWith(' ')}"); //Determines whether the beginning of this string instance matches the specified string.
                Console.WriteLine($"Метод Contains\t   {banner} to :={banner.Contains(' ')}"); //Returns a value indicating whether a specified substring occurs within this string.
                Console.WriteLine($"Метод ToLower\t   {banner} to :={banner.ToLower()}");// Returns a copy of this string converted to lowercase.
                Console.WriteLine($"Метод ToUpper\t   {banner} to :={banner.ToUpper()}");// Returns a copy of this string converted to uppercase.          
                Console.WriteLine($"Метод ToLowerInvariant\t   {banner} to :={banner.ToLowerInvariant()}");// Returns a copy of this String object converted to lowercase using the casing rules of the invariant culture.
                Console.WriteLine($"Метод ToUpperInvariant\t   {banner} to :={banner.ToUpperInvariant()}");// Returns a copy of this String object converted to lowercase using the casing rules of the invariant culture.
                Console.WriteLine($"Метод IndexOf\t   {banner} to :={banner.IndexOf(banner)}");// Reports the zero-based index of the first occurrence of a specified Unicode character or string within this instance.
                                                                                               // The method returns -1 if the character or string is not found in this instance.
                Console.WriteLine($"Метод IndexOf\t   {banner} to :={banner.LastIndexOf(banner)}");// Reports the zero-based index position of the last occurrence of a specified Unicode character or string within this instance.
                                                                                                   // The method returns -1 if the character or string is not found in this instance.
                char pad = '.';
                Console.WriteLine($"Метод PadLeft\t   {mess1} to :='{mess1.PadLeft(30, pad)}'");//Returns a new string that right-aligns the characters in this instance by padding them on the
                                                                                                //left with a specified Unicode character, for a specified total length.
                Console.WriteLine($"Метод PadRight\t   {mess1} to :='{mess1.PadRight(20, pad)}'"); //Returns a new string that left-aligns the characters in this string by padding them on the right with
                                                                                                   //a specified Unicode character, for a specified total length.
                Console.WriteLine($"Метод Replace\t   {mess1} to :='{mess1.Replace("can", "can't")}'"); // Returns a new string in which all occurrences of a specified
                                                                                                        // string in the current instance are replaced with another specified string.
                Console.WriteLine($"Метод Remove\t   {mess1} to :='{mess1.Remove(10)}'");  //Returns a new string in which all the characters in the current instance,
                                                                                           //beginning at a specified position and continuing through the last position, have been deleted.
                Console.WriteLine($"Метод Insert\t   {mess1} to :='{mess1.Insert(11, "also ")}'");// Returns a new string in which a specified string is inserted at a specified index position in this instance
                Console.WriteLine();
                string[] split = mess1.Split(' ');
                Console.WriteLine("Метод Split");
                foreach (var sub in split)
                {
                    Console.WriteLine($"Substring: {sub}");
                }
                Console.WriteLine();

                StringBulderApp.Run();
            }
        }
    }
    

