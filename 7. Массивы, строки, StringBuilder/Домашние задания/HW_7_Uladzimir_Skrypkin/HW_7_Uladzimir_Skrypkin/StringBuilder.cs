﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Diagnostics;

namespace HW_7_Uladzimir_Skrypkin
{
    public class StringBuilderApp
    {
        public static void Run()
        {
            StringBuilder sb = new StringBuilder();     // The following example shows how to call many of the methods defined by the StringBuilder class
            int var1 = 123;
            float var2 = 1.22F;
            string var3 = "Строка";
            object[] var4 = { 1, 1.4, 'X' };
            Console.WriteLine();
            Console.WriteLine("StringBuilder.AppendFormat метод:");
            sb.AppendFormat("1) {0}", var1);
            Console.WriteLine(sb.ToString());
            sb.Length = 0;
            sb.AppendFormat("2) {0}, {1}", var1, var2);
            Console.WriteLine(sb.ToString());
            sb.Length = 0;
            sb.AppendFormat("3) {0}, {1}, {2}", var1, var2, var3);
            Console.WriteLine(sb.ToString());
            sb.Length = 0;
            sb.AppendFormat("4) {0}, {1}, {2}", var4);
            Console.WriteLine(sb.ToString());
            sb.Length = 0;
            Console.WriteLine();
            var sb1 = new StringBuilder("The following example "); // The following example shows how to call many of the methods defined by the StringBuilder class
            var Text = sb1.ToString();
            Console.WriteLine($"Первая строка - '{Text}'");
            sb1.Append(" shows how to call ");
            Console.WriteLine(" StringBuilder.Append метод:");
            Text = sb1.ToString();
            Console.WriteLine($"  '{Text}'");
            sb1.Insert(4, "  many of the methods defined by the StringBuilder class ");
            Console.WriteLine(" StringBuilder.Insert метод:");
            Text = sb1.ToString();
            Console.WriteLine($"  '{Text}'");
            sb1.Replace(" to call ", " many");
            Console.WriteLine("StringBuilder.Replace метод:");
            Text = sb1.ToString();
            Console.WriteLine($" '{Text}'");
            sb1.Remove(16, 9);
            Console.WriteLine(" StringBuilder.Remove метод:");
            Text = sb1.ToString();
            Console.WriteLine($"  '{Text}'");
            Console.WriteLine();
            Console.WriteLine($"  Замер времени выполнения в цикле от 0 до 1000000 присоединения строки и с использованием класса StringBuilder");
            var str = "OK";
            var begin = new DateTime();
            var contin = new DateTime();
            var end = new DateTime();
            Console.WriteLine($" Начало стрингового цикла, пример цикла  на 1000 и с использованием класса StringBuilder");
            Console.WriteLine($"Нажмите любую клавишу для продолжения");
            Console.ReadKey();
            begin = DateTime.Now;
            for (var j = 0; j <= 1000; j++)  // массив на 1000 000 будет идти около часа
            {
                str += "OK";
                Console.Write(str);
            }
            contin = DateTime.Now;
            StringBuilder sb2 = new StringBuilder(capacity: 1_000_000);
            for (var i = 0; i <= 1_000_000; i++)   // идет пару секунд
            {
                sb2.Append("OK");
            }
            var mess4 = sb2.ToString();
            Console.WriteLine(mess4);
            Console.WriteLine(" Конец создания строки с использованием класса StringBuilder");
            end = DateTime.Now;
            //Console.WriteLine(begin);
            //Console.WriteLine(contin);
            //Console.WriteLine(end);
            Console.WriteLine($" Время выполнения первого цикла без  StringBuilder :{contin.Minute - begin.Minute} минут и {contin.Second - begin.Second} секунд");
            Console.WriteLine($" Время выполнения второго цикла с  StringBuilder :{end.Minute - contin.Minute} минут и {end.Second - contin.Second} секунд");
        }
    }
}
