1. Создать двумерный массив. Пройти по строкам и столбцам. Вывести на консоль.
2. Создать зубчатый массив. Пройти по строкам и столбцам. Вывести на консоль.
3. Вывести на колноль выполнение следующих методов работы со сроками:
    3.1.    Equals
    3.2.    Compare, CompareOrdinal
    3.3.    CompareTo
    3.4.    Trim, TrimStart, TrimEnd
    3.5.    StartsWith, EndsWith, Contains
    3.6.    ToLower, ToLowerInvariant, ToUpper, ToUpperInvariant,
    3.7.    IndexOf, LastIndexOf
    3.8.    PadLeft, PadRight
    3.9.    Replace, Remove, Insert
    3.10.   Split
4. С помощью класса StringBuilder выполнить операции:
    4.1.    Append
    4.2.    AppendFormat
    4.3.    Insert
    4.4.    Remove
    4.5.    Replace
5. Произвести замер времени выполнения в цикле от 0 до 1000000 присоединения строки и с использованием класса StringBuilder