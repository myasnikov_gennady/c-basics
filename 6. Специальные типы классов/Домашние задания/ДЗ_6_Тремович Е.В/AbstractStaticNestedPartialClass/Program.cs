﻿using System;

namespace AbstractStaticNestedPartialClass
{
    class Program
    {
        static void Main(string[] args)
        {
            var nonStaticClassObject = new NonStaticClass();
            Console.WriteLine($"Static field value: {NonStaticClass.StaticField}");
            Console.WriteLine($"Non static field value: {nonStaticClassObject.NonStaticField}");
            
            NonStaticClass.StaticMethod();
            Console.WriteLine($"Static field value after execute static method: {NonStaticClass.StaticField}");
            Console.WriteLine($"Non static field value after execute static method: {nonStaticClassObject.NonStaticField}");

            nonStaticClassObject.NonStaticMethod();
            Console.WriteLine($"Static field value after execute static and non static methods: {NonStaticClass.StaticField}");
            Console.WriteLine($"Non static field value after execute static and non static methods: {nonStaticClassObject.NonStaticField}");


        }
    }
}
