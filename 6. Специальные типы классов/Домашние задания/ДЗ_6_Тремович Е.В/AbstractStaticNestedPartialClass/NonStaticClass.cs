﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractStaticNestedPartialClass
{
    public class NonStaticClass
    {
        public static void StaticMethod()
        {
            StaticField += 1; 
        }

        public void NonStaticMethod()
        {
            StaticField += 2;
            NonStaticField = "After";
        }

        public static int StaticField = 0;

        public string NonStaticField = "Before";

    }
}
