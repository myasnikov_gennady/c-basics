﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HW6_Uladzimir_Skrrypkin
{
    public abstract partial class Abstract1
    {
        public class C1
        {
            private string _name { get; set; }
            private string _lastname { get; set; }
            public string Lastname
            {
                get => _lastname;
                set => _lastname = value;
            }
            public string Name
            {
                get => _name;
                set => _name = value;
            }

            protected C1(string name, string lastname)
            {
                Name = name;
               Lastname = lastname;
              
            }

        }
    }
}
