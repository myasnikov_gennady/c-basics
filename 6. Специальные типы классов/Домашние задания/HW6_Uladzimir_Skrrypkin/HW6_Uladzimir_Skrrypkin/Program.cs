﻿using System;


namespace HW6_Uladzimir_Skrrypkin
{
    class Program
    {
        static void Main(string[] args)
        {
            var notstaticMet = new NotStaticClass(3,4);
            var staticMet = NotStaticClass.Sum();
            Console.WriteLine($"Значание  не статического поля = {notstaticMet.digit02}, значание статического поля = {staticMet}");
            Console.WriteLine();
            notstaticMet.ChangeTwo();
            var musk = new B1("Musk", "Ilon");
             Console.WriteLine($"Фамилия:{musk.Name}, Имя {musk.Lastname}");
            var muskcpec = new B2(49, "Martian");
           Console.WriteLine($"Возраст:{muskcpec._age}, Статус {muskcpec._work}");
        }
        public class B1 : Abstract1.C1
        {
            public B1(string name, string lastname) : base(name, lastname)
            {
                Console.WriteLine();
                Console.WriteLine("Вызван метод наследования из первого файла класса");
            }
        }
        public class B2 : Abstract1.C2
        {
            public B2(int age, string work) : base(age, work)
            {
                Console.WriteLine();
                Console.WriteLine("Вызван метод наследования из второго файла  класса");
            }
        }
    }
}
