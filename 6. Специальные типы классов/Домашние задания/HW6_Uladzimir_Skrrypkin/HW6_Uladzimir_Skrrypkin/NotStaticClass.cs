﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HW6_Uladzimir_Skrrypkin
{
    public class NotStaticClass
    {
        private static int _digit01;
        public int digit02;
        public NotStaticClass(int number1, int number2)
        {
            Digit01 = number1;
            digit02 = number2;
            Console.WriteLine($"Значание    статического поля:{Digit01}");
            Console.WriteLine($"Значание не статического поля:{digit02}");
        }
       public static int Digit01 { get => _digit01; set => _digit01 = value; }
        public static int Sum()
        {
            Console.WriteLine($"Вызван статический метод: статическое поле +10.");
            return _digit01+=10;
        }
        public void ChangeTwo()
        {
            Digit01++;
            digit02--;
            Console.WriteLine($"Не статический метод вызван: увеличение статического поля +1; уменьщение не статического поля на -1.");
            Console.WriteLine($"Значание    статического поля:{Digit01}");
            Console.WriteLine($"Значание не статического поля:{digit02}");
        }
    }
}
