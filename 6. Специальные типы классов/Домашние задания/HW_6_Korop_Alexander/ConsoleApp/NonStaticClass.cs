﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp
{
    class NonStaticClass
    {
        private string nonStaticField;
        private static string staticField;

        public string NonStaticField
        {
            get
            {
                return nonStaticField;
            }
            set
            {
                nonStaticField = value;
            }
        }
        public string StaticField
        {
            get
            {
                return staticField;
            }
            set
            {
                staticField = value;
            }
        }


        public void NonStaticMethod()
        {
            nonStaticField = "Now nonStaticField is NonStatic Method";
            staticField = "Now staticField is Static Method";
        }
        public static void StaticMethod()
        {
            staticField = "Now static Field is Static Method";
        }


        public NonStaticClass()
            {
            nonStaticField = "It's nonStaticField";
            staticField = "It's staticField";
            }


    }
}
