﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp
{
    public abstract partial class AbstractClass
    {
        public class FirstInbuiltClass
        {
            private string firstField;

            public string FirstField
            {
                get
                {
                    return firstField;
                }
                set
                {
                    firstField = value;
                }


            }
        }
    }
}
