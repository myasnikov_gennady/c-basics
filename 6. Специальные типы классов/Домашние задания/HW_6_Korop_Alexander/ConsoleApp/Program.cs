﻿using System;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {

            var nonStaticClass = new NonStaticClass();



            Console.WriteLine($"Fields after object initialization " +
                              $"\nnonStaticField: {nonStaticClass.NonStaticField}" +
                              $"\nstaticField   : {nonStaticClass.StaticField}");



            NonStaticClass.StaticMethod();
            Console.WriteLine($"\nStaticMethod was called" +
                              $"\nnow nonStaricField: {nonStaticClass.NonStaticField}" +
                              $"\nnow staricField   : {nonStaticClass.StaticField}");


            nonStaticClass.NonStaticMethod();
            Console.WriteLine($"\nNonStaticMethod was called" +
                              $"\nnow nonStaricField: {nonStaticClass.NonStaticField}" +
                              $"\nnow staricField   : {nonStaticClass.StaticField}");




            Console.ReadKey();
        }
    }
}
