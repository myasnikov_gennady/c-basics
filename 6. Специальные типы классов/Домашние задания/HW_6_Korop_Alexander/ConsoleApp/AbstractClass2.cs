﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp
{
    public abstract partial class AbstractClass
    {
        public class SecondInbuiltClass
        {
            private string secondField;

            public string SecondField
            {
                get
                {
                    return secondField;
                }
                set
                {
                    secondField = value;
                }


            }
        }
    }
}
