﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ_6.STEPANOV.A.S
{
    public abstract partial class Abstract1
    {
        public class C2
        {
            public string _work;
            public int _age;
            protected C2(int age, string work)
            {
                _work = work;
                _age = age;
            }
        }
    }
}
