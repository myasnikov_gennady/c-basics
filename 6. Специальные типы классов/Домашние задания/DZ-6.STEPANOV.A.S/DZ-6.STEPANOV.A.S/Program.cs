﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ_6.STEPANOV.A.S
{
    class Program   // полностью скопирована работа у Скрипкина. 0 - не засчитана
    {
        static void Main(string[] args)
        {
            var notstaticMet = new NotStaticClass(7, 9);
            var staticMet = NotStaticClass.Sum();
            Console.WriteLine($"Значание  не статического поля = {notstaticMet.digit02}, значание статического поля = {staticMet}");
            Console.WriteLine();
            notstaticMet.ChangeTwo();
            var al = new B1("Stefan", "Alex");
            Console.WriteLine($"Фамилия:{al.Name}, Имя {al.Lastname}");
            var ale = new B2(49, "Officer");
            Console.WriteLine($"Возраст:{ale._age}, Статус {ale._work}");
        }
        public class B1 : Abstract1.C1
        {
            public B1(string name, string lastname) : base(name, lastname)
            {
                Console.WriteLine();
                Console.WriteLine("Вызван метод наследования из первого файла класса");
            }
        }
        public class B2 : Abstract1.C2
        {
            public B2(int age, string work) : base(age, work)
            {
                Console.WriteLine();
                Console.WriteLine("Вызван метод наследования из второго файла  класса");
                Console.ReadKey();
            }
            
        }
       
    }
}
