﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ_6.STEPANOV.A.S
{
    class NotStaticClass
    {
        private static int _digit01;
        public int digit02;
        public NotStaticClass(int number1, int number2)
        {
            Digit01 = number1;
            digit02 = number2;
            Console.WriteLine($"Значание    статического поля:{Digit01}");
            Console.WriteLine($"Значание не статического поля:{digit02}");
        }
        public static int Digit01 { get => _digit01; set => _digit01 = value; }
        public static int Sum()
        {
            Console.WriteLine($"Вызван статический метод: статическое поле +20.");
            return _digit01 += 20;
        }
        public void ChangeTwo()
        {
            Digit01++;
            digit02--;
            Console.WriteLine($"Не статический метод вызван: увеличение статического поля +1; уменьщение не статического поля на -1.");
            Console.WriteLine($"Значание    статического поля:{Digit01}");
            Console.WriteLine($"Значание не статического поля:{digit02}");
        }
    }
}
