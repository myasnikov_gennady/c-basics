﻿using System;
namespace HomeWork6
{
    public class NonStaticClass
    {
        public int Number1;
        private static int _number2;

        public NonStaticClass(int number1, int number2)
        {
            Number1 = number1;
            _number2 = number2;
            Console.WriteLine($"non-static number1 = {Number1}");
            Console.WriteLine($"static number2 = {_number2}");
        }

        public static int Multiply()
        {
            return _number2 *=2;
        }

        public void TwoNumbers()
        {
            Number1++;
            _number2 *= 2;
            Console.WriteLine($"number1 after increasing - {Number1}, number2 multiplied by 2 - {_number2}");
            
        }
    }
}
