﻿namespace HomeWork6
{
    public abstract partial class OuterClass
    {
        public class InnerClass
        {
            private string _name;
            private string _lastName;
            private int _age;

            protected InnerClass(string name, string lastName, int age)
            {
                _name = name;
                _lastName = lastName;
                _age = age;
            }
        }
    }
}
