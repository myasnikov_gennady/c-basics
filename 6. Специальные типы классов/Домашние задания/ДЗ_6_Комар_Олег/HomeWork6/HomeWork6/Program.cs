﻿using System;
namespace HomeWork6
{
    public class Program
    {
        private static void Main()
        {
            var objNonStaticClass = new NonStaticClass(5, 7);
            var mult = NonStaticClass.Multiply();

            Console.WriteLine($"number1 is {objNonStaticClass.Number1}, number2 multiplied by 2 is - {mult}");

            objNonStaticClass.TwoNumbers();

            Console.WriteLine("\n");
            Console.WriteLine("Задание ч.2");
            // Работа с абстрактным разделяемым классом со встроенными подклассами
            var student = new Man("Олег", "Комар", 36);
            var work = new Work("ОАО \"Гродно Азот\"","ИТ", 12345);

            work.ToConsole();
            var stud = new Outer();
            stud.ToConsole();
        }
    }

    public class Man : OuterClass.InnerClass
    {
        public Man(string name, string lastName, int age) : base(name, lastName, age)
        {
            Console.WriteLine(name + " " + lastName + ", " + age + " лет ");
        }
    }

    public class Work : OuterClass.InnerClass2
    {
        private readonly string _companyName;
        public Work(string companyName, string department, int numTabel) : base(department, numTabel)
        {
            _companyName = companyName;
        }
        public void ToConsole()
        {
            Console.WriteLine("Работает в "+ _companyName +", в отделе " + Department + ", табельный номер: " + NumTabel);
        }
    }

    public class Outer : OuterClass
    {
        private string _text;
        public override void ToConsole()
        {
            _text ="Переопределение абстрактного метода из абстрактного класса";
                        Console.WriteLine(_text);
        }
    }

}

