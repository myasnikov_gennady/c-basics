﻿namespace HomeWork6
{
    public abstract partial class OuterClass
    {
        public class InnerClass2
        {
            protected readonly string Department;
            protected readonly int NumTabel;

            protected InnerClass2(string department, int numTabel)
            {
                Department = department;
                NumTabel = numTabel;
            }
        }
        public abstract void ToConsole();
    }
}
