﻿using System;

namespace ДЗ_6_Филатов_ВС
{
    // partial классы не относятся друг к другу
    class Program
    {
        public class Class1
        {
            public static void m1()
            {
                Console.WriteLine("Статический метод вызван");
                y = "Знач1";
            }
            public void m2()
            {
                Console.WriteLine("Нестатический метод вызван");
                x = "Знач2";
                y = "Знач2";
            }
            public string x;
            public static string y;
            public Class1(string X, string Y)
            {
                X = x;
                Y = y;
            }
            public Class1()
            {

            }
        }
        class Class2 : Example
        {
            public override void Summ()
            {
                int summ = x + y;
                Console.WriteLine(summ);
            }
        }
        static void Main(string[] args)
        {
            var a = new Class1();
            Console.WriteLine("Значение переменной x = " + a.x);
            Console.WriteLine("Значение переменной y = " + Class1.y);
            Class1.m1();
            Console.WriteLine("Значение переменной x = " + a.x);
            Console.WriteLine("Значение переменной y = " + Class1.y);
            a.m2();
            Console.WriteLine("Значение переменной x = " + a.x);
            Console.WriteLine("Значение переменной y = " + Class1.y);
            var b = new Class2();
            b.Summ();
            Console.ReadKey();
        }
    }
}
