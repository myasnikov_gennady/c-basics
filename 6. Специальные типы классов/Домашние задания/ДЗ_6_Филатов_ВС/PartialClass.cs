﻿using System;

namespace ДЗ_6_Филатов_ВС
{
    public abstract partial class Example
    {
        public int x { get; set; } = 4;
        public class NestedClass
        {

        }
        public int summ { get; set; }
        public abstract void Summ();
    }
}
